<%--
  Created by IntelliJ IDEA.
  User: CY
  Date: 2020-03-23
  Time: 16:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--<base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}"/>-->
</head>
<body>
    <script>
        function  checkUsername(username) {
            //发送ajax请求的代码包括四步，这四步是固定的。  比传统请求要麻烦一些。
            //Ajax发送请求全靠浏览器内置的对象：  XMLHttpRequest   使用该对象可以在浏览器当中单独启动一个新的线程，通过新线程发送请求，达到异步效果
            //1.创建Ajax核心对象 XMLHttpRequest（浏览器内置的，可以直接使用）
            var xhr;
            if(window.XMLHttpRequest){
                xhr=new XMLHttpRequest();
            }else{
                //IE5和IE6是不支持 XMLHttpRequest对象的，它只支持ActiveXObject对象
                xhr=new ActiveXObject("Microsoft.XMLHTTP");
            }
            //alert(xhr);//部分浏览器（或部分版本）会连续触发onblur事件，因此在blur中尽量不要写alert  或者使用jQuery的bind方法，应该可以防止起泡

            //2. 注册回调函数
            //程序执行到这里的时候，后面的回调函数还没有执行，只是将回调函数注册给xhr对象
            //等XHR对象的 readyState发生改变的时候，执行回调函数
            /*
            * 存有 XMLHttpRequest 的状态。从 0 到 4 发生变化。
                •0: 请求未初始化
                •1: 服务器连接已建立
                •2: 请求已接收
                •3: 请求处理中
                •4: 请求已完成，且响应已就绪
            * */
            xhr.onreadystatechange = function () {
                if(xhr.readyState==4){//只有响应结束才处理
                    if(xhr.status==200){//200表示服务器正常响应结束，其他不行
                        //在浏览器使用xhr对象接收服务器端传过来的代码
                        document.getElementById("nameTipMsg").innerHTML = xhr.responseText;
                    }else{
                        alert(xhr.status);//xhr.status可以获取到HTTP的错误响应代码
                    }

                }
            }
            //3.开启浏览器和服务器之间的通道
            /*
            * method: 制定请求方式为get或者post
            * url: 请求路径
            * async: true表示支持异步，false表示支持同步
            * */
            xhr.open("GET","${pageContext.request.contextPath}/checkusername.do?username="+username,true);
            //4.发送请求
            xhr.send();
        }
    </script>
    用户名：<input type="text" name="username" onblur="checkUsername(this.value)"/><span id="nameTipMsg"></span><br>
    密  码： <input type="password" name="password"/><br>
    "${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}"
</body>
</html>
