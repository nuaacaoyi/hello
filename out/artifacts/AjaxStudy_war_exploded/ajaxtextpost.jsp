<%--
  Created by IntelliJ IDEA.
  User: chy
  Date: 2020/3/23
  Time: 21:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
</head>
<body>
    <script>
        function  login() {
            //获取用户提交的用户名和密码
            var uname=document.getElementById("num").value;
            var pwd=document.getElementById("password").value;
            //发送ajax post请求完成登录
            //1创建ajax核心对象
            var xhr=null;
            if(window.XMLHttpRequest){
                xhr=new XMLHttpRequest();
            }else{
                xhr=new ActiveXObject("Microsoft.XMLHTTP");
            }
            //2注册回调函数
            xhr.onreadystatechange = function () {
                if(xhr.readyState==4){//只有响应结束才处理
                    if(xhr.status==200){//200表示服务器正常响应结束，其他不行
                        //接收JSON
                        var jsonString=xhr.responseText;
                        alert(jsonString);
                        eval("var jsonObj = "+jsonString);
                        document.getElementById("tipMsg").innerText=jsonObj.success?"登陆成功":"登录失败";
                    }else{
                        alert(xhr.status);//xhr.status可以获取到HTTP的错误响应代码
                    }

                }
            }
            //3开启通道
            xhr.open("POST","${pageContext.request.contextPath}/login.do",true);
            //POST请求必须 使用 表单 form 进行提交，只有form才能提交post请求   ， 且ajax 进行post请求的乱码需要在这里处理，字符集需要和浏览器和服务器的字符集相同
            xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");//模拟表单提交数据  字符串来自于 form 的 enctype属性提示
            //4发送请求
            xhr.send("username="+uname+"&password="+pwd);//POST请求提交数据在send方法中提交
        }
    </script>

    学号： <input type="text" id="num" name="num" /><br>
    密码： <input type="password" id="password" name="password" /><br>
    <input type="button" value="登录" onclick="login()"><br>
    <span id="tipMsg" style="font-size: 12px;color: red"></span>
<a href="#">注册</a>
</body>
</html>
