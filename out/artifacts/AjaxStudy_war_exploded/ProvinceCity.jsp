<%--
  Created by IntelliJ IDEA.
  User: CY
  Date: 2020-03-24
  Time: 8:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ProvinceCity</title>
</head>
<body>
    <script>
        window.onload= function () {
            var xhr;
            if(window.XMLHttpRequest){
                xhr=new XMLHttpRequest();
            }else{
                //IE5和IE6是不支持 XMLHttpRequest对象的，它只支持ActiveXObject对象
                xhr=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhr.onreadystatechange = function () {
                if(xhr.readyState==4){//只有响应结束才处理
                    if(xhr.status==200){//200表示服务器正常响应结束，其他不行
                        //在浏览器使用xhr对象接收服务器端传过来的代码
                        var jsonString=xhr.responseText;
                        eval("var jsonObjs = "+jsonString);
                        var html="";
                        //alert(jsonString);
                        for(var i=0;i<jsonObjs.length;i++){
                            html +="<option value = "+jsonObjs[i].provincecode+">";
                            html +=jsonObjs[i].provincename;
                            html += "</option>";
                        }
                        document.getElementById("provinceSelect").innerHTML=html;
                    }else{
                        alert(xhr.status);//xhr.status可以获取到HTTP的错误响应代码
                    }

                }
            }
            xhr.open("GET","${pageContext.request.contextPath}/province.do",true);
            //4.发送请求
            xhr.send();

            document.getElementById("provinceSelect").onchange=function () {
                var xhrCity;
                if(window.XMLHttpRequest){
                    xhrCity=new XMLHttpRequest();
                }else{
                    //IE5和IE6是不支持 XMLHttpRequest对象的，它只支持ActiveXObject对象
                    xhrCity=new ActiveXObject("Microsoft.XMLHTTP");
                }
                //2注册回调函数
                xhrCity.onreadystatechange = function () {
                    if(xhrCity.readyState==4){//只有响应结束才处理
                        if(xhrCity.status==200){//200表示服务器正常响应结束，其他不行
                            //接收JSON
                            var jsonString=xhrCity.responseText;
                            eval("var jsonObjs = "+jsonString);
                            var cityhtml="";
                            //alert(jsonString);
                            for(var i=0;i<jsonObjs.length;i++){
                                cityhtml +="<option value = "+jsonObjs[i].citycode+">";
                                cityhtml +=jsonObjs[i].cityname;
                                cityhtml += "</option>";
                            }
                            document.getElementById("citySelect").innerHTML=cityhtml;

                        }else{
                            alert(xhrCity.status);//xhr.status可以获取到HTTP的错误响应代码
                        }

                    }
                }
                //3开启通道
                xhrCity.open("POST","${pageContext.request.contextPath}/city.do",true);
                //POST请求必须 使用 表单 form 进行提交，只有form才能提交post请求   ， 且ajax 进行post请求的乱码需要在这里处理，字符集需要和浏览器和服务器的字符集相同
                xhrCity.setRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");//模拟表单提交数据  字符串来自于 form 的 enctype属性提示
                //4发送请求
                xhrCity.send("pcode="+document.getElementById("provinceSelect").value);//POST请求提交数据在send方法中提交
            }
        }
    </script>
    <select id="provinceSelect">

    </select>
    <select id="citySelect">

    </select>
</body>
</html>
