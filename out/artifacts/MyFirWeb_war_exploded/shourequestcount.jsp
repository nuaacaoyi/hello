<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: CY
  Date: 2020-03-27
  Time: 17:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    本页面已经被浏览过 ${count} 次， 请求一次就加一次；<br>
    如果只是将计数放在 request监听器，那么一个客户请求一次，次数都会+1；<br>
    如果只是将计数放在 session创建监听器里，那么一个客户用两个浏览器访问服务器，次数都会+1；<br>
    统计客户端数量，应该是 记录 请求的IP<br>

    您是第 ${map.size()} 位访客<br>
    <c:forEach items="${map}" var="entry">
        ${entry.key}=${entry.value.size()}
    </c:forEach>


</body>
</html>
