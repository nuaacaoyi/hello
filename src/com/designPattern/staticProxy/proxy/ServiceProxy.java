package com.designPattern.staticProxy.proxy;

import com.designPattern.staticProxy.service.ISomeService;

/**
 * @author caoyi
 * @create 2020-04-30 17:54
 * 静态代理类  必须 与被代理类实现同一个接口      （就像代理律师和当事人必须为同一件事，打官司）努力
 * 当目标类有多个接口时，代理类可以选择不实现，或者实现这些接口但不增强
 */
public class ServiceProxy implements ISomeService {
  private ISomeService service;

  //采用有参构造器时，可以进行有选择性的增强 目标对象
  public ServiceProxy(ISomeService service) {
    this.service = service;
  }

  @Override
  public String doFirst() {
    //调用目标对象的目标方法，该方法返回全小写字母
    String str = service.doFirst();

    //增强：将目标方法返回的全小写字母变为大写
    str = str.toUpperCase();

    return  str;
  }

  @Override
  public void doSecond() {
    service.doSecond();
  }
}
