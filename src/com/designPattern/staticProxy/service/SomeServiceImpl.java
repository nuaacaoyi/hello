package com.designPattern.staticProxy.service;

/**
 * @author caoyi
 * @create 2020-04-30 17:31
 * 目标类：或被代理类，需要被代理增强的类
 */
public class SomeServiceImpl implements ISomeService {
  @Override
  public String doFirst() {
    System.out.println("执行doFirst()方法");
    return "abcde";
  }

  @Override
  public void doSecond() {
    System.out.println("执行doSecond()方法");
  }
}
