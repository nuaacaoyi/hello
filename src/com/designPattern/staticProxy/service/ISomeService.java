package com.designPattern.staticProxy.service;

/**
 * @author caoyi
 * @create 2020-04-30 17:30
 * 业务接口，本接口中的方法将要被代理增强  代理类和被代理类都要实现该接口
 *
 * 有点类似于装饰者模式，区别在于装饰者模式偏向于增强    静态代理偏向于功能控制
 */
public interface ISomeService {
  String doFirst();
  void doSecond();
}
