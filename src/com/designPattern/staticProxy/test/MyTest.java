package com.designPattern.staticProxy.test;

import com.designPattern.staticProxy.proxy.ServiceProxy;
import com.designPattern.staticProxy.service.ISomeService;
import com.designPattern.staticProxy.service.SomeServiceImpl;

/**
 * @author caoyi
 * @create 2020-04-30 17:32
 */
public class MyTest {
  public static void main(String[] args) {
    ISomeService service1 = new SomeServiceImpl();
    String str1 = service1.doFirst();
    System.out.println(str1);
    service1.doSecond();

    System.out.println("-------");

    ISomeService service2 = new ServiceProxy(service1);
    String str2 = service2.doFirst();
    System.out.println(str2);
    service2.doSecond();
  }
}
