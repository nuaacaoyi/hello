package com.designPattern.listenerMode.test;

import com.designPattern.listenerMode.listener.IListener;
import com.designPattern.listenerMode.listener.ListenerImpl;
import com.designPattern.listenerMode.listenerable.ListenerableImpl;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:33
 */
public class Test {
    public static void main(String[] args) {

        //定义监听器
        IListener listener=new ListenerImpl();

        //定义事件源
        ListenerableImpl eventSource=new ListenerableImpl();


        //事件源注册监听器
        eventSource.setListener(listener);

        //事件源执行自己的业务方法，业务方法触发监听
        eventSource.saveStudent();
        eventSource.modifyStudent();
        eventSource.findStudent();
        eventSource.removeStudent();
    }
}
