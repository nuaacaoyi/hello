package com.designPattern.listenerMode.events;

import com.designPattern.listenerMode.listenerable.IListenerable;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:18
 */
public class CurdEventImpl implements ICurdEvent {
    //声明一个事件源
    private IListenerable eventSource;
    //声明一个 事件源所执行的方法名称
    private String methodName;

    public CurdEventImpl(IListenerable eventSource, String methodName) {
        this.eventSource = eventSource;
        this.methodName = methodName;
    }

    @Override
    public IListenerable getListenerable() {
        return eventSource;
    }

    //根据事件源所执行的不同的方法返回不同的事件类型
    @Override
    public String getEventType() {
        String eventType=null;
        if(methodName.startsWith("save")){
            eventType=CRE_EVENT;
        }else if(methodName.startsWith("remove")){
            eventType=DEL_EVENT;
        }else if(methodName.startsWith("modify")){
            eventType=UPD_EVENT;
        }else if(methodName.startsWith("find")){
            eventType=RET_EVENT;
        }else{
            eventType="have not this event type";
        }
        return  eventType;
    }
}
