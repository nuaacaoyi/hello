package com.designPattern.listenerMode.events;

import com.designPattern.listenerMode.listenerable.IListenerable;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:04
 * 定义增删改查事件
 * Create Update  Retrieve（检索）  Delete
 *
 * 通常，对于事件对象，我们一般是需要从事件对象中获取到事件源对象的
 */
public interface ICurdEvent {
    //声明事件类型  增删改查四种类型
    //接口类的成员  都是 公共静态的  默认带有 public static
    String CRE_EVENT="create event";
    String UPD_EVENT="update event";
    String RET_EVENT="retrieve event";
    String DEL_EVENT="delete event";

    //接口里的方法  都是  公共抽象的  默认带有 public abstract
    //获得事件源
    IListenerable getListenerable();
    //获取事件类型
    String getEventType();
}
