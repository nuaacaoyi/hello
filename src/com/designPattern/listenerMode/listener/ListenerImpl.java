package com.designPattern.listenerMode.listener;

import com.designPattern.listenerMode.events.ICurdEvent;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:26
 * 定义监听器类
 */
public class ListenerImpl implements IListener {
    @Override
    public void handle(ICurdEvent event) {
        //获取事件的事件类型
        String eventType=event.getEventType();
        //若事件类型为添加
        if(ICurdEvent.CRE_EVENT.equals(eventType)){
            System.out.println("事件源执行了 添加 操作");
        }else if(ICurdEvent.DEL_EVENT.equals(eventType)){
            System.out.println("事件源执行了 删除 操作");
        }else if(ICurdEvent.UPD_EVENT.equals(eventType)){
            System.out.println("事件源执行了 修改 操作");
        }else if(ICurdEvent.RET_EVENT.equals(eventType)){
            System.out.println("事件源执行了 查询 操作");
        }

    }
}
