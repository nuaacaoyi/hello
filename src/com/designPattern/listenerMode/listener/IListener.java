package com.designPattern.listenerMode.listener;

import com.designPattern.listenerMode.events.ICurdEvent;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:15
 */
public interface IListener {
    //定义处理事件的逻辑
    void handle(ICurdEvent event);
}
