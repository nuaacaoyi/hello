package com.designPattern.listenerMode.listenerable;

import com.designPattern.listenerMode.events.ICurdEvent;
import com.designPattern.listenerMode.listener.IListener;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:08
 * 事件源接口（被监听者）
 */
public interface IListenerable {
    //为事件源注册监听器（监听者模式，是一个被监听者对应一个监听者)
    void setListener(IListener listener);
    //触发监听器
    void triggerListener(ICurdEvent event);

}
