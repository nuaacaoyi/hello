package com.designPattern.listenerMode.listenerable;

import com.designPattern.listenerMode.events.CurdEventImpl;
import com.designPattern.listenerMode.events.ICurdEvent;
import com.designPattern.listenerMode.listener.IListener;

/**
 * @author CY_JFXX
 * @create 2020-03-26 21:30
 * 定义事件源类
 */
public class ListenerableImpl implements  IListenerable{
    private  IListener listener;

    //注册监听器
    @Override
    public void setListener(IListener listener) {
        this.listener=listener;
    }

    //触发监听器
    @Override
    public void triggerListener(ICurdEvent event) {
        listener.handle(event);
    }

    //下面的方法是 事件源类真正的业务逻辑，而监听器监听的就是这些业务方法的执行
    public  void saveStudent(){
        System.out.println("在数据库插入一条数据");
        ICurdEvent event=new CurdEventImpl(this,"saveStudent");
        this.triggerListener(event);
    }

    public  void removeStudent(){
        System.out.println("从数据库删除一条数据");
        ICurdEvent event=new CurdEventImpl(this,"removeStudent");
        this.triggerListener(event);
    }

    public  void modifyStudent(){
        System.out.println("在数据库修改了一条数据");
        ICurdEvent event=new CurdEventImpl(this,"modifyStudent");
        this.triggerListener(event);
    }

    public  void findStudent(){
        System.out.println("从DB中查出数据");
        ICurdEvent event=new CurdEventImpl(this,"findStudent");
        this.triggerListener(event);
    }
}
