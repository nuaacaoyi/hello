package com.designPattern.adapter.test;

import com.designPattern.adapter.adapters.IWorker;
import com.designPattern.adapter.adapters.WorkerImpl;
import com.designPattern.adapter.worker.ICooker;
import com.designPattern.adapter.worker.IProgrammer;
import com.designPattern.adapter.worker.impl.CookerImpl;
import com.designPattern.adapter.worker.impl.ProgrammerImpl;

/**
 * @author caoyi
 * @create 2020-05-01 15:16
 * 适配器模式的测试，  情景说明： 有两个接口ICooker,IProgrammer，同时他们又各有一个实现类
 * 需求：  各个工种打印一下自己的工作工作内容
 */
public class MyTest {
  public static void main(String[] args) {
    //常规的实现方法，将所有的工种都实例化，然后每个工种都打印一下自己的工作内容
    ICooker qjdCooker = new CookerImpl();
    IProgrammer jdProgrammer = new ProgrammerImpl();
    System.out.println(qjdCooker.cook());
    System.out.println(jdProgrammer.program());

    System.out.println("-------------------");

    //适配器模式  如果业务类的方法只调用一次，不需要适配器，因为适配器中也需要针对每个业务类写判断逻辑，但是对业务方法有很多调用时，适配器更加简单高效
    IWorker adapter = new WorkerImpl();
    Object[] workers = {qjdCooker,jdProgrammer};
    for (Object worker:workers
         ) {
      String workContent = adapter.work(worker);
      System.out.println(workContent);
    }
  }
}
