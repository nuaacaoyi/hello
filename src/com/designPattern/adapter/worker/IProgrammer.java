package com.designPattern.adapter.worker;

/**
 * @author caoyi
 * @create 2020-05-01 15:13
 */
public interface IProgrammer {
  String program();
}
