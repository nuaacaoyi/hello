package com.designPattern.adapter.worker;

/**
 * @author caoyi
 * @create 2020-05-01 15:11
 */
public interface ICooker {
  public String  cook();
}
