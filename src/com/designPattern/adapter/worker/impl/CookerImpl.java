package com.designPattern.adapter.worker.impl;

import com.designPattern.adapter.worker.ICooker;

/**
 * @author caoyi
 * @create 2020-05-01 15:14
 */
public class CookerImpl implements ICooker {
  @Override
  public String cook() {
    return "烧烤美味烤鸭";
  }
}
