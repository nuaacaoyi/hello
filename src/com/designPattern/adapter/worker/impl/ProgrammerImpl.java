package com.designPattern.adapter.worker.impl;

import com.designPattern.adapter.worker.IProgrammer;

/**
 * @author caoyi
 * @create 2020-05-01 15:14
 */
public class ProgrammerImpl implements IProgrammer {
  @Override
  public String program() {
    return "编写高效程序";
  }
}
