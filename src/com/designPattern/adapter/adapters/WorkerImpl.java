package com.designPattern.adapter.adapters;

import com.designPattern.adapter.worker.ICooker;
import com.designPattern.adapter.worker.IProgrammer;

/**
 * @author caoyi
 * @create 2020-05-01 15:26
 * 适配器类
 */
public class WorkerImpl implements IWorker {
  @Override
  public String work(Object worker) {
    String result = null;

    if(worker instanceof ICooker){
      result = ((ICooker)worker).cook();
    }

    else if(worker instanceof IProgrammer){
      result = ((IProgrammer)worker).program();
    }

    return  result;
  }
}
