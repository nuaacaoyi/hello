package com.designPattern.adapter.adapters;

/**
 * @author caoyi
 * @create 2020-05-01 15:22
 * 定义适配器接口
 */
public interface IWorker {
  String work(Object worker);//接口 接入 多种引用类型
}
