package com.designPattern.dynamicProxy_jdk.test;


import com.designPattern.dynamicProxy_jdk.service.ISomeService;
import com.designPattern.dynamicProxy_jdk.service.SomeServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author caoyi
 * @create 2020-04-30 17:32
 */
public class MyTest {
  public static void main(String[] args) {
    final ISomeService service1 = new SomeServiceImpl();
    String str1 = service1.doFirst();
    System.out.println(str1);
    service1.doSecond();

    System.out.println("-------");

    ISomeService service2 = (ISomeService) Proxy.newProxyInstance(service1.getClass().getClassLoader(), //第一个入参：目标类的类加载器
        service1.getClass().getInterfaces(),  //第二个入参：目标类实现的所有接口
        new InvocationHandler() { //第三个入参： 匿名内部类，主要用于填写增强的功能
          @Override
          public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            //匿名内部类中 invoke方法的三个参数：  proxy 代理对象；  method： 目标方法；   args：目标方法的参数列表
            Object result = method.invoke(service1,args);  //执行 service1 对象的method方法，传入args参数
            if(result != null){
              result = ((String)result).toUpperCase();
            }
            return (Object) result;
          }
        });//代理的意义在于service2的所有方法的执行，都会增加 toUpper功能，因此下面只调用service2.doFirst()不会报错，而执行doSecond会报错，因为doSecond没有返回值，所以需要在代理方法中增加非空判断
    String str2 = service2.doFirst();//
    System.out.println(str2);
    service2.doSecond();//若invoke方法中 没有If判断，此句话会抛出空指针异常
  }
}
