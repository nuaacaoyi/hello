package com.designPattern.multiAdapter.test;

import com.designPattern.multiAdapter.adapters.IWorkerAdapter;
import com.designPattern.multiAdapter.adapters.impl.CookerAdapter;
import com.designPattern.multiAdapter.adapters.impl.ProgrammerAdapter;
import com.designPattern.multiAdapter.worker.ICooker;
import com.designPattern.multiAdapter.worker.IProgrammer;
import com.designPattern.multiAdapter.worker.impl.CookerImpl;
import com.designPattern.multiAdapter.worker.impl.ProgrammerImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author caoyi
 * @create 2020-05-01 15:16
 * 适配器模式的测试，  情景说明： 有两个接口ICooker,IProgrammer，同时他们又各有一个实现类
 * 需求：  各个工种打印一下自己的工作工作内容
 * 多适配器模式，每个业务接口都有一个 adapter
 */
public class MyTest {
  public static void main(String[] args) {
    //常规的实现方法，将所有的工种都实例化，然后每个工种都打印一下自己的工作内容
    ICooker qjdCooker = new CookerImpl();
    IProgrammer jdProgrammer = new ProgrammerImpl();

    System.out.println(qjdCooker.cook());
    System.out.println(jdProgrammer.program());
    System.out.println("-------------------");

    //适配器模式
    Object[] workers = {qjdCooker,jdProgrammer};
    for (Object worker:workers
         ) {
      IWorkerAdapter adapter = getAdapter(worker);
      System.out.println(adapter.work(worker));
    }
  }

  //根据worker获取相应的适配器对象。      遍历所有的 adapters，找到worker对应的适配器，并返回该适配器
  private static IWorkerAdapter getAdapter(Object worker) {
    List<IWorkerAdapter> adapters = getAllAdapters();
    for (IWorkerAdapter adapter: adapters
         ) {
      if(adapter.supports(worker)) return adapter;
    }
    return  null;
  }

  //获取所有的适配器对象
  private static List<IWorkerAdapter> getAllAdapters() {
    List<IWorkerAdapter> adapters = new ArrayList<>();
    adapters.add(new CookerAdapter());
    adapters.add(new ProgrammerAdapter());
    return adapters;
  }
}
