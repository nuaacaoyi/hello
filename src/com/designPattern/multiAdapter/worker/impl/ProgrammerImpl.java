package com.designPattern.multiAdapter.worker.impl;

import com.designPattern.multiAdapter.worker.IProgrammer;

/**
 * @author caoyi
 * @create 2020-05-01 15:14
 */
public class ProgrammerImpl implements IProgrammer {
  @Override
  public String program() {
    return "编写高效程序";
  }
}
