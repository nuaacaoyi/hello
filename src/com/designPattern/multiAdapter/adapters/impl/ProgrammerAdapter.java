package com.designPattern.multiAdapter.adapters.impl;

import com.designPattern.multiAdapter.adapters.IWorkerAdapter;
import com.designPattern.multiAdapter.worker.IProgrammer;

/**
 * @author caoyi
 * @create 2020-05-01 15:44
 */
public class ProgrammerAdapter implements IWorkerAdapter {


  @Override
  public String work(Object worker) {
    return ((IProgrammer)worker).program();
  }

  @Override
  public boolean supports(Object worker) {
    return (worker instanceof  IProgrammer);
  }
}
