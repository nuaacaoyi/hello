package com.designPattern.multiAdapter.adapters.impl;

import com.designPattern.multiAdapter.worker.ICooker;
import com.designPattern.multiAdapter.adapters.IWorkerAdapter;

/**
 * @author caoyi
 * @create 2020-05-01 15:43
 */
public class CookerAdapter implements IWorkerAdapter {

  @Override
  public String work(Object worker) {
    return ((ICooker)worker).cook();
  }

  @Override
  public boolean supports(Object worker) {
    return (worker instanceof  ICooker);
  }
}
