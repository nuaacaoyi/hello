package com.designPattern.multiAdapter.adapters;

/**
 * @author caoyi
 * @create 2020-05-01 15:22
 * 定义适配器接口
 */
public interface IWorkerAdapter {
  String work(Object worker);//接口 接入 多种引用类型
  boolean supports(Object worker);//判断 woker对象对应哪种业务类
}
