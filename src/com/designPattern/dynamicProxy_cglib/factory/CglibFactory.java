package com.designPattern.dynamicProxy_cglib.factory;

import com.designPattern.dynamicProxy_cglib.service.SomeService;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author caoyi
 * @create 2020-05-01 8:53
 * MethodInterceptor直译叫做 方法拦截器
 * CGLIB可以对没有实现任何接口的 目标类 实现代理， 实现原理就是子类继承父类
 * CGLIB也可以实现 有接口的类的代理，只需要把 SomeService替换为 ISomeService
 */
public class CglibFactory implements MethodInterceptor {
  private SomeService target;

  public CglibFactory(SomeService target) {
    this.target = target;
  }

  public CglibFactory() {
  }

  //需要创建一个方法，方法返回 目标类
  public SomeService myCglibCreator(){
    //proxy下有个Enhancer类，增强...
    Enhancer enhancer = new Enhancer();

    //反射机制 SomeService.class是一个对象，该对象代表SomeService类
    //CGLIB的原理就是 使用继承 增强目标类 ， 即子类继承父类，在子类增加功能。因此使用CGLIB要求目标类不能是final类，因为final类不能有子类
    enhancer.setSuperclass(SomeService.class);//指定Enhancer的父类为 目标类
    //IDEA中class编译结果不在当前文件夹，所以无法运行

    //设置回调接口对象
    enhancer.setCallback(this);


    //enhancer对象的 create方法用于创建 cglib对象
    return (SomeService)enhancer.create();
  }

  //回调接口的方法
  @Override
  public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
    Object invoke = method.invoke(target,objects);
    if(invoke != null){
      invoke = ((String)invoke).toUpperCase();
    }

    return invoke;
  }
}
