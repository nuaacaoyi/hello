package com.designPattern.dynamicProxy_cglib.test;


import com.designPattern.dynamicProxy_cglib.factory.CglibFactory;
import com.designPattern.dynamicProxy_cglib.service.SomeService;

/**
 * @author caoyi
 * @create 2020-04-30 17:32
 */
public class MyTest {
  public static void main(String[] args) {
    SomeService target = new SomeService();
    SomeService service = new CglibFactory(target).myCglibCreator();
    String result = service.doFirst();
    System.out.println(result);
    service.doSecond();
  }
}
