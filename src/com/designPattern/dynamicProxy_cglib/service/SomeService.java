package com.designPattern.dynamicProxy_cglib.service;

/**
 * @author caoyi
 * @create 2020-04-30 17:31
 * 目标类：CGLIB可以对没有实现任何接口的 目标类 实现代理
 */
public class SomeService {
  public String doFirst() {
    System.out.println("执行doFirst()方法");
    return "abcde";
  }

  public void doSecond() {
    System.out.println("执行doSecond()方法");
  }
}
