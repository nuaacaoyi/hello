package com.designPattern.observerModel.observers;

/**
 * @author CY_JFXX
 * @create 2020-03-26 20:39
 * 观察者接口
 *  观察者的作用：接收信息  和 作出相应处理动作
 */
public interface IObserverTest {
    //处理被观察者发送来的信息
    void handleNotify(String message);
}
