package com.designPattern.observerModel.observers;

/**
 * @author CY_JFXX
 * @create 2020-03-26 20:45
 * 定义二号观察者
 */
public class SecondIObserverTestImpl implements IObserverTest {
    @Override
    public void handleNotify(String message) {
        System.out.println("二号观察者接收到消息："+message);
    }
}
