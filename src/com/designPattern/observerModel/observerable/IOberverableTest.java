package com.designPattern.observerModel.observerable;

import com.designPattern.observerModel.observers.IObserverTest;

/**
 * @author CY_JFXX
 * @create 2020-03-26 20:42
 * 被观察者接口
 */
public interface IOberverableTest {
    //添加观察者
    void addObserver(IObserverTest oberverableTest);
    //删除观察者
    void removeObserver(IObserverTest oberverableTest);
    //向观察者发送信息
    void notifyObserver(String message);
}
