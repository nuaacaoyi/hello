package com.designPattern.observerModel.observerable;

import com.designPattern.observerModel.observers.IObserverTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CY_JFXX
 * @create 2020-03-26 20:47
 * 定义被观察者
 */
public class TheObservableTestImpl implements IOberverableTest {
    //声明观察者集合
    private List<IObserverTest> observers;

    public TheObservableTestImpl(){
        //在被观察者对象创建的同时，创建观察者集合
        observers=new ArrayList<>();
    }

    @Override
    public void addObserver(IObserverTest oberverableTest) {
        observers.add(oberverableTest);
    }

    @Override
    public void removeObserver(IObserverTest oberverableTest) {
        observers.remove(oberverableTest);
    }

    @Override
    public void notifyObserver(String message) {
        for (IObserverTest observer:observers
             ) {
            observer.handleNotify(message);
        }
    }
}
