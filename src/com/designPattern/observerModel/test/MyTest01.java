package com.designPattern.observerModel.test;

import com.designPattern.observerModel.observerable.TheObservableTestImpl;
import com.designPattern.observerModel.observers.SecondIObserverTestImpl;
import com.designPattern.observerModel.observerable.IOberverableTest;
import com.designPattern.observerModel.observers.FirstIObserverTestImpl;
import com.designPattern.observerModel.observers.IObserverTest;

/**
 * @author CY_JFXX
 * @create 2020-03-26 20:53
 */
public class MyTest01 {
    public static void main(String[] args) {
        //创建多个观察者
        IObserverTest firstObserver=new FirstIObserverTestImpl();
        IObserverTest secondObserver=new SecondIObserverTestImpl();

        //创建被观察者
        IOberverableTest observable=new TheObservableTestImpl();

        //被观察者添加观察者
        observable.addObserver(firstObserver);
        observable.addObserver(secondObserver);
        //被观察者发送消息
        observable.notifyObserver("全体注意，出发!");
    }
}
