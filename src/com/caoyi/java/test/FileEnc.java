package com.caoyi.java.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author caoyi
 * @create 2020-10-28-16:50
 */
public class FileEnc {
  public static void main(String[] args) {
    String path = "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\res";
    getFile(path,0);


  }
  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void getFile(String path,int deep){
    // 获得指定文件对象
    File file = new File(path);
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        //System.out.println(array[i].getPath());
        encDecFile(array[i].getPath(),array[i].getPath() + ".enc");
      }
      else if(array[i].isDirectory())//如果是文件夹
      {
        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");

        System.out.println( array[i].getName());
        //System.out.println(array[i].getPath());
        //文件夹需要调用递归 ，深度+1
        getFile(array[i].getPath(),deep+1);
      }

    }
  }
  //文件加密、解密函数（对称），传入源文件、目标文件进行加密或解密， --任老师
  public static void encDecFile(String srcFile, String destFile) {
    File f = new File(srcFile);
    if (!f.exists()) {
      return;
    }
    File o = new File(destFile);
    byte encCode = -111; //加密解密秘钥
    byte[] data = new byte[1024 * 32]; //每次读取32M
    int ilen;
    try {
      FileInputStream fis = new FileInputStream(f);
      FileOutputStream fos = new FileOutputStream(o);
      while ((ilen = fis.read(data)) != -1) {
        byte[] dest = new byte[ilen];
        for (int i = 0; i < ilen; i++) {
          dest[i] = (byte) (data[i] ^ encCode);
        }
        fos.write(dest);
      }
      fis.close();
      fos.close();
    } catch (Exception e) {

    }
  }
}
