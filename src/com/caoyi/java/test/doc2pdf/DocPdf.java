package com.caoyi.java.test.doc2pdf;

import com.aspose.words.Document;
import com.aspose.words.OutlineOptions;
import com.aspose.words.PdfSaveOptions;

import com.aspose.words.SaveFormat;

import java.io.*;

/**
 * @author caoyi
 * @create 2020-11-16-18:01
 */
public class DocPdf {
  /**
   * word to pdf
   * @para  inPath  word 全路径
   * @para  outPath 生成的pdf 全路径
   * @author an
   * @throws Exception
   */
  public static void main(String[] args) {
    String inPath = "D:\\工作\\过往\\华润\\部门\\管理文档\\信息管理制度\\信息制度\\教材";
    String outPath = "D:\\工作\\过往\\华润\\部门\\管理文档\\信息管理制度\\xxzdpdf";
    getFile(inPath,outPath, 0);
  }

  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void getFile(String path,String outPath, int deep){
    // 获得指定文件对象
    File file = new File(path);
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        String docName = array[i].getName();
        int dot = docName.lastIndexOf('.');
        String dataId =  dot>-1?docName.substring(0, dot):docName;
        String dataType =  dot>-1?docName.substring(dot+1):docName;



        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");
        // 只输出文件名字
        /*if("doc".equals(dataType) ||  "docx".equals(dataType)){
          //System.out.println(readPdfText(array[i].getPath()));
          docPdf(array[i].getPath(),outPath + "\\" + dataId + ".pdf");
        }*/
        if("doc".equals(dataType) || "docx".equals(dataType)){
          //System.out.println(readPdfText(array[i].getPath()));
          docPdf(array[i].getPath(),outPath + "\\" + dataId + ".pdf");
        }
        // 输出当前文件的完整路径
        // System.out.println("#####" + array[i]);
        // 同样输出当前文件的完整路径   大家可以去掉注释 测试一下
        // System.out.println(array[i].getPath());
      }
      else if(array[i].isDirectory())//如果是文件夹
      {
        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");

        System.out.println( array[i].getName());
        //System.out.println(array[i].getPath());
        //文件夹需要调用递归 ，深度+1
        getFile(array[i].getPath(),outPath,deep+1);
      }
    }
  }


  public static String docPdf(String inPath, String outPath)   {

    if (!isWordLicense()) {
      return null;
    }

    try {
      String path = outPath.substring(0, outPath.lastIndexOf(File.separator));
      File file = null;
      file = new File(path);
      if (!file.exists()) {//创建文件夹
        file.mkdirs();
      }
      file = new File(outPath);// 新建一个空白pdf文档
      FileOutputStream os = new FileOutputStream(file);
      Document doc = new Document(inPath); // Address是将要被转化的word文档
      // 处理word的目录，转化时将目录转为pdf书签
      PdfSaveOptions saveOptions = new PdfSaveOptions();

      OutlineOptions outlineOptions = saveOptions.getOutlineOptions();
      outlineOptions.setCreateMissingOutlineLevels(false);
      outlineOptions.setDefaultBookmarksOutlineLevel(1);
      outlineOptions.setExpandedOutlineLevels(1);
      outlineOptions.setHeadingsOutlineLevels(1);

      //doc.save(os, saveOptions);  //自己转换的目录右乱码，而且只到1级
      doc.save(os, SaveFormat.PDF);// 不带目录的转化写法，全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF,
    } catch (Exception e) {
      e.printStackTrace();
    }
    // EPUB, XPS, SWF 相互转换
    return outPath;
  }


  /**
   * @Description: 验证aspose.word组件是否授权：无授权的文件有水印和试用标记
   */
  public static boolean isWordLicense() {
    boolean result = false;
    try {
      // InputStream inputStream = new
      // FileInputStream("D:\\Workspaces\\TestFilters\\lib\\license.xml");
      // 避免文件遗漏
      String licensexml = "<License>\n" + "<Data>\n" + "<Products>\n"
          + "<Product>Aspose.Total for Java</Product>\n" + "<Product>Aspose.Words for Java</Product>\n"
          + "</Products>\n" + "<EditionType>Enterprise</EditionType>\n"
          + "<SubscriptionExpiry>20991231</SubscriptionExpiry>\n"
          + "<LicenseExpiry>20991231</LicenseExpiry>\n"
          + "<SerialNumber>23dcc79f-44ec-4a23-be3a-03c1632404e9</SerialNumber>\n" + "</Data>\n"
          + "<Signature>\n"
          + "sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=\n"
          + "</Signature>\n" + "</License>";
      InputStream inputStream = new ByteArrayInputStream(licensexml.getBytes());
      com.aspose.words.License license = new com.aspose.words.License();
      license.setLicense(inputStream);
      result = true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  // outputStream转inputStream
  public static ByteArrayInputStream parse(OutputStream out) throws Exception {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    baos = (ByteArrayOutputStream) out;
    ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
    return swapStream;
  }
}

