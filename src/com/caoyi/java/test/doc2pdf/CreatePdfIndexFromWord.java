package com.caoyi.java.test.doc2pdf;

import com.aspose.pdf.License;
import com.aspose.words.*;
import com.aspose.pdf.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.caoyi.java.test.doc2pdf.DocPdf.isWordLicense;

/**
 * @author caoyi
 * @create 2020-11-16-18:01
 */
public class CreatePdfIndexFromWord {
  /**
   * word to pdf
   * @para  inPath  word 全路径
   * @para  outPath 生成的pdf 全路径
   * @author an
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    // doc的注册码
    if (!DocPdf.isWordLicense()) {
      return;
    }
    // pdf的注册码
    InputStream is = null;
    try {
      is = new FileInputStream("D:\\tmp\\license.xml");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    // 注意，这个License是pdf下的类
    License license = new License();
    license.setLicense(is);


    String inPath = "D:\\tmp\\word";
    String outPath = "D:\\tmp\\pdf";
    //String pdfPath = "D:\\tmp\\Diagram 1.pdf";
    //getFile(inPath,outPath, 0);
    getFile(inPath, outPath, 0);
  }


  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void getFile(String path,String outPath, int deep){
    // 获得指定文件对象
    File file = new File(path);
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        String docName = array[i].getName();
        int dot = docName.lastIndexOf('.');
        String dataId =  dot>-1?docName.substring(0, dot):docName;
        String dataType =  dot>-1?docName.substring(dot+1):docName;



        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");
        // 只输出文件名字
        /*if("doc".equals(dataType) ||  "docx".equals(dataType)){
          //System.out.println(readPdfText(array[i].getPath()));
          docPdf(array[i].getPath(),outPath + "\\" + dataId + ".pdf");
        }*/
        if("doc".equals(dataType) || "docx".equals(dataType)){
          //System.out.println(readPdfText(array[i].getPath()));
          String outPdfPath = DocPdf.docPdf(array[i].getPath(),outPath + "\\" + dataId + ".pdf");
          try {
            handleIndex(array[i].getPath(), outPdfPath);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
        // 输出当前文件的完整路径
        // System.out.println("#####" + array[i]);
        // 同样输出当前文件的完整路径   大家可以去掉注释 测试一下
        // System.out.println(array[i].getPath());
      }
      else if(array[i].isDirectory())//如果是文件夹
      {
        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");

        System.out.println( array[i].getName());
        //System.out.println(array[i].getPath());
        //文件夹需要调用递归 ，深度+1
        getFile(array[i].getPath(),outPath,deep+1);
      }
    }
  }


  // 工具函数   从指定word中读取大纲  插入指定pdf
  public static void handleIndex(String wordPath,String pdfPath) throws Exception {

    com.aspose.words.Document originDoc =
        new com.aspose.words.Document(wordPath);
    com.aspose.pdf.Document pdfDocument = new com.aspose.pdf.Document(pdfPath);


    // 后面需要用这个对象去获取当前段落所在的页码
    LayoutCollector layoutCollector = new LayoutCollector(originDoc);
    // 需要获取所有的section，要不然部分word提取目录不完整
    Section[] sections = originDoc.getSections().toArray();
    // 获取所有的段落
    List<Paragraph> paragraphs = new ArrayList<>();
    for (Section s:
        sections) {
      //paragraphs.addAll(Arrays.asList(s.getBody().getParagraphs().toArray()));
      ParagraphCollection collection = s.getBody().getParagraphs();
      paragraphs.addAll(Arrays.asList(s.getBody().getParagraphs().toArray()));
    }

    // 游标数组，记录当前的所有祖父-孙子节点
    OutlineItemCollection[] flagOutlines = new OutlineItemCollection[9];
    int flagLevel = 0;

    // 当前pdf的大纲
    OutlineCollection pdfOutline = pdfDocument.getOutlines();


    // 获取标题的关键代码。标题对应的值为0-8，提取自己所需标题即可
    for(Paragraph p:paragraphs){
      // 标题级别
      int outlineLevel = p.getParagraphFormat().getOutlineLevel();
      // 获取页码
      int endPageIndex = layoutCollector.getEndPageIndex(p);
      // 获取标题名称
      String title = p.getListLabel().getLabelString() + " " + p.getText();

      // 根据取到的outlineLevel 判断该title是否为目录
      // 9为正文， 0-8对应九级标题
      if(outlineLevel < 9){
        // 创建一个大纲项，装载本次标题和页码
        OutlineItemCollection pdfOutlineItem = new OutlineItemCollection(pdfDocument.getOutlines());
        pdfOutlineItem.setTitle(title);
        pdfOutlineItem.setAction(new GoToAction(pdfDocument.getPages().get_Item(endPageIndex)));

        // 游标级别为0 ，直接插入大纲，并更新游标
        if(outlineLevel == 0){
          pdfOutline.add(pdfOutlineItem);
          flagLevel = 0;
          flagOutlines[0] = pdfOutlineItem;
        }
        // 标题级别不为0，但与游标相等。添加大纲到父亲，更新游标数组
        else if(outlineLevel <= flagLevel || outlineLevel-1 == flagLevel){
          flagOutlines[outlineLevel-1].add(pdfOutlineItem);
          flagOutlines[outlineLevel] = pdfOutlineItem;
          flagLevel = outlineLevel;
        }
        // 当标题级别小于游标时， 发现处理逻辑等价于 两者相等，合并
        /*else if(outlineLevel < flagLevel){
          flagOutlines[outlineLevel-1].add(pdfOutlineItem);
          flagOutlines[outlineLevel] = pdfOutlineItem;
          flagLevel = outlineLevel;
        }*/
        // 标题级别大于游标级别，且差为1，跨级别的（比如1及标题下直接放3级标题，不做录入）
        // 发现处理逻辑仍然等价于 两者相等，也可以合并
       /* else if(outlineLevel-1 == flagLevel){
          flagOutlines[outlineLevel-1].add(pdfOutlineItem);
          flagOutlines[outlineLevel] = pdfOutlineItem;
          flagLevel = outlineLevel;
        }   */
        // 其他情况，如1级标题下直接上3级标题，不做考虑


        //System.out.println("outlineLevel:"+outlineLevel+"; endPageIndex:"+endPageIndex + "; title:"+title);
      }
    }

    //pdfDocument.save(pdfPath + "AddBookmark_out.pdf");
    pdfDocument.save();

    // 以两个item为游标，指定item当前和其父亲，
    // 遍历所有的 paragraphs，判断 当前标题级别与游标的关系，分别为 < = >，三种情况

  }

  public static void setOutlines(String pdfPath) throws Exception {



    com.aspose.pdf.Document pdfDocument = new com.aspose.pdf.Document(pdfPath);

    // Create a bookmark object
    OutlineItemCollection pdfOutline = new OutlineItemCollection(pdfDocument.getOutlines());
    pdfOutline.setTitle("Test Outline");
    pdfOutline.setItalic(true);
    pdfOutline.setBold(true);

    // Set the destination page number
    pdfOutline.setAction(new GoToAction(pdfDocument.getPages().get_Item(2)));

    // Add a bookmark in the document's outline collection.
    pdfDocument.getOutlines().add(pdfOutline);

    // Save the update document
    pdfDocument.save(pdfPath + "AddBookmark_out.pdf");

  }


}

