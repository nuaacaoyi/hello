package com.caoyi.java.test.doc2pdf;

/**
 * @author caoyi
 * @create 2020-12-18-15:02
 */
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.NodeCollection;
import com.aspose.words.NodeType;
import com.aspose.words.Run;

public class DocPdfFont {
  //扫描路径
  String readPath = "E://InjectLearning"+File.separator+new SimpleDateFormat("yyyyMMdd").format(new Date());

  //输出路径
  String writePath = readPath+File.separator+"output";

  //被扫描的文件类型
  String[] scanFormat =new String[]{"docx","doc"};

  File outputDirectory = new File(writePath);
  String fileName = "";

  //导出文件类型
  String exportFileType = "pdf";

  //需要被替换的字体
  static final String FONT_STYLE = "Intense Emphasis";
  Document doc;

  public static void main(String[] args){
    DocPdfFont wr = new DocPdfFont();
    wr.run();
  }

  public void run(){
    /*if(!getLicense()){
      return;
    }*/
    List<File> fileList = readDirectory();

    //如果不存在这个目录，或者这个文件不是一个目录，则新建这个目录，同时删除与目录同名的文件
    if(outputDirectory.exists()){
      if(outputDirectory.isDirectory()){
      }else{
        outputDirectory.deleteOnExit();
        outputDirectory.mkdirs();
      }
    }else{
      outputDirectory.mkdirs();
    }

    //循环所有的文件，如果是指定类型的文件，则读取文件，然后保存为pdf格式
    for(File file : fileList){
      for(String str : scanFormat){
        if(file.getName().toLowerCase().endsWith(str.toLowerCase())){
          fileName = file.getName().substring(0, file.getName().lastIndexOf("."));
          read(file.getPath());
          write(fileName);
        }
      }
    }
  }

  /**
   * 获取license
   * @return
   */
  public static boolean getLicense(){
    boolean result = false;
    InputStream is = DocPdfFont.class.getClassLoader().getResourceAsStream("license.xml");
    License aposeLic = new License();
    try {
      aposeLic.setLicense(is);
      result = true;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

  /**
   * @author vikiyang  2014-8-8 下午05:28:34
   *  获取指定目录下的所有文件
   * @return List<File>
   */
  public List<File> readDirectory(){
    File file = new File(readPath);
    List<File> fileList = new ArrayList<File>();

    while(file.isDirectory()){
      fileList.addAll(Arrays.asList(file.listFiles()));
      fileList.remove(file);
      if(fileList.size()>0){
        file = fileList.get(0);
      }else{
        return fileList;
      }
    }
    return fileList;
  }

  /**
   * @author vikiyang  2014-8-8 下午05:30:00
   *  读取文本，替换掉所有指定字体的文字
   * @return void
   */
  public void read(String filePath) {
    try {
      doc = new Document(filePath);
      ArrayList<Run> runs = getRunsByStyleName(doc, FONT_STYLE);
      for (Run run : runs) {
        run.setText(replaceWithBlank(run.getText()));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @SuppressWarnings("unchecked")
  public ArrayList<Run> getRunsByStyleName(Document doc, String styleName) throws Exception {
    ArrayList<Run> runsWithStyle = new ArrayList<Run>();
    NodeCollection<Run> runs = doc.getChildNodes(NodeType.RUN, true);
    for (Run run : (Iterable<Run>) runs) {
      if (run.getFont().getStyle().getName().equals(styleName)){
        runsWithStyle.add(run);
      }
    }
    return runsWithStyle;
  }


  private String replaceWithBlank(String question){
    StringBuilder blank = new StringBuilder("");
    for(Character c: question.toCharArray()){
      if((c >= 0x4e00)&&(c <= 0x9fbb)){
        blank.append("__");
      }else{
        blank.append("_");
      }
    }
    return blank.toString();
  }

  public void write(String fileName){
    try {
      doc.save(writePath + "//" + fileName+"." + exportFileType);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


}
