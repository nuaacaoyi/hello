package com.caoyi.java.test.ppt2imgRename;

import com.aspose.words.OutlineOptions;
import com.aspose.words.PdfSaveOptions;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author caoyi
 * @create 2020-11-16-18:01
 */
public class PptImgRename2Json {
  /**
   * word to pdf
   * @para  inPath  word 全路径
   * @para  outPath 生成的pdf 全路径
   * @author an
   * @throws Exception
   */
  public static void main(String[] args) {
    String inPath = "D:\\tmp\\001_002";
    RenameFile(inPath);
  }

  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void RenameFile(String path){
    // 获得指定文件对象
    File file = new File(path);
    String fName = file.getName();
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();
    List fileList = Arrays.asList(array);
    // 按名称排序
    Collections.sort(fileList, new Comparator<File>() {
      @Override
      public int compare(File o1, File o2) {
        if (o1.isDirectory() && o2.isFile())
          return -1;
        if (o1.isFile() && o2.isDirectory())
          return 1;


        String flag = "幻灯片";
//        System.out.println("比较开始：");

        String o1Name = o1.getName();
        int dot1 = o1Name.lastIndexOf('.');
        String dataId1 = dot1 > -1 ? o1Name.substring(0, dot1) : o1Name;
        String o1N = dataId1.substring(flag.length());
        Integer o1Num= Integer.parseInt(o1N);

        String o2Name = o2.getName();
        int dot2 = o2Name.lastIndexOf('.');
        String dataId2 = dot2 > -1 ? o2Name.substring(0, dot2) : o2Name;
        String o2N = dataId2.substring(flag.length());
        Integer o2Num= Integer.parseInt(o2N);
//        System.out.println("o1:"+o1Num + "; o2:"+o2Num);

       // System.out.println("比较结束。");

        return o1Num<o2Num?-1:1;
      }
    });

    System.out.println(fileList);

    StringBuilder jStr= new StringBuilder("[\n");


    for(int i=0;i<fileList.size();i++)
    {
      //File oneFile = array[i];
      File oneFile = (File) fileList.get(i);

      if(oneFile.isFile())//如果是文件
      {
        String jpgName = oneFile.getName();
        int dot = jpgName.lastIndexOf('.');
        String dataId = dot > -1 ? jpgName.substring(0, dot) : jpgName;
        String dataType = dot > -1 ? jpgName.substring(dot + 1) : jpgName;
        String flag = "幻灯片";

        if ("jpg".equals(dataType) || "JPG".equals(dataType)
            || "png".equals(dataType) || "PNG".equals(dataType)) {
          String num = dataId.substring(flag.length());
          String newName = fName + "_" + num + "." + dataType;
          String newPath = oneFile.getPath().replace(jpgName, newName);
          //System.out.println("老路径:"+oneFile.getPath() +";  新路径："+newPath);


          jStr.append("{\n");
          jStr.append("\t\"id\": \""+ num +"\"\n");
          jStr.append("\t\"name\": \""+ newName +"\"\n");
          jStr.append("\t\"url\": \"res/img/"+ newName +"\"\n");
          jStr.append("}");

          //oneFile.renameTo(new File(newPath));
        }
      }
      if(i<fileList.size()-1){
        jStr.append(",");
      }
      jStr.append("\n");
    }
    jStr.append("]\n");
    String jsonPath = path + "\\" +fName+".json";
    writeStringToJSON(jStr.toString(), jsonPath);
  }

  // 写json到文件
  public static void writeStringToJSON(String  str, String path){
    File f = new File(path);
    if (!f.exists()) {
      try {
        f.createNewFile();// 不存在则创建
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    try {
      // Java FileWriter 默认是用（ISO-8859-1 or US-ASCII）西方编码的，而FileWriter类没有setEncoding的方法
      // BufferedWriter output = new BufferedWriter(new FileWriter(f,true));//true,则追加写入text文本
      BufferedWriter output = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(f,true),"UTF-8"));

      output.write(str);
      // json 数组需要添加逗号分隔
      //output.write("\r\n");//换行
      output.flush();
      output.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}

