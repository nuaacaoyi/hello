package com.caoyi.java.test.reflection;

/**
 * @Author caoyi
 * @Create 2022-10-08-16:49
 * @Description: TODO
 */
public class Test03 {
  public static void main(String[] args) throws ClassNotFoundException {
    Person person = new Student();
    System.out.println("这个人是： "+ person.name);

    // 方式1：通过对象获得
    Class c1 = person.getClass();
    System.out.println(c1.hashCode());

    // 方式2：通过forName获得
    Class c2 = Class.forName("com.caoyi.java.test.reflection.Student");
    System.out.println(c2.hashCode());

    // 方式3：通过类名.class获得
    Class<Student> c3 = Student.class;
    System.out.println(c3.hashCode());

    // 方式4：基本内置类型的包装类都有一个Type属性
    Class c4 = Integer.TYPE;

    // 获得父类类型
    Class c5 = c1.getSuperclass();
    System.out.println(c5);

  }
}
class  Person{
  public String name;

  public Person() {
  }

  public Person(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Person{" +
        "name='" + name + '\'' +
        '}';
  }
}

class Student extends  Person{
  public Student(){
    this.name = "学生";
  }
}
class Teacher extends  Person{
  public Teacher(){
    this.name = "老师";
  }
}

class User{
  String name;
  int age;
  int score;
  public User() {
  }

  public User(String name, int age, int score) {
    this.name = name;
    this.age = age;
    this.score = score;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "User{" +
        "name='" + name + '\'' +
        ", age=" + age +
        ", score=" + score +
        '}';
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }
}
