package com.caoyi.java.test.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author caoyi
 * @Create 2022-10-09-10:13
 * @Description: TODO
 */
public class Test09 {
  public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
    // 获得Class对象
    Class c1 = Class.forName("com.caoyi.java.test.reflection.User");

    // 构造一个对象
    User u1 = (User)c1.newInstance();// 本质上调用的是User类的无参构造器
    System.out.println(u1);

    // 通过有参构造器创建对象
    Constructor constructor = c1.getDeclaredConstructor(String.class, int.class, int.class);
    User u2 = (User) constructor.newInstance("曹溢", 1, 1);
    System.out.println(u2);

    System.out.println("============================");
    // 通过反射调用普通方法
    User u3 = (User)c1.newInstance();
    Method setName = c1.getDeclaredMethod("setName", String.class);
    setName.invoke(u3, "abc"); // invoke：激活，参数1是对象，参数2是方法所需参数
    System.out.println(u3);

    // 通过反射操作属性
    User u4 = (User)c1.newInstance();
    Field name = c1.getDeclaredField("name");
    name.setAccessible(true); // 关闭权限安全监测，此时可以操作私有属性了
    name.set(u4, "efc"); // 默认状态下无法访问private权限的属性
    System.out.println(u4);
  }
}
