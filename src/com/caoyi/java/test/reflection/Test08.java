package com.caoyi.java.test.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @Author caoyi
 * @Create 2022-10-09-9:57
 * @Description: TODO
 */
// 获得类的信息
public class Test08 {
  public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
    Class c1 = Class.forName("com.caoyi.java.test.reflection.Person");
    // 获得类的名字
    System.out.println(c1.getName()); // 类的全引用名，带包名
    System.out.println(c1.getSimpleName()); // 获得类名

    // 获得类的属性
    Field[] fields = c1.getFields(); // 只能找到public属性
    for (Field field : fields) {
      System.out.println(field);
    }

    fields = c1.getDeclaredFields(); // 找到所有的属性
    for (Field field : fields) {
      System.out.println(field);
    }

    Field name = c1.getField("name"); // 同理，通过属性名找公共属性
    Field name1 = c1.getDeclaredField("name");// 所有属性

    System.out.println("===============================");
    // 获得类的方法
    Method[] methods = c1.getMethods(); // 获得本类和父类的全部public方法
    for (Method method : methods) {
      System.out.println(method);
    }
    methods = c1.getDeclaredMethods(); // 获得本类的所有方法
    for (Method method : methods) {
      System.out.println(method);
    }

    // 获得指定的方法 , 参数2是方法所需参数的类型
    Method getName = c1.getMethod("getName", null);
    Method setName = c1.getMethod("setName", String.class);

    System.out.println("===============================");
    // 获得指定的构造器
    Constructor[] constructors = c1.getConstructors(); // 获取 public构造方法
    constructors = c1.getDeclaredConstructors(); // 获取所有的构造方法
  }
}
