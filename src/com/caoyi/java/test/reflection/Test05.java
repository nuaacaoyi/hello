package com.caoyi.java.test.reflection;

/**
 * @Author caoyi
 * @Create 2022-10-09-8:53
 * @Description: 讲解类如何加载
 */
public class Test05 {
  public static void main(String[] args) {
    A a = new A();
    System.out.println(A.m);
    /*
    1.加载到内存，会产生一个类对象Class对象
    2.链接，链接结束后m=0
    3.类的初始化
      <clinit>(){
        System.out.println("A类静态代码块初始化");
        m = 300;
        m = 100;
      }
    * */
  }
}

class A{
  // 执行顺序：1
  static {
    System.out.println("A类静态代码块初始化");
    m = 300;
  }

  // 执行顺序：3
  static  int m = 100 ;

  // 执行顺序：2
  public A(){
    System.out.println("A类的无参构造初始化");
  }
}
