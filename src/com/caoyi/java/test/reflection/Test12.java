package com.caoyi.java.test.reflection;

import java.lang.annotation.*;
import java.lang.reflect.Field;

/**
 * @Author caoyi
 * @Create 2022-10-09-10:53
 * @Description: TODO
 */
// 练习反射操作注解
public class Test12 {
  public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {
    Class  c1 = Class.forName("com.caoyi.java.test.reflection.Student2");

    // 通过反射获取注解
    Annotation[] annotations = c1.getAnnotations();
    for (Annotation annotation : annotations) {
      // 获得注解的全部定义内容
      System.out.println(annotation);
    }

    // 获取指定注解的value值
    Tablekuang a1 = (Tablekuang)c1.getAnnotation(Tablekuang.class);
    String value = a1.value();
    System.out.println(value);

    // 获得类指定属性的注解中value值
    Field name = c1.getDeclaredField("name");
    Fieldkuang a2 = name.getAnnotation(Fieldkuang.class);
    System.out.println("columnName: " + a2.columnName());
    System.out.println("type: " + a2.type());
    System.out.println("length: " + a2.length());
  }
}

@Tablekuang("db_student")
class Student2{
  @Fieldkuang(columnName = "db_id", type="int", length = 10)
  private int id;
  @Fieldkuang(columnName = "db_age", type="int", length = 10)
  private int age;
  @Fieldkuang(columnName = "db_name", type="varchar", length = 10)
  private String name;

  public Student2() {
  }

  public Student2(int id, int age, String name) {
    this.id = id;
    this.age = age;
    this.name = name;
  }

  @Override
  public String toString() {
    return "Student2{" +
        "id=" + id +
        ", age=" + age +
        ", name='" + name + '\'' +
        '}';
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

// 类名的注解
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Tablekuang{
  String value();
}

// 属性的注解
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface Fieldkuang{
  String columnName();
  String type();
  int length();
}
