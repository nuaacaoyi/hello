package com.caoyi.java.test.reflection;

import java.lang.annotation.ElementType;

/**
 * @Author caoyi
 * @Create 2022-10-09-8:37
 * @Description: TODO
 */
public class Test04 {
  public static void main(String[] args) {
    Class c1 = Object.class;// 类
    Class c2 = Comparable.class; // 接口
    Class c3 = String[].class; // 一维数组
    Class c4 = int[][].class; // 二维数组
    Class c5 = Override.class; // 注解
    Class c6 = ElementType.class; // 枚举
    Class c7 = Integer.class; // 基本数据引用类型
    Class c8 = void.class; // void
    Class c9 = Class.class; // Class类本身
    Class c10 = int.class; // 基本数据类型

    System.out.println(c1 );
    System.out.println(c2 );
    System.out.println(c3 );
    System.out.println(c4 );
    System.out.println(c5 );
    System.out.println(c6 );
    System.out.println(c7 );
    System.out.println(c8 );
    System.out.println(c9 );
    System.out.println(c10);

    // 只要元素类型和维度一样，就是通过一个Class，不管数组长度
    int[] a = new int[10];
    int[] b = new int[100];
    System.out.println(a.getClass().hashCode());
    System.out.println(b.getClass().hashCode());

  }
}
