package com.caoyi.java.test.pdfIndex;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.io.File;

import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;

/**
 * @Author caoyi
 * @Create 2021-12-10-16:57
 * @Description: 解析带目录的pdf文件，获取目录并打印
 */

public class AnalysisPdfIndex {

  public void printBookmarks(PDOutlineNode bookmark,String indentation) throws IOException{
    PDOutlineItem current = bookmark.getFirstChild();
    while(current != null){
      int pages = 0;
      if(current.getDestination() instanceof PDPageDestination){
        PDPageDestination pd = (PDPageDestination) current.getDestination();
        pages = pd.retrievePageNumber() + 1;
      }
      if (current.getAction()  instanceof PDActionGoTo) {
        PDActionGoTo gta = (PDActionGoTo) current.getAction();
        if (gta.getDestination() instanceof PDPageDestination) {
          PDPageDestination pd = (PDPageDestination) gta.getDestination();
          pages = pd.retrievePageNumber() + 1;
        }
      }
      if (pages == 0) {
        System.out.println(indentation+current.getTitle());
      }else{
        System.out.println(indentation+current.getTitle()+"  "+pages);
      }
      printBookmarks( current, indentation + "    " );
      current = current.getNextSibling();
    }
  }
  public static void main(String[] args) throws IOException {
    File file = new File("D:/tmp/维修保障库存决策支持系统研制总结报告.pdf");
    PDDocument doc = null;
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(file);
      PDFParser parser = new PDFParser(new RandomAccessBuffer(fis));
      parser.parse();
      doc = parser.getPDDocument();
      PDDocumentOutline outline = doc.getDocumentCatalog().getDocumentOutline();
      AnalysisPdfIndex pdf = new AnalysisPdfIndex();
      if (outline != null) {
        pdf.printBookmarks(outline, "");
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

  }
}
