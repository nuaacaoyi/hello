package com.caoyi.java.test.pdfIndex;

import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;

import javax.swing.plaf.synth.Region;
import java.io.*;

/**
 * @Author caoyi
 * @Create 2021-12-10-16:57
 * @Description: 解析带目录的pdf文件，获取目录并打印
 */

public class BatchAnalysisPdfIndex {

  // 索引中每一条的处理方法 递归读取pdf索引，并写入xml文件
  public void printBookmarks(PDOutlineNode bookmark,FileOutputStream fos, String indentation) throws IOException{
    int level = indentation.length()/4;// 获取当前标题等级
    PDOutlineItem current = bookmark.getFirstChild();
    while(current != null){
      int pages = 0;
      if(current.getDestination() instanceof PDPageDestination){
        PDPageDestination pd = (PDPageDestination) current.getDestination();
        pages = pd.retrievePageNumber() + 1;
      }
      if (current.getAction()  instanceof PDActionGoTo) {
        PDActionGoTo gta = (PDActionGoTo) current.getAction();
        if (gta.getDestination() instanceof PDPageDestination) {
          PDPageDestination pd = (PDPageDestination) gta.getDestination();
          pages = pd.retrievePageNumber() + 1;
        }
      }
      String content = "";
      if (pages == 0) {
        content= indentation+current.getTitle();
        System.out.println(content);
      }else{
        content = indentation+"<node level=\""+level+"\" title=\"" + current.getTitle()+"\" page=\""+pages+"\">\n";
        System.out.println(content);
      }

      byte[] bytes=(content).getBytes();
      fos.write(bytes);
      printBookmarks( current, fos,indentation + "    " );
      current = current.getNextSibling();

      byte[] tail=(indentation+ "</node>\n").getBytes();
      fos.write(tail);
    }
  }

  // 针对某一个pdf文件的处理级别
  public static void handleFile(String filePath, String fileName) throws IOException {
    File file = new File(filePath);
    PDDocument doc = null;
    FileInputStream fis = null;
    FileOutputStream fos=null;
    try {
      // 以原文件名 加上 后缀.xml作为其目录映射文件
      fos=new FileOutputStream(filePath+".xml");
      {//写入xml文件头
        String head = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<book title=\""+fileName+"\">";
        byte[] bytes=(head+"\n").getBytes();
        fos.write(bytes);
      }

      fis = new FileInputStream(file);
      PDFParser parser = new PDFParser(new RandomAccessBuffer(fis));
      parser.parse();
      doc = parser.getPDDocument();
      PDDocumentOutline outline = doc.getDocumentCatalog().getDocumentOutline();
      BatchAnalysisPdfIndex pdf = new BatchAnalysisPdfIndex();
      if (outline != null) {
        // 递归调用完成目录写入xml
        pdf.printBookmarks(outline,fos, "    ");
      }

      {// 写入xml尾
        String tail = "</book>";
        byte[] bytes=(tail+"\n").getBytes();
        fos.write(bytes);
      }
      fos.flush();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    finally {
      if(fos!=null) {
        try {
          fos.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // main函数读取某一个文件目录，遍历目录下所有pdf文件
  public static void main(String[] args) throws IOException {

    // 指定目录
    File file = new File("D:/tmp/abcd");
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    // 遍历
    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        String docName = array[i].getName();
        int dot = docName.lastIndexOf('.');
        String dataId =  dot>-1?docName.substring(0, dot):docName;
        String dataType =  dot>-1?docName.substring(dot+1):docName;
        if("pdf".equals(dataType) || "PDF".equals(dataType)){
          handleFile(array[i].getPath(), dataId);
        }
      }
    }
  }
}
