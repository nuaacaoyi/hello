package com.caoyi.java.test.pdfIndex;

import org.apache.pdfbox.io.RandomAccessBuffer;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineNode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @Author caoyi
 * @Create 2021-12-10-16:57
 * @Description: 解析带目录的pdf文件，获取目录并打印
 */

public class AnaPdfIndexToJson {

  public void printBookmarks(PDOutlineNode bookmark,String indentation, int id) throws IOException{
    int cnt = 1;
    PDOutlineItem current = bookmark.getFirstChild();

    String newIden = indentation+"  ";
    //String resultPrint = "";

    while(current != null){
      int childId = id*100+cnt++;

      int pages = 0;
      if(current.getDestination() instanceof PDPageDestination){
        PDPageDestination pd = (PDPageDestination) current.getDestination();
        pages = pd.retrievePageNumber() + 1;
      }
      if (current.getAction()  instanceof PDActionGoTo) {
        PDActionGoTo gta = (PDActionGoTo) current.getAction();
        if (gta.getDestination() instanceof PDPageDestination) {
          PDPageDestination pd = (PDPageDestination) gta.getDestination();
          pages = pd.retrievePageNumber() + 1;
        }
      }
     /* resultPrint += String.format(indentation+"{\n%s\"id\": %d,\n%s\"label\": \"%s\" ,\n%s\"page\": %d,\n%s\"children\":[\n",
          newIden, childId,newIden,current.getTitle(), newIden,pages,newIden);*/
      System.out.println(String.format(indentation+"{\n%s\"id\": %d,\n%s\"label\": \"%s\" ,\n%s\"page\": %d,\n%s\"children\":[",
          newIden, childId,newIden,current.getTitle(), newIden,pages,newIden));

      /*if (pages == 0) {
        System.out.println(indentation+current.getTitle());
      }else{
        System.out.println(indentation+current.getTitle()+"  "+pages);
      }*/
      printBookmarks( current, indentation + "  " ,childId );
      current = current.getNextSibling();

      //resultPrint += String.format("%s]\n%s},\n", newIden,indentation);
      if(current != null){
        System.out.println(String.format("%s]\n%s},", newIden,indentation));
      }else{
        System.out.println(String.format("%s]\n%s}", newIden,indentation));
      }

    }
    //System.out.println(resultPrint.lastIndexOf(",")>0?resultPrint.substring(0, resultPrint.lastIndexOf(",")): "");
  }
  public static void main(String[] args) throws IOException {
    File file = new File("D:/tmp/abcd/维修保障库存决策支持系统研制总结报告.pdf");
    PDDocument doc = null;
    FileInputStream fis = null;
    int id = 14; // 书目id
    String bookName = "税法"; // 书籍名称
    String url = "res/doc/007_1.pdf"; // pdf文件路径
    String backImgUrl = "res/img/back_1.png"; // 卡片缩略图路径
    System.out.println(String.format("{\n  \"id\": %d,\n  \"type\": \"pdf\",\n  \"typeName\":\"书籍\",\n  \"title\": \"%s\",\n  \"url\": \"%s\",\n  \"backImgUrl\": \"%s\",", id,bookName,url, backImgUrl));
    System.out.println("  \"docTree\": [");

    try {
      fis = new FileInputStream(file);
      PDFParser parser = new PDFParser(new RandomAccessBuffer(fis));
      parser.parse();
      doc = parser.getPDDocument();
      PDDocumentOutline outline = doc.getDocumentCatalog().getDocumentOutline();
      AnaPdfIndexToJson pdf = new AnaPdfIndexToJson();
      if (outline != null) {
        pdf.printBookmarks(outline, "  ", id);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    System.out.println("  ]\n}");
  }
}
