package com.caoyi.java.test.excel;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;

/**
 * @Author caoyi
 * @Create 2022-08-24-17:22
 * @Description: TODO
 */
public class ReadExcelByPOI {
  private XSSFSheet sheet;

  /**
   * 20      * 构造函数，初始化excel数据
   * 21      * @param filePath  excel路径
   * 22      * @param sheetName sheet表名
   * 23
   */
  ReadExcelByPOI(String filePath, String sheetName) {
    FileInputStream fileInputStream = null;
    try {
      fileInputStream = new FileInputStream(filePath);
      XSSFWorkbook sheets = new XSSFWorkbook(fileInputStream);
      //获取sheet
      sheet = sheets.getSheet(sheetName);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 37      * 根据行和列的索引获取单元格的数据
   * 38      * @param row
   * 39      * @param column
   * 40      * @return
   * 41
   */
  public String getExcelDateByIndex(int row, int column) {
    XSSFRow row1 = sheet.getRow(row);
    String cell = row1.getCell(column).toString();
    return cell;
  }

  /**
   * 49      * 根据某一列值为“******”的这一行，来获取该行第x列的值
   * 50      * @param caseName
   * 51      * @param currentColumn 当前单元格列的索引
   * 52      * @param targetColumn 目标单元格列的索引
   * 53      * @return
   * 54
   */
  public String getCellByCaseName(String caseName, int currentColumn, int targetColumn) {
    String operateSteps = "";
    //获取行数
    int rows = sheet.getPhysicalNumberOfRows();
    for (int i = 0; i < rows; i++) {
      XSSFRow row = sheet.getRow(i);
      String cell = row.getCell(currentColumn).toString();
      if (cell.equals(caseName)) {
        operateSteps = row.getCell(targetColumn).toString();
        break;
      }
    }
    return operateSteps;
  }

  //打印excel数据
  public void readRead_EXCEL() {
    //获取行数
    int rows = sheet.getPhysicalNumberOfRows();
    for (int i = 0; i < rows; i++) {
      //获取列数
      XSSFRow row = sheet.getRow(i);
      int columns = row.getPhysicalNumberOfCells();
      for (int j = 0; j < columns; j++) {
        String cell = row.getCell(j).toString();
        System.out.print(cell+"\t");
      }
      System.out.println();
    }
  }

  //测试方法
  public static void main(String[] args) {
    ReadExcelByPOI sheet1 = new ReadExcelByPOI("C:\\Users\\86185\\Downloads\\2022年7月一次测试分析20220823094638.xlsx", "2022年7月一次测试分析20220823094638.xls");
    //获取第二行第4列
    //String cell2 = sheet1.getExcelDateByIndex(1, 0);
    //根据第3列值为“customer23”的这一行，来获取该行第2列的值
    //String cell3 = sheet1.getCellByCaseName("名字", 0, 0);
    //System.out.println(cell2);
    //System.out.println(cell3);

    sheet1.readRead_EXCEL();
  }

}
