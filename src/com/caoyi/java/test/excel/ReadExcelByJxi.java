package com.caoyi.java.test.excel;

import jxl.Cell;
import jxl.JXLException;
import jxl.Sheet;
import jxl.Workbook;

import java.io.File;


/**
 * @Author caoyi
 * @Create 2022-08-24-17:15
 * @Description: TODO
 */
// jxl仅支持2003版本的excel，放弃使用
public class ReadExcelByJxi {
  public static void main(String[] args) throws JXLException, Exception{
    java.io.File file=new File("C:\\\\Users\\\\86185\\\\Downloads\\\\2022年7月一次测试分析20220823094638.xlsx");
    if(!file.exists())
      file.createNewFile();
    //从文件中获取表
    Workbook workbook=Workbook.getWorkbook(file);

    //获取第一页
    Sheet sheet=workbook.getSheet(0);

    //打印
    //i代表行，j代表列
    for (int i = 0; i < sheet.getRows(); i++)
    {
      for (int j = 0; j < sheet.getColumns(); j++)
      {
        Cell cell = sheet.getCell(j,i);
        System.out.print(cell.getContents()+"\t");
      }
      System.out.println();
    }
    workbook.close();
  }
}
