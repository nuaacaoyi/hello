package com.caoyi.java.test;

import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author caoyi
 * @create 2020-10-19-9:42
 */
public class ReadPdfText {
  public static void main(String[] args) {
    String path = "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\res";
    getFile(path,0);


  }

  //文件加密、解密函数（对称），传入源文件、目标文件进行加密或解密， --任老师
  public static void encDecFile(String srcFile, String destFile) {
    File f = new File(srcFile);
    if (!f.exists()) {
      return;
    }
    File o = new File(destFile);
    byte encCode = -111; //加密解密秘钥
    byte[] data = new byte[1024 * 32]; //每次读取32M
    int ilen;
    try {
      FileInputStream fis = new FileInputStream(f);
      FileOutputStream fos = new FileOutputStream(o);
      while ((ilen = fis.read(data)) != -1) {
        byte[] dest = new byte[ilen];
        for (int i = 0; i < ilen; i++) {
          dest[i] = (byte) (data[i] ^ encCode);
        }
        fos.write(dest);
      }
      fis.close();
      fos.close();
    } catch (Exception e) {

    }
  }


  /*
  * 更新数据库中的全文本字段
  * */
  private static void updateFullText(String id,String flTxt){
    Connection conn=null;
    PreparedStatement ps=null;
    ResultSet rs=null;
    try {
      conn= DBUtil.getConnection();
      String sql="UPDATE T_FACT_ACCIDENT t SET t.FULL_CONTENT=? where t.ACC_ID = ?";
      ps=conn.prepareStatement(sql);
      ps.setString(1,flTxt);
      ps.setString(2,id);
      ps.executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }finally {
      DBUtil.close(conn,ps,rs);
    }
  }


  /*
  * 使用spire读取pdf文本内容
  * */
  private static String readPdfText(String path){
    //创建PdfDocument实例
    PdfDocument doc= new PdfDocument();

    //加载PDF文件
    //eg  "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\test.pdf"
    doc.loadFromFile(path);

    StringBuilder sb= new StringBuilder();

    PdfPageBase page;

    //遍历PDF页面，获取文本
    for(int i=0;i<doc.getPages().getCount();i++){
      page=doc.getPages().get(i);
      sb.append(page.extractText(true));
    }

    /*FileWriter writer;

    try {
      //将文本写入文本文件
      //eg  "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\ExtractText.txt"
      writer = new FileWriter(path + ".txt");
      writer.write(sb.toString());
      writer.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }*/

    doc.close();
    return sb.toString();
  }
  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void getFile(String path,int deep){
    // 获得指定文件对象
    File file = new File(path);
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        String pdfName = array[i].getName();
        int dot = pdfName.lastIndexOf('.');
        String dataId =  dot>-1?pdfName.substring(0, dot):pdfName;
        String dataType =  dot>-1?pdfName.substring(dot+1):pdfName;

        //目标目录
        String resuDir = "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\result";
        String newDir =  file.getName() + "_" +dataId;
        //String newDir =  array[i].getParent() + "_" +dataId;
        File fFile = new File(array[i].getPath());
        File tFile = new File(resuDir + "\\" + newDir + "\\" + array[i].getName());
        //复制文件
        copyByStream(fFile,tFile);
        encDecFile(tFile.getPath(),tFile.getPath() + ".enc");

        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");
        // 只输出文件名字
        System.out.println(newDir);
        if("pdf".equals(dataType) ||  "PDF".equals(dataType)){
          System.out.println(readPdfText(array[i].getPath()));
          updateFullText(newDir,readPdfText(array[i].getPath()));
        }
        // 输出当前文件的完整路径
        // System.out.println("#####" + array[i]);
        // 同样输出当前文件的完整路径   大家可以去掉注释 测试一下
        // System.out.println(array[i].getPath());
      }
      else if(array[i].isDirectory())//如果是文件夹
      {
        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");

        System.out.println( array[i].getName());
        //System.out.println(array[i].getPath());
        //文件夹需要调用递归 ，深度+1
        getFile(array[i].getPath(),deep+1);
      }
    }
  }


  /*
  * 复制文件的函数
  * */
  private static void copyFileUsingFileStreams(File source, File dest)
      throws IOException {
    InputStream input = null;
    OutputStream output = null;
    try {
      input = new FileInputStream(source);
      output = new FileOutputStream(dest);
      byte[] buf = new byte[1024];
      int bytesRead;
      while ((bytesRead = input.read(buf)) > 0) {
        output.write(buf, 0, bytesRead);
      }
    } finally {
      input.close();
      output.close();
    }
  }

  /**复制文件的函数  如目标父路径不存在则创建
   * @param fromFile
   * @param toFile
   */
  public static void copyByStream(File fromFile, File toFile)
  {
    //如果文件不存在
    if (!toFile.exists())
    {
      //先创建该文件的所有上级目录
      if (toFile.getParentFile().mkdirs())
      {
        try
        {
          //再创建该文件
          toFile.createNewFile();
        } catch (IOException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

    }

    FileInputStream ins = null;
    FileOutputStream out = null;
    try
    {
      ins = new FileInputStream(fromFile);
      /*
       * 使用FileOutputStream写文件
       * 如果目标文件不存在，则FileOutputStream会创建目标文件(前提是父路径文件对象必须存在)。
       */
      out = new FileOutputStream(toFile);
      byte[] buf = new byte[1024];
      int size = 0;
      // 每次读取1024个字节,然后写入1024自己
      while ((size = ins.read(buf)) != -1)
      {
        out.write(buf, 0, size);
      }

    } catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally
    {
      if (ins != null)
      {
        try
        {
          ins.close();
        } catch (IOException e)
        {
          e.printStackTrace();
        }
      }
      if (out != null)
      {
        try
        {
          out.close();
        } catch (IOException e)
        {
          e.printStackTrace();
        }
      }

    }
  }// 方法结束
}
