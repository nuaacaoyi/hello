package com.caoyi.java.test.doc2txt;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

import java.io.File;

/**
 * @Author caoyi
 * @Create 2021-12-11-21:09
 * @Description: TODO
 */
public class Word2TxtUtil {
  public static void word2Txt(String wordPath,String txtPath) {
    ActiveXComponent app = new ActiveXComponent("Word.Application");
    app.setProperty("Visible", new Variant(false));
    Dispatch doc1 = app.getProperty("Documents").toDispatch();
    Dispatch doc2 = Dispatch.invoke(
        doc1,
        "Open",
        Dispatch.Method,
        new Object[]{wordPath, new Variant(false), new Variant(true)},
        new int[1]
    ).toDispatch();
    Dispatch.invoke(
        doc2,
        "SaveAs",
        Dispatch.Method,
        new Object[]{txtPath,new Variant(7)//7为txt格式， 8保存为html格式
        },
        new int[1]
    );
    Variant f = new Variant(false);
    Dispatch.call(doc2, "Close", f);
  }

  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void getFile(String path,String outPath, int deep){
    // 获得指定文件对象
    File file = new File(path);
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        String docName = array[i].getName();
        int dot = docName.lastIndexOf('.');
        String dataId =  dot>-1?docName.substring(0, dot):docName;
        String dataType =  dot>-1?docName.substring(dot+1):docName;



        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");
        // 只输出文件名字
        /*if("doc".equals(dataType) ||  "docx".equals(dataType)){
          //System.out.println(readPdfText(array[i].getPath()));
          docPdf(array[i].getPath(),outPath + "\\" + dataId + ".pdf");
        }*/
        if("doc".equals(dataType) ||"docx".equals(dataType) ){
          //System.out.println(readPdfText(array[i].getPath()));
          //word2Txt(array[i].getPath(),outPath + "\\" + dataId + ".txt");
          word2Txt(array[i].getPath(),array[i].getPath()+ ".txt");
        }
        // 输出当前文件的完整路径
        // System.out.println("#####" + array[i]);
        // 同样输出当前文件的完整路径   大家可以去掉注释 测试一下
        // System.out.println(array[i].getPath());
      }
      else if(array[i].isDirectory())//如果是文件夹
      {
        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");

        System.out.println( array[i].getName());
        //System.out.println(array[i].getPath());
        //文件夹需要调用递归 ，深度+1
        getFile(array[i].getPath(),outPath,deep+1);
      }
    }
  }

  public static void main(String[] args) {
    String inPath = "D:\\工作\\过往\\华润\\部门\\管理文档\\信息管理制度\\信息制度";
    String outPath = "D:\\工作\\过往\\华润\\部门\\管理文档\\信息管理制度\\xxzdtxt";
    getFile(inPath,outPath, 0);
    //word2Txt("D:\\工作\\过往\\华润\\部门\\管理文档\\信息管理制度\\华润医药商业集团有限公司信息化管理制度.docx",
     //   "D:\\工作\\过往\\华润\\部门\\管理文档\\信息管理制度\\华润医药商业集团有限公司信.txt");
  }
}
