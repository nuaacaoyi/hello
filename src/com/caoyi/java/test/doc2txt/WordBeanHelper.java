package com.caoyi.java.test.doc2txt;

import java.io.File;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.ComFailException;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class WordBeanHelper {
	/**
	 * ��ȡCom�ӿ��쳣��������Դ���.
	 */
	private static final int MAX_RETRY = 10;
	/**
	 * word�ĵ�.
	 */
	private Dispatch doc;
	/**
	 * word���г������.
	 */
	private ActiveXComponent wordApp = null;
	/**
	 * ѡ���ķ�Χ������.
	 */
	private Dispatch selection;
	/**
	 * �˳�ʱ�Ƿ񱣴��ĵ�.
	 */
	private boolean saveOnExit = true;

	/**
	 * ���캯��.
	 *
	 * @param show
	 *            �Ƿ�ɼ�.
	 */
	public WordBeanHelper(final boolean show) {
		if (wordApp == null) {
			wordApp = new ActiveXComponent("Word.Application");
			wordApp.setProperty("Visible", new Variant(show));
		}
	}

	/**
	 * �����˳�ʱ����.
	 *
	 * @param b
	 *            boolean true-�˳�ʱ�����ļ���false-�˳�ʱ�������ļ�
	 */
	public void setSaveOnExit(final boolean b) {
		this.saveOnExit = b;
	}

	/**
	 * ��ѡ�������ݻ����������ƶ�.
	 *
	 * @param pos
	 *            �ƶ��ľ���
	 */
	public void moveUp(final int pos) {

		if (selection == null) {
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		}

		for (int i = 0; i < pos; i++) {
			Dispatch.call(selection, "MoveUp");
		}
	}

	/**
	 * ��ѡ�������ݻ��߲���������ƶ�.
	 *
	 * @param pos
	 *            �ƶ��ľ���
	 */
	public void moveDown(final int pos) {

		if (selection == null) {
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		}

		for (int i = 0; i < pos; i++) {
			Dispatch.call(selection, "MoveDown");
		}
	}

	/**
	 * ��ѡ�������ݻ��߲���������ƶ�.
	 *
	 * @param pos
	 *            �ƶ��ľ���
	 */
	public void moveLeft(final int pos) {

		if (selection == null) {
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		}

		for (int i = 0; i < pos; i++) {
			Dispatch.call(selection, "MoveLeft");
		}
	}

	/**
	 * ��ѡ�������ݻ��߲���������ƶ�.
	 *
	 * @param pos
	 *            �ƶ��ľ���
	 */
	public void moveRight(final int pos) {

		if (selection == null) {
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		}

		for (int i = 0; i < pos; i++) {
			Dispatch.call(selection, "MoveRight");
		}
	}

	/**
	 * �Ѳ�����ƶ����ļ���λ��.
	 */
	public void moveStart() {
		if (selection == null) {
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		}

		Dispatch.call(selection, "HomeKey", new Variant(6));
	}

	/**
	 * �Ѳ�����ƶ����ļ�βλ��.
	 */
	public void moveEnd() {
		if (selection == null) {
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		}

		Dispatch.call(selection, "EndKey", new Variant(6));
	}

	/**
	 * ��������.
	 *
	 * @param pos
	 *            ������
	 */
	public void listIndent(final int pos) {

		Dispatch range = Dispatch.get(this.selection, "Range").toDispatch();
		Dispatch listFormat = Dispatch.get(range, "ListFormat").toDispatch();
		for (int i = 0; i < pos; i++) {
			Dispatch.call(listFormat, "ListIndent");
		}
	}

	/**
	 * ��������.
	 *
	 * @param pos
	 *            ������
	 */
	public void listOutdent(final int pos) {

		Dispatch range = Dispatch.get(this.selection, "Range").toDispatch();
		Dispatch listFormat = Dispatch.get(range, "ListFormat").toDispatch();
		for (int i = 0; i < pos; i++) {
			Dispatch.call(listFormat, "ListOutdent");
		}
	}

	/**
	 * �س�����.
	 */
	public void enter() {
		int index = 1;
		while (true) {
			try {
				Dispatch.call(this.selection, "TypeParagraph");
				break;
			} catch (ComFailException e) {
				if (index++ >= MAX_RETRY) {
					throw e;
				} else {
					continue;
				}
			}
		}
	}

	/**
	 * ����һ����ҳ��.
	 */
	public void insertPageBreak() {
		Dispatch.call(this.selection, "InsertBreak", new Variant(2));
	}

	/**
	 * ����word�ĵ��Ƿ�ɼ�.
	 *
	 * @param isVisible
	 *            �Ƿ�ɼ�
	 */
	public void setIsVisible(final boolean isVisible) {

		wordApp.setProperty("Visible", new Variant(isVisible));
	}

	/**
	 * �ж��ĵ��Ƿ����.
	 *
	 * @param docName
	 *            �ĵ�����.
	 * @return boolean �Ƿ����.
	 */
	private boolean isExist(final String docName) {

		boolean result = false;
		File file = new File(docName);
		result = file.exists();
		file = null;
		return result;
	}

	/**
	 * ��ȡ�ļ�����.
	 *
	 * @param docName
	 *            �ĵ�·��.
	 * @return �ļ�����
	 */
	public String getFileName(final String docName) {

		int pos = docName.lastIndexOf("\\");
		return docName.substring(pos + 1);
	}

	/**
	 * ���ĵ�.
	 *
	 * @param docName
	 *            �ĵ�·��.
	 * @throws WordException
	 *             �쳣
	 */
	public void openDocument(final String docName) throws Exception {

		Dispatch docs = wordApp.getProperty("Documents").toDispatch();

		if (isExist(docName)) {
			this.closeDocument();
			doc = Dispatch.call(docs, "Open", docName).toDispatch();
		} else {
			wordApp.invoke("Quit", new Variant[] {});
			new Exception("[Open doc failed]: file[" + docName
					+ "] isn't existed!");
		}

		selection = Dispatch.get(wordApp, "Selection").toDispatch();
	}

	/**
	 * ���һ�����ĵ�.
	 *
	 * @param docName
	 *            �ĵ�·��.
	 * @throws WordException
	 *             �쳣
	 */
	public void newDocument(final String docName) throws Exception {

		try {
			Dispatch docs = wordApp.getProperty("Documents").toDispatch();
			doc = Dispatch.call(docs, "Add").toDispatch();
			selection = Dispatch.get(wordApp, "Selection").toDispatch();
		} catch (com.jacob.com.ComFailException cfe) {
			throw new Exception(cfe.getMessage());
		} catch (com.jacob.com.ComException ce) {
			throw new Exception(ce.getMessage());
		}
	}

	/**
	 * ����һ������.
	 *
	 * @param textToInsert
	 *            ����
	 * @param style
	 *            ��ʽ
	 */
	public void insertText(final String textToInsert, final String style) {

		Dispatch.put(selection, "Text", textToInsert);
		Dispatch.put(this.selection, "Style", style);
		Dispatch.call(selection, "MoveRight");
	}

	/**
	 * insert a picture.
	 *
	 * @param imagePath
	 * @param style
	 */
	public void insertImage(final String imagePath, final String style) {

		Dispatch.call(Dispatch.get(selection, "InLineShapes").toDispatch(),
				"AddPicture", imagePath);

		//Dispatch.call(selection, "MoveRight");
		//Dispatch.put(selection, "Style", getOutlineStyle(style));
		//this.enter();
	}

	/**
	 * ��ȡ��Ӧ���Ƶ�Style����.
	 *
	 * @param style
	 *            Style����.
	 * @return Style����
	 */
	public Variant getOutlineStyle(final String style) {

		int index = 1;
		while (true) {
			try {
				return Dispatch.call(Dispatch.get(this.doc, "Styles")
						.toDispatch(), "Item", new Variant(style));
			} catch (ComFailException e) {
				if (index++ >= MAX_RETRY) {
					throw e;
				} else {
					continue;
				}
			}
		}
	}

	/**
	 * �������.
	 *
	 * @param text
	 *            ��������.
	 * @param style
	 *            ���ñ��������
	 */
	public void insertOutline(final String text, final String style) {

		this.insertText(text, style);
		this.enter();
	}

	/**
	 * ����Ŀ¼. tablesOfContents�Ĳ����ĺ��� Add(Range As Range, [UseHeadingStyles],
	 * [UpperHeadingLevel], [LowerHeadingLevel], [UseFields], [TableID],
	 * --������Ҫ��Ҫ������ [RightAlignPageNumbers],[IncludePageNumbers], [AddedStyles],
	 * --�������������ֵ,����������,���������,��com.jacob.com.ComFailException
	 * [UseHyperlinks],[HidePageNumbersInWeb], [UseOutlineLevels])
	 */
	public void insertTablesOfContents() {
		Dispatch tablesOfContents = Dispatch.get(this.doc, "TablesOfContents")
				.toDispatch();

		Dispatch range = Dispatch.get(this.selection, "Range").toDispatch();
		// Dispatch.call�еĲ��������9��,�������9��,����Dispatch.callN����Dispathc.invoke
		/*
		 * Dispatch.invoke(tablesOfContents, "Add", Dispatch.Method,new
		 * Object[]{range,new Variant(true),new Variant(1), new Variant(3),new
		 * Variant(true), new Variant(true),new Variant(true) ,new
		 * Variant("1"),new Variant(true),new Variant(true)},new int[10]);
		 */
		Dispatch.callN(tablesOfContents, "Add", new Object[] { range,
				new Variant(true), new Variant(1), new Variant(3),
				new Variant(false), new Variant(true), new Variant(true),
				new Variant("1"), new Variant(true), new Variant(true) });
	}

	/**
	 * ��ѡ�����ݻ����㿪ʼ�����ı�.
	 *
	 * @param toFindText
	 *            Ҫ���ҵ��ı�
	 * @return boolean true-���ҵ���ѡ�и��ı���false-δ���ҵ��ı�
	 */
	public boolean find(final String toFindText) {

		if (toFindText == null || toFindText.equals("")) {
			return false;
		}

		// ��selection����λ�ÿ�ʼ��ѯ
		Dispatch find = Dispatch.call(selection, "Find").toDispatch();
		// ����Ҫ���ҵ�����
		Dispatch.put(find, "Text", toFindText);
		// ��ǰ����
		Dispatch.put(find, "Forward", "True");
		// ���ø�ʽ
		Dispatch.put(find, "Format", "True");
		// ��Сдƥ��
		Dispatch.put(find, "MatchCase", "True");
		// ȫ��ƥ��
		Dispatch.put(find, "MatchWholeWord", "True");
		// ���Ҳ�ѡ��
		return Dispatch.call(find, "Execute").getBoolean();
	}

	/**
	 * ��ѡ��ѡ�������趨Ϊ�滻�ı�.
	 *
	 * @param toFindText
	 *            �����ַ���
	 * @param newText
	 *            Ҫ�滻������
	 * @return boolean true-���ҵ���ѡ�и��ı���false-δ���ҵ��ı�
	 */
	public boolean replaceText(final String toFindText, final String newText) {

		if (!find(toFindText)) {
			return false;
		}

		Dispatch.put(selection, "Text", newText);
		return true;
	}

	/**
	 * �������.
	 *
	 * @param numCols
	 *            ����
	 * @param numRows
	 *            ����
	 * @param autoFormat
	 *            Ĭ�ϸ�ʽ
	 * @return ������
	 */
	public Dispatch createTable(final int numRows, final int numCols,
			final int autoFormat) {

		int index = 1;
		while (true) {
			try {
				Dispatch tables = Dispatch.get(doc, "Tables").toDispatch();
				Dispatch range = Dispatch.get(selection, "Range").toDispatch();
				Dispatch newTable = Dispatch.call(tables, "Add", range,
						new Variant(numRows), new Variant(numCols))
						.toDispatch();

				Dispatch.call(selection, "MoveRight");
				Dispatch.call(newTable, "AutoFormat", new Variant(autoFormat));
				return newTable;
			} catch (ComFailException e) {
				if (index++ >= MAX_RETRY) {
					throw e;
				} else {
					continue;
				}
			}
		}
	}

	public Dispatch createTable(final int numRows, final int numCols,
			final String style) {

		int index = 1;
		while (true) {
			try {
				Dispatch tables = Dispatch.get(doc, "Tables").toDispatch();
				Dispatch range = Dispatch.get(selection, "Range").toDispatch();
				Dispatch newTable = Dispatch.call(tables, "Add", range,
						new Variant(numRows), new Variant(numCols))
						.toDispatch();

				Dispatch.call(selection, "MoveRight");
				Dispatch.put(newTable, "Style", style);
				/*Object item = Dispatch.call(newTable, "Item", String.valueOf(0)).toDispatch();
				Object Columns = Dispatch.call(item, "Columns", String.valueOf(0)).toDispatch();
				 Dispatch.put(Columns, "PreferredWidth", String.valueOf(500));*/
				//Dispatch.call(newTable, "AutoFormat", new Variant(autoFormat));
				return newTable;
			} catch (ComFailException e) {
				if (index++ >= MAX_RETRY) {
					throw e;
				} else {
					continue;
				}
			}
		}
	}

	/**
	 * ��ָ���ı�ͷ����д����.
	 *
	 * @param table
	 *            ���
	 * @param cellColIdx
	 *            �к�
	 * @param txt
	 *            ����
	 * @param style
	 *            ��ʽ
	 */
	public void putTableHeader(final Dispatch table, final int cellColIdx,
			final String txt, final String style) {

		Dispatch cell = Dispatch.call(table, "Cell", new Variant(1),
				new Variant(cellColIdx)).toDispatch();

		Dispatch.call(cell, "Select");
		Dispatch.put(selection, "Text", txt);
		Dispatch.put(this.selection, "Style", getOutlineStyle(style));
	}

	/**
	 * ��ָ���ĵ�Ԫ������д����.
	 *
	 * @param table
	 *            ���
	 * @param cellRowIdx
	 *            �к�
	 * @param cellColIdx
	 *            �к�
	 * @param txt
	 *            ����
	 * @param style
	 *            ��ʽ
	 */
	public void putTableCell(final Dispatch table, final int cellRowIdx,
			final int cellColIdx, final String txt, final String style) {

		Dispatch cell = Dispatch.call(table, "Cell", new Variant(cellRowIdx),
				new Variant(cellColIdx)).toDispatch();

		Dispatch.call(cell, "Select");
		Dispatch.put(selection, "Text", txt);
		Dispatch.put(this.selection, "Style", getOutlineStyle(style));
	}

	/**
	 * �رյ�ǰword�ĵ�.
	 */
	private void closeDocument() {
		if (doc != null) {
			Dispatch.call(doc, "Save");
			Dispatch.call(doc, "Close", new Variant(saveOnExit));
			doc = null;
		}
	}

	/**
	 * �ļ���������Ϊ.
	 *
	 * @param savePath
	 *            ��������Ϊ·��
	 */
	public void saveFileAs(final String savePath) {

		Dispatch.call(doc, "SaveAs", savePath);
	}

	/**
	 * �ر��ĵ�.
	 */
	public void close() {
		closeDocument();
		if (wordApp != null) {
			Dispatch.call(wordApp, "Quit");
			wordApp = null;
		}
		selection = null;
	}
}
