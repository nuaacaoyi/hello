
  Evaluation Warning : The document was created with Spire.PDF for Java.
                                                                                                      Maven 的配置  
                                                                                                            



                                                     Maven 的配置                          

                                                      尚硅谷 Java 研究院 
                                                       www.atguigu.com 

             一、安装 Maven 核心程序 



                   【1】确认当前系统正确的配置了 JAVA_HOME 环境变量 

                         C:\Users\weiyh>echo %JAVA_HOME% 

                         C:\Java\jdk1.7.0_75 

                   【2】将Maven核心程序的压缩包解压到一个非中文无空格目录下 

                         D:\DevInstall\apache-maven-3.2.2 

                   【3】配置 MAVEN_HOME 或 M2_HOME环境变量（※配置到主目录） 

              

              

              

              

              



                   【4】配置 path环境变量（※配置到bin 目录;变量值直接配置完整路径也可以：例如：

                   D:\DevInstall\apache-maven-3.2.2\bin） 

              



                更多 Java –大数据 –前端 –python人工智能 –区块链资料下载，可访问百度：尚硅谷官网 


  Evaluation Warning : The document was created with Spire.PDF for Java.
                                                                                                      Maven 的配置  
                                                                                                            



                    



                    



                   【5】验证是否正确： mvn -v 

                         C:\Users\zhangyu>mvn -v 

                         Apache Maven 3.2.2 (45f7c06d68e745d05611f7fd14efb6594181933e; 

                         2014-06-17T21:51:42+08:00) 

                         Maven home: D:\DevInstall\apache-maven-3.2.2 

                         Java version: 1.7.0_75, vendor: Oracle Corporation 

                         Java home: C:\Java\jdk1.7.0_75\jre 

                         Default locale: zh_CN, platform encoding: GBK 

                         OS name: "windows 8.1", version: "6.3", arch: "amd64", family: "windows" 



             二、设置本地库 



                   Maven的核心程序并不包含具体功能，仅负责宏观调度。具体功能由插件来完成。 

                   Maven核心程序会到本地仓库中查找插件。 

                   如果本地仓库中没有就会从远程中央仓库下载。 

                   此时如果不能上网则无法执行Maven的具体功能。 

                更多 Java –大数据 –前端 –python人工智能 –区块链资料下载，可访问百度：尚硅谷官网 


  Evaluation Warning : The document was created with Spire.PDF for Java.
                                                                                                      Maven 的配置  



                   为了解决这个问题，我们可以将Maven的本地仓库指向一个在联网情况下下载好的目录。 



                   【1】默认的本地仓库: ~/.m2/repository 

                          ~表示：当前系统的家目录 

                         C:\Users\shkstart\.m2\repository 

                   【2】修改默认的本地仓库，需要先查到settings.xml 

                         D:\DevInstall\apache-maven-3.2.2\conf\settings.xml 

                   【9】在settings.xml 文件中设置 

                         <localRepository>本地仓库的实际路径</localRepository> 



                         <localRepository>D:\RepMaven</localRepository> 



               

              



                更多 Java –大数据 –前端 –python人工智能 –区块链资料下载，可访问百度：尚硅谷官网 

