package com.caoyi.java.test;

import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author caoyi
 * @create 2020-10-30-9:42
 */
public class Pdf2Txt {
  public static void main(String[] args) {
    String path = "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test";
    getFile(path,0);


  }

  /*
   * 使用spire读取pdf文本内容
   * */
  private static String readPdfText(String path){
    //创建PdfDocument实例
    PdfDocument doc= new PdfDocument();

    //加载PDF文件
    //eg  "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\test.pdf"
    doc.loadFromFile(path);

    StringBuilder sb= new StringBuilder();

    PdfPageBase page;

    //遍历PDF页面，获取文本
    for(int i=0;i<doc.getPages().getCount();i++){
      page=doc.getPages().get(i);
      sb.append(page.extractText(true));
    }

    FileWriter writer;

    try {
      //将文本写入文本文件
      //eg  "D:\\javawork\\hello\\src\\com\\caoyi\\java\\test\\ExtractText.txt"
      writer = new FileWriter(path.substring(0,path.length()-3) + ".txt");
      writer.write(sb.toString().replace("Evaluation Warning : The document was created with Spire.PDF for Java."
            ," "));
      writer.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }

    doc.close();
    return sb.toString();
  }
  /*
   * 函数名：getFile
   * 作用：使用递归，输出指定文件夹内的所有文件
   * 参数：path：文件夹路径   deep：表示文件的层次深度，控制前置空格的个数
   * 前置空格缩进，显示文件层次结构
   */
  private static void getFile(String path,int deep){
    // 获得指定文件对象
    File file = new File(path);
    // 获得该文件夹内的所有文件
    File[] array = file.listFiles();

    for(int i=0;i<array.length;i++)
    {
      if(array[i].isFile())//如果是文件
      {
        String pdfName = array[i].getName();
        int dot = pdfName.lastIndexOf('.');
        String dataId =  dot>-1?pdfName.substring(0, dot):pdfName;
        String dataType =  dot>-1?pdfName.substring(dot+1):pdfName;


        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");
        // 只输出文件名字

        if("pdf".equals(dataType) ||  "PDF".equals(dataType)){
          System.out.println(readPdfText(array[i].getPath()));
          //updateFullText(newDir,readPdfText(array[i].getPath()));
        }
        // 输出当前文件的完整路径
        // System.out.println("#####" + array[i]);
        // 同样输出当前文件的完整路径   大家可以去掉注释 测试一下
        // System.out.println(array[i].getPath());
      }
      /*else if(array[i].isDirectory())//如果是文件夹
      {
        for (int j = 0; j < deep; j++)//输出前置空格
          System.out.print(" ");

        System.out.println( array[i].getName());
        //System.out.println(array[i].getPath());
        //文件夹需要调用递归 ，深度+1
        getFile(array[i].getPath(),deep+1);
      }*/
    }
  }

}
