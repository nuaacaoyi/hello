package com.wkcto.thread.threadgroup;

// 创建线程组
public class Test01 {

  public static void main(String[] args) {
    // 返回当前main线程的线程组
    ThreadGroup mainGroup = Thread.currentThread().getThreadGroup();
    System.out.println("main线程所在线程组： " + mainGroup);

    // 定义线程组，如果不指定所属的线程组，则自动归属到当前线程所属的线程组，即main线程所在的组
    ThreadGroup group1 = new ThreadGroup("group1");
    System.out.println(group1);

    // 定义线程组，同时指定父线程组
    ThreadGroup group2 = new ThreadGroup(mainGroup, "group2");
    // 现在group1和group2都时maingroup线程组中的子线程组
    System.out.println(group1.getParent() == mainGroup);
    System.out.println(group2.getParent() == mainGroup);

    // 创建线程时，指定所属的线程组
    Runnable r = new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread());
      }
    };
    // 若创建线程时没有指定线程组，则默认归属到父线程所在的组
    Thread t1 = new Thread(r, "t1");
    System.out.println(t1); // Thread[t1,5,main]
    // 创建时，指定线程所属的线程组
    Thread t2 = new Thread(group1, r , "t2");
    Thread t3 = new Thread(group2, r , "t3");
    System.out.println(t2); //Thread[t2,5,group1]
    System.out.println(t3); //Thread[t3,5,group2]
  }
}
