package com.wkcto.thread.threadgroup;

// 线程组的复制操作
public class Test03 {
  public static void main(String[] args) {
    ThreadGroup mainGroup = Thread.currentThread().getThreadGroup();
    // 在main线程组中定义了两个子线程组
    ThreadGroup group1 = new ThreadGroup("group1");// 默认group1的父线程组就是mainGroup
    ThreadGroup group2 = new ThreadGroup(mainGroup, "group2");// 同上

    Runnable r = new Runnable() {
      @Override
      public void run() {
        while(true){
          System.out.println("---- " + Thread.currentThread());
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    };

    Thread t1 = new Thread(r, "t1"); // 在mainGroup中创建线程
    Thread t2 = new Thread(group1, r, "t2"); // 在group1线程组中创建线程
    Thread t3 = new Thread(group2, r, "t3");// 在group2线程组中创建线程

    t1.start();
    t2.start();
    t3.start();
    // 把main线程组中的线程复制到一个数组中
    Thread[] threadList = new Thread[mainGroup.activeCount()];// 定义一个存储线程的数组
    // 把mainGroup中所有线程及其子线程组中的线程放入list
    /*mainGroup.enumerate(threadList);
    for (Thread thread : threadList) {
      System.out.println(thread);
    }*/
    System.out.println("==========================================");
    // 不复制子线程组中的线程，仅复制mainGroup下的线程
    mainGroup.enumerate(threadList, false);
    for (Thread thread : threadList) {
      System.out.println(thread);
    }

    // 把main线程组中的子线程组复制到数组中
    ThreadGroup[] threadGroups = new ThreadGroup[mainGroup.activeGroupCount()]; // 定义一个数组
    // 把main线程组中的所有子线程组复制到数组中
    mainGroup.enumerate(threadGroups);
    for (ThreadGroup threadGroup : threadGroups) {
      System.out.println(threadGroup);
      System.out.println(threadGroup.activeCount());// 打印发现该组下还是有线程的，说明是浅拷贝
    }
  }
}
