package com.wkcto.thread.threadgroup;

/**
 * @Author caoyi
 * @Create 2022-10-25-15:34
 * @Description: TODO
 */
public class Test04 {
  public static void main(String[] args) throws InterruptedException {
    Runnable r = new Runnable() {
      @Override
      public void run() {
        System.out.println("begin-- ： " + Thread.currentThread() + " -- 开始循环");
        // 当线程未被中断，就一致循环
        while (!Thread.currentThread().isInterrupted()){
          System.out.println(Thread.currentThread().getName() + "----- 一直");
          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            // 如果中断睡眠中的线程，会产生中断异常，同时会清除中断标志
            // 此时使用 Thread.currentThread().isInterrupted() 进行判断就不合理了
            e.printStackTrace();
          }
        }
        System.out.println("end-- ： " +Thread.currentThread().getName() + " 循环结束 ");
      }
    };

    ThreadGroup group = new ThreadGroup("group");
    // 在group中创建五个线程
    for (int i = 0; i < 5; i++) {
      new Thread(group, r, "t"+i).start();
    }
    // main线程睡眠2秒
    Thread.sleep(2000);
    // 中断group组中所有的线程
    group.interrupt();
  }
}
