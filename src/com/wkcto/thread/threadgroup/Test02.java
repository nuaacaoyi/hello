package com.wkcto.thread.threadgroup;

// 线程组的基本操作
public class Test02 {
  public static void main(String[] args) {
    ThreadGroup mainGroup = Thread.currentThread().getThreadGroup();
    ThreadGroup group = new ThreadGroup("group");

    Runnable r = new Runnable() {
      @Override
      public void run() {
        while(true){
          System.out.println("-------当前线程： " + Thread.currentThread());
          try{
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    };

    Thread t1 = new Thread(r, "t1");
    Thread t2 = new Thread(group, r, "t2");

    t1.start();
    t2.start();

    // 打印线程组的相关属性
    // 输出结果： 4, main线程组中活动线程： main  t1 t2 垃圾回收器（守护线程）
    System.out.println("main 线程组中的活动线程数量： " + mainGroup.activeCount());
    System.out.println("group子线程组中的活动线程数量： " + group.activeCount()); // 1  t2
    System.out.println("main线程组中子线程组的数量："+  mainGroup.activeGroupCount()); // 1  group
    System.out.println("main线程组的父线程组："+ mainGroup.getParent());// ava.lang.ThreadGroup[name=system,maxpri=10]
    System.out.println("group线程组的父线程组："+ group.getParent());
    System.out.println("main线程组是否为自身的父线程组" +mainGroup.parentOf(mainGroup));
    System.out.println("main线程组是否为group子线程组的父线程组"+mainGroup.parentOf(group));
    mainGroup.list();
  }
}
