package com.wkcto.thread.threadmethod;

/**
 * @Author caoyi
 * @Create 2022-10-12-16:30
 * @Description: TODO
 */
public class Test05Interrupt extends Thread {
  @Override
  public void run() {
    super.run();

    for (int i = 0; i < 10000; i++) {
      if(this.isInterrupted()){
        System.out.println("==当前线程已被中断==");
        break;
      }
      System.out.println("sub thread ==> " + i);
    }
  }
}
