package com.wkcto.thread.threadmethod;

/**
 * @Author caoyi
 * @Create 2022-10-12-15:10
 * @Description: TODO
 */
public class SubThread2 extends Thread {
  public SubThread2() {
    System.out.println("构造方法中： Thread.currentThread().getName()" + Thread.currentThread().getName());
    System.out.println("构造方法中： this.getName()" + this.getName());
  }

  @Override
  public void run() {
    System.out.println("run方法中： Thread.currentThread().getName()" + Thread.currentThread().getName());
    System.out.println("run方法中： this.getName()" + this.getName());
  }

  public static void main(String[] args) throws InterruptedException {
    SubThread2 t2 = new SubThread2();
    t2.setName("t2");
    t2.start();

    Thread.sleep(500); // main线程睡眠500ms，保证t2已经启动

    Thread t3 = new Thread(t2);
    t3.setName("t3");
    t3.start();

  }
}
