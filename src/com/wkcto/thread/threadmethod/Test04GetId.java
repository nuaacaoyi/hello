package com.wkcto.thread.threadmethod;

import com.caoyi.java.test.reflection.Test04;
import com.dujubin.java.HotelManagement.Test;

/**
 * @Author caoyi
 * @Create 2022-10-12-16:03
 * @Description: TODO
 */
public class Test04GetId extends Thread {
  @Override
  public void run() {
    System.out.println("执行代码的线程名称： "+ Thread.currentThread().getName()
     + "， 当前线程id："+ this.getId());
  }

  public static void main(String[] args) {
    System.out.println("执行main的线程：" + Thread.currentThread().getName()
    + "， 执行代码的线程id： "+ Thread.currentThread().getId());

    for (int i = 0; i < 6; i++) {
      new Test04GetId().start();
    }
  }
}
