package com.wkcto.thread.threadmethod;

/**
 * @Author caoyi
 * @Create 2022-10-12-15:03
 * @Description: TODO
 */
public class SubThread1 extends Thread{
  public SubThread1() {
    System.out.println("构造方法中打印当前线程名称：" + Thread.currentThread().getName());
  }

  @Override
  public void run() {
    System.out.println("run方法中打印当前线程名称： " + Thread.currentThread().getName());
  }
}
