package com.wkcto.thread.threadmethod;

/**
 * @Author caoyi
 * @Create 2022-10-12-10:57
 * @Description: TODO
 */
public class Test01CurrentThread {
  public static void main(String[] args) {
    System.out.println("main方法中有没有线程呢："+ Thread.currentThread().getName());

    SubThread1 subThread1 = new SubThread1();
    subThread1.start(); // 启动另一个线程

    subThread1.run(); // main方法中直接调用run方法，没有开启新的线程
  }
}
