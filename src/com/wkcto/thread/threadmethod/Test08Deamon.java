package com.wkcto.thread.threadmethod;


/**
 * @Author caoyi
 * @Create 2022-10-12-16:41
 * @Description: TODO
 */
public class Test08Deamon extends Thread {
  @Override
  public void run() {
    super.run();
    while(true){
      System.out.println("sub thread. . . . ");
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    Test08Deamon th = new Test08Deamon();
    th.setDaemon(true);// 设置线程为守护线程
    th.start();

    // 当前线程为main
    for (int i = 0; i < 10; i++) {
      System.out.println("main线程： "+ i );
    }
  }
}
