package com.wkcto.thread.threadmethod;


/**
 * @Author caoyi
 * @Create 2022-10-12-15:27
 * @Description: TODO
 */
public class SubThread3 extends Thread {

  @Override
  public void run() {

    try {
      System.out.println("run， threadname = " + Thread.currentThread().getName()
          + ", begin = " + System.currentTimeMillis());
      Thread.sleep(2000);
      System.out.println("run， threadname = " + Thread.currentThread().getName()
          + ", end = " + System.currentTimeMillis());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  public static void main(String[] args) {
    SubThread3 t3= new SubThread3();
    System.out.println("main, begin = " + System.currentTimeMillis());
    t3.start();
    //t3.run();
    System.out.println("main, end = " + System.currentTimeMillis());
  }
}
