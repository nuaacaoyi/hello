package com.wkcto.thread.intrinsiclock;

/**
 * @Author caoyi
 * @Create 2022-10-14-10:59
 * @Description: TODO
 */
public class Test02Const {

  public static void main(String[] args) {
    // 创建两个线程，分别调用mm方法
    Test02Const t01= new Test02Const();
    Test02Const t02= new Test02Const();
    new Thread(new Runnable() {
      @Override
      public void run() {
        t01.mm(); // 使用的锁对象 OBJ
      }
    }).start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        t02.mm();
      }
    }).start();

  }

  public static final Object OBJ = new Object(); // 定义一个常量作为锁对象

  public void mm(){
    synchronized (OBJ) { // 使用常量对象作为锁对象
      for (int i = 0; i < 101; i++) {
        System.out.println(Thread.currentThread().getName() + " --> " +i);
      }
    }
  }
}
