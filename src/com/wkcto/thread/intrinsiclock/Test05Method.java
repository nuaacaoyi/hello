package com.wkcto.thread.intrinsiclock;

/**
 * @Author caoyi
 * @Create 2022-10-14-10:59
 * @Description: TODO
 */
// 同步实例方法，把整个方法体作为同步代码块，默认锁对象就是this
public class Test05Method {

  public static void main(String[] args) {
    // 创建两个线程，分别调用mm方法
    Test05Method t01= new Test05Method();
    new Thread(new Runnable() {
      @Override
      public void run() {
        t01.mm(); // 使用的锁对象this就是 t01对象
      }
    }).start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        t01.m2();
      }
    }).start();

  }

  public void mm(){
    synchronized (this) { // 经常使用this当前对象作为锁对象
      for (int i = 0; i < 101; i++) {
        System.out.println(Thread.currentThread().getName() + " --> " +i);
      }
    }
  }

  // 使用synchronized修饰实例方法，同步实例方法，默认锁对象就是this
  public synchronized void m2(){
    for (int i = 0; i < 101; i++) {
      System.out.println(Thread.currentThread().getName() + " --> " +i);
    }
  }
}
