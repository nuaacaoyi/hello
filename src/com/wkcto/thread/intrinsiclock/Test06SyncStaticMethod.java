package com.wkcto.thread.intrinsiclock;

/**
 * @Author caoyi
 * @Create 2022-10-14-10:59
 * @Description: TODO
 */
// 同步静态方法，把整个方法体作为同步代码块，默认锁对象是当前类的运行时类对象 即 Test06SyncStaticMethod.class;
public class Test06SyncStaticMethod {

  public static void main(String[] args) {
    // 创建两个线程，分别调用mm方法
    Test06SyncStaticMethod t01= new Test06SyncStaticMethod();
    new Thread(new Runnable() {
      @Override
      public void run() {
        t01.mm(); // 使用的锁对象this就是 t01对象
      }
    }).start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        Test06SyncStaticMethod.m2();
      }
    }).start();

  }

  public void mm(){
    synchronized (Test06SyncStaticMethod.class) { // 经常使用this当前对象作为锁对象
      for (int i = 0; i < 101; i++) {
        System.out.println(Thread.currentThread().getName() + " --> " +i);
      }
    }
  }

  // 使用synchronized修饰静态方法，同步静态方法，默认锁对象就是当前类的运行时类对象
  public synchronized static void m2(){
    for (int i = 0; i < 101; i++) {
      System.out.println(Thread.currentThread().getName() + " --> " +i);
    }
  }
}
