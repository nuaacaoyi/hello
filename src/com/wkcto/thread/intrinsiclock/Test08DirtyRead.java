package com.wkcto.thread.intrinsiclock;

/**
 * @Author caoyi
 * @Create 2022-10-14-12:45
 * @Description: TODO
 */
public class Test08DirtyRead {
  public static void main(String[] args) throws InterruptedException {
    // 开启子线程，设置用户名和密码
    Publicvalue publicvalue = new Publicvalue();
    SubThread t1 = new SubThread(publicvalue);
    t1.start();
    // 为了确定设置成功，我们等待一会儿
    Thread.sleep(100);
    // 在main线程中读取用户名和密码
    publicvalue.getValue();

  }
  // 定义线程的功能，设置用户名和密码
  static class SubThread extends Thread{
    private Publicvalue publicvalue;
    public SubThread(Publicvalue publicvalue){
      this.publicvalue = publicvalue;
    }

    @Override
    public void run() {
      publicvalue.setValue("bjpowernode", "123");
    }
  }

  static class Publicvalue{
    private String name = "wkcto";
    private String pwd = "666";

    public void getValue(){
      System.out.println(Thread.currentThread().getName() + ", getter -- name: " + name
      + ", --pwd: "+ pwd);
    }

    public void setValue(String name, String pwd){
      this.name  = name;
      try {
        Thread.sleep(1000);// 模拟需要一定时间
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      this.pwd = pwd;
      System.out.println(Thread.currentThread().getName() + ", setter --name: "+ name
      + ", --pwd: "+ pwd);
    }
  }
}
