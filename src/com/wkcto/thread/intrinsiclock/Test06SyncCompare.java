package com.wkcto.thread.intrinsiclock;

import org.apache.poi.ss.formula.functions.T;

import javax.sound.midi.Soundbank;

/**
 * @Author caoyi
 * @Create 2022-10-14-11:40
 * @Description: TODO
 */
// 对比同步方法和同步代码块的执行效率
public class Test06SyncCompare {
  public static void main(String[] args) {
    Test06SyncCompare obj = new Test06SyncCompare();

    new Thread(new Runnable() {
      @Override
      public void run() {
        obj.doLongTimeTask();
      }
    }).start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        obj.doLongTimeTask();
      }
    }).start();
  }

  public synchronized void doLongTimeTask(){
    try {
      System.out.println("....任务开始...");
      Thread.sleep(3000); // 模拟执行任务需要3秒钟

      System.out.println("开始同步");
      for (int i = 0; i < 100; i++) {
        System.out.println(Thread.currentThread().getName() + " --> "+ i);
      }

      System.out.println("....任务结束...");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void doLongTimeTask2(){
    try {
      System.out.println("....任务开始...");
      Thread.sleep(3000); // 模拟执行任务需要3秒钟
      synchronized (this){
        System.out.println("开始同步");
        for (int i = 0; i < 100; i++) {
          System.out.println(Thread.currentThread().getName() + " --> "+ i);
        }
      }
      System.out.println("....任务结束...");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
