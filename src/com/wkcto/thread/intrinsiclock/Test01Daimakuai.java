package com.wkcto.thread.intrinsiclock;

import com.guoxin.java.SAXTest.SAXTest;

/**
 * @Author caoyi
 * @Create 2022-10-14-10:59
 * @Description: TODO
 */
public class Test01Daimakuai {

  public static void main(String[] args) {
    // 创建两个线程，分别调用mm方法
    Test01Daimakuai t01= new Test01Daimakuai();
    new Thread(new Runnable() {
      @Override
      public void run() {
        t01.mm(); // 使用的锁对象this就是 t01对象
      }
    }).start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        t01.mm();
      }
    }).start();

  }

  public void mm(){
    synchronized (this) { // 经常使用this当前对象作为锁对象
      for (int i = 0; i < 101; i++) {
        System.out.println(Thread.currentThread().getName() + " --> " +i);
      }
    }
  }
}
