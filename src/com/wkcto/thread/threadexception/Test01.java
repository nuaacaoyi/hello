package com.wkcto.thread.threadexception;

// 线程的unCaughtExceptionHandler
public class Test01 {
  public static void main(String[] args) {
    // 设置线程的全局回调接口
    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
      @Override
      public void uncaughtException(Thread t, Throwable e) {
        System.out.println("-----进入全局异常处理器！！------");
        // t参数接收发生异常的线程， e参数就是该线程发生的异常
        System.out.println(t.getName() + " 线程产生异常： " + e.getMessage());
      }
    });

    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getName() + "开始运行");
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          // 线程中的受检异常必须捕获处理
          e.printStackTrace();
        }
        System.out.println(12/0); // 会产生算术异常
      }
    });
    t1.start();

    new Thread(new Runnable() {
      @Override
      public void run() {
        String txt = null;
        System.out.println(txt.length()); // 会产生空指针异常
      }
    }).start();
  }
}
