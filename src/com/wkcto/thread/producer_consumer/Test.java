package com.wkcto.thread.producer_consumer;

import java.util.function.Consumer;

/**
 * @Author caoyi
 * @Create 2022-10-19-15:05
 * @Description: TODO
 */
public class Test {
  public static void main(String[] args) {
    ValueOP valueOP = new ValueOP();

    ProducerThread p = new ProducerThread(valueOP);
    ConsumerThread c = new ConsumerThread(valueOP);

    p.start();
    c.start();
  }
}
