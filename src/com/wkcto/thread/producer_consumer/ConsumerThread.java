package com.wkcto.thread.producer_consumer;

/**
 * @Author caoyi
 * @Create 2022-10-19-15:14
 * @Description: TODO
 */
public class ConsumerThread extends Thread {
  private ValueOP obj;

  public ConsumerThread(ValueOP obj) {
    this.obj = obj;
  }

  @Override
  public void run() {
    while (true) {
      obj.getValue();
    }
  }
}
