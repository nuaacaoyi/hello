package com.wkcto.thread.producer_consumer;

//定义一个线程类，模拟生产者
public class ProducerThread extends Thread {
  // 生产者生产数据，就是调用ValueOP的setValue方法给value赋值
  private ValueOP obj;

  public ProducerThread(ValueOP obj) {
    this.obj = obj;
  }

  @Override
  public void run() {
    while (true){
      obj.setValue();
    }
  }
}
