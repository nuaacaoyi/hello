package com.wkcto.thread.producer_consumer;


// 定义一个操作数据的类
public class ValueOP {
  private String value = "";

  public void getValue() {
    synchronized (this){
      //获得字段时，如果value是空串就等待
      while(value.equalsIgnoreCase("")){
        //
        try {
          this.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      // 如果不是空串就读取value的值
      System.out.println("get的值是： ： " + value);
      this.value = "";
      this.notify();
    }
  }

  public void setValue() {
    synchronized (this){
      // 如果value不是空串，就等待
      while(!value.equalsIgnoreCase("")){
        try {
          this.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      // 如果是空串，就设置value的值
      this.value = System.currentTimeMillis() + " - " + System.nanoTime();
      System.out.println("set设置的值是： " + value);
      this.notify();
    }
  }
}
