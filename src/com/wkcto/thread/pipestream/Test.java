package com.wkcto.thread.pipestream;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

// 使用PipedInputStream和PipedOutputStream，管道字节流在线程之间传输数据
public class Test {
  public static void main(String[] args) throws IOException {
    // 定义管道字节流
    PipedInputStream inputStream = new PipedInputStream();
    PipedOutputStream outputStream = new PipedOutputStream();

    // 在输入管道流和输出管道流之间建立一个链接
    inputStream.connect(outputStream);

    // 创建一个线程向管道流中写入数据
    new Thread(new Runnable() {
      @Override
      public void run() {
        writeData(outputStream);
      }
    }).start();

    // 创建一个线程读取数据
    new Thread(new Runnable() {
      @Override
      public void run() {
        readData(inputStream);
      }
    }).start();

  }

  // 向管道流中写数据
  public static void writeData(PipedOutputStream out){
    // 分别把0-100之间的数写入管道
    try {
      for (int i = 0; i < 15; i++) {
        String data = "" + i;
        out.write(data.getBytes()); // 把一个字节数组写入到输出管道流
      }
      out.close();// 关闭管道流
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // 从管道流中读取数据
  public static void readData(PipedInputStream in){
    byte[] bytes = new byte[1024]; // 一般长度为 1024*8
    try {
      int len = in.read(bytes); // 从管道输入字节流中读取字节保存到字节数组中
      while(len != -1){
        // 把byte数组中从0开始读到的len个字节转换为字符串打印
        System.out.println(new String(bytes, 0, len));
        System.out.println("--");// 第一次循环就把所有的数据读完了。。 这样读会导致数字读取混乱，字符串无法转为int数组了
        len = in.read(bytes); // 打印完之后继续读
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}
