package com.wkcto.thread.volatilewk;

import javax.sound.midi.Soundbank;

/**
 * @Author caoyi
 * @Create 2022-10-14-14:15
 * @Description: TODO
 */
public class Test01 {
  public static void main(String[] args) {
    PrintString printString = new PrintString();
    printString.pringtStringMethod();

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("在main线程中修改打印标志====");
    printString.setContinuePrint(false);
    // 修改完打印标志后，运行程序查看程序的运行结果
    // 程序根本不会停止，程序无法执行到 set方法
  }

  // 定义一个类，打印字符串
  static class PrintString{
    private boolean continuePrint = true;

    public void setContinuePrint(boolean continuePrint) {
      this.continuePrint = continuePrint;
    }

    public void pringtStringMethod(){
      while (continuePrint){
        System.out.println(Thread.currentThread().getName());
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
