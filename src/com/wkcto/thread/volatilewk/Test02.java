package com.wkcto.thread.volatilewk;

/**
 * @Author caoyi
 * @Create 2022-10-14-14:15
 * @Description: TODO
 */
public class Test02 {
  public static void main(String[] args) {
    PrintString printString = new PrintString();
    // 开启子线程执行打印方法
    new Thread(new Runnable() {
      @Override
      public void run() {
        printString.pringtStringMethod();
      }
    }).start();

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("在main线程中修改打印标志====");
    printString.setContinuePrint(false);
    // 修改完打印标志后，运行程序查看程序的运行结果
    // 修改变量后，子线程仍未结束，程序出现死循环
    // main修改了printString对象的打印标志后，子线程读不到
    // 解决方法：使用volatile关键字修饰printString对象的打印标志
    // volatile的作用可以强制线程从公共内存中读取变量的值，而不是从工作内存中读取
  }

  // 定义一个类，打印字符串
  static class PrintString{
    private volatile boolean continuePrint = true;

    public void setContinuePrint(boolean continuePrint) {
      this.continuePrint = continuePrint;
    }

    public void pringtStringMethod(){
      System.out.println(Thread.currentThread().getName() + "开始...");
      while (continuePrint){

      }
      System.out.println(Thread.currentThread().getName() + "结束++++");
    }
  }
}
