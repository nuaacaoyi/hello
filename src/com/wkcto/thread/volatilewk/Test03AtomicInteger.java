package com.wkcto.thread.volatilewk;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author caoyi
 * @Create 2022-10-14-14:43
 * @Description: TODO
 */
public class Test03AtomicInteger {
  public static void main(String[] args) throws InterruptedException {
    // 创建100个子线程
    for (int i = 0; i < 100; i++) {
      new MyThread().start();
    }

    Thread.sleep(1000);
    System.out.println(MyThread.count.get());
  }

  static class MyThread extends Thread{
    private static AtomicInteger count = new AtomicInteger();

    public static void addCount(){
      for (int i = 0; i < 1000; i++) {
        count.getAndIncrement();
      }
      System.out.println(Thread.currentThread().getName() + " count = " + count.get());
    }

    @Override
    public void run() {
      addCount();
    }
  }
}
