package com.wkcto.thread.volatilewk;

import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;

/**
 * @Author caoyi
 * @Create 2022-10-14-14:43
 * @Description: TODO
 */
public class Test03NonAtomic {
  public static void main(String[] args) {
    // 创建10个子线程
    for (int i = 0; i < 100; i++) {
      new MyThread().start();
    }
  }

  static class MyThread extends Thread{
    volatile public static int count;

    public static void addCount(){
      for (int i = 0; i < 1000; i++) {
        count++;
      }
      System.out.println(Thread.currentThread().getName() + " count = " + count);
    }

    @Override
    public void run() {
      addCount();
    }
  }
}
