package com.wkcto.thread.createthread.p1;

/**
 * 1 定义一个类继承Thread
 * 2 重写父类的run方法
 * 3 run方法体中的代码就是子线程要执行的任务
 * 4 在主线程中，创建子线程对象
 * 5 调用start方法，启动线程，
 *    启动线程的本质就是请求JVM运行相应的线程，这个线程具体在什么时候运行，由线程调度器Scheduler决定
 *    注意：
 *      1）start()方法调用结束并不意味着子线程开始运行，只是通知线程调度器可以执行了
 *      2）新开启的线程会执行run方法
 *      3）如果开启了多个线程，start()调用的顺序并不一定是线程启动的顺序
 */
public class MyThread extends Thread{
  @Override
  public void run() {
    System.out.println("这是子线程要执行的内容");
  }

  public static void main(String[] args) {
    System.out.println("JVM启动main线程，main线程执行main方法");

    MyThread thread = new MyThread();
    thread.start();

    System.out.println("这是main线程后面的 其他代码");
  }
}
