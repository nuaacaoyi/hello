package com.wkcto.thread.createthread.p1;

/**
 * @Author caoyi
 * @Create 2022-10-11-17:09
 * @Description: TODO
 */
public class Test {
  public static void main(String[] args) {

    MyThread2 thread2 = new MyThread2();
    thread2.start(); // 开启子线程

    try {
      for (int i = 0; i < 11; i++) {
        System.out.println("main thread: " + i);
        int time = (int) (1000);
        Thread.sleep(time);
      }
    }catch (InterruptedException e){
      e.printStackTrace();
    }
  }
}
