package com.wkcto.thread.createthread.p1;

/**
 * @Author caoyi
 * @Create 2022-10-11-17:19
 * @Description: TODO
 */
public class MyThread2 extends Thread {

  @Override
  public void run() {

    try {
      for (int i = 0; i < 11; i++) {
        System.out.println("sub thread: " + i);
        int time = (int) (Math.random() * 1000);
        Thread.sleep(time);
      }
    }catch (InterruptedException e){
      e.printStackTrace();
    }
  }

}
