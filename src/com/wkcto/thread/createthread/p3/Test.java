package com.wkcto.thread.createthread.p3;

/**
 * @Author caoyi
 * @Create 2022-10-12-10:45
 * @Description: TODO
 */
public class Test {
  public static void main(String[] args) {
    MyRunnable rn = new MyRunnable();
    Thread thread = new Thread(rn);



    thread.start();

    for (int i = 0; i < 100; i++) {
      System.out.println("main内打印："+i);
    }


    Thread thread1 = new Thread(new Runnable() {
      @Override
      public void run() {
        for (int i = 0; i < 100; i++) {
          System.out.println("sub === > " + i);
        }
      }
    });
    thread1.start();
  }
}
