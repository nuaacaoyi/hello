package com.wkcto.thread.createthread.p3;

/**
 * 1 定义类实现Runnable接口
 * 2 重写run方法
 * 3 主线程中创建该实现类对象
 * 4 主线程中创建Thread对象
 * 5 启动 Thread thread = new Thread(rn);
 */

public class MyRunnable implements Runnable{
  @Override
  public void run() {
    for (int i = 0; i < 100; i++) {
      System.out.println("线程内打印："+i);
    }
  }
}
