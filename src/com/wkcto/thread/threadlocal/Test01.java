package com.wkcto.thread.threadlocal;

//threadlocal的基本使用
public class Test01 {
  // 定义一个ThreadLocal对象
  static ThreadLocal threadLocal = new ThreadLocal();
  //定义线程类
  static class Subthread extends Thread{
    @Override
    public void run() {
      for (int i = 0; i < 20; i++) {
        // 设置线程关联的值
        threadLocal.set(Thread.currentThread().getName() + " - " + i);
        // 调用get方法读取关联的值
        System.out.println(Thread.currentThread().getName() + " value = " + threadLocal.get());
      }
    }
  }

  public static void main(String[] args) {
    Subthread t1 = new Subthread();
    Subthread t2 = new Subthread();

    t1.start();
    t2.start();
  }
}
