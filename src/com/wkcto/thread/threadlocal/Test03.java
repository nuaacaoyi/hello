package com.wkcto.thread.threadlocal;

import java.util.Date;
import java.util.Random;

// ThreadLocal初始值的问题
public class Test03 {
  // 设置threadlocal的初始值
  //1) 定义ThreadLocal的子类
  static class SubThreadLocal extends ThreadLocal<Date>{
    //2) 重写intialValue方法
    @Override
    protected Date initialValue() {
      return new Date(); // 把当前日期设置为初始值
    }
  }
  // 3) 使用自定义的ThreadLocal对象作为内部对象
  // 定义ThreadLocal对象
  static ThreadLocal threadLocal = new SubThreadLocal();

  // 定义线程类
  static class SubThread3 extends Thread{
    @Override
    public void run() {
      for (int i = 0; i < 10; i++) {
        // 第一次调用threadlocal的get()方法会返回null
        System.out.println("--------"+ Thread.currentThread().getName() + " value = " + threadLocal.get());
        // 如果没有初始值，就设置一个值
        if(threadLocal.get() == null){
          threadLocal.set(new Date());
        }
        try {
          Thread.sleep(new Random().nextInt(500));
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static void main(String[] args) {
    SubThread3 t1 = new SubThread3();
    t1.start();
    SubThread3 t2 = new SubThread3();
    t2.start();
  }
}
