package com.wkcto.thread.threadlocal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

// 在多线程的环境中，把字符串转换为日期对象
// 为每个线程指定自己的SimpleDateFormat对象
public class Test02 {

  //定义SimpleDataFormat对象，该对象可以把字符串转换为日期
  //private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

  static ThreadLocal<SimpleDateFormat> threadLocal = new ThreadLocal<>();

  static class ParseDate implements Runnable{
    private int i = 0;

    public ParseDate(int i) {
      this.i = i;
    }

    @Override
    public void run() {
      try {
        String text = "2018年11月22日 08:28:"+ Integer.toString(this.i%60);
//        Date date = sdf.parse(text);
//        System.out.println(i + " -- " + date);
        // 先判断当前线程是否有SimpleDateFromat对象，如果没有就创建一个，如果有就直接取用
        if(threadLocal.get() == null ){
          threadLocal.set(new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss"));
        }
        Date date = threadLocal.get().parse(text);
        System.out.println(i + " -- " + date);
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    // 创建100个线程
    for (int i = 0; i < 100; i++) {
      new Thread(new ParseDate(i)).start();
    }
  }
}
