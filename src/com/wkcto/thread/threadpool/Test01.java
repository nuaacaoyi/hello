package com.wkcto.thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// 线程池的基本使用
public class Test01 {
  public static void main(String[] args) {
    // 创建一个线程池，利用工厂方法
    // 创建一个固定数量的线程池
    ExecutorService executorService = Executors.newFixedThreadPool(5);

    // 向线程池中提交18个任务
    for (int i = 0; i < 18; i++) {
      executorService.execute(new Runnable() {
        @Override
        public void run() {
          // 通过getId发现，执行的线程只有5个
          System.out.println(Thread.currentThread().getId() + " 的任务在执行任务，开始时间：" + System.currentTimeMillis());
          try {
            Thread.sleep(3000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      });
    }
  }
}
