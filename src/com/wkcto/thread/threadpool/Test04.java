package com.wkcto.thread.threadpool;

import java.util.Random;
import java.util.concurrent.*;

//自定义线程工厂
public class Test04 {
  public static void main(String[] args) throws InterruptedException {
    // 定义一个任务
    Runnable r = new Runnable() {
      @Override
      public void run() {
        int num = new Random().nextInt(10);
        System.out.println(Thread.currentThread().getId() + " -- " + System.currentTimeMillis()
            + "开始睡眠： " + num + "秒");
        try {
          TimeUnit.SECONDS.sleep(num);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };

    // 创建线程池，使用自定义的线程工厂
    ExecutorService executorService = new ThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS,
        new SynchronousQueue<>(), new ThreadFactory() {
      @Override
      public Thread newThread(Runnable r) {
        // 根据参数r接收的任务，创建一个线程
        Thread t = new Thread(r);
        t.setDaemon(true); // 设置为守护线程，当主线程运行结束，线程池的线程自动退出
        System.out.println("创建了线程： " + t);
        return t;
      }
    });

    for (int i = 0; i < 5; i++) {
      executorService.submit(r);
    }

    Thread.sleep(3000);

  }
}
