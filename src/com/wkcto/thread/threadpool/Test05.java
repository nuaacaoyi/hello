package com.wkcto.thread.threadpool;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//线程池中的常用API，监控线程池
public class Test05 {
  public static void main(String[] args) throws InterruptedException {
    // 定义一个任务
    Runnable r = new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getId() + " 编号的线程开始执行 -- " + System.currentTimeMillis());
        try {
          TimeUnit.SECONDS.sleep(5000);// 睡眠5s模拟任务执行时长
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };
    // 创建线程
    ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2,5,0, TimeUnit.SECONDS,
        new ArrayBlockingQueue<>(5),
        Executors.defaultThreadFactory(),
        new ThreadPoolExecutor.DiscardPolicy());

    // 现在向线程池中提交30个任务
    for (int i = 0; i < 30; i++) {
      poolExecutor.submit(r);
      System.out.println("当前线程池核心线程数量："  + poolExecutor.getCorePoolSize()
      +"; 最大线程数："+ poolExecutor.getMaximumPoolSize()
      + "; 当前线程池大小：" + poolExecutor.getPoolSize()
      + "; 共收到任务数量："+ poolExecutor.getTaskCount()
      +"; 完成任务数量：" + poolExecutor.getCompletedTaskCount()
      +"; 活动线程数：" + poolExecutor.getActiveCount()
      +"; 等待的任务数："+ poolExecutor.getQueue().size());
      TimeUnit.MILLISECONDS.sleep(500);

    }
    System.out.println("============================================");
    while (poolExecutor.getActiveCount()>0){
      System.out.println("当前线程池核心线程数量："  + poolExecutor.getCorePoolSize()
          +"; 最大线程数："+ poolExecutor.getMaximumPoolSize()
          + "; 当前线程池大小：" + poolExecutor.getPoolSize()
          + "; 共收到任务数量："+ poolExecutor.getTaskCount()
          +"; 完成任务数量：" + poolExecutor.getCompletedTaskCount()
          +"; 活动线程数：" + poolExecutor.getActiveCount()
          +"; 等待的任务数："+ poolExecutor.getQueue().size());
      TimeUnit.SECONDS.sleep(1);
    }
  }
}
