package com.wkcto.thread.threadpool;

import java.util.Random;
import java.util.concurrent.*;

//自定义拒绝策略
public class Test03 {
  public static void main(String[] args) {
    // 定义一个任务
    Runnable r = new Runnable() {
      @Override
      public void run() {
        int num = new Random().nextInt(5);
        System.out.println(Thread.currentThread().getId() + " -- " + System.currentTimeMillis()
        + "开始睡眠： " + num + "秒");
        try {
          TimeUnit.SECONDS.sleep(num);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };

    // 创建线程池 ， 并自定义拒绝策略
    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS,
        new LinkedBlockingDeque<>(10), Executors.defaultThreadFactory(),
        new RejectedExecutionHandler() {
          @Override
          public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            System.out.println(r + " 被丢弃了..");
          }
        });

    for (int i = 0; i < 100; i++) {
      threadPoolExecutor.submit(r);
    }
  }
}
