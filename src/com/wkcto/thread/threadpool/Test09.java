package com.wkcto.thread.threadpool;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

// 使用ForkJoinPool实现数列的若干求和
public class Test09 {
  // 计算数列的和，需要返回结果，可以定义一个任务继承 RecursiveTask<T>   T泛型就是返回值类型
  private static class CountTask extends RecursiveTask<Long>{
    // 定义一个数据规模的阈值，允许计算10000个数内的和，当超过这个数时就进行分解
    private static final int THRESHOLD = 10000;
    private static final int TASKNUM = 100;// 定义每次拆分任务时 的 拆分数量

    private long start;// 计算数列的起始值
    private long end; // 计算数列的结束值

    public CountTask(long start, long end) {
      this.start = start;
      this.end = end;
    }

    @Override
    protected Long compute() {
      long sum = 0; // 保存计算的结果
      // 判断这个任务是否需要继续分解，判断条件：起始值和结束值的差值是否  >  阈值
      if(end - start < THRESHOLD){
        // 小于阈值，直接计算
        for (long i = start;  i<=end ; i++) {
          sum +=i;
        }
      }else{//超过阈值，进行分解
        // 约定每次分解成100个小任务，计算每个任务的计算量
        long step = (end-start)/TASKNUM;
        /*需要注意的是：如果任务划分的层次很深，即THRESHOLD阈值太小，每个人物的计算量很小，层次的划分就很深可能导致以下情况：
            1）系统内的线程数量会越积越多，导致性能下降严重；
            2）分散的次数过多，方法调用过多可能会导致内存溢出
        * */
        // 创建一个存储任务的集合
        ArrayList<CountTask> subTasks = new ArrayList<>();
        long pos = start;
        for (int i = 0; i < TASKNUM; i++) {
          long lastOne = (pos + step)>end? end:(pos + step);// 计算每个任务的结束位置
          // 创建子任务
          CountTask task = new CountTask(pos, lastOne);
          // 把任务添加到集合中
          subTasks.add(task);
          // 调用fork提交子任务
          task.fork();
          // 调整下个子任务的起始位置
          pos = lastOne+1;
        }

        // 等待所有的子任务计算结束后，合并计算结果
        for (CountTask subTask : subTasks) {
          sum += subTask.join(); // join()会一直等待子任务执行完毕返回执行结果
        }


      }
      return sum;
    }
  }

  public static void main(String[] args) {
    // 创建ForkJoinPool线程池
    ForkJoinPool forkJoinPool = new ForkJoinPool();
    // 创建一个大的任务
    CountTask task = new CountTask(0, 200000L);
    // 把任务提交给线程池
    ForkJoinTask<Long> result = forkJoinPool.submit(task);
    try{
      Long res = result.get();
      System.out.println("计算数列结果为： " + res);
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
  }
}
