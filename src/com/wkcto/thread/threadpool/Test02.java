package com.wkcto.thread.threadpool;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

// 线程池的计划任务
public class Test02 {
  public static void main(String[] args) {
    // 创建一个有调度功能的线程池
    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);

    // 可以在延迟2秒后执行任务
    // schedule三个参数：1）Runnable任务；2）延迟时长；3）时间单位
    scheduledExecutorService.schedule(new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getId() + " -- " + System.currentTimeMillis());

      }
    }, 2, TimeUnit.SECONDS);

    // 以固定的频率执行任务，开启任务的时间时固定的
    // 在3秒后执行任务，以后每隔2秒重新执行一次
    // 注意：这里是将第一个参数代表的任务 作为一个需要重复执行的任务执行的
    /*scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getId() + " --- 以固定频率开启任务 --- " + System.currentTimeMillis());

        try {
          // 如果任务执行时间超过了调度的时间间隔，则任务完成后立即开启下一个任务
          TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    },3, 2, TimeUnit.SECONDS);
*/
    // 在上次任务结束后，在固定延迟后再次执行该任务，不管任务执行耗时多长，总是在任务执行结束后的2秒开启新的任务
    scheduledExecutorService.scheduleWithFixedDelay(new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getId() + " --- 以固定频率开启任务 --- " + System.currentTimeMillis());

        try {
          // 如果任务执行时间超过了调度的时间间隔，则任务完成后立即开启下一个任务
          TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    },3, 2, TimeUnit.SECONDS);



  }
}
