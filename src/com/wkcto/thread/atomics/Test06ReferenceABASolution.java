package com.wkcto.thread.atomics;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @Author caoyi
 * @Create 2022-10-14-16:52
 * @Description: TODO
 */
// AtomicStampedReference原子类中有一个整数标记值stamp，每次执行CAS操作是，需要对比它的版本，即比较stamp的值
public class Test06ReferenceABASolution {
  private static AtomicStampedReference<String> stampedReference =
      new AtomicStampedReference<>("abc", 0 );

  public static void main(String[] args) throws InterruptedException {
    // 创建第一个线程，将字符串改为def，然后还原为abc
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        stampedReference.compareAndSet("abc", "def",
            stampedReference.getStamp(),stampedReference.getStamp()+1);
        System.out.println(Thread.currentThread().getName() + "---" + stampedReference.getReference());
        stampedReference.compareAndSet("def", "abc",
            stampedReference.getStamp(),stampedReference.getStamp()+1);
      }
    });
    Thread t2 = new Thread(new Runnable() {
      @Override
      public void run() {
        int stamp = stampedReference.getStamp(); // 获得版本号
        try {
          TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println(stampedReference.compareAndSet("abc", "ghg",
            stamp,stamp+1));
      }
    });

    t1.start();
    t2.start();
    t1.join();
    t2.join();
    System.out.println(stampedReference.getReference());
  }
}
