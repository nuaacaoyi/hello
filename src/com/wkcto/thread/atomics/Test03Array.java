package com.wkcto.thread.atomics;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @Author caoyi
 * @Create 2022-10-14-16:15
 * @Description: TODO
 */
public class Test03Array {

  //定义一个原子数组
  static AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(10);

  public static void main(String[] args) {
    Thread[] thread = new Thread[10];
    for (int i = 0; i < 10; i++) {
      thread[i] = new AddTread();
    }
    // 开启所有子线程
    for (Thread thread1 : thread) {
      thread1.start();
    }
    // 在主线程中查看自增完以后原子数组中各个元素的值，在主线程中需要在所有子线程中都执行完后再查看
    // 把所有的子线程合并到当前主线程中
    for (Thread thread1 : thread) {
      try {
        thread1.join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println(atomicIntegerArray);

  }

  // 定义一个线程类，修改这个数组
  static class AddTread extends Thread{
    @Override
    public void run() {
      // 把原子数组的每个元素自增1000次
      for (int i=0; i<10000; i++) {
        atomicIntegerArray.getAndIncrement(i % atomicIntegerArray.length());
      }

    }
  }
}
