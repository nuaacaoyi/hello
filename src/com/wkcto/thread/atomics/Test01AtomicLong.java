package com.wkcto.thread.atomics;

import java.util.Random;

/**
 * @Author caoyi
 * @Create 2022-10-14-15:49
 * @Description: TODO
 */
public class Test01AtomicLong {
  public static void main(String[] args) {
    // 通过线程来模拟请求
    for (int i = 0; i < 10000; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          // 每个线程就是一个请求
          Indicator.getInstance().newRequestReceive();
          int num = new Random().nextInt();
          if(num%2 ==0 ){
            Indicator.getInstance().requestProcessSuccess();
          }else {
            Indicator.getInstance().requestProcessFailure();
          }
        }
      }).start();
    }

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    // 打印结果
    System.out.println("总的请求数：" + Indicator.getInstance().getRequestCount());
    System.out.println("成功请求数：" + Indicator.getInstance().getSuccessCount());
    System.out.println("失败请求数：" + Indicator.getInstance().getFailureCount());
  }
}
