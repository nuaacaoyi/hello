package com.wkcto.thread.atomics;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * @Author caoyi
 * @Create 2022-10-14-16:30
 * @Description: TODO
 */
public class Test03FieldUpdater {

  public static void main(String[] args) {
    User user = new User(0, 12);
    // 开启10个线程对user的age进行自增
    for (int i = 0; i < 10; i++) {
      new SubThread(user).start();
    }

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(user);
  }
}

// 使用AtomicIntegerFieldUpdater更新的字段必须使用volatile修饰
class User{
  int id;
  volatile int age;

  public User(int id, int age) {
    this.id = id;
    this.age = age;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", age=" + age +
        '}';
  }
}

class SubThread extends Thread{
  private User user; // 要更细你的uer对象
  // 创建一个AtomicIntegerFieldUpdater更新器
  private AtomicIntegerFieldUpdater<User> updater
      = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");

  public SubThread(User user) {
    this.user = user;
  }

  @Override
  public void run() {
    // 在子线程中对User对象的age字段自增
    for (int i = 0; i < 10; i++) {
      System.out.println(updater.getAndIncrement(user));
    }
  }
}
