package com.wkcto.thread.atomics;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @Author caoyi
 * @Create 2022-10-14-15:50
 * @Description: TODO
 */
// 使用原子变量类定义一个计数器，该计数器在整个程序中都能使用，并且所有的地方都使用这一个计数器
  //这个计数器就可以设计为单例
public class Indicator {
  // 构造方法私有化
  private Indicator(){}
  // 定义一个私有的奔雷的静态对象
  private static final Indicator INSTANCE = new Indicator();
  // 提供一个公共静态方法返回这个私有实例
  public static Indicator getInstance(){
    return INSTANCE;
  }
  // 记录请求总数;  使用原子变量类记录三个指标
  private final AtomicLong requestCount = new AtomicLong(0); // 请求总数
  private final AtomicLong successCount = new AtomicLong(0); // 成功总数
  private final AtomicLong failureCount = new AtomicLong(0); // 失败总数

  // 有新的请求
  public void newRequestReceive(){
    requestCount.incrementAndGet();
  }
  // 处理成功了
  public void requestProcessSuccess(){
    successCount.incrementAndGet();
  }
  // 处理失败了
  public void requestProcessFailure(){
    failureCount.incrementAndGet();
  }

  // 查看三个指标值
  public long getRequestCount(){
    return requestCount.get();
  }
  public long getSuccessCount(){
    return successCount.get();
  }
  public long getFailureCount(){
    return failureCount.get();
  }
}
