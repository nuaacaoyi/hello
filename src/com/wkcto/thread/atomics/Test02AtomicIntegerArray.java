package com.wkcto.thread.atomics;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @Author caoyi
 * @Create 2022-10-14-16:04
 * @Description: TODO
 */
public class Test02AtomicIntegerArray {
  public static void main(String[] args) {
    // 1 创建一个指定长度的原子数组
    AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(10);
    System.out.println(atomicIntegerArray);

    // 2 返回指定位置的元素
    System.out.println(atomicIntegerArray.get(0));// 0
    System.out.println(atomicIntegerArray.get(1));// 0
    // 3 设置指定位置的元素
    atomicIntegerArray.set(0,10);
    // 在设置数组元素的新值时，同时返回数组元素的旧值
    System.out.println(atomicIntegerArray.getAndSet(1, 11)); // 0
    System.out.println(atomicIntegerArray); // [10, 11, 0, 0, 0, 0, 0, 0, 0, 0]

    // 4 修改数组元素的值，把数组元素加上某个值
    System.out.println(atomicIntegerArray.addAndGet(0, 22)); //32
    System.out.println(atomicIntegerArray.getAndAdd(1, 33)); // 11
    System.out.println(atomicIntegerArray); // [32, 44, 0, 0, 0, 0, 0, 0, 0, 0]
    // 5 CAS操作
    // 如果数组中索引值为0的元素值为32，就将其修改为222
    System.out.println(atomicIntegerArray.compareAndSet(0,32,222));
    System.out.println(atomicIntegerArray); // [222, 44, 0, 0, 0, 0, 0, 0, 0, 0]
    System.out.println(atomicIntegerArray.compareAndSet(1,11,333));
    System.out.println(atomicIntegerArray); // [222, 44, 0, 0, 0, 0, 0, 0, 0, 0]
    // 6 自增/自减
    System.out.println(atomicIntegerArray.incrementAndGet(0)); // 223
    System.out.println(atomicIntegerArray.getAndIncrement(1)); // 44
    System.out.println(atomicIntegerArray); // [223, 45, 0, 0, 0, 0, 0, 0, 0, 0]
    System.out.println(atomicIntegerArray.decrementAndGet(2)); // -1
    System.out.println(atomicIntegerArray.getAndDecrement(3)); // 0
    System.out.println(atomicIntegerArray); // [223, 45, -1, -1, 0, 0, 0, 0, 0, 0]

  }
}
