package com.wkcto.thread.atomics;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author caoyi
 * @Create 2022-10-14-16:39
 * @Description: TODO
 */
public class Test04AtomicReference {
  // 创建一个atomicReference对象
  static AtomicReference<String> atomicReference = new AtomicReference<>("abc");
  public static void main(String[] args) {
    // 创建100个线程，修改字符串
    for (int i = 0; i < 100; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          if(atomicReference.compareAndSet("abc", "def")){
            System.out.println(Thread.currentThread().getName() + "把字符串abc修改为def");
          }
        }
      }).start();
    }
    // 再创建100个贤臣俄国，修改字符串
    for (int i = 0; i < 100; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          if(atomicReference.compareAndSet("def", "abc")){
            System.out.println(Thread.currentThread().getName() + "把字符串def还原为abc");
          }
        }
      }).start();
    }
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(atomicReference);
  }
}

