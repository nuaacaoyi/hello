package com.wkcto.thread.threadsafe;

import java.util.Random;

/**
 * @Author caoyi
 * @Create 2022-10-13-15:03
 * @Description: TODO
 */
public class Test03Visibility {
  public static void main(String[] args) throws InterruptedException {
    MyTask myTask = new MyTask();
    new Thread(myTask).start();
    Thread.sleep(1000);

    // 主线程1秒后，取消子线程
    myTask.Cancel();
    /*可能存在以下情况:
        在main线程中调用了task.cancel()方法，修改了toCancel
        但是子线程看不到main线程对toCancel的修改，在子线程中toCancel变量一直为false
    * */
  }

  static class MyTask implements Runnable{
    private boolean toCancel = false;

    @Override
    public void run() {
      while (!toCancel){
        if(doSomething()){
          //break;
        }
      }
      if(toCancel){
        System.out.println("任务被取消。。。");
      }
      else {
        System.out.println("任务正常结束。。。");
      }
    }

    private boolean doSomething(){
      System.out.println("执行任务。。。");
      /*try {
        System.out.println("执行任务。。。");
        Thread.sleep(new Random().nextInt(1000));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }*/
      return true;
    }

    public void Cancel(){
      toCancel = true;
      System.out.println("收到取消线程的消息。。");
    }


  }
}
