package com.wkcto.thread.threadsafe;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author caoyi
 * @Create 2022-10-13-14:51
 * @Description: TODO
 */
public class Test01Atmoic {
  public static void main(String[] args) {
    // 启动两个线程，不断调用getNUm方法
    MyInt myInt = new MyInt();

    for (int i = 0; i < 2; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          while (true){
            try {
              System.out.println(Thread.currentThread().getName() + " --> " + myInt.getNum());
              Thread.sleep(100);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }).start();
    }
  }

  static class MyInt1{
    int num;
    public int getNum(){
      // 以下操作包含了  读  加 赋值三个操作
      // 如果没有加锁，就可能导致两个线程读重等问题
      return num++;
    }
  }

  // 在java中提供了一个线程安全的AtomicInteger类，保证了操作的原子性
  static class MyInt{
    AtomicInteger num= new AtomicInteger();
    public int getNum(){
      return num.getAndIncrement();
    }
  }
}
