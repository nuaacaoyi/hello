package com.wkcto.thread.producerStatck;

/**
 * @Author caoyi
 * @Create 2022-10-19-15:39
 * @Description: TODO
 */
public class ConsumerThread  extends Thread  {
  private MyStack stack;

  public ConsumerThread(MyStack stack) {
    this.stack = stack;
  }

  @Override
  public void run() {
    while (true){
      stack.pop();
    }
  }
}
