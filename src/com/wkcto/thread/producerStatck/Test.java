package com.wkcto.thread.producerStatck;

//
public class Test {
  public static void main(String[] args) {
    MyStack myStack = new MyStack();

    ProducerThread p1 = new ProducerThread(myStack);
    ProducerThread p2 = new ProducerThread(myStack);
    ProducerThread p3 = new ProducerThread(myStack);
    ConsumerThread c1 = new ConsumerThread(myStack);
    ConsumerThread c2 = new ConsumerThread(myStack);
    ConsumerThread c3 = new ConsumerThread(myStack);

    p1.setName("生产线程1..");
    p2.setName("生产线程2..");
    p3.setName("生产线程3..");
    c1.setName("消费线程1..");
    c2.setName("消费线程2..");
    c3.setName("消费线程3..");

    p1.start();
    p2.start();
    p3.start();
    c1.start();
    c2.start();
    c3.start();
  }
}
