package com.wkcto.thread.producerStatck;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2022-10-19-15:32
 * @Description: TODO
 */
public class MyStack {
  private List list = new ArrayList(); // 定义一个集合模拟栈
  private static final int MAX =5 ; // 集合的最大容量

  // 入栈
  public synchronized void push(){
    //如果栈中数据不满就可以放数据，如果已经满了，就等待
    //if(list.size() >= MAX){ //使用if作为条件，可能在唤醒当前线程时 判断条件发生变化，size再次大于MAX，此时会导致逻辑错误
    while(list.size() >= MAX){ // 同理，此处使用while，如果环星是条件变化了，那么会重复进入判断，再次wait
      System.out.println(Thread.currentThread().getName() + " 入栈 begin wait ... ");
      try {
        this.wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    String data = "data - " + Math.random();
    System.out.println(Thread.currentThread().getName() + " 添加了数据： " + data);
    list.add(data);
    //this.notify(); // 已经有数据入栈，通知可以出栈了。
    // 如果使用notify，在多生产多消费环境中，可能会导致此处唤醒的不是消费线程，而是生产线程，造成假死
    this.notifyAll();// 因此应该使用notifyAll
  }

  // 出栈
  public synchronized void pop(){
    // 出栈时，如果没有数据就等待
    while(list.size() == 0){
      try {
        System.out.println(Thread.currentThread().getName() + " 出栈，begin wait ... ");
        this.wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println(Thread.currentThread().getName() + " 取出了数据： " + list.remove(0));
    this.notifyAll(); // 弹出了数据，说明list不满了，通知入栈线程
  }
}
