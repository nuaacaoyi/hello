package com.wkcto.thread.lock.reentrant;

//锁的可重入性
public class Test01 {

  //静态方法，内部锁
  public synchronized void sm1(){
    // 线程执行sm1方法，默认this作为锁对象，在sm1方法中调用sm2时，此时当前线程是持有 this锁对象的
    // sm2同步方法默认的锁对象也是this对象，要执行sm2必须获得this锁对象，当前线程 第二次 获得this锁对象
    // 这就是锁的可重入性
    System.out.println("同步方法1");
    sm2();
  }

  private synchronized void sm2(){
    System.out.println("同步方法2");
    sm3();
  }

  private synchronized void sm3(){
    System.out.println("同步方法3");

  }

  public static void main(String[] args) {
    Test01 obj = new Test01();
    new Thread(new Runnable() {
      @Override
      public void run() {
        obj.sm1();
      }
    }).start();
  }
}
