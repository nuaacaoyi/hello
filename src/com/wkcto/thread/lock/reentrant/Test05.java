package com.wkcto.thread.lock.reentrant;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author caoyi
 * @Create 2022-10-25-8:46
 * @Description: TODO
 */
public class Test05 {
  static class Service{
    private Lock lock = new ReentrantLock();
    public void serviceMethod(){
      try {
        lock.lock();  // 获得锁定，即使调用了线程的interrupt方法，它也没有真正的中断该线程
        lock.lockInterruptibly();// 使用该方法允许线程在等待锁的时候进行中断处理
        System.out.println(Thread.currentThread().getName() + " -- begin lock~");
        //执行耗时操作
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
          new StringBuilder();
        }
        System.out.println(Thread.currentThread().getName() + " -- end lock!");
      } catch (Exception e) {
        e.printStackTrace();
      }
      finally {
        System.out.println(Thread.currentThread().getName() + " **** 释放锁");
        lock.unlock();
      }
    }
  }

  public static void main(String[] args) throws InterruptedException {
    Service s = new Service();
    Runnable r = new Runnable() {
      @Override
      public void run() {
        s.serviceMethod();
      }
    };
    Thread t1 = new Thread(r);
    t1.start();

    Thread.sleep(50);

    Thread t2 = new Thread(r);
    t2.start();
    Thread.sleep(50);

    t2.interrupt();// 如果是lock方法无法中断，// 如果lockInterruptibly方法，此处会中断处理
  }
}
