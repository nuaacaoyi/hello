package com.wkcto.thread.lock.reentrant;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author caoyi
 * @Create 2022-10-20-11:29
 * @Description: TODO
 */
public class Test04 {
  static class SubThread extends Thread{
    private Lock lock = new ReentrantLock(); // 定义锁对象
    public static int num = 0 ;// 定义一个变量

    @Override
    public void run() {
      for (int i = 0; i < 10000; i++) {
        num++;
      }
    }
  }

  public static void main(String[] args) throws InterruptedException {
    SubThread t1 = new SubThread();
    SubThread t2 = new SubThread();
    t1.start();
    t2.start();
    t1.join();
    t2.join();
    System.out.println(SubThread.num);
  }
}
