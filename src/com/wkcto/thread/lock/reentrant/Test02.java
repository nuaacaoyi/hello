package com.wkcto.thread.lock.reentrant;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//Lock锁的基本使用
public class Test02 {
  // 定义一个显示锁
  static Lock lock = new ReentrantLock();

  // 定义一个方法
  public static void sm(){
    // 先获得锁
    lock.lock();
    // 此处for循环就是同步代码块
    for (int i = 0; i < 100; i++) {
      System.out.println("ReentrantLock" + Thread.currentThread().getName() + " --- " + i);
    }
    // 释放锁
    lock.unlock();
  }

  public static void sm2(){
    synchronized (lock){
      for (int i = 0; i < 100; i++) {
        System.out.println("synchronized" + Thread.currentThread().getName() + " --- " + i);
      }
    }
  }

  public static void main(String[] args) {
    Runnable r = new Runnable() {
      @Override
      public void run() {
        sm();
      }
    };

    Runnable r2 = new Runnable() {
      @Override
      public void run() {
        sm2();
      }
    };
    // 启动三个线程
    new Thread(r).start();
    new Thread(r2).start();
    new Thread(r).start();
    new Thread(r2).start();
    new Thread(r).start();
  }
}
