package com.wkcto.thread.lock.reentrant;


import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// 同步代码块在不同的方法中
public class Test03 {
  static Lock lock = new ReentrantLock();
  public static void sm1(){
    // 经常在try代码块中获得lock锁， 在finally子句中释放锁
    try {
      lock.lock();
      System.out.println(Thread.currentThread().getName() + " --method 1 begin-- "+ System.currentTimeMillis());
      Thread.sleep(new Random().nextInt(1000));
      System.out.println(Thread.currentThread().getName() + " --method 1 end-- "+ System.currentTimeMillis());
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  public static void sm2(){
    // 经常在try代码块中获得lock锁， 在finally子句中释放锁
    try {
      lock.lock();
      System.out.println(Thread.currentThread().getName() + " --method 2 begin-- "+ System.currentTimeMillis());
      Thread.sleep(new Random().nextInt(1000));
      System.out.println(Thread.currentThread().getName() + " --method 2 end-- "+ System.currentTimeMillis());
    }
    catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  public static void main(String[] args) {
    Runnable r1 = new Runnable() {
      @Override
      public void run() {
        sm1();
      }
    };
    Runnable r2 = new Runnable() {
      @Override
      public void run() {
        sm2();
      }
    };

    new Thread(r1).start();
    new Thread(r1).start();
    new Thread(r1).start();
    new Thread(r2).start();
    new Thread(r2).start();
    new Thread(r2).start();

  }
}
