package com.wkcto.thread.lock.reentrant;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

// 使用trylock避免死锁
public class Test08 {
  static class IntLock implements Runnable{
    public static ReentrantLock lock1 = new ReentrantLock();
    public static ReentrantLock lock2 = new ReentrantLock();
    int lockNum;

    public IntLock(int lockNum) {
      this.lockNum = lockNum;
    }

    @Override
    public void run() {
      if(lockNum % 2 == 1){
        while (true) {
          try {
            if(lock1.tryLock()){

              System.out.println(Thread.currentThread().getName() + "获得锁1，还需要获得锁2");
              Thread.sleep(new Random().nextInt(100));
              try {
                if(lock2.tryLock()){
                  System.out.println(Thread.currentThread().getName() + " 同时获得了锁1和锁2.。。");
                  return; // 任务执行完毕，当前线程结束
                }
              } finally {
                if(lock2.isHeldByCurrentThread())lock2.unlock();
              }
            }
          } catch (InterruptedException e) {
            e.printStackTrace();
          } finally {
            if(lock1.isHeldByCurrentThread()){lock1.unlock();}
          }
        }

      }else{
        while (true) {
          try {
            if(lock2.tryLock()){

              System.out.println(Thread.currentThread().getName() + "获得锁2，还需要获得锁1");
              Thread.sleep(new Random().nextInt(500));
              try {
                if(lock1.tryLock()){
                  System.out.println(Thread.currentThread().getName() + " 同时获得了锁2和锁1.。。");
                  return; // 任务执行完毕，当前线程结束
                }
              } finally {
                if(lock1.isHeldByCurrentThread())lock1.unlock();
              }
            }
          } catch (InterruptedException e) {
            e.printStackTrace();
          } finally {
            if(lock2.isHeldByCurrentThread()){lock2.unlock();}
          }
        }
      }
    }
  }


  public static void main(String[] args) {
    IntLock intLock1 = new IntLock(11);
    IntLock intLock2 = new IntLock(22);
    Thread t1 = new Thread(intLock1);
    Thread t2 = new Thread(intLock2);

    t1.start();
    t2.start();
    // 运行后，使用tryLock方法尝试获得锁，不会傻等
  }
}
