package com.wkcto.thread.lock.readwrite;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

// 写锁互斥，只允许一个线程慈幼
public class Test02 {
  static class Service{
    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void write(){
      try {
        readWriteLock.writeLock().lock(); // 申请写锁
        System.out.println(Thread.currentThread().getName() + " -- 获得写锁： "+ System.currentTimeMillis());
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        System.out.println(Thread.currentThread().getName() + " -- 读取完毕，释放锁： " + System.currentTimeMillis());
        readWriteLock.writeLock().unlock();
      }
    }
  }

  public static void main(String[] args) {
    Service s  = new Service();
    // 创建5个线程修改数据
    for (int i = 0; i < 5; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          s.write();
        }
      }).start();
    }
  }
}
