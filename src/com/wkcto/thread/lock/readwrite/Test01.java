package com.wkcto.thread.lock.readwrite;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

// 读读共享，即允许多个线程同时获得读锁
public class Test01 {
  static class Service{
    // 定义读写锁
    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    // 定义方法读取数据
    public void read(){
      try{
        readWriteLock.readLock().lock(); // 申请读锁
        System.out.println(Thread.currentThread().getName() + "获得读锁： " + System.currentTimeMillis());
        TimeUnit.SECONDS.sleep(3); // 模拟读取数据的耗时

      } catch (InterruptedException e) {
        e.printStackTrace();
      }finally {
        System.out.println(Thread.currentThread().getName() + "--释放了锁： " + System.currentTimeMillis());
        readWriteLock.readLock().unlock();// 释放读锁
      }
    }
  }

  public static void main(String[] args) {
    Service service = new Service();
    // 创建五个线程，调用方法
    for (int i = 0; i < 5; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          service.read();
        }
      }).start();
    }
  }
}
