package com.wkcto.thread.lock.method;

import java.util.concurrent.locks.ReentrantLock;

//公平锁与非公平锁
public class Test01 {
  static ReentrantLock lock = new ReentrantLock(true); // 默认是非公平锁

  public static void main(String[] args) {
    Runnable runnable = new Runnable() {
      @Override
      public void run() {
        while(true){
          try {
            lock.lock();
            System.out.println(Thread.currentThread().getName() + " 获得了锁对象");
          } finally {
            lock.unlock();
          }
        }
      }
    };
    for (int i = 0; i < 5; i++) {
      new Thread(runnable).start();
    }
  }
}
