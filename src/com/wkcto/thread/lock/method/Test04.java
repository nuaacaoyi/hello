package com.wkcto.thread.lock.method;

import com.aspose.words.Run;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/// 返回在Condition条件上等待的线程预估数
public class Test04 {
  static class Service{
    private ReentrantLock lock = new ReentrantLock(); // 定义锁对象
    private Condition condition = lock.newCondition(); // 返回锁对象给定一个condition
    public void waitMehthod(){
      try {
        lock.lock();
        System.out.println(Thread.currentThread().getName() + " 进入等待前，现在该condition条件上等待的线程预估数： "
            + lock.getWaitQueueLength(condition));
        condition.await();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlock();
      }
    }

    public void notifyMethod(){
      try{
        lock.lock();
        condition.signalAll();
        System.out.println("唤醒所有的等待线程后，condition条件上等待线程预估数： " + lock.getWaitQueueLength(condition));
      }finally {
        lock.unlock();
      }
    }
  }

  public static void main(String[] args) throws InterruptedException {
    Service service = new Service();
    Runnable r = new Runnable() {
      @Override
      public void run() {
        service.waitMehthod();
      }
    };
    for (int i = 0; i < 10; i++) {
      new Thread(r).start();
    }
    Thread.sleep(1000);
    service.notifyMethod(); // 1秒钟后唤醒所有的等待
  }
}
