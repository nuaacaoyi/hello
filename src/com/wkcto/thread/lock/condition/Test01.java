package com.wkcto.thread.lock.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// Condition等待与通知
public class Test01 {
  static Lock lock = new ReentrantLock();

  static Condition condition = lock.newCondition();

  static class SubThread extends Thread{
    @Override
    public void run() {
      try {
        lock.lock();
        System.out.println("method lock");
        condition.await();
        System.out.println("method await");
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlock();
        System.out.println("method unlock");
      }
    }
  }

  public static void main(String[] args) throws InterruptedException {
    SubThread t  = new SubThread();
    t.start();
    // 子线程启动后会转入等待状态

    Thread.sleep(3000);
    try {
      // 唤醒之前需要先持有锁
      lock.lock();
      condition.signal();
    } finally {
      lock.unlock();
    }

  }
}
