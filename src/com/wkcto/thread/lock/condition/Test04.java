package com.wkcto.thread.lock.condition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

// 模拟多对多的生产者消费者模式，
public class Test04 {
  static class MyService{
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    private boolean flag = true; // 定义交替打印的标记
    // 定义方法打印
    public void printOne(){
      try {
        lock.lock();
        while (flag){ // 当flag为true进行等待
          condition.await();
        }
        // flag为false时打印
        System.out.println(Thread.currentThread().getName() + " --------------------------");
        flag=true;
        condition.signalAll();// 通知另外的线程打印
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlock();
      }
    }

    // 定义方法打印
    public void printTwo(){
      try {
        lock.lock();
        while (!flag){ // 当flag为true进行等待
          condition.await();
        }
        // flag为false时打印
        System.out.println(Thread.currentThread().getName() + " *********************** ");
        flag=false;
        condition.signalAll();// 通知另外的线程打印
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlock();
      }
    }
  }

  public static void main(String[] args) {
    MyService myService = new MyService();
    for (int i = 0; i<10; i++) {
      // 创建一个线程打印
      new Thread(new Runnable() {
        @Override
        public void run() {
          for (int i = 0; i < 100; i++) {
            myService.printOne();
          }
        }
      }).start();
      new Thread(new Runnable() {
        @Override
        public void run() {
          for (int i = 0; i < 100; i++) {
            myService.printTwo();
          }
        }
      }).start();
    }
  }
}
