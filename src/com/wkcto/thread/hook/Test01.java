package com.wkcto.thread.hook;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

// 通过Hook线程防止程序的重复执行
public class Test01 {
  public static void main(String[] args) {
    // 1 注入Hook线程，在程序退出时删除.lock文件
    Runtime.getRuntime().addShutdownHook(new Thread(){
      @Override
      public void run() {
        System.out.println("JVM退出会启动当前Hook线程， 在Hook线程中删除.lock文件");
        getLockFile().toFile().delete();// 删除这个文件
      }
    });

    // 2 程序运行时检查lock文件是否存在，如果存在说明就是重复启动，抛出异常
    if(getLockFile().toFile().exists()){
      throw new RuntimeException("程序已启动");
    }else {
      System.out.println("程序启动了。。。");
      try {
        System.out.println("程序启动时创建了.lock文件");
        getLockFile().toFile().createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    // 3 模拟程序运行
    for (int i = 0; i < 100; i++) {
      System.out.println("程序正在运行");
      try {
        TimeUnit.SECONDS.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  private static Path getLockFile(){
    return Paths.get("", "tmp.lock");
  }
}
