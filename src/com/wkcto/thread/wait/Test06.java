package com.wkcto.thread.wait;

//notify 与 notifyAll
public class Test06 {
  public static void main(String[] args) throws InterruptedException {
    Object lock = new Object(); // 定义一个对象作为子线程的锁对象
    SubThread t1 = new SubThread(lock);
    SubThread t2 = new SubThread(lock);
    SubThread t3 = new SubThread(lock);
    t1.setName("t1");
    t2.setName("t2");
    t3.setName("t3");

    t1.start();
    t2.start();
    t3.start();
    Thread.sleep(2000);

    synchronized (lock){
      //lock.notify();
      lock.notifyAll();
    }
  }

  static class SubThread extends Thread{
    private  Object lock; // 定义变量作为锁对象

    public SubThread(Object lock) {
      this.lock = lock;
    }

    @Override
    public void run() {
      synchronized (lock){
        try {
          System.out.println(Thread.currentThread().getName() + "... begin wait...");
          lock.wait();
          System.out.println(Thread.currentThread().getName() + "... end wait...");
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
