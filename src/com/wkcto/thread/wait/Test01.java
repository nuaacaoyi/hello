package com.wkcto.thread.wait;

// 演示wait方法和notify方法，需要放在同步代码块中
// 任何对象都可以调用wait()和notify()方法，这两个方法继承于Object
public class Test01 {
  public static void main(String[] args) {
    try {
      String test = "wkcto";
      test.wait(); //java.lang.IllegalMonitorStateException
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
