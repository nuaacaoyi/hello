package com.wkcto.thread.wait;

import javax.sound.midi.Soundbank;

// 需要通过notify唤醒等待的线程
public class Test03 {
  public static void main(String[] args) throws InterruptedException {
    String lock = "wkcto";
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (lock){
          System.out.println("线程1开始等待。。。" + System.currentTimeMillis());
          try {
            lock.wait(); // 线程等待
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          System.out.println("线程结束等待。。。 " + System.currentTimeMillis());
        }

      }
    });

    // 定义第二个线程，在第二个线程中唤醒第一个线程
    Thread t2 = new Thread(new Runnable() {
      @Override
      public void run() {
        // notify方法也需要在同步代码块中，由同一个锁对象调用
        synchronized (lock){
          System.out.println("线程2 开始唤醒：" + System.currentTimeMillis());
          lock.notify();// 唤醒在lock锁对象上等待的线程
          System.out.println("线程2 结束唤醒： " + System.currentTimeMillis());
        }
      }
    });

    t1.start(); // 开启t1线程
    Thread.sleep(3000); // 为了确保wait在notify之前执行
    t2.start(); // 开启t2线程
  }
}
