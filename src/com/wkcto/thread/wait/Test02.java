package com.wkcto.thread.wait;

// wait使得线程等待，必须放到同步代码块中，通过同步代码块的锁对象调用
public class Test02 {
  public static void main(String[] args) {

    try {
      String text = "wkcto";
      System.out.println("同步前的代码..");
      synchronized (text){
        System.out.println("同步代码块开始。。。");
        text.wait();// 调用wait方法后，当前线程就会释放锁对象；如果没有人唤醒，它就一致处于等待状态
        System.out.println("wait后面的代码。。。。。");
      }
      System.out.println("同步代码块后面的代码");
    }catch (InterruptedException e){
      e.printStackTrace();
    }
    System.out.println("main线程后面的代码块");
  }
}
