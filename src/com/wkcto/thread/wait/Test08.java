package com.wkcto.thread.wait;

//notify 通知过早
public class Test08 {
  public static void main(String[] args) {
    final Object obj = new Object();
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (obj){
          try {
            System.out.println("begin wait ...");
            obj.wait();
            System.out.println("wait end ..");
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    });
    Thread t2 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (obj){
          System.out.println("begin notify...");
          obj.notify();
          System.out.println("end notify...");
        }
      }
    });

    t2.start();
    t1.start();
  }
}
