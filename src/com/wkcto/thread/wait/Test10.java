package com.wkcto.thread.wait;

import com.aspose.pdf.StampIcon;

import java.util.ArrayList;
import java.util.List;

//wait条件发生变化
// 定义一个集合，定义一个线程往里添加数据，添加完成后通知另外一个线程取数据
// 定义一个线程从集合中取数据，如果集合中没有数据则等待
public class Test10 {
  public static void main(String[] args) {
    // 创建线程类对象
    ThreadAdd threadAdd = new ThreadAdd();
    ThreadSubstract threadSubstract = new ThreadSubstract();
    threadSubstract.setName("substract 1");

    // 测试1：先开启添加数据线程，然后开启取数据线程，大部分情况下，执行时ok。有时候stract执行会更快更早
//    threadAdd.start();
//    threadSubstract.start();

    // 测试2：先开启取数据线程，再开启添加数据线程，取数据线程会先等待，等到添加数据后，再唤醒取数据
//    threadSubstract.start();
//    threadAdd.start();

    // 模式3：开启两个线程取数据，再开启添加数据的线程
    // 抛出异常 java.lang.IndexOutOfBoundsException
    /*分析：
        threadSubstract线程先启动，取数据时，因为集合中没有数据，进入分支，并wait等待
        threadAdd线程第二个启动，添加数据，并threadSubstract线程唤醒
        但是threadSubstract没有立即获得CPU执行权，而是threadSubstract2先获得了执行权
        threadSubstract2未进入分支，直接取出数据
        threadSubstract获得了执行权，进入wait后的执行代码，取数据时发生下标超界异常
    * */
    /*解决方案：
        当等待的线程被唤醒后，再判断一次集合中是否有数据可以取。即需要吧substract方法中的if判断改为while
    * */
    ThreadSubstract threadSubstract2 = new ThreadSubstract();
    threadSubstract2.setName("substract 2");
    threadSubstract.start();
    threadSubstract2.start();
    threadAdd.start();


  }

  static List list = new ArrayList<>();

  // 定义方法，从集合中取数据
  public static void substract(){
    synchronized (list){
      if(list.size() == 0){
        try {
          System.out.println(Thread.currentThread().getName() + " begin wait... ");
          list.wait();
          System.out.println(Thread.currentThread().getName() + " end wait.");
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      // 唤醒后，list里至少有一个元素了
      Object data = list.remove(0); // 从集合中取出一个数据
      System.out.println(Thread.currentThread().getName() + "从集合中取出一个数据："+ data
          + "后，集合中的数据量："+list.size());



    }

  }

  // 定义一个添加数据的方法，添加数据后，通知等待的线程取数据
  public static void add(){
    synchronized (list){
      list.add("data");
      list.notifyAll();
    }
  }

  //定义一个线程类调用add方法
  static class ThreadAdd extends  Thread{
    @Override
    public void run() {
      add();
    }
  }

  // 定义一个线程类调用substract方法
  static class ThreadSubstract extends Thread{
    @Override
    public void run() {
      substract();
    }
  }
}
