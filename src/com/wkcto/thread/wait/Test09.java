package com.wkcto.thread.wait;

//notify 通知过早，就不让线程等待了
public class Test09 {
  static boolean isFirst = true;  // 定义静态变量是否第一个运行的线程标志
  public static void main(String[] args) {
    final Object obj = new Object();
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (obj){
          while (isFirst){
            try {
              System.out.println("begin wait ...");
              obj.wait();
              System.out.println("wait end ..");
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

        }
      }
    });
    Thread t2 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (obj){
          System.out.println("begin notify...");
          obj.notify();
          System.out.println("end notify...");
          isFirst = false;
        }
      }
    });

    t2.start();
    t1.start();
  }
}
