package com.wkcto.thread.wait;

// Interrupt会中断线程wait()方法
public class Test05 {
  public static void main(String[] args) throws InterruptedException {
    SubThread t = new SubThread();
    t.start();

    Thread.sleep(2000);
    t.interrupt();

    //LOCK.notify();// 异常发生后，就是线程终止了，notify已经没有作用了
  }
  private static final Object LOCK = new Object(); // 定义常量为锁对象

  static class SubThread extends Thread{
    @Override
    public void run() {
      synchronized (LOCK){
        try {
          System.out.println("begin wait ...");
          LOCK.wait();
          System.out.println("end wait.");
        } catch (InterruptedException e) {
          System.out.println("wait 等待被中断时抛出异常。。。" );
          e.printStackTrace(); //java.lang.InterruptedException
        }
      }
    }
  }
}
