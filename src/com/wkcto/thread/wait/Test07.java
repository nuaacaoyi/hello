package com.wkcto.thread.wait;

import javax.sound.midi.Soundbank;

//
public class Test07 {
  public static void main(String[] args) {
    final Object obj = new Object();
    Thread t = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (obj){
          try {
            System.out.println("Thread begin wait...");
            obj.wait(5000);
            System.out.println("end wait");
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
    });

    t.start();
  }
}
