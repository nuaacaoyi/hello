package com.wkcto.thread.wait;

import java.util.ArrayList;
import java.util.List;

// notify 和wait必须放在 同步代码块中， 且必须由同一个锁对象调用
public class Test04 {
  public static void main(String[] args) {
    // 定义一个list作为锁对象
    List<String> list = new ArrayList<>();

    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (list){
          if ((list.size() != 5)){// 注意，这里如果用while的化，必须保证执行完代码后条件不能进入循环！！！
            System.out.println("线程1开启等待： "+ System.currentTimeMillis() );
            try {
              list.wait();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            System.out.println("线程1 被唤醒： " + System.currentTimeMillis());
          }
        }
      }
    });

    Thread t2 = new Thread(new Runnable() {
      @Override
      public void run() {
        synchronized (list){
          for (int i = 0; i < 10; i++) {
            list.add("123");
            System.out.println("线程2添加第 "+ (i+1) + "个元素");
            if(list.size() == 5 ){
              list.notify();
              System.out.println("发出通知， 唤醒线程1");
            }
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }

          }
        }

      }
    });

    t1.start();
    try {
      Thread.sleep(500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    t2.start();
  }
}
