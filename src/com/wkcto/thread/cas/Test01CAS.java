package com.wkcto.thread.cas;

/**
 * @Author caoyi
 * @Create 2022-10-14-15:20
 * @Description: TODO
 */
// 使用CAS算法实现一个线程安全的计数器
public class Test01CAS {
  public static void main(String[] args) {
    CASCounter casCounter = new CASCounter();
    for (int i = 0; i < 1000; i++) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          System.out.println(casCounter.incrementAndGet());
        }
      }).start();
    }
  }
}

class CASCounter{
  // volatile实现线程可见
  volatile private long value;

  public long getValue() {
    return value;
  }

  public void setValue(long value) {
    this.value = value;
  }

  // 定义compare and swap方法
  private boolean compareAndSwap(long expectedValue, long newValue){
    // 如果当前value的值与期望值相同，就把当前value替换为 newValue值
    synchronized (this){
      if(value == expectedValue){
        value = newValue;
        return true;
      }
      else {
        return false;
      }
    }
  }

  // 定义自增的方法
  public long incrementAndGet(){
    long oldValue;
    long newValue;
    do{
      oldValue = value;
      newValue = oldValue+1;
    }while (!compareAndSwap(oldValue, newValue));
    return newValue;
  }
}
