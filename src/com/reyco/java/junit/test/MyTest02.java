package com.reyco.java.junit.test;

import org.junit.Test;

/**
 * @author caoyi
 * @create 2020-05-01 13:41
 * 可以先写类，然后再输入@，提示形成导包和代码
 */
public class MyTest02 {

  @Test
  public void test01(){
    System.out.println("test01");
  }

}
