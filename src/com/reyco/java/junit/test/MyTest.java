package com.reyco.java.junit.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author caoyi
 * @create 2020-05-01 9:51
 */
public class MyTest {
  @Before
  public void setUp() throws Exception{//setup方法是所有测试方法的执行前置，所以这里一般都存放初始化代码
    System.out.println("Setup被执行 ");
  }

  @After
  public void tearDown() throws Exception{//teardown方法是所有测试方法的执行后置，所以这里一般存放资源释放代码

  }

  //注解
  @Test
  public void test01(){
    System.out.println("111执行测试方法 test0101 ");
  }

  @Test
  public void test02(){
    System.out.println("222执行测试方法 test222 ");
  }
}
