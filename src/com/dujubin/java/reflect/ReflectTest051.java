package com.dujubin.java.reflect;

/**
 * @author caoyi
 * @create 2020-04-30 22:18
 */
public class ReflectTest051 {
  public static void main(String[] args) {

  }
}


class CustomerService
{
  public CustomerService() {
  }

  //登录方法
  public boolean login(String name,String pwd){
    if("admin".equals(name) && "123".equals(pwd)){
      return  true;
    }
    return false;
  }

  //退出
  public void logout(){
    System.out.println("系统已安全退出");
  }
}