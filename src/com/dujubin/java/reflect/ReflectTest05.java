package com.dujubin.java.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.*;

/**
 * @author CY_JFXX
 * @create 2020-02-21 11:39
 * 反编译获取类属性
 */
public class ReflectTest05{
    public static void main(String[] args)  throws Exception{
        //创建个对象，对象代表 目标类
        Class c=  Class.forName("java.util.Date");
        //Field[] fs=c.getFields();//只能获取 public类型的属性
        //System.out.println(fs.length);
        //System.out.println(fs[0].getName());

        //如果要获取所有的属性
        Field[] fs1=c.getDeclaredFields();

        StringBuffer sb=new StringBuffer();
        sb.append(Modifier.toString(c.getModifiers())+" class "+c.getSimpleName()+" {\n");//获取类的修饰符  类名

        for(Field fl:fs1){
            Class type = fl.getType();//获取属性的类型

            int i=fl.getModifiers();
            String strM=Modifier.toString(i);//获取属性的修饰符
            sb.append("\t");
            sb.append(strM+" ");
            sb.append(type.getSimpleName()+" ");
            sb.append(fl.getName()+";\n");//获取属性的名字

        }
        sb.append("}");
        System.out.println(sb);



        Field f = c.getDeclaredField("cdate"); //获取指定名字的一个属性，可以用于对目标类的实例进行 set 和get
        //想要操作属性，必须先有对象
        Object o = c.newInstance();
        //f.set(o,"110"); //给o对象的 cdate 属性赋值 110
        System.out.println(f.get(o));//私有的属性无法访问
        //打破封装
        //f.setAccessible(true);//可以打破封装，访问私有数据，但是会导致Java对象的属性不安全

    }
}