package com.dujubin.java.reflect;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author CY_JFXX
 * @create 2020-02-21 12:44
 *
 *
 * 通过某个特定的方法，通过反射机制执行
 * 尝试调用length()方法
 */
public class ReflectTest07 {

    public static void main(String[] args) {
        String a="abcdefghskj";
        Method m1;
        Method m2;
        Method m4;
        Constructor m3;
        Class c;
        //1.获取类
        try {
            c = Class.forName("java.lang.String");
            //2.获取某个特定方法
            //由于同一个类里面方法可以重名，所以需要通过两个参数获取一个特定方法，
            m1=c.getDeclaredMethod("length");//length()方法没有参数
            m2=c.getDeclaredMethod("lastIndexOf", String.class);//lastIndexOf有一个参数，参数类型是String
            m4 = c.getDeclaredMethod("lastIndexOf", String.class,int.class); //获取有两个参数（String,int)  的lastIndexOf方法

            m3=c.getConstructor(String.class);//获取构造方法

            //3.通过反射机制执行这个方法
            Object o=c.newInstance();
            Object re= m1.invoke(o);  //打印 0  m1代表的是哪个方法  o是对象
            Object rr=m2.invoke(o,"a"); // 打印 1   调用o对象的m2方法（lastIndexOf），，传入 "a"参数，返回值是 rr对象
            System.out.println(re.toString()+"--"+rr.toString());

            //4.通过反射机制执行构造方法实例化对象
            Object ab=m3.newInstance("abcdef");
            System.out.println(ab);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }


}
