//package com.dujubin.java.reflectReview;

/**
 * @author CY_JFXX
 * @create 2020-02-20 19:55
 */
public class ReflectTest02 {
    public static void main(String[] args)  throws Exception{
		//这句话其实是将A.class装载到JVM的过程  此时会 打印  A...
        //Class.forName("A");
		
		//这句话不会执行A类的静态语句块
       Class c = A.class;
        //Class c=a.getClass();
        //System.out.println("aaaa");
    }
}

class A
{
    static{//类加载到JVM会执行静态语句块
        System.out.println("A....");
    }
}