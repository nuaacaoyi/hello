package com.dujubin.java.reflectReview;

import java.util.Date;

/**
 * @author caoyi
 * @create 2020-04-30 20:22
 * java.lang.Class类的使用   创建对象的三种方式
 * 总结： c1 c2 c3代表 Employee类型；   c4 c5 c6代表Date类型    c7代表int类型
 *
 */
public class ReflectTest01 {
  public static void main(String[] args) throws ClassNotFoundException {
/*    //第一种方式
    Class c1 = Class.forName("Employee"); //c1引用保存内存地址指向堆中的对象，该对象代表的是Employee整个类    再次强调：c1是一个对象，该对象代表一个类

    //第二种方式：  java中每个类型（包括基础上数据类型 和 引用数据类型） 都有class属性
    Class c2 = Employee.class;

    //第三种方式: Object祖类中有个 getClass()方法。  任何一个java对象都有getClass方法  作用：返回此对象的运行时类
    Class c3 = (new Employee()).getClass();//c3 就是 new Employee() 对象的 运行时类
    System.out.println(c1==c2);
    System.out.println(c1==c3);*/


    Class c4 = Date.class;
    Class c5 = Class.forName("java.util.Date");
    Class c6 = (new Date()).getClass();
    System.out.println(c4==c5);//true
    System.out.println(c4==c6);//true  说明 c4  c5  c6的内存地址是相同的，指向堆中唯一的一个对象（可能是单例模式？）

    //c7代表 int 类型
    Class c7 = int.class;
  }
}
