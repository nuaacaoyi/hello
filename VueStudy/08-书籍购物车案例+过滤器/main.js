
 
  const app = new Vue({
    el: '#app',
    data: {
      books: [
        {
          id:1,
          name:'《算法导论》',
          date: '2006-9',
          price:85.00,
          count:1
        },
        {
          id:2,
          name:'《UNIX编程艺术》',
          date:'2006-2',
          price:59.00,
          count:1
        },
        {
          id:3,
          name:'《编程珠玑》',
          date:'2008-10',
          price:44.00,
          count:1
        },
        {
          id:4,
          name:'《代码大全》',
          date:'2006-3',
          price:128.00,
          count:1
        }
      ]
    },
    computed:{
      totalPrice(){
        let sum=0;
        //方式1
        // for(let i=0;i<this.books.length;i++){
        //   sum += this.books[i].price*this.books[i].count;
        // }
        //方式2
        // for(let i in this.books){//此时i是一个索引值
        //   sum += this.books[i].price*this.books[i].count;
        // }
        //方式3
        // for(let i of this.books){
        //   sum += i.price * i.count;
        // }
        //方式4 高阶函数reduce
        sum= this.books.reduce(function(preValue,book){
          return preValue+book.price*book.count;
        },0);

        return sum;
      }
    },
    filters:{//过滤器
      priceFormat(price){
        return '￥'+price.toFixed(2);
      }
    },
    methods:{
      increment(index){
        this.books[index].count++;
      },
      decrement(index){ 
        this.books[index].count--;
      },
      removeHandle(index){
        this.books.splice(index,1);
      }
    }
  }); 