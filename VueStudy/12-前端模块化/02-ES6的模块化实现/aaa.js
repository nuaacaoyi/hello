var name = '小明';
var age = 18;
var flag = true;

function sum(num1,num2){
  return num1+num2;
}

if(flag){
  console.log(sum(20,30) );
}

export default{}

//1.导出方式一：  可以存在多个 export关键字
export{
  flag,
  sum
}

//2.导出方式二：
export var num = 1000;
export var obj = '小明';

//3.导出方式三：
export function mul(num1,num2){
  return num1 * num2
}

//4.导出类
export class Person{
  constructor(name,age){
    this.name=name;
    this.age = age;
  }
  run(){
    console.log(this.name + '在奔跑');
  }
}

//5. export default 当需要使用者自己命名时，用这个
const address = '北京市';
export default address;// 一个js中只能有一个 default

