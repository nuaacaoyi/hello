前端全局变量使用复杂
INDEX的引用顺序也有强制性，必须按照某个先后顺序才能执行；多人开发时很难保障这些顺序正常。
此时可以通过 匿名函数（闭包函数）的方式解决，就是利用函数自己的作用域，如aaa.js ,但是小明在 mmm.js 也无法使用 flag了，代码复用性又差了
  //小明写了  aaa.js
(function(){
  var name='小明';
  var age=22;

  function sum(num1,num2){
    return num1+num2;
  }

  var flag=true;
  if(flag){
    console.log(sum(10,20));
  }
})()


要想复用 ,模块化:
var moduleA = (function(){
  //导出对象
  var obj={};

  var name='小明';
  var age=22;

  function sum(num1,num2){
    return num1+num2;
  }

  var flag=true;
  if(flag){
    console.log(sum(10,20));
  }

  obj.flag=flag;
  obj.sum=sum;

  return obj;
})()

