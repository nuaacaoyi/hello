//该文件用于封装 axios
//在使用第三方工具时，一定要封装，在其他开发过程中使用工具，一定是使用自己封装的接口。  以避免三方工具万一倒闭（或者要更换三方工具），而带来的 重构灾难
import Axios from "axios";

export function request1(config,success,failure) { //success 和 failure都是函数
  //1.创建axios的实例
  const instance = Axios.create({
    baseURL:'http://123.207.32.32:8000',
    timeout:5000
  })
  //2发送网络请求
  instance(config)
    .then(res => {
      success(res)
    })
    .catch(err => {
      failure(err)
    })
}


export function request2(config) {  //第二种写法    Promise方式
  return new Promise((resolve,reject) => {
    //1.创建axios的实例
    const instance = Axios.create({
      baseURL:'http://123.207.32.32:8000',
      timeout:5000
    })
    //2发送网络请求
    instance(config)
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}


export function request3(config) {  //第三种写法  更简单   Promise方式
  return new Promise((resolve,reject) => {
    //1.创建axios的实例
    const instance = Axios.create({
      baseURL:'http://123.207.32.32:8000',
      timeout:5000
    })

    //3. 发送网络请求
    return instance(config)    //instance函数本身就是返回 Promise对象

  })
}


export function request4(config) {  //第三种写法  更简单   Promise方式
  return new Promise((resolve,reject) => {
    //1.创建axios的实例
    const instance = Axios.create({
      baseURL:'http://123.207.32.32:8000',
      timeout:5000
    })
    //2.axios的拦截器
    /*请求拦截的作用：1.请求信息不符合服务器要求需要做加工；2.比如我们每次发送网络请求时，都希望在界面中显示 转圈圈；
                     3.某些网络请求（比如登录（token） ），必须携带一些特殊信息
    * */
    instance.interceptors.request.use(config =>{ //第一个参数是请求成功时拦截器的回调函数；第二个参数时请求失败时的回调函数
        console.log(config);
        return config  //为什么要加config，如果不加，就相当于config被拦截掉了，请求继续下去根本不会有信息，加上return config，请求继续，携带的还有config
      },
      err =>{
        console.log(err);
        //return Promise.reject(err)
      })

    instance.interceptors.response.use(res =>{
        console.log(res);//一般响应拦截是需要 从 res中拿出 data做处理
        return res    //同样的，响应结果也需要返回出去
      },
      err =>{
        console.log(err);
      })

    //3. 发送网络请求
    return instance(config)    //instance函数本身就是返回 Promise对象

  })
}
