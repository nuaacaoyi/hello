import Vue from 'vue'
import App from './App'
import router from './router'

import Axios from "axios";
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
Axios.defaults.baseURL = 'http://123.207.32.32:8000'
Axios.defaults.timeout = 5000

Axios({  //全局配置，代码复用
  url: '/home/multidata', //系统会默认使用default配置
  method: 'get'

}).then(res => {
  console.log(res);
});
Axios({  //参数config是个对象  默认发送get请求 axios的基本使用
  url: 'http://123.207.32.32:8000/home/multidata',
  method: 'get'   //没有这句话时时默认get请求，如果想要post请求，就填写 post

}).then(res => {
  console.log(res);
});

Axios({  //参数config是个对象  默认发送get请求 axios的基本使用
  url: 'http://123.207.32.32:8000/home/data',
  params: {   //等同于 url: http://123.207.32.32:8000/home/data?type=pop&page=1
    type:'pop',
    page: 1
  },
  method: 'get'   //没有这句话时时默认get请求，如果想要post请求，就填写 post

}).then(res => {
  console.log(res);
});


//axios并发请求，两个请求都得到后再处理
Axios.all([Axios({
  url: 'http://123.207.32.32:8000/home/data'
}),Axios({
  url: 'http://123.207.32.32:8000/home/data',
  params: {   //等同于 url: http://123.207.32.32:8000/home/data?type=pop&page=1
    type:'sell',
    page: 1
  },
  method: 'get'   //没有这句话时时默认get请求，如果想要post请求，就填写 post
})]).then(results =>{
  console.log(results[0]);
  console.log(results[1]);
})

//Axios算是一个静态类？  如果想要有不同的服务器访问，最好将 Axios实例化，不同的url建立不同的Axios实例
const instanceA = Axios.create({
  baseURL: 'http://123.207.32.32:8000',
  timeout: 5000
})
instanceA({
  url: '/home/multidata'
}).then(res => {
  console.log('实例化后的请求');
  console.log(res);
})


//使用封装的axios模块进行网络请求
import {request1} from "./network/request";

request1({url:'/home/multidata'},  //第一个参数 config
  res => {     //第二个参数  success函数
    console.log(res);
  },
  err => {   //第三个参数 failure函数
    console.log(err);
  })


import {request3} from "./network/request";//Promis的方式进行封装
request3({
  url:'/home/multidata'
}).then(res => {
  console.log(res);
}).catch(err => {
  console.log(err);
})


import {request4} from "./network/request";//Promis的方式进行封装
request4({
  url:'/home/data'
}).then(res => {
  console.log(res);
}).catch(err => {
  console.log(err);
})
