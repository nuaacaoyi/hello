// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

console.log(App);//import  App对象时， vue-template-compiler 工具已经将 App的template解析为了 render
/* eslint-disable no-new */
/*const cpn={
  template: '<div>{{message}}</div>',
  data(){
    return{
      message: '我是子组件，啦啦啦'
    }
  }
}*/
new Vue({
  el: '#app',
  /* 创建项目选择  runtime + compiler时，代码情况，较为标准，但体积大，效率会稍微第一点
  components: { App },
  template: '<App/>'*/
  //创建项目runtime-only时，使用箭头函数，其本质时调用  createElement函数
  render: function (h) {
    return h(App)
  }


 /* render: function (createElement) {  //本质 createElement函数
    //1.createElement('标签','标签属性',['内容',其他元素等])
    //return createElement('h2',{class: 'box'},['HelloWorld！']);//<h2 class: 'box'>HelloWorld！</h2> 会将 <div id='app'></div>整个替换掉
    return createElement('h2',
        {class: 'box'},
        ['Hello World',createElement('button',['按钮'])]
      )
  }*/
  // //传入内置组件
  // render: function (createElement) {
  //   return createElement(cpn) //自定义组件 cpn    不能有引号
  // }
  //传入外部组件
 /* render: function (createElement) {
    return createElement(App) //自定义组件 cpn    不能有引号
  }*/


})
