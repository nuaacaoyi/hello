import {request} from './request'
//该文件的存在，目的是为了 对request进行再一次封装，将 url封装好，home页面多次调用都只是调用函数，如果后期home页面的url变更，也只用变一下这里

export function getHomeMultiData() {
  return request({
    url:'/home/multidata'
  });
}

export function getHomeGoodsData(type,page) {
  return request({
    url: '/home/data',
    params:{
      type,
      page
    }
  })
}
