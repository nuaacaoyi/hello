export function debounce(func,delay) {//封装一个防抖函数，避免频繁的刷新
  let timer = null
  return function (...args) {
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);//args是函数自有属性，里面存的是 执行函数时传入的参数；   这里其实可以不填写，因为refresh函数不需要入参
      //this的作用时 将 this.$refs.scroller.scRefresh函数赋给  this对象即Home，允许Home对象执行 refresh函数
    }, delay)
  }
}
//算法十分精巧 ，假设得到的时间时 3时4分5秒，需要补0
function padLeftZero(str) {
  return ("00" + str).substr(str.length);
}

// 时间格式化   一般情况下服务器返回时间时，不会给客户端直接返回想要的格式
export function formatDate(date, fmt) {
  //获取年份
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  //获取其他
  let o = {
    "M+": date.getMonth() + 1,
    "d+": date.getDate(),
    "h+": date.getHours(),
    "m+": date.getMinutes(),
    "s+": date.getSeconds()
  };

  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + "";
      fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str));
    }
  }

  return fmt;
}
