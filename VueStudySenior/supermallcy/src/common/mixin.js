
import BackTop from "components/content/backTop/BackTop";  //详情页也需要用到 回到顶部小图标， 使用混入技术，实现代码共用
import {debounce} from "common/utils";

export const itemListener = {
  data(){
    return{
      itemImgListener:null,
      newRefresh: null
    }
  }
  ,
  mounted(){
    this.newRefresh = debounce(this.$refs.scroller.scRefresh, 200) //第一个参数是函数名，不要加（），（）加括号这语句就是执行函数
    //对监听的事件进行保存
    this.itemImgListener = () => {
      this.newRefresh();
    }
    this.$bus.$on('itemImgLoaded',this.itemImgListener)
    //console.log('混入的内容');
  }
}


export const backTopMixin = {
  components:{
    BackTop
  },
  data(){
    return{
      isShowBackTop: false
    }
  },
  methods: {
    backTopClick() {
      // 通过$refs拿到组件中的对象
      this.$refs.scroller.scrollTo(0, 0, 500) //前两个参数是x,y坐标，第三个参数是 返回顶部的速度  单位毫秒   这里调用的是 Scroller封装好的函数
    },
    listenShowBack(position){
      this.isShowBackTop = (-position.y) > 500
    }
  }
}
