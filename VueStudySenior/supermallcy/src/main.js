import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store";

import myToast from "./components/common/toast";

Vue.config.productionTip = false

Vue.prototype.$bus = new Vue();//为Vue原型增加 新属性 $bus， 用于接收和触发总线事件，用于 goodslistItem 向 Home发送数据

//安装自定义的toast插件
Vue.use(myToast)//本质上时调用install函数

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
