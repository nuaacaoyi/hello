import {
  ADD_COUNTER,
  ADD_TO_CART,
  CLEAR_CART_LIST,
  SET_CART_LIST,
  SET_LOADING,
  SET_TABBAR_SHOW
} from "./types";

export default {//mutations唯一的目的是修改state中的状态，   mutation中的每个方法需要尽可能完成的事件比较单一，不然不方便跟踪调错
  // 设置tabBar显示和隐藏
  [SET_TABBAR_SHOW](state, bol) {
    state.tabBarShow = bol;
  },
  // 设置请求加载
  [SET_LOADING](state, bol) {
    state.isLoading = bol;
  },
  addCounter(state,payload){
    payload.count++
  },
  addToCart(state,payload){
    state.cartList.push(payload)
  }
  // addCart(state,payload){
  //   //需要判断数组里是否已有 该元素
  //   // let product = state.cartList.find((item) => {
  //   //   return item.iid === payload.iid
  //   // })
  // find函数可以用于查找某一个元素   判断条件就是 箭头函数内容
  //   let oldproduct = state.cartList.find(item => item.iid === payload.iid  )//箭头函数的简便写法
  //   if(oldproduct){
  //     oldproduct.count += 1;
  //   }else{
  //     payload.count = 1;
  //     state.cartList.push(payload)
  //   }
  // }
}
