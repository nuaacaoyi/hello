export  default {
  addCart(context,payload){
    return new Promise((resolve,reject) => {
      let oldproduct = context.state.cartList.find(item => item.iid === payload.iid  )//箭头函数的简便写法
      if(oldproduct){
        context.commit('addCounter',oldproduct)//按照规范 addCounter这里应该定义为常量
        resolve('当前的商品数量+1')
      }else{
        payload.count = 1;
        payload.checked = true; //默认商品购物车为选中状态
        context.commit('addToCart',payload)
        resolve('添加新商品')
      }
    })

  }
}
