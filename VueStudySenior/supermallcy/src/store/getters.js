export  default {
  cartLength(state) {
    return state.cartList.length;
  },
  getCartList(state) {
    return state.cartList;
  },
  getTotal(state){
    return '￥'+state.cartList.filter(item => {
      return item.checked
    }).reduce((preValue,item) => {
      return preValue + item.price * item.count
    },0).toFixed(2)
  },
  getChecked(state){
    return state.cartList.filter(item => {
      return item.checked
    }).length
  }
}
