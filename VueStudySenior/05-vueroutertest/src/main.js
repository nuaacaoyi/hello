import Vue from 'vue'
import App from './App'
import router from './router'//语法糖？  如果这里导入填写的是一个目录（directory），它会自动寻找index

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
