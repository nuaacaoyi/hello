import Vue from 'vue'
import Router from 'vue-router'
import HomeNews from "../components/HomeNews";
import HomeMessage from "../components/HomeMessage"; //导入路由，这个是自动安装的
//import HelloWorld from '@/components/HelloWorld'
/*常规加载方式，访问时一次性加载
import Home from "../components/Home";
import About from "../components/About";
import User from "../components/User";*/
//懒加载方式，懒加载的方式可以使得项目第一次访问时不必一次性的 响应过多的资源
const Home = () => import("../components/Home")
const About = () => import("../components/About")
const User = () => import("../components/User")
const Profile = () => import("../components/Profile")
const TestWebgl = ()=> import("../components/TestWebGL")
//1.通过Vue.use传入路由对象(或者其他插件)，  Vue.use(plugin: Router), 安装插件
Vue.use(Router)

//增强写法
const routes = [
  {
    path: '/',  //特殊的路径放到最前面
    redirect:'/home'
  },
  {
    path:'/webgl',
    name:'Webgl',
    component: TestWebgl
  },
  {
    path: '/home',
    name: 'Home',
    meta:{
      title: '首页'
    },
    component: Home,
    children:[

      {
        path:'news',  //不需要加斜杠
        component:HomeNews
      },
      {
        path:'message',  //不需要加斜杠
        component:HomeMessage
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    meta:{
      title: '关于'
    },
    component: About,
    beforeEnter:(to,from,next) => {
      //console.log('beforeEnter');
      next()
    }
  },
  {
    path: '/user/:userId',  //如果需要在url中添加参数
    name: 'User',
    meta:{
      title: '用户'
    },
    component: User
  },
  {
    path:'/profile',
    meta:{
      title: '档案'
    },
    component: Profile
  }
]
/*
//2. 创建VueRouter对象
const router1 = new router({
  routes
})
//3.将router对象导出  传给 vue实例
export default router1
*/
 const router = new Router({
  routes,
  mode: 'history'    //模式，设置uri为hash还是history的方式
})
export default router

//前置钩子，就是跳转之前回调函数    回调函数 <=> 钩子
router.beforeEach((to,from,next) =>{
  //document.title = to.meta.title;//取到 to这个route的 meta的title
  document.title = to.matched[0].title;//当默认路径和具体路径重叠 或 路由嵌套 时？
  // to这个route会有两个，成为数组，所以用上面的方法只能取到 undefined

  next();  //这一步必须有，没有这一步，前端页面就不会发生跳转
})

//系统自动创建的写法
/*export default new Router({
  //routes属性，配置  路由和组件的 映射关系
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})*/
