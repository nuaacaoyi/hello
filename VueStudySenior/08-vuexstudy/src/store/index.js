import Vue from 'vue'
import Vuex from 'vuex'
import {
  INCREMENT
} from './mutations-types'
//1.安装插件  use会执行install代码
Vue.use(Vuex)

//创建module对象
const moduleA = {
  state: {
    name: 'zhangsan'
  },
  mutations: {},
  getters:{
    fullname3(state,getters,rootstate){//在子module中要想调用根的 state，就通过第三个参数
      return 123;
    }
  }
}

//2.创建对象
    //单一状态树概念， 再创建store时，建议只 创建一个 store，方便以后的管理和维护。因此所有的需要管理的信息都在一个store里
const store = new Vuex.Store({
  state: {
    counter:1000,
    students:[
      {id:110,name:'why',age:18},
      {id:111,name:'koby',age:24},
      {id:112,name:'james',age:25},
      {id:113,name:'caoyi',age:30},
    ],
    info:{
      name: 'kobe',
      age: 40,
      height: 1.98
    }
  },
  mutations: {
    //方法  increment叫做事件类型，后面的不分叫做回调函数
    //为了防止 App调用方法与此地不一致导致调用失败，所以设置一个常量，存放于 mutations-types.js中，两个地方都用这个常量列表的名字
    [INCREMENT](state){  //默认有个参数state，其对应的上面state对象，这个参数可以不用写
      state.counter++
    },
    decrement(a){
      a.counter--
    },
    incrementCount(state,count){  //参数被称为 payload，是mutation的载荷
      state.counter += count
    },

    addStu(state,stu){
      state.students.push(stu)
    },
    updateInfo(state){
      //state.info.name ='codrewhy'//state里的内容时响应式的,执行到这里时，页面上内容会直接发生变化
      //state.info['address']='L.A.'//不会变化，因为响应式的前提必须时针对的 提前声明好的 应该按照下面的方式写

      Vue.set(state.info,'address','L.A.')//响应式

      //delete state.info.age //页面不会直接变化，这个方法也不是响应式的
      Vue.delete(state.info,'age')//响应式

      // setTimeout(() => {//异步调用导致 devtools无法记录mutation变更过程，此时需要把updateInfo放在 actions中
      //   state.info.name ='codrewhy'
      // },1000)
    }
  }, //mutation或者其他属性中内容过多时不太好看，可以另建js文件，然后在这里导入import
  actions: {
    // aUpdateInfo(context,payload){//这里的默认参数不是state,是上下文，这里相当于 store
    //   setTimeout(() => {//异步调用导致 devtools无法记录mutation变更过程，此时需要把updateInfo放在 actions中
    //    // context.state.info.name ='codrewhy'   //这里调用相当于跳过 mutation ，直接修改参数，这样 的devtool同样不会记录
    //     context.commit('updateInfo');
    //     console.log(payload);
    //   },1000)
    // }
    aUpdateInfo(context,payload){
      return new Promise((resolve,reject) => {
        setTimeout(() => {//异步调用导致 devtools无法记录mutation变更过程，此时需要把updateInfo放在 actions中
              context.commit('updateInfo');
              console.log(payload);

              resolve('1111')
            },1000)  //then写在 App里调用dispatch那里
      })
    }
  },
  getters: {  //类似于单个组件的computed属性，当每次都需要获取 state信息的固定加工结果时
    powerCounter(state){   //增强写法   powerCounter:powerCounter(){}
      return state.counter * state.counter
    },
    more2Stu(state){
      return state.students.filter(s => s.age>=20)
    },
    more2StuLength(state,getters){//在getters内的方法中，存在第二个参数，这个参数就是 getters 对象
      return getters.more2Stu.length
    },
    moreAgeStu(state){//可以直接返回一个函数   在对象内不建议写箭头函数
      // return function (age) {
      //   return state.students.filter(s => s.age>=age)
      // }
      //箭头函数写法
      return age => {return  state.students.filter(s => s.age>=age)}
    },
    getAge(state){
      return function(name){
        let rname;
        for (let i = 0; i < state.students.length; i++) {
          if (state.students[i].name == name)
            return state.students[i].age
          else return ''
        }
      }
    }
  },
  modules: { //当vuex 中的state信息过多时，会导致可读性变差，所以可以搞多个模块，用于 store的分组，每个module又可以自己的state，mutations action等
    moduleA

  }
})

//3.导出store对象

export default store
