
import {request} from '../network/request'
//该文件的存在，目的是为了 对request进行再一次封装，将 url封装好，home页面多次调用都只是调用函数，如果后期home页面的url变更，也只用变一下这里



export function logCheck(username,password) {
  return request({
    url:'/api/login.do',
    method: 'POST',
    params:{
      username,
      password
    }
  })
}
