
import {request} from '../network/request'
//该文件的存在，目的是为了 对request进行再一次封装，将 url封装好，home页面多次调用都只是调用函数，如果后期home页面的url变更，也只用变一下这里



export function getUsers(data) {
  return request({
    url:'/api/send/compose.json',
    method: 'POST',
    headers:{
      'Content-Type': 'application/json;charset=UTF-8'//设置请求体的请求内容为json对象！！
    },
    data
  })
}



export function getAdmins(data) {
  return request({
    url:'/api/alladmin.do',
    method: 'POST',
    headers:{
      'Content-Type': 'application/json;charset=UTF-8'//设置请求体的请求内容为json对象！！
    },
    data
  })
}



export function getMenu(data) {
  return request({
    url:'/api/menu/tree.json',
    method: 'POST',
    headers:{
      'Content-Type': 'application/json;charset=UTF-8'//设置请求体的请求内容为json对象！！
    },
    data
  })
}
