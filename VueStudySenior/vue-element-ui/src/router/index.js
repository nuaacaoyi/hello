import Vue from 'vue'
import VueRouter from 'vue-router'


const Home = () => import('../views/Home')
const Login = () => import('../views/Login')
const Users = () => import('../views/Users')
const Admin = () => import('../views/Admin')
const Tree = () => import('../views/Tree')
Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/users',
    name: 'users',
    component: Users
  },
  {
    path: '/admins',
    name: 'admins',
    component: Admin
  },
  {
    path: '/tree',
    name: 'tree',
    component: Tree
  }
]

const router = new VueRouter({
  routes,
  // url模式  默认的hash模式可以避免 JS不运行？
  mode:'hash'
})

export default router
