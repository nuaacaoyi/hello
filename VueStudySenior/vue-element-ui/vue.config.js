//已解决跨域问题，现在使用mock假数据，暂时不适用后台自定义数据

const path = require('path')
//自定义配置文件，用于代理实现跨域问题
module.exports = {
  devServer: {
    // Paths
    host: 'localhost',
    port: 8080, //后端服务端口号     注意，这里是后端端口号！！！！！！！！！！
    /*https:false,// https:{type:Boolean}
    open:true,//配置自动启动浏览器*/
    //proxy: 'http://127.0.0.1:8080'  //配置跨域处理，只有一个代理
    proxy: {
      '/api': {
        target: 'http://localhost:8080/crowdfunding',//要访问的接口域名
        changeOrigin: true,//开启代理：在本地会创建一个虚拟服务端
                        //然后发送请求的数据，并同时接收请求的数据
                        //这样服务端和服务端进行数据交互就不会有跨域问题
        ws: true,
        pathRewrite:{
          '^/api':''
        }
      }
    }
  }
}
