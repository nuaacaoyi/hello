//webpack的配置文件  这样webpack的命令可以简单很多
const path = require('path');  //动态获取路径 node语法  必须有path包才能用

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.resolve(__dirname,'./dist'),  //这里必须是绝对路径  动态获取路径
        filename: 'bundle.js',
        publicPath: 'dist/'   //该属性用于打包url文件，所有的打包路径都会加上该dist
    },
    module:{
        rules:[
            {
                test: /\.css$/,
                use:['style-loader','css-loader'] //css-loader只负责加载，不负责解析，想要解析生效，还得再安装一个style-loader
                //style-loader负责将样式添加到dom中生效
                //webpack读取多个loader时是从右向左读取的
            },
            {
                test: /\.less$/,
                use:[{
                        loader: 'style-loader'
                    },
                    {
                        loader:'css-loader'
                    },
                    {
                        loader:'less-loader'
                    }] //css-loader只负责加载，不负责解析，想要解析生效，还得再安装一个style-loader

            },
            {//url-loader
                test: /\.(png|jpg|gif)$/,
                use:[
                    {
                        loader: 'url-loader',
                        options:{
                            //单位为byte
                            // 当加载的图片小于limit时，会将图片编译为 base64 的字符串形式
                            // 当加载的而图片大于 limit时，系统会使用  file-loader，这个loader不需要配置，只需要安装
                            limit: 110000,

                            //对 图片打包的图片的命名进行规范 无该属性时，会使用32位hashcode进行命名
                            name: 'img/[name].[hash:8].[ext]'
                        }
                    }
                ]
            },
            {//js文件从ES6转为 ES5
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use:{
                    loader: 'babel-loader',
                    options:{
                        presets:['es2015']
                    }
                }

            }
        ]
    }
}

