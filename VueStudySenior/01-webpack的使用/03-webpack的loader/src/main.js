const {add,multi} = require('./js/mathUtils'); //依赖1：commonjs模块化规范的代码，浏览器识别不了，需要webpack打包

console.log(add(20, 30));

console.log(multi(20, 30));

import {name,age,height} from "./js/info"; //依赖2：使用ES6模块化规范，浏览器识别不了，需要webpack打包

console.log(name);
console.log(age);
console.log(height);

//3.依赖CSS文件，以便于 webpack打包，但是webpack本身只能打包js，需要扩展功能loader
require('./css/normal.css');

//4.依赖less文件
require('./css/special.less');

document.writeln('<h2>你好啊，李银河！</h2>');
