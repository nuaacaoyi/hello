const {add,multi} = require('./js/mathUtils'); //commonjs模块化规范的代码，浏览器识别不了，需要webpack打包

console.log(add(20, 30));

console.log(multi(20, 30));

import {name,age,height} from "./js/info"; //使用ES6模块化规范，浏览器识别不了，需要webpack打包

console.log(name);
console.log(age);
console.log(height);

require('./css/normal.css');