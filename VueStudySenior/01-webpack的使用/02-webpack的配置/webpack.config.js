//webpack的配置文件  这样webpack的命令可以简单很多
const path = require('path');  //动态获取路径 node语法  必须有path包才能用

module.exports = {
    entry: './src/main.js',
    output: {
        path: path.resolve(__dirname,'./dist'),  //这里必须是绝对路径  动态获取路径
        filename: 'bundle.js'
    },
    module:{
        rules:[
            {
                test: /\.css$/,
                use:['style-loader','css-loader'] //css-loader只负责加载，不负责解析，想要解析生效，还得再安装一个style-loader
                //style-loader负责将样式添加到dom中生效
                //webpack读取多个loader时是从右向左读取的
            }
        ]
    }
}

