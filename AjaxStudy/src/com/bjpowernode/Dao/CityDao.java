package com.bjpowernode.Dao;

/**
 * @author cy_hnmx
 * @create 2020-03-24-11:16
 */
public interface CityDao {
    String getCityJson(String provinceCode);
}
