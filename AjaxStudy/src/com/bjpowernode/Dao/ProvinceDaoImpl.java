package com.bjpowernode.Dao;

import com.bjpowernode.utils.DBUtils;

import java.sql.*;

/**
 * @author cy_hnmx
 * @create 2020-03-24-8:30
 */
public class ProvinceDaoImpl implements ProvinceDao {
    private Connection conn;
    private Statement stmt;
    private PreparedStatement ps;
    private ResultSet rs;
    @Override
    public String getProvinceJson() {
        StringBuilder jsonString=new StringBuilder();

        String sql="select * from t_province";
        try {
            conn= DBUtils.getConnection();
            //ps=conn.prepareStatement(sql);
            stmt=conn.createStatement();
            //rs=ps.executeQuery();
            rs=stmt.executeQuery(sql);
            jsonString.append("[");
            while(rs.next()){
                jsonString.append("{\"provincecode\":\"");
                jsonString.append(rs.getString("code"));
                jsonString.append("\",\"provincename\":\"");
                jsonString.append(rs.getString("name"));
                jsonString.append("\"},");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                DBUtils.close(conn,stmt,rs);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return jsonString.substring(0,jsonString.length()-1)+"]";
    }
}
