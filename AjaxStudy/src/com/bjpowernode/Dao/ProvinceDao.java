package com.bjpowernode.Dao;

/**
 * @author cy_hnmx
 * @create 2020-03-24-8:29
 */
public interface ProvinceDao {
    String getProvinceJson();
}
