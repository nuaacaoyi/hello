package com.bjpowernode.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author CY_JFXX
 * @create 2020-03-23 20:05
 */
public class CheckUsernameAction extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //链接数据库  获取用户名  验证用户名是否存在 这里简单验证一下，不再链接数据库了
        String username=req.getParameter("username");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out=resp.getWriter();
        if("admin".equals(username)){
            //用户名不可用
            out.print("<font color='red'>用户名不可用</font>");
        }else{
            //用户名可用
            out.print("<font color='green'>用户名可用</font>");
        }

    }
}
