package com.bjpowernode.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-23 21:50
 */
public class CheckUserPost extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         String username=req.getParameter("username");
         String password=req.getParameter("password");

         StringBuilder json =new StringBuilder();
         if("admin".equals(username)&& "admin".equals(password)){
             HttpSession session=req.getSession();
             session.setAttribute("user",username);
             json.append("{\"success\":true}");
         }else{
             json.append("{\"success\":false}");
         }
         //响应json
        resp.setContentType("text/html;charset=utf-8");
         resp.getWriter().print(json);
    }
}
