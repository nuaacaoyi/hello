package com.bjpowernode.servlets;

import com.bjpowernode.Dao.ProvinceDao;
import com.bjpowernode.Dao.ProvinceDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author cy_hnmx
 * @create 2020-03-24-8:22
 */
public class Province extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProvinceDao pd=new ProvinceDaoImpl();
        String jsonString=pd.getProvinceJson();
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out=resp.getWriter();
        out.write(jsonString);
    }
}
