package com.bjpowernode.servlets;

import com.bjpowernode.Dao.CityDao;
import com.bjpowernode.Dao.CityDaoImpl;
import com.bjpowernode.Dao.ProvinceDao;
import com.bjpowernode.Dao.ProvinceDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author cy_hnmx
 * @create 2020-03-24-11:20
 */
public class City extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pcode=req.getParameter("pcode");
        CityDao cd=new CityDaoImpl();
        String jsonString=cd.getCityJson(pcode);
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out=resp.getWriter();
        out.write(jsonString);
    }
}
