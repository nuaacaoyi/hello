package com.atguigu.pattern.k_memento;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:38
 * @Description:
 * 游戏角色有攻击力和防御力，在大战 Boss 前保存自身的状态(攻击力和防御力)，
 * 当大战 Boss 后攻击力和防御力下降，从备忘录对象恢复到大战前的状态
 */
public class Client {
  public static void main(String[] args) {
    // 创建一个游戏角色
    GameRole gr = new GameRole();
    gr.setDef(100);
    gr.setVit(100);

    System.out.println("=====和BOSS大战前状态=====");
    gr.display();

    Caretaker ct = new Caretaker();
    ct.setMemento(gr.createMemento());

    System.out.println("=====和boss大战后=====");
    gr.setVit(30);
    gr.setDef(20);
    gr.display();

    System.out.println("=====恢复到原来状态======");
    gr.recoverFromMemento(ct.getMemento());
    gr.display();


  }
}
