package com.atguigu.pattern.k_memento.theory;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:53
 * @Description: TODO
 */
public class Client {
  public static void main(String[] args) {
    Originator originator = new Originator();

    Caretaker caretaker = new Caretaker();

    originator.setState("状态1：半血");
    caretaker.addMemento(originator.saveStateMemento());

    originator.setState("状态2：残血");
    caretaker.addMemento(originator.saveStateMemento());

    originator.setState("状态3：死亡");

    System.out.println("当前状态：" + originator.getState());

    // 还原状态1
    originator.getStateFromMemento(caretaker.get(0));
    System.out.println("还原后状态：" + originator.getState());

  }
}
