package com.atguigu.pattern.k_memento.theory;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:47
 * @Description: TODO
 */
public class Memento {
  private String state;

  public Memento(String state) {
    this.state = state;
  }

  public String getState() {
    return state;
  }
}
