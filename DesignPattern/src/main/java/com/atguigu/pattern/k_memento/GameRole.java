package com.atguigu.pattern.k_memento;

/**
 * @Author caoyi
 * @Create 2021-01-21-14:19
 * @Description: TODO
 */
public class GameRole {
  // 攻击力
  private int vit;
  // 防御力
  private int def;

  // 创建一个Memento 的方法
  public Memento createMemento(){
    return new Memento(vit,def);
  }

  // 根据memento恢复状态的方法
  public void recoverFromMemento(Memento me){
    this.def = me.getDef();
    this.vit = me.getVit();
  }

  // 显示当前游戏角色的状态
  public void display(){
    System.out.println("游戏角色当前状态：攻击力 " + this.vit +"; 防御力 "+this.def);
  }

  public int getVit() {
    return vit;
  }

  public void setVit(int vit) {
    this.vit = vit;
  }

  public int getDef() {
    return def;
  }

  public void setDef(int def) {
    this.def = def;
  }
}
