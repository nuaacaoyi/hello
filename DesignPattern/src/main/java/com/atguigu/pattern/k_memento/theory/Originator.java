package com.atguigu.pattern.k_memento.theory;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:46
 * @Description: TODO
 */
public class Originator {
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  private String state; // 状态信息

  // 编写一个方法，可以保存一个状态对象 Memento
  // 因此编写一个方法，返回一个memento对象
  public Memento saveStateMemento(){
    return new Memento(state);
  }

  // 恢复对象原本状态的方法
  public void getStateFromMemento(Memento memento){
    this.state = memento.getState();
  }
}
