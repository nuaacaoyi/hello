package com.atguigu.pattern.k_memento.theory;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:51
 * @Description: TODO
 */
public class Caretaker {
  // 在list集合中有很多备忘录对象
  private List<Memento> mementos = new ArrayList<>();

  public void addMemento(Memento memento){
    mementos.add(memento);
  }

  // 根据索引获取备忘录对象，即某一时刻保存的状态
  public Memento get(int index){
    return  mementos.get(index);
  }
}
