package com.atguigu.pattern.k_memento;

/**
 * @Author caoyi
 * @Create 2021-01-21-14:16
 * @Description: 守护者对象，保存 保存好的状态
 */
public class Caretaker {
  // 如果只保存一个状态，这里只需要一个 Memento对象即可
  // 如果要保存多个状态，这里可以用list；如果要保存多个角色对应的多个状态，这里可以用map
  private Memento memento;

  public void setMemento(Memento memento) {
    this.memento = memento;
  }

  public Memento getMemento() {
    return memento;
  }
}
