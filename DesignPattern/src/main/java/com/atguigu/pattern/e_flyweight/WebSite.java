package com.atguigu.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-16:29
 * @Description: 产品抽象类：享元抽象，一个网站
 */
public abstract class WebSite {
  public abstract void use(User user);
}
