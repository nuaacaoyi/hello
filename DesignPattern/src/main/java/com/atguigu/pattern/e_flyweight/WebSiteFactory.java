package com.atguigu.pattern.e_flyweight;

import java.util.HashMap;

/**
 * @Author caoyi
 * @Create 2021-01-19-16:32
 * @Description: 构造网站池，WebSite工厂类
 * 根据需要返回一个具体网站
 */
public class WebSiteFactory {
  // 网站池
  private HashMap<String,ConcreteWebSite> pool = new HashMap<>();

  // 根据网站类型返回一个网站，如果没有就创建一个网站，并放入到池中， 并返回
  public WebSite getWebSiteCategory(String type){
    if(!pool.containsKey(type)){
      // 如果池中没有，就创建一个放入池中
      pool.put(type, new ConcreteWebSite(type));
    }

    return pool.get(type);
  }

  // 获取池中网站类型的总数
  public int getWebSiteCount(){
    return pool.size();
  }
}
