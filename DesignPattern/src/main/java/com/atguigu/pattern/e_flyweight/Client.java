package com.atguigu.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-16:24
 * @Description:
 * 小型的外包项目，给客户 A 做一个产品展示网站，客户 A 的朋友感觉效果不错，也希望做这样的产品展示网站，但是要求都有些不同：
 * 1)	有客户要求以新闻的形式发布
 * 2)	有客户人要求以博客的形式发布
 * 3)	有客户希望以微信公众号的形式发布
 */
public class Client {
  public static void main(String[] args) {

    // 工厂可以搞成单例模式，这里先不处理了
    WebSiteFactory factory = new WebSiteFactory();

    // 客户要一个新闻形式网站
    WebSite webSite1 = factory.getWebSiteCategory("新闻");

    User user1 = new User();
    user1.setName("客户1");
    webSite1.use(user1);

    // 客户2要一个博客形式网站
    WebSite webSite2 = factory.getWebSiteCategory("博客");

    // 客户3也要一个博客形式网站
    WebSite webSite3 = factory.getWebSiteCategory("博客");

    System.out.println(webSite2 == webSite3);
    System.out.println("网站分类共：" + factory.getWebSiteCount());
  }
}
