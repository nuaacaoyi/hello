package com.atguigu.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-16:43
 * @Description: TODO
 */
public class User {
  // 产品的非共享状态，外部状态
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
