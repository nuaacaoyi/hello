package com.atguigu.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-16:30
 * @Description: 具体的网站产品
 */
public class ConcreteWebSite extends WebSite {
  // 内部状态，可共享的部分
  private String type = ""; // 网站发布的形式（类型）

  public ConcreteWebSite(String type) {
    this.type = type;
  }

  @Override
  public void use(User user) {
    System.out.println("网站的发布形式：" + type + "；使用者是；" + user.getName());
  }
}
