package com.atguigu.pattern.l_state;

/**
 * @Author caoyi
 * @Create 2021-01-21-14:57
 * @Description: TODO
 */
public interface State {
  public void deduceMoney();
  public boolean raffle();
  public void dispensePrize();
}
