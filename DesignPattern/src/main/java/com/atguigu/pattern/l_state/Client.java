package com.atguigu.pattern.l_state;

/**
 * @Author caoyi
 * @Create 2021-01-21-14:37
 * @Description:
 * 请编写程序完成 APP 抽奖活动 具体要求如下:
 * 1)	假如每参加一次这个活动要扣除用户 50 积分，中奖概率是 10%
 * 2)	奖品数量固定，抽完就不能抽奖
 * 3)	活动有四个状态: 可以抽奖、不能抽奖、发放奖品和奖品领完
 * 4)	活动的四个状态转换关系图(右图)
 */
public class Client {
  public static void main(String[] args) {
    // TODO Auto-generated method stub
    // 创建活动对象，奖品有 1 个奖品
    RaffleActivity activity = new RaffleActivity(1);

    // 我们连续抽 300 次奖
    for (int i = 0; i < 30; i++) {
      System.out.println("--------第" + (i + 1) + "次抽奖----------");
      // 参加抽奖，第一步点击扣除积分
      activity.deduceMoney();

      // 第二步抽奖
      activity.raffle();
    }

  }
}
