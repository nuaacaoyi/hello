package com.atguigu.pattern.l_state;

/**
 * @Author caoyi
 * @Create 2021-01-21-15:01
 * @Description: TODO
 */
public class DispenseOutState implements State {
  // 初始化时传入活动引用
  RaffleActivity activity;


  public DispenseOutState(RaffleActivity activity) {
    this.activity = activity;
  }
  @Override
  public void deduceMoney() {
    System.out.println("奖品发送完了，请下次再参加");
  }


  @Override
  public boolean raffle() {
    System.out.println("奖品发送完了，请下次再参加");
    return false;
  }


  @Override
  public void dispensePrize() {
    System.out.println("奖品发送完了，请下次再参加");
  }

}
