package com.atguigu.pattern.a_singleton.type2;

/**
 * @author caoyi
 * @create 2021-01-04-11:50
 * 单例模式第二种写法，饿汉式（静态代码块）
 */
public class SingletonTest {
  public static void main(String[] args) {
    //测试
    Singleton singleton = Singleton.getInstance();
    Singleton singleton2 = Singleton.getInstance();

    System.out.println(singleton==singleton2);
    System.out.println("instance.hashCode=" + singleton.hashCode());
    System.out.println("instance2.hashCode=" + singleton2.hashCode());
  }
}

class Singleton{
  // 1.构造器私有化，外部不能new
  private Singleton(){

  }

  // 2.本类内部声明本类对象
  private static Singleton instance;

  static {// 3.静态代码块中实例化
    instance = new Singleton();
  }
  // 4.提供一个共有的静态方法，返回实例对象
  public static Singleton getInstance(){
    return instance;
  }
}