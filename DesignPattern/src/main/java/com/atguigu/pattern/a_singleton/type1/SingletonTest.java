package com.atguigu.pattern.a_singleton.type1;

/**
 * @author caoyi
 * @create 2021-01-04-11:50
 * 单例模式第一种写法，饿汉式（静态常量）
 */
public class SingletonTest {
  public static void main(String[] args) {
    //测试
    Singleton singleton = Singleton.getInstance();
    Singleton singleton2 = Singleton.getInstance();

    System.out.println(singleton==singleton2);
    System.out.println("instance.hashCode=" + singleton.hashCode());
    System.out.println("instance2.hashCode=" + singleton2.hashCode());
  }
}

class Singleton{
  // 1.构造器私有化，外部不能new
  private Singleton(){

  }

  // 2.本类内部创建对象实例
  private final static Singleton instance = new Singleton();

  // 3.提供一个共有的静态方法，返回实例对象
  public static Singleton getInstance(){
    return instance;
  }
}