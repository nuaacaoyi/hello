package com.atguigu.pattern.a_singleton.type4;

/**
 * @author caoyi
 * @create 2021-01-04-11:50
 * 单例模式第四种写法，懒汉式（线程安全，方法前面加 synchronized修饰，要求方法同步执行）
 */
public class SingletonTest {
  public static void main(String[] args) {
    //测试
    Singleton singleton = Singleton.getInstance();
    Singleton singleton2 = Singleton.getInstance();

    System.out.println(singleton==singleton2);
    System.out.println("instance.hashCode=" + singleton.hashCode());
    System.out.println("instance2.hashCode=" + singleton2.hashCode());
  }
}


/*如果在多线程下，一个线程进入了if判断语句，还没来得及往下执行
* 另一个线程也通过了这个判断语句，这时便会产生多个实例，所以多线程环境下不可以使用这种方式
* */
class Singleton{
  // 1.构造器私有化，外部不能new
  private Singleton(){

  }

  // 2.本类内部声明本类对象
  private static Singleton instance;

  // 3.提供一个公有的静态方法，用到该方法时去实例化并返回
  public static synchronized Singleton getInstance(){
    if(instance == null)instance = new Singleton();
    return instance;
  }
}