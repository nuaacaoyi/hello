package com.atguigu.pattern.a_singleton.type8;

/**
 * @author caoyi
 * @create 2021-01-04-11:50
 * 单例模式第八种写法，枚举
 *
 */
public class SingletonTest {
  public static void main(String[] args) {
    //测试
    Singleton singleton = Singleton.INSTANCE;
    Singleton singleton2 = Singleton.INSTANCE;

    System.out.println(singleton==singleton2);
    System.out.println("instance.hashCode=" + singleton.hashCode());
    System.out.println("instance2.hashCode=" + singleton2.hashCode());
  }
}

/* 使用枚举可以实现单例，也推荐使用
* */
enum  Singleton{
  INSTANCE;
  public void sayOk(){
    System.out.println("ok");
  }
}