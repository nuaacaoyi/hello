package com.atguigu.pattern.a_singleton.type7;

/**
 * @author caoyi
 * @create 2021-01-04-11:50
 * 单例模式第七种写法，静态内部类
 *
 */
public class SingletonTest {
  public static void main(String[] args) {
    //测试
    Singleton singleton = Singleton.getInstance();
    Singleton singleton2 = Singleton.getInstance();

    System.out.println(singleton==singleton2);
    System.out.println("instance.hashCode=" + singleton.hashCode());
    System.out.println("instance2.hashCode=" + singleton2.hashCode());
  }
}
/*当类装载时，其内部静态类时不会装载的
* 类装载时，线程安全
* */
class Singleton{
  // 1.构造器私有化，外部不能new
  private Singleton(){

  }

  // 2.提供一个静态内部类，静态内部类中含有一个常量
  private static class SingletonInstance{
   private static final Singleton INSTANCE = new Singleton();
  }


  // 3.提供一个公有的静态方法，返回实例对象
  public static Singleton getInstance(){
    return SingletonInstance.INSTANCE;
  }
}