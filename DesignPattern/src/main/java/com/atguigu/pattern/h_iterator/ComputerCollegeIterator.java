package com.atguigu.pattern.h_iterator;

import java.util.Iterator;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:12
 * @Description: 自定义一个 计算机学院的 迭代器，实现的接口Iterator为jdk自带接口
 */
public class ComputerCollegeIterator implements Iterator {
  // 这里我们需要Department是以怎样的方式存放--假设计算机学院使用的是数组
  Department[] departments;
  // 索引
  int position = 0;

  public ComputerCollegeIterator() {
  }

  public ComputerCollegeIterator(Department[] departments) {
    this.departments = departments;
  }

  @Override
  public boolean hasNext() {
    return departments !=null && position<departments.length;
  }

  @Override
  public Object next() {
    Department department = departments[position];
    position += 1;
    return department;
  }

  // 删除的方法默认空实现，暂时不需要
  @Override
  public void remove() {

  }
}
