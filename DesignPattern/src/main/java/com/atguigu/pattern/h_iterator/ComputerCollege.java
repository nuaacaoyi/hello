package com.atguigu.pattern.h_iterator;

import java.util.Iterator;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:25
 * @Description: TODO
 */
public class ComputerCollege implements College {
  Department[] departments;
  int numOfDepartment = 0; // 保存当前数组的对象个数

  public ComputerCollege() {
    // 给学员设置系个数上线，假设为5
    departments = new Department[3];
    addDepartment("Java专业", "Java专业");
    addDepartment("PHP专业", "php专业");
    addDepartment("大数据专业", "大数据专业");
  }

  @Override
  public String getName() {
    return "计算机学院";
  }

  @Override
  public void addDepartment(String name, String desc) {
    // 这里有bug，可能发生溢出
    departments[numOfDepartment] = new Department(name,desc);
    numOfDepartment++;
  }

  @Override
  public Iterator createIterator() {
    return new ComputerCollegeIterator(departments);
  }
}
