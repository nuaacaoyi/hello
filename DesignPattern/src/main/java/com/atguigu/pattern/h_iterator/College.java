package com.atguigu.pattern.h_iterator;

import java.util.Iterator;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:22
 * @Description: 机构的接口
 */
public interface College {
  public String getName();

  // 增加系的方法
  public void addDepartment(String name, String desc);

  // 返回一个迭代器
  public Iterator createIterator();
}
