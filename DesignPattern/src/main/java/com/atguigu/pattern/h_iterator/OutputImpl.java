package com.atguigu.pattern.h_iterator;

import java.util.Iterator;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:34
 * @Description: 输出的总出口
 */
public class OutputImpl {
  // 拿到学院的集合
  List<College> colleges;

  public OutputImpl() {
  }

  public OutputImpl(List<College> colleges) {
    this.colleges = colleges;
  }

  // 遍历所有学员，然后调用printDepartment输出各个学员的所有系
  public void printCollege(){
    /*for (College c :
        colleges) {
      printDepartment(c.createIterator());
    }*/
    // 从colleges中取出自己的迭代器，因为list是实现了 迭代器的
    Iterator<College> iterator = colleges.iterator();
    while (iterator.hasNext()){
      College next = iterator.next();
      System.out.println("====="+ next.getName()+ "====");
      printDepartment(next.createIterator());
    }
  }

  // 输出 学院输出 系
  public void printDepartment(Iterator iterator){
    while (iterator.hasNext()){
      Department next = (Department) iterator.next();
      System.out.println(next.getName());
    }
  }

}
