package com.atguigu.pattern.h_iterator;

import java.util.Iterator;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:17
 * @Description: TODO
 */
public class InfoCollegeIterator implements Iterator {
  List<Department> departments; // 信息工程学院以list的方式存放系
  int index = -1;

  public InfoCollegeIterator() {
  }

  public InfoCollegeIterator(List<Department> departments) {
    this.departments = departments;
  }

  @Override
  public boolean hasNext() {
    if(index >= departments.size()-1) {
      return false;
    }
    else {
      index += 1;
      return true;
    }
  }

  @Override
  public Object next() {
    return departments.get(index);
  }

  @Override
  public void remove() {

  }
}
