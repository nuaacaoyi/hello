package com.atguigu.pattern.h_iterator;

import java.util.*;

/**
 * @Author caoyi
 * @Create 2021-01-20-19:31
 * @Description:
 * 编写程序展示一个学校院系结构：需求是这样，要在一个页面中展示出学校的院系组成，一个学校有多个学院， 一个学院有多个系。
 */
public class Client {
  public static void main(String[] args) {
    // 创建学院
    List<College> colleges = new ArrayList<>();

    ComputerCollege computerCollege = new ComputerCollege();
    InfoCollege infoCollege  = new InfoCollege();

    colleges.add(computerCollege);
    colleges.add(infoCollege);

    // 变化来了尝试
    EcnomicCollege ecnomicCollege = new EcnomicCollege();
    colleges.add(ecnomicCollege);

    OutputImpl opi = new OutputImpl(colleges);

    opi.printCollege();
  }
}


// ==================时空线，新增一个经济学院，下面包括 金融学系和工业工程系，由于某些原因按照 Set存===========================
class EcnomicCollege implements College{
  Set<Department> departments;
  int numOfDepartment = 0; // 保存当前数组的对象个数

  public EcnomicCollege() {
    departments = new HashSet<>();
    addDepartment("金融系", "金融系");
    addDepartment("工业工程系", "工业工程系");
  }

  @Override
  public String getName() {
    return "经济学院";
  }

  @Override
  public void addDepartment(String name, String desc) {
    departments.add(new Department(name,desc));
  }

  @Override
  public Iterator createIterator() {
    return new EcnomicCollegeIterator(departments);
  }
}


class EcnomicCollegeIterator implements Iterator{
  Set<Department> departments;
  Iterator<Department> iterator;


  public EcnomicCollegeIterator() {
  }

  public EcnomicCollegeIterator(Set<Department> departments) {
    this.departments = departments;
    iterator = departments.iterator();
  }

  @Override
  public boolean hasNext() {
    return iterator.hasNext();
  }

  @Override
  public Object next() {
    return iterator.next();
  }

  @Override
  public void remove() {

  }
}