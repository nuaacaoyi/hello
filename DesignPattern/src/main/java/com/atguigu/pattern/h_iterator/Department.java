package com.atguigu.pattern.h_iterator;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:11
 * @Description: 学员下面单位：系
 */
public class Department {
  private String name;
  private String desc;

  public Department(String name, String desc) {
    this.name = name;
    this.desc = desc;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }
}
