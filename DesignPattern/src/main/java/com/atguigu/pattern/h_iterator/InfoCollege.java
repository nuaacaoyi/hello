package com.atguigu.pattern.h_iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-20-20:31
 * @Description: TODO
 */
public class InfoCollege implements College {
  List<Department> departments;
  int numOfDepartment = 0; // 保存当前数组的对象个数

  public InfoCollege() {
    departments = new ArrayList<>();
    addDepartment("信息管理", "信息管理");
    addDepartment("信息安全", "信息安全");
    addDepartment("服务器管理", "服务器管理");
  }

  @Override
  public String getName() {
    return "信息工程学院";
  }

  @Override
  public void addDepartment(String name, String desc) {
    departments.add(new Department(name, desc));
  }

  @Override
  public Iterator createIterator() {
    return new InfoCollegeIterator(departments);
  }
}
