package com.atguigu.pattern.i_observer.pro;


import java.util.ArrayList;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-21-9:50
 * @Description: 核心类：包含最新的天气情况信息，同时包含了一个当前天气信息对象
 */
public class WeatherData implements Subject{
  private float temperatrue;
  private float pressure;
  private float humidity;
  // 观察者集合
  private List<Observer> observers;


  public WeatherData() {
    this.observers = new ArrayList<>();
  }


  public float getTemperature() {
    return temperatrue;
  }
  public float getPressure() {
    return pressure;
  }


  public float getHumidity() {
    return humidity;
  }


  // 当数据有更新时，就调用 setData
  public void setData(float temperature, float pressure, float humidity) {
    this.temperatrue = temperature;
    this.pressure = pressure;
    this.humidity = humidity;
    // 调用 dataChange， 将最新的信息 推送给 接入方 currentConditions
    notifyObservers();
  }


  @Override
  public void register(Observer observer) {
    observers.add(observer);
  }

  @Override
  public void remove(Observer observer) {
    if(observers.contains(observer))
      observers.remove(observer);
  }

  // 遍历所有的观察者并通知
  @Override
  public void notifyObservers() {
    for (Observer o :
        observers) {
      o.update(temperatrue, pressure, humidity);
    }
  }
}
