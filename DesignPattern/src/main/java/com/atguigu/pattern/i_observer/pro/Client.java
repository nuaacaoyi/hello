package com.atguigu.pattern.i_observer.pro;

/**
 * @Author caoyi
 * @Create 2021-01-21-9:08
 * @Description:
 * 1)	气象站可以将每天测量到的温度，湿度，气压等等以公告的形式发布出去(比如发布到自己的网站或第三方)。
 * 2)	需要设计开放型 API，便于其他第三方也能接入气象站获取数据。
 * 3)	提供温度、气压和湿度的接口
 * 4)	测量数据更新时，要能实时的通知给第三方
 *
 * 针对于tra包的问题，解决方案：观察者模式
 */
public class Client {
  public static void main(String[] args) {
    // 创建一个WeatherData
    WeatherData weatherData = new WeatherData();

    // 创建一个观察者
    Observer cd = new CurrentConditions();

    // 注册到weatherData
    weatherData.register(cd);

    System.out.println("修改天气信息");

    weatherData.setData(10f,100f,33.3f);

    // 变化来了
    weatherData.register(new SinaCondition());
    weatherData.setData(11f,111f,22.2f);
  }
}

// ===================时空线===============================
class SinaCondition implements Observer{
  // 温度，气压，湿度
  private float temperature;
  private float pressure;
  private float humidity;

  // 更新 天气情况，是由 WeatherData 来调用，我使用推送模式
  @Override
  public void update(float temperature, float pressure, float humidity) {
    this.temperature = temperature;
    this.pressure = pressure;
    this.humidity = humidity;
    display();
  }

  // 显示
  public void display() {
    System.out.println("***百度天气温度: " + temperature + "***");
    System.out.println("***百度天气-气压: " + pressure + "***");
    System.out.println("***百度天气-湿度: " + humidity + "***");
  }
}
