package com.atguigu.pattern.i_observer.pro;

/**
 * @Author caoyi
 * @Create 2021-01-21-10:14
 * @Description: 观察者接口
 */
public interface Observer {
  public void update(float temperature, float pressure, float humidity);
}
