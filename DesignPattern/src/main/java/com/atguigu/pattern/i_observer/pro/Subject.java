package com.atguigu.pattern.i_observer.pro;

/**
 * @Author caoyi
 * @Create 2021-01-21-10:13
 * @Description: 订阅内容接口
 */
public interface Subject {
  public void register(Observer observer);
  public void remove(Observer observer);
  public void notifyObservers();
}
