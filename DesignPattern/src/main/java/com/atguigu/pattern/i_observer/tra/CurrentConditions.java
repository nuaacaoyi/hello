package com.atguigu.pattern.i_observer.tra;

/**
 * @Author caoyi
 * @Create 2021-01-21-9:51
 * @Description: 显示当前天气情况，可以理解为气象站的展示网站
 */
public class CurrentConditions {
  // 温度，气压，湿度
  private float temperature;
  private float pressure;
  private float humidity;

  // 更新 天气情况，是由 WeatherData 来调用，我使用推送模式
  public void update(float temperature, float pressure, float humidity) {
    this.temperature = temperature;
    this.pressure = pressure;
    this.humidity = humidity;
    display();
  }

  // 显示
  public void display() {
    System.out.println("***Today mTemperature: " + temperature + "***");
    System.out.println("***Today mPressure: " + pressure + "***");
    System.out.println("***Today mHumidity: " + humidity + "***");
  }

}
