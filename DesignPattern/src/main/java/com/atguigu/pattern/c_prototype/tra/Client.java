package com.atguigu.pattern.c_prototype.tra;

/**
 * @author caoyi
 * @create 2021-01-04-16:16
 * 克隆羊 传统模式
 *  优点：简单容易理解
 *  缺点：代码重复、效率差
 */
public class Client {
  //传统方法使用Sheep
  public static void main(String[] args) {
    Sheep sheep = new Sheep("tom", 1, "白色");

    Sheep sheep2 = new Sheep(sheep.getName(),sheep.getAge(),sheep.getColor());
    Sheep sheep3 = new Sheep(sheep.getName(),sheep.getAge(),sheep.getColor());
    Sheep sheep4 = new Sheep(sheep.getName(),sheep.getAge(),sheep.getColor());
    Sheep sheep5 = new Sheep(sheep.getName(),sheep.getAge(),sheep.getColor());

    System.out.println(sheep.toString());
    System.out.println(sheep2.toString());
    System.out.println(sheep3.toString());
    System.out.println(sheep4.toString());
    System.out.println(sheep5.toString());
  }
}
