package com.atguigu.pattern.c_prototype.deepclone1;

import java.io.*;

/**
 * @author caoyi
 * @create 2021-01-04-19:35
 */
public class DeepProtoType implements Serializable,Cloneable {
  public String name;//String属性
  public DeepCloneableTarget deepCloneableTarget;//引用类型

  public DeepProtoType() {
    super();
  }

  //深拷贝1：重写clone方法，如果引用类型比较多，这样写会有点麻烦
  @Override
  protected Object clone() throws CloneNotSupportedException {
    Object deep = null;
    //第一步，先对值类型成员变量做一个clone
    deep = super.clone();
    //第二步，对引用类型属性，进行单独处理
    DeepProtoType deepProtoType = (DeepProtoType)deep;
    deepProtoType.deepCloneableTarget = (DeepCloneableTarget) this.deepCloneableTarget.clone();

    return deepProtoType;
  }

  //深拷贝方式2： 通过对象的序列化实现（推荐）
  public DeepProtoType deepClone(){
    DeepProtoType copy = null;
    // 1.创建流对象
    ByteArrayOutputStream bos = null;
    ObjectOutputStream oos = null;
    ByteArrayInputStream bis = null;
    ObjectInputStream ois = null;

    try {
      // 2.序列化操作
      //    2.1 实例化字节输出流
      bos = new ByteArrayOutputStream();
      //    2.2 将字节输出流转为对象输出流
      oos = new ObjectOutputStream(bos);
      oos.writeObject(this); //将当前这个对象以对象流的方式输出

      //反序列化
      bis = new ByteArrayInputStream(bos.toByteArray());
      ois = new ObjectInputStream(bis);
      copy = (DeepProtoType) ois.readObject();

    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    finally {
      // 3.关闭流
      try {
        bos.close();
        oos.close();
        bis.close();
        ois.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    return copy;
  }

}
