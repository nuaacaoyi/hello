package com.atguigu.pattern.c_prototype.pro;


/**
 * @author caoyi
 * @create 2021-01-04-16:16
 * 克隆羊  原型模式
 *  当羊的属性信息发生变化时，使用clone可以避免代码的修改
 */
public class Client {
  //传统方法使用Sheep
  public static void main(String[] args) {
    Sheep sheep = new Sheep("tom", 1, "白色");
    sheep.friend = new Sheep("Jack", 2,"黑色");
    Sheep sheep2 = (Sheep) sheep.clone();

    //验证：两个sheep的friend的hashcode相同，说明 Object.clone()属于浅拷贝
    System.out.println(sheep.toString() + " -- friend is " + sheep.friend.hashCode());
    System.out.println(sheep2.toString() + " -- friend is " + sheep2.friend.hashCode());
  }
}
