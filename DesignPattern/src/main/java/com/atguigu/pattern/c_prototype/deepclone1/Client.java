package com.atguigu.pattern.c_prototype.deepclone1;

/**
 * @author caoyi
 * @create 2021-01-04-19:42
 */
public class Client {
  public static void main(String[] args) {
    DeepProtoType p = new DeepProtoType();
    p.name ="宋江";
    p.deepCloneableTarget = new DeepCloneableTarget("大牛","大牛的类");
    DeepProtoType p2 = null;
    /*方式一
    try {
      p2 = (DeepProtoType) p.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }*/
    //方式2
    p2 = p.deepClone();

    System.out.println(p== p2);
    System.out.println(p.deepCloneableTarget == p2.deepCloneableTarget);
  }
}
