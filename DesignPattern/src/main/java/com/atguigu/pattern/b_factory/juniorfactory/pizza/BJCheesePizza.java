package com.atguigu.pattern.b_factory.juniorfactory.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-15:28
 */
public class BJCheesePizza extends Pizza {
  public BJCheesePizza() {
    this.name = "北京奶酪披萨";
  }

  @Override
  public void prepare() {
    System.out.println("给北京口味的奶酪披萨准备原材料");
  }
}
