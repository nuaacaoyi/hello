package com.atguigu.pattern.b_factory.juniorfactory.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-15:28
 */
public class LDCheesePizza extends Pizza {
  public LDCheesePizza() {
    this.name = "伦敦奶酪披萨";
  }

  @Override
  public void prepare() {
    System.out.println("给伦敦口味的奶酪披萨准备原材料");
  }
}
