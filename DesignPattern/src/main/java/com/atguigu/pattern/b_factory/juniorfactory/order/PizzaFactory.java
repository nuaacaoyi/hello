package com.atguigu.pattern.b_factory.juniorfactory.order;

import com.atguigu.pattern.b_factory.juniorfactory.pizza.Pizza;

/**
 * @author caoyi
 * @create 2021-01-04-15:33
 */
public abstract class PizzaFactory {

  //定义一个抽象方法，createPizza，让各个工厂子类自己实现
  abstract Pizza createPizza(String orderType);
}
