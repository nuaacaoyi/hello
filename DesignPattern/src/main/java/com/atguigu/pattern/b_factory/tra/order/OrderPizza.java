package com.atguigu.pattern.b_factory.tra.order;

import com.atguigu.pattern.b_factory.tra.pizza.CheesePizza;
import com.atguigu.pattern.b_factory.tra.pizza.GreekPizza;
import com.atguigu.pattern.b_factory.tra.pizza.Pizza;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author caoyi
 * @create 2021-01-04-14:25
 * 实现披萨的订购功能
 */
public class OrderPizza {
  public OrderPizza() {
    Pizza pizza = null;
    String orderType;//订购pizza的类型
    do{
      orderType = gettype();
      if("greek".equals(orderType)){
        pizza = new GreekPizza();
        pizza.setName("希腊披萨");
      }else if("cheese".equals(orderType)){
        pizza = new CheesePizza();
        pizza.setName("奶酪披萨");
      }else{
        break;
      }
      //输出pizza的制作过程
      pizza.prepare();
      pizza.bake();
      pizza.cut();
      pizza.box();
    }while (true);
  }

  //动态获取客户想要获取的披萨种类
  private String gettype(){
    try{
      BufferedReader strin = new BufferedReader(new InputStreamReader(System.in));
      System.out.println("input pizza type:");
      String str = strin.readLine();
      return str;
    }catch (IOException e){
      e.printStackTrace();
      return "";
    }
  }
}
