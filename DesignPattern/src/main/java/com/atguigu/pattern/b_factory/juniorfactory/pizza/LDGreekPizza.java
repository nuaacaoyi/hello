package com.atguigu.pattern.b_factory.juniorfactory.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-15:29
 */
public class LDGreekPizza extends Pizza {
  public LDGreekPizza() {
    this.name = "伦敦 希腊披萨";
  }

  @Override
  public void prepare() {
    System.out.println("给伦敦口味的希腊披萨准备原材料");
  }
}
