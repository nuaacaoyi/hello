package com.atguigu.pattern.b_factory.juniorfactory.order;

import com.atguigu.pattern.b_factory.juniorfactory.pizza.*;

/**
 * @author caoyi
 * @create 2021-01-04-15:39
 */
public class LDPizzaFactory extends PizzaFactory {
  @Override
  Pizza createPizza(String orderType) {
    Pizza pizza = null;
    System.out.println("使用简单工厂模式");
    switch (orderType){
      case "greek":
        pizza = new LDGreekPizza();
        break;
      case "cheese":
        pizza = new LDCheesePizza();
        break;
    }
    return pizza;
  }
}
