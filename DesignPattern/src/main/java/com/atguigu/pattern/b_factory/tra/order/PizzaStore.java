package com.atguigu.pattern.b_factory.tra.order;

/**
 * @author caoyi
 * @create 2021-01-04-14:34
 * 相当于客户端，完成订购任务
 */
public class PizzaStore {
  public static void main(String[] args) {
    new OrderPizza();
  }
}
