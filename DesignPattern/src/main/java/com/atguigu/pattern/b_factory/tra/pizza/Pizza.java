package com.atguigu.pattern.b_factory.tra.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-14:19
 */
public abstract class Pizza {
  protected String name;

  //准备原材料  ，不同的披萨需要的原材料不同，因此该方法为抽象方法
  public abstract void prepare();
  //烘焙
  public void bake(){
    System.out.println(name + "baking;");
  }
  //切割
  public void cut(){
    System.out.println(name + "cuting;");
  }
  //打包
  public void box(){
    System.out.println(name + "boxing;");
  }

  public void setName(String name) {
    this.name = name;
  }
}
