package com.atguigu.pattern.b_factory.simplefactory.order;

import com.atguigu.pattern.b_factory.simplefactory.pizza.CheesePizza;
import com.atguigu.pattern.b_factory.simplefactory.pizza.GreekPizza;
import com.atguigu.pattern.b_factory.simplefactory.pizza.Pizza;

/**
 * @author caoyi
 * @create 2021-01-04-14:52
 * 简单工厂类，
 */
public class SimpleFactory {
  public static Pizza createPizza(String type){
    Pizza pizza = null;
    System.out.println("使用简单工厂模式");
    switch (type){
      case "greek":
        pizza = new GreekPizza();
        break;
      case "cheese":
        pizza = new CheesePizza();
        break;
      case "pepper":
        pizza = new CheesePizza();
        break;
    }
    return pizza;
  }
}
