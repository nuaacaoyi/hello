package com.atguigu.pattern.b_factory.simplefactory.order;

import com.atguigu.pattern.b_factory.simplefactory.pizza.Pizza;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author caoyi
 * @create 2021-01-04-14:25
 * 实现披萨的订购功能
 */
public class OrderPizza {

  public OrderPizza() {
    Pizza pizza = null;
    String orderType;//订购pizza的类型
    do{
      orderType = gettype();
      pizza = SimpleFactory.createPizza(orderType);
      //输出pizza的制作过程
      if(pizza != null){
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
      }
      else {
        System.out.println("本店没有该类型披萨");
        break;
      }
    }while (true);
  }

  //动态获取客户想要获取的披萨种类
  private String gettype(){
    try{
      BufferedReader strin = new BufferedReader(new InputStreamReader(System.in));
      System.out.println("input pizza type:");
      String str = strin.readLine();
      return str;
    }catch (IOException e){
      e.printStackTrace();
      return "";
    }
  }
}
