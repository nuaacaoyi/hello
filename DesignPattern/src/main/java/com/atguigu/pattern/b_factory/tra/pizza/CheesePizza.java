package com.atguigu.pattern.b_factory.tra.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-14:20
 */
public class CheesePizza extends Pizza{
  @Override
  public void prepare() {
    System.out.println("给奶酪披萨准备原材料-奶酪");
  }
}

