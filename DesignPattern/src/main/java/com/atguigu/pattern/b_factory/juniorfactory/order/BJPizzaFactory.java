package com.atguigu.pattern.b_factory.juniorfactory.order;

import com.atguigu.pattern.b_factory.juniorfactory.pizza.BJCheesePizza;
import com.atguigu.pattern.b_factory.juniorfactory.pizza.BJGreekPizza;
import com.atguigu.pattern.b_factory.juniorfactory.pizza.Pizza;

/**
 * @author caoyi
 * @create 2021-01-04-15:37
 */
public class BJPizzaFactory extends PizzaFactory {
  @Override
  Pizza createPizza(String orderType) {
    Pizza pizza = null;
    System.out.println("使用简单工厂模式");
    switch (orderType){
      case "greek":
        pizza = new BJGreekPizza();
        break;
      case "cheese":
        pizza = new BJCheesePizza();
        break;
    }
    return pizza;
  }
}
