package com.atguigu.pattern.b_factory.juniorfactory.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-15:29
 */
public class BJGreekPizza extends Pizza {
  public BJGreekPizza() {
    this.name = "北京希腊披萨";
  }

  @Override
  public void prepare() {
    System.out.println("给北京口味的希腊披萨准备原材料");
  }
}
