package com.atguigu.pattern.b_factory.tra.pizza;

/**
 * @author caoyi
 * @create 2021-01-04-14:21
 */
public class GreekPizza extends Pizza {
  @Override
  public void prepare() {
    System.out.println("给希腊披萨准备原材料");
  }
}
