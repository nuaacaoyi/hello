package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-16:02
 * @Description: 请求类，包含要审批的内容
 */
public class PurchaseRequest {
  private int type=0; // 请求类型

  private float price = 0.0f; // 需要审批的采购额

  private int id = 0; // 请求id

  public PurchaseRequest(int type, float price, int id) {
    this.type = type;
    this.price = price;
    this.id = id;
  }

  public int getType() {
    return type;
  }

  public float getPrice() {
    return price;
  }

  public int getId() {
    return id;
  }
}
