package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-16:12
 * @Description: TODO
 */
public class ViceSchoolMasterApprover extends Approver{
  public ViceSchoolMasterApprover(String name) {
    super(name);
  }

  @Override
  public void processRequest(PurchaseRequest purchaseRequest) {
    if(purchaseRequest.getPrice() > 10000 && purchaseRequest.getPrice()<=30000){
      System.out.println("请求id= " + purchaseRequest.getId() + " 被" + this.name + " 处理");
    }
    else {
      this.approver.processRequest(purchaseRequest);
    }
  }
}
