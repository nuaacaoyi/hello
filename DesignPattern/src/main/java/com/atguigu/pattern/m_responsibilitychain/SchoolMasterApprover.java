package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-16:13
 * @Description: TODO
 */
public class SchoolMasterApprover extends Approver{
  public SchoolMasterApprover(String name) {
    super(name);
  }

  @Override
  public void processRequest(PurchaseRequest purchaseRequest) {
    if(purchaseRequest.getPrice() > 30000){
      System.out.println("请求id= " + purchaseRequest.getId() + " 被" + this.name + " 处理");
    }
    else {
      this.approver.processRequest(purchaseRequest);
    }
  }
}
