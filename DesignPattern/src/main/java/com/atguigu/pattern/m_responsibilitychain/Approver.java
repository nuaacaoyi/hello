package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-16:05
 * @Description: TODO
 */
public abstract class Approver {
  protected Approver approver;
  String name;

  public Approver(String name) {
    this.name = name;
  }

  // 下一个审批人是谁
  public void setApprover(Approver approver) {
    this.approver = approver;
  }

  // 处理审批请求的方法 ， 得到一个请求，
  public abstract void processRequest(PurchaseRequest purchaseRequest);
}
