package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-15:55
 * @Description:
 * 采购员采购教学器材
 * 1)	如果金额 小于等于 5000,  由教学主任审批 （0<=x<=5000）
 * 2)	如果金额 小于等于 10000,  由院长审批 (5000<x<=10000)
 * 3)	如果金额 小于等于 30000,  由副校长审批 (10000<x<=30000)
 * 4)	如果金额 超过 30000 以上，有校长审批 ( 30000<x)
 * 请设计程序完成采购审批项目
 */
public class Client {
  public static void main(String[] args) {
    // 创建一个请求
    PurchaseRequest purchaseRequest = new PurchaseRequest(1, 31000, 1);
    
    // 创建相关的审批人
    DepartmentApprover departmentApprover = new DepartmentApprover("张主任");
    CollegeApprover collegeApprover = new CollegeApprover("李院长");
    ViceSchoolMasterApprover viceSchoolMasterApprover = new ViceSchoolMasterApprover("王副校长");
    SchoolMasterApprover schoolMasterApprover = new SchoolMasterApprover("赵校长");

    // 设置链条（如果想要不考虑顺序的审批，审批人需要构成闭环）
    departmentApprover.setApprover(collegeApprover);
    collegeApprover.setApprover(viceSchoolMasterApprover);
    viceSchoolMasterApprover.setApprover(schoolMasterApprover);
    schoolMasterApprover.setApprover(departmentApprover);

    // 随便找一个审批人接收请求
    departmentApprover.processRequest(purchaseRequest);

  }
}

// 变化来了，需要修改审批节点  将副校长审批额度调整为 <= 100000; 校长审批流程调整为 >100000