package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-16:11
 * @Description: TODO
 */
public class CollegeApprover extends Approver{
  public CollegeApprover(String name) {
    super(name);
  }

  @Override
  public void processRequest(PurchaseRequest purchaseRequest) {
    if(purchaseRequest.getPrice() > 5000 && purchaseRequest.getPrice()<=10000){
      System.out.println("请求id= " + purchaseRequest.getId() + " 被" + this.name + " 处理");
    }
    else {
      this.approver.processRequest(purchaseRequest);
    }
  }
}
