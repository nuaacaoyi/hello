package com.atguigu.pattern.m_responsibilitychain;

/**
 * @Author caoyi
 * @Create 2021-01-21-16:08
 * @Description: TODO
 */
public class DepartmentApprover extends Approver {
  public DepartmentApprover(String name) {
    super(name);
  }

  @Override
  public void processRequest(PurchaseRequest purchaseRequest) {
    if(purchaseRequest.getPrice() <= 5000){
      System.out.println("请求id= " + purchaseRequest.getId() + " 被" + this.name + " 处理");
    }
    else {
      this.approver.processRequest(purchaseRequest);
    }
  }
}
