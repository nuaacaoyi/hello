package com.atguigu.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-20-16:26
 * @Description: 遥控器（命令调用者）
 */
public class RemoteController {
  // 开 按钮的命令数组
  Command[] onCommands;
  Command[] offCommands;

  // 执行撤销的命令
  Command undoCommand;

  // 构造器，完成对按钮的初始化
  public RemoteController() {
    // 根据设计，遥控器中有5个开按钮，5个关按钮，一个撤销按钮
    onCommands = new Command[5];
    offCommands = new Command[5];

    for (int i = 0; i < 5; i++) {
      onCommands[i] = new NoCommand();
      offCommands[i] = new NoCommand();
    }
  }

  // 给按钮设置你需要的命令
  public void setCommand(int num, Command onCmd, Command offCmd){
    onCommands[num] = onCmd;
    offCommands[num] = offCmd;
  }

  // 按下开的按钮
  public void onButtonWasPush(int num){
    // 当某一个开 按钮，按下时，根据索引找到按钮，并调用该按钮对应的 方法
    onCommands[num].execute();
    // 记录最后一次操作，用于可能发生的撤销操作
    this.undoCommand = onCommands[num];
  }

  // 按下关的按钮
  public void offButtonWasPush(int num){
    offCommands[num].execute();
    this.undoCommand = offCommands[num];
  }

  // 按下撤销的按钮
  public void undoButtonWasPush(){
    this.undoCommand.undo();
  }
}
