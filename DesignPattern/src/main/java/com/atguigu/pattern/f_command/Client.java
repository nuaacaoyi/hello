package com.atguigu.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-20-15:05
 * @Description:
 * 需求：
 * 1)	我们买了一套智能家电，有照明灯、风扇、冰箱、洗衣机，我们只要在手机上安装 app 就可以控制对这些家电工作。
 * 2)	这些智能家电来自不同的厂家，我们不想针对每一种家电都安装一个 App，分别控制，我们希望只要一个 app
 * 就可以控制全部智能家电。
 * 3)	要实现一个 app 控制所有智能家电的需要，则每个智能家电厂家都要提供一个统一的接口给 app 调用，这时 就可以考虑使用命令模式。
 * 4)	命令模式可将“动作的请求者”从“动作的执行者”对象中解耦出来.
 * 5)	在我们的例子中，动作的请求者是手机 app，动作的执行者是每个厂商的一个家电产品
 */
public class Client {
  public static void main(String[] args) {
    // 通过遥控器对电灯进行操作
    LightReceiver light = new LightReceiver();
    Command lightOnCommand = new LightOnCommand(light);
    Command lightOffCommand = new LightOffCommand(light);

    // 创建一个遥控器
    RemoteController controller = new RemoteController();

    // 为遥控器设置相关命令，比如第一组设置为电灯的开关
    controller.setCommand(0,lightOnCommand,lightOffCommand);

    System.out.println("------按下灯的开按钮-----");
    controller.onButtonWasPush(0);
    System.out.println("------按下灯的关按钮-----");
    controller.offButtonWasPush(0);
    System.out.println("------按下撤销-----");
    controller.undoButtonWasPush();

    // 变化来了，增加电视的操作
    TVReceiver tv = new TVReceiver();
    Command tvOnCommand = new TVOnCommand(tv);
    Command tvOffCommand = new TVOffCommand(tv);

    controller.setCommand(1,tvOnCommand,tvOffCommand);
    System.out.println("------按下电视的开按钮-----");
    controller.onButtonWasPush(0);
    System.out.println("------按下撤销-----");
    controller.undoButtonWasPush();
    System.out.println("------按下电视的关按钮-----");
    controller.offButtonWasPush(0);


  }
}


//======================时空线，增加TV的命令，放到遥控器的第二组命令=============================
class TVReceiver {

  public void on(){
    System.out.println("电视打开了...");
  }

  public void off(){
    System.out.println("电视关闭了...");
  }
}
class TVOnCommand implements Command {

  private TVReceiver tv;

  public TVOnCommand(TVReceiver tv) {
    this.tv = tv;
  }

  @Override
  public void execute() {
    // 调用接收者的方法
    tv.on();
  }

  @Override
  public void undo() {
    tv.off();
  }
}
class TVOffCommand implements Command {

  private TVReceiver tv;

  public TVOffCommand(TVReceiver tv) {
    this.tv = tv;
  }

  @Override
  public void execute() {
    // 调用接收者的方法
    tv.off();
  }

  @Override
  public void undo() {
    tv.on();
  }
}