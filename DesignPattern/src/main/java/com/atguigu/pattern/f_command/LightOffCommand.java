package com.atguigu.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-20-16:23
 * @Description: TODO
 */
public class LightOffCommand implements Command {

  private LightReceiver light;

  public LightOffCommand(LightReceiver light) {
    this.light = light;
  }

  @Override
  public void execute() {
    // 调用接收者的方法
    light.off();
  }

  @Override
  public void undo() {
    light.on();
  }
}
