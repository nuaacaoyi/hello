package com.atguigu.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-20-16:19
 * @Description: 具体的命令实现，聚合了 receiver（命令接收者）
 */
public class LightOnCommand implements Command {

  private LightReceiver light;

  public LightOnCommand(LightReceiver light) {
    this.light = light;
  }

  @Override
  public void execute() {
    // 调用接收者的方法
    light.on();
  }

  @Override
  public void undo() {
    light.off();
  }
}
