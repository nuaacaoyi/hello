package com.atguigu.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-20-16:20
 * @Description: 电灯（电灯相关命令的接收者）
 */
public class LightReceiver {

  public void on(){
    System.out.println("电灯打开了...");
  }

  public void off(){
    System.out.println("电灯关闭了...");
  }
}
