package com.atguigu.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-20-16:17
 * @Description: 创建命令接口
 */
public interface Command {
  // 执行某个操作
  public void execute();

  // 撤销某个操作
  public void undo();
}
