package com.atguigu.pattern.d_builder.pro;

/**
 * @author caoyi
 * @create 2021-01-04-20:37
 * 对应角色：指挥者，动态的指定制作产品流程
 */
public class HouseDirector {
  HouseBuilder houseBuilder = null;

  public HouseDirector() {
  }
  // 构造器传入housebuilder

  public HouseDirector(HouseBuilder houseBuilder) {
    this.houseBuilder = houseBuilder;
  }

  //如何处理建造房子的流程，交给指挥者处理，因为指挥者可能有很多个
  public House constructHouse(){
    houseBuilder.buildBasic();
    houseBuilder.buildWalls();
    houseBuilder.roofed();
    return houseBuilder.buildHouse();
  }
}
