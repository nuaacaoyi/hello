package com.atguigu.pattern.d_builder.tra;

/**
 * @author caoyi
 * @create 2021-01-04-20:08
 * 程序的扩展和维护性不好，这种方案把产品（即房子）和创建产品的过程（即建房子流程）封装在一起了，耦合性增强了
 */
public abstract class AbstractHouse {
  //打地基
  public abstract void buildBasic();
  //砌墙
  public abstract void buildWall();
  //封顶
  public abstract void roofed();

  //建造过程中，这三个方法是有顺序的，所以进一步封装以下
  public void build(){
    buildBasic();
    buildWall();
    roofed();
  }
}
