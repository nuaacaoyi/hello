package com.atguigu.pattern.d_builder.tra;

/**
 * @author caoyi
 * @create 2021-01-04-20:12
 */
public class Client {
  public static void main(String[] args) {
    CommonHouse ch = new CommonHouse();
    ch.build();
  }
}
