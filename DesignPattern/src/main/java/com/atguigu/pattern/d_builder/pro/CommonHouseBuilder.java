package com.atguigu.pattern.d_builder.pro;

/**
 * @author caoyi
 * @create 2021-01-04-20:34
 */
public class CommonHouseBuilder extends HouseBuilder {
  @Override
  public void buildBasic() {
    System.out.println("普通房子打地基5米");
  }

  @Override
  public void buildWalls() {
    System.out.println("普通房子砌墙10cm");
  }

  @Override
  public void roofed() {
    System.out.println("普通房子盖普通房子的屋顶");
  }
}
