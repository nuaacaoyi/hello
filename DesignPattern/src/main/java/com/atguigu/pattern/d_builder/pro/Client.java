package com.atguigu.pattern.d_builder.pro;

/**
 * @author caoyi
 * @create 2021-01-04-20:40
 */
public class Client {
  public static void main(String[] args) {
    HouseDirector hd = new HouseDirector(new CommonHouseBuilder());
    hd.constructHouse();
  }
}
