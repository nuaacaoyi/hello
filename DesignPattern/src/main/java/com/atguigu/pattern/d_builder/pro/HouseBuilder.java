package com.atguigu.pattern.d_builder.pro;

/**
 * @author caoyi
 * @create 2021-01-04-20:30
 * 对应角色：抽象建造者
 */
public abstract class HouseBuilder {
  protected House house = new House();

  //将建造的流程写好
  public abstract void buildBasic();
  public abstract void buildWalls();
  public abstract void roofed();

  //建造房子好后，将产品（House）返回
  public House buildHouse(){

    return house;
  }
}
