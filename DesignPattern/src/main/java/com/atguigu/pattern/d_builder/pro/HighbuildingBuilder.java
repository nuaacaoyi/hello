package com.atguigu.pattern.d_builder.pro;

/**
 * @author caoyi
 * @create 2021-01-04-20:35
 */
public class HighbuildingBuilder extends HouseBuilder{
  @Override
  public void buildBasic() {
    System.out.println("高楼打地基50米");
  }

  @Override
  public void buildWalls() {
    System.out.println("高楼砌墙30cm");
  }

  @Override
  public void roofed() {
    System.out.println("高楼的屋顶");
  }
}
