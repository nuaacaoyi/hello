package com.atguigu.pattern.d_builder.tra;

/**
 * @author caoyi
 * @create 2021-01-04-20:10
 */
public class CommonHouse extends AbstractHouse {
  @Override
  public void buildBasic() {
    System.out.println("普通房子--打地基");
  }

  @Override
  public void buildWall() {
    System.out.println("普通房子--砌墙");
  }

  @Override
  public void roofed() {
    System.out.println("普通房子--封顶");
  }
}
