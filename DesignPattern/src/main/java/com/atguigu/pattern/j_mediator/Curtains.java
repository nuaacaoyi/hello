package com.atguigu.pattern.j_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-11:47
 * @Description: TODO
 */
public class Curtains extends Colleague {
  public Curtains(Mediator mediator, String name) { super(mediator, name);
    // TODO Auto-generated constructor stub
    mediator.Register(name, this);
  }


  @Override
  public void SendMessage(int stateChange) {
    // TODO Auto-generated method stub
    this.GetMediator().GetMessage(stateChange, this.name);
  }

  public void UpCurtains() {
    System.out.println("I am holding Up Curtains!");
  }

}
