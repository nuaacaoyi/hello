package com.atguigu.pattern.j_mediator;


/**
 * @Author caoyi
 * @Create 2021-01-21-10:43
 * @Description:
 * 智能家庭项目：
 * 1)	智能家庭包括各种设备，闹钟、咖啡机、电视机、窗帘 等
 * 2)	主人要看电视时，各个设备可以协同工作，自动完成看电视的准备工作，
 *    比如流程为：闹铃响起->咖啡机开始做咖啡->窗帘自动落下->电视机开始播放
 */
public class Client {
  public static void main(String[] args) {
    //创建一个中介者对象
    Mediator mediator = new ConcreteMediator();
    //创建 Alarm  并且加入到	ConcreteMediator 对象的 HashMap
    Alarm alarm = new Alarm(mediator, "alarm");

    //创建了 CoffeeMachine 对象，并	且加入到	ConcreteMediator 对象的 HashMap
    CoffeeMachine coffeeMachine = new CoffeeMachine(mediator,    "coffeeMachine");

    //创建  Curtains , 并	且加入到	ConcreteMediator 对象的 HashMap
    Curtains curtains = new Curtains(mediator, "curtains");
    TV tV = new TV(mediator, "TV");

    //让闹钟发出消息alarm.SendAlarm(0);
    coffeeMachine.FinishCoffee();
    alarm.SendAlarm(1);

  }
}
