package com.atguigu.pattern.j_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-11:41
 * @Description: 同事类的抽象类
 */
public abstract class Colleague {
  private Mediator mediator;
  public String name;

  public Colleague(Mediator mediator, String name) {
    this.mediator = mediator;
    this.name = name;
  }


  public Mediator GetMediator() {
    return this.mediator;
  }


  public abstract void SendMessage(int stateChange);

}
