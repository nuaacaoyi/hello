package com.atguigu.pattern.g_visitor;


/**
 * @Author caoyi
 * @Create 2021-01-20-17:43
 * @Description:
 * 这里我们使用到了双分派，即 首先在客户端程序中将具体的状态作为参数传递到了Woman中（第一次分派）
 *    然后Woman这个类 调用了作为参数的 “具体方法”中的 get方法，同时将自己 作为参数传递进去
 */
public class Woman extends Person {
  @Override
  public void accept(Action action) {
    action.getWomenResult(this);
  }
}
