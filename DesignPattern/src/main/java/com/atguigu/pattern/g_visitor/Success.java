package com.atguigu.pattern.g_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-17:44
 * @Description: TODO
 */
public class Success extends Action {
  @Override
  public void getManResult(Man man) {
    System.out.println("男人给该歌手的评价是 通过");
  }

  @Override
  public void getWomenResult(Woman woman) {
    System.out.println("女人给该歌手的评价是 通过");
  }
}
