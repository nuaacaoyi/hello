package com.atguigu.pattern.g_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-17:41
 * @Description: 进行测评的抽象人员
 */
public abstract class Person {
  // 提供一个方法让访问者可以访问
  public abstract void accept(Action action);
}
