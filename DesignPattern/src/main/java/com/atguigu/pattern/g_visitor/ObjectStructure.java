package com.atguigu.pattern.g_visitor;

import java.util.LinkedList;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-20-17:52
 * @Description: 这是一个数据结构，管理了很多人，man 和 woman
 */
public class ObjectStructure {
  // 维护了一个集合(很多人的评价集合)
  private List<Person> persons = new LinkedList<>();

  // 添加元素
  public void attach(Person p){
    persons.add(p);
  }

  // 移除元素
  public void detach(Person p ){
    persons.remove(p);
  }

  // 显示测评情况
  public void display(Action action){
    for (Person p :
        persons) {
      p.accept(action);
    }
  }
}
