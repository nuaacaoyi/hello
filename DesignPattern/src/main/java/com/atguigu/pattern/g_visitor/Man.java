package com.atguigu.pattern.g_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-17:44
 * @Description: TODO
 */
public class Man extends Person {
  @Override
  public void accept(Action action) {
    action.getManResult(this);
  }
}
