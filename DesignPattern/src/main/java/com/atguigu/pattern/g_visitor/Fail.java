package com.atguigu.pattern.g_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-17:45
 * @Description: TODO
 */
public class Fail extends Action {
  @Override
  public void getManResult(Man man) {
    System.out.println("男人给该歌手的评价是 失败");
  }

  @Override
  public void getWomenResult(Woman woman) {
    System.out.println("女人给该歌手的评价是 失败");
  }
}
