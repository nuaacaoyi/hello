package com.atguigu.pattern.g_visitor;


/**
 * @Author caoyi
 * @Create 2021-01-20-17:14
 * @Description:
 * 18.1  测评系统的需求
 * 完成测评系统需求
 * 1)	将观众分为男人和女人，对歌手进行测评，当看完某个歌手表演后，得到他们对该歌手不同的评价(评价 有不同的种类，比如 成功、失败 等)
 * 2)	传统方案
 */
public class Client {
  public static void main(String[] args) {
    // 创建objectStructure
    ObjectStructure os = new ObjectStructure();
    os.attach(new Man());
    os.attach(new Woman());
    os.attach(new Man());
    // 成功
    Success success = new Success();

    os.display(success);
  }
}
