package com.atguigu.pattern.g_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-17:40
 * @Description: 获得测评结果的抽象类
 */
public abstract class Action {
  // 得到男性的测评结果
  public abstract void getManResult(Man man);

  // 得到女性的测评结果
  public abstract void getWomenResult(Woman woman);
}
