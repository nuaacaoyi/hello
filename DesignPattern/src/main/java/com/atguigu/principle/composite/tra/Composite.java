package com.atguigu.principle.composite.tra;

/**
 * @author caoyi
 * @create 2021-01-04-9:32
 */
public class Composite {
}


class A{
  void operation1(){

  }

  void operation2(){}
}

//为了让B调用A的两个方法，直接采用继承的方式，会导致耦合度加强
//如果仅仅是让B调用A的方法，可以使用简单依赖（成员变量（合成）、方法传参、构造器（组合））
class B extends A{

}