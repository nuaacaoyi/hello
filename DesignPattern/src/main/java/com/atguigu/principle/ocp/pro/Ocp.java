package com.atguigu.principle.ocp.pro;


/**
 * @author caoyi
 * @create 2021-01-03-21:04
 * open and closed 模式
 */
public class Ocp {
  public static void main(String[] args) {
    //使用看看传统模式存在的问题
    GraphicEditor graphicEditor = new GraphicEditor();
    graphicEditor.drawShape(new Rectangle());
    graphicEditor.drawShape(new Circle());
  }
}

//增加一个抽象类，具体绘制功能放在具体实现类
//需要新增一个三角形： 增加一个shape的实现类，修改main函数即可
class GraphicEditor{
  //接收shape对象，然后根据type绘制不同的图形
  public void drawShape(Shape s){
    s.draw();
  }
}

abstract class Shape{
  int m_type;

  public abstract  void draw();//抽象方法
}

class Rectangle extends Shape{
  @Override
  public void draw() {
    System.out.println("矩形");
  }

  Rectangle(){
    this.m_type = 1;
  }
}

class Circle extends Shape{
  @Override
  public void draw() {
    System.out.println("圆形");
  }

  Circle(){
    this.m_type = 2;
  }
}