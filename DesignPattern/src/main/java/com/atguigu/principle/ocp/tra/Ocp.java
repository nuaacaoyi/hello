package com.atguigu.principle.ocp.tra;

/**
 * @author caoyi
 * @create 2021-01-03-21:04
 * open and closed 模式
 */
public class Ocp {
  public static void main(String[] args) {
    //使用看看传统模式存在的问题
    GraphicEditor graphicEditor = new GraphicEditor();
    graphicEditor.drawShape(new Rectangle());
    graphicEditor.drawShape(new Circle());
  }
}

//违反了设计模式的ocp原则，即对扩展开放，对修改关闭。
//即当我们给类增加新的功能的时候，尽量步修改代码，或者尽可能少的修改代码。
//如果此时我们需要增加一个三角形的图形，需要1新增加类，修改GraphicEditor类，增加方法，修改调用，修改main
//这是一个用于绘图的类
class GraphicEditor{
  //接收shape对象，然后根据type绘制不同的图形
  public void drawShape(Shape s){
    if(s.m_type ==1 ){
      drawRectangle();
    }else{
      drawCircle();
    }
  }

  public void drawRectangle(){
    System.out.println("矩形");
  }


  public void drawCircle(){
    System.out.println("圆形");
  }
}

class Shape{
  int m_type;
}

class Rectangle extends Shape{
  Rectangle(){
    this.m_type = 1;
  }
}

class Circle extends Shape{
  Circle(){
    this.m_type = 2;
  }
}