package com.atguigu.principle.singleresponsibility;

/**
 * @author caoyi
 * @create 2021-01-03-15:06
 */
public class SingleResponsibilityPro {
  public static void main(String[] args) {
    RoadVehicle vehicle = new RoadVehicle();
    vehicle.run("汽车");
    vehicle.run("摩托车");

    SkyVehicle vehicle2 = new SkyVehicle();
    vehicle2.run("飞机");
  }
}

//方案2的分析
//1.遵守了单一职责原则
//2.但是这样改动很大
//交通工具类
class RoadVehicle{
  public  void run(String vehicle){
    System.out.println(vehicle + " 在公路上运行...");
  }
}

class SkyVehicle{
  public  void run(String vehicle){
    System.out.println(vehicle + " 在空中运行...");
  }
}