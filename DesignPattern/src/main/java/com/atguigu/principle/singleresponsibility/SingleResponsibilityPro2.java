package com.atguigu.principle.singleresponsibility;

/**
 * @author caoyi
 * @create 2021-01-03-15:23
 */
public class SingleResponsibilityPro2 {
  public static void main(String[] args) {
    Vehicle2 vehicle2 = new Vehicle2();
    vehicle2.run("汽车");
    vehicle2.runAir("飞机");
  }
}

/*
* 方案3
* 1.这种修改方法没有对原来的类做大的修改，只是增加方法
* 2.这里虽然没有在类这个级别上遵守单一职责原则，但是在方法级别上，仍然是遵守单一职责原则
* */
class Vehicle2{
  public  void run(String vehicle){
    System.out.println(vehicle + " 在公路上运行...");
  }
  public  void runAir(String vehicle){
    System.out.println(vehicle + " 在空中运行...");
  }
  public  void runWater(String vehicle){
    System.out.println(vehicle + " 在水中运行...");
  }
}