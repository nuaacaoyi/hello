package com.atguigu.principle.singleresponsibility;

/**
 * @author caoyi
 * @create 2021-01-03-14:58
 */
public class SingleResponsibilityTra {
  public static void main(String[] args) {
    Vehicle vehicle = new Vehicle();
    vehicle.run("汽车");
    vehicle.run("摩托车");
    vehicle.run("飞机");
  }
}

// run方法违反了单一职责设计原则，解决方案非常简单，根据交通工具运行方式的不同，分解成不同类即可
//交通工具类
class Vehicle{
  public  void run(String vehicle){
    System.out.println(vehicle + " 在公路上运行...");
  }
}