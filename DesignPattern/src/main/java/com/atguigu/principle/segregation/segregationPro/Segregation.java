package com.atguigu.principle.segregation.segregationPro;

/**
 * @author caoyi
 * @create 2021-01-03-15:51
 */
public class Segregation {
  public static void main(String[] args) {
    A a = new A();
    B b = new B();
    a.depend1(b);
    a.depend2(b);
    a.depend3(b);

  }
}

interface Interface1{
  void operation1();
}
interface Interface2{
  void operation2();
  void operation3();
}
interface Interface3{
  void operation4();
  void operation5();
}

class B implements Interface1,Interface2{
  @Override
  public void operation1() {
    System.out.println("B类实现了接口的方法1");
  }

  @Override
  public void operation2() {
    System.out.println("B类实现了接口的方法2");
  }

  @Override
  public void operation3() {
    System.out.println("B类实现了接口的方法3");
  }
}


class D implements Interface1,Interface3{
  @Override
  public void operation1() {
    System.out.println("D类实现了接口的方法1");
  }
  @Override
  public void operation4() {
    System.out.println("D类实现了接口的方法4");
  }

  @Override
  public void operation5() {
    System.out.println("D类实现了接口的方法5");
  }
}

class A{//A类通过接口Interface1依赖B类，但是只会用到1、2、3方法
  public void depend1(Interface1 i){
    i.operation1();
  }
  public void depend2(Interface2 i){
    i.operation2();
  }
  public void depend3(Interface2 i){
    i.operation3();
  }
}

class C{//C类通过接口Interface1依赖D类，但是只会用到1、4、5方法
  public void depend1(Interface1 i){
    i.operation1();
  }
  public void depend4(Interface3 i){
    i.operation4();
  }
  public void depend5(Interface3 i){
    i.operation5();
  }
}