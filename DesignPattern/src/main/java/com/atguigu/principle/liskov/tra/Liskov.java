package com.atguigu.principle.liskov.tra;

/**
 * @author caoyi
 * @create 2021-01-03-20:40
 */
public class Liskov {
  public static void main(String[] args) {
    A a=new A();
    System.out.println("11-3=" + a.fucn1(11,3));
    System.out.println("1-8=" + a.fucn1(1,8));

    System.out.println("-----------------------");
    B b = new B();
    System.out.println("11-3=" + b.fucn1(11,3));
    System.out.println("1-8=" + b.fucn1(1,8));
    System.out.println("11+3+9=" + b.fucn2(11,3));
  }
}

class A{
  public int fucn1(int n1,int n2){
    return n1-n2;
  }
}

//B类继承了A，然后再重写A的方法，就会导致耦合度加强
//解决方法：原来的父类和子类继承更基础的一个基类
class B extends A{
  public int fucn1(int n1, int n2) {
    return n1+n2;
  }
  public int fucn2(int n1, int n2) {
    return fucn1(n1, n2) + 9;
  }
}