package com.atguigu.principle.liskov.pro;

/**
 * @author caoyi
 * @create 2021-01-03-20:40
 */
public class Liskov {
  public static void main(String[] args) {
    A a=new A();
    System.out.println("11-3=" + a.fucn1(11,3));
    System.out.println("1-8=" + a.fucn1(1,8));

    System.out.println("-----------------------");
    B b = new B();
    System.out.println("11-3=" + b.fucn1(11,3));
    System.out.println("1-8=" + b.fucn1(1,8));
    System.out.println("11+3+9=" + b.fucn2(11,3));
  }
}
//创建一个更加基础的集类
class Base{
  //把基础的方法和成员写在base类中
}

class A extends Base{
  public int fucn1(int n1,int n2){
    return n1-n2;
  }
}

//如果B类需要使用A类的方法，使用组合关系
class B extends Base{
  private A a1 = new A();

  public int fucn1(int n1, int n2) {
    return n1+n2;
  }
  public int fucn2(int n1, int n2) {
    return fucn1(n1, n2) + 9;
  }

  public int func3(int n1, int n2){
    return this.a1.fucn1(n1,n2);
  }
}