package com.atguigu.principle.inversion.pro;

/**
 * @author caoyi
 * @create 2021-01-03-16:24
 */
public class DependencyPro {
  public static void main(String[] args) {
    Person p2 = new Person();
    p2.receive(new Email());
  }

}

class Person{
  public void receive(IReceiver i){
    System.out.println(i.getInfo());
  }
}

interface IReceiver{
  public String getInfo();
}

//电子邮件类
class Email implements IReceiver{
  public String getInfo(){
    return "电子邮件信息：Hello";
  }
}

class Wx implements IReceiver{
  public String getInfo(){
    return "微信信息： Hello";
  }
}