package com.atguigu.principle.inversion.pro;

/**
 * @author caoyi
 * @create 2021-01-03-16:34
 * 实现依赖传递的三种方式
 */
public class ThreeMethodImpl {


  /*
  * 方式1：通过接口传递实现依赖
  * 开关接口
  * */
  interface IOpenAndClose{
    public void open(ITV itv);//抽象方法，接收接口
  }
  interface ITV{
    public void play();
  }
  class OpenAndClose implements IOpenAndClose{
    @Override
    public void open(ITV itv) {
      itv.play();
    }
  }

  /*通过构造方法传递依赖  *
  * */
  /*interface IOpenAndClose{
    public void open();
  }
  interface ITV{
    public void play();
  }
  class OpenAndClose implements IOpenAndClose{
    public ITV tv;
    public OpenAndClose(ITV tv) {
      this.tv = tv;
    }
    @Override
    public void open() {
      this.tv.play();
    }
  }
*/

  /*通过setter方法实现传递*/
  /*interface IOpenAndClose{
    public void open();
    public void setTv(ITV tv);
  }
  interface ITV{
    public void play();
  }
  class OpenAndClose implements IOpenAndClose{
    public ITV tv;

    @Override
    public void setTv(ITV tv) {
     this.tv = tv;
    }

    @Override
    public void open() {
      this.tv.play();
    }
  }
*/

}
