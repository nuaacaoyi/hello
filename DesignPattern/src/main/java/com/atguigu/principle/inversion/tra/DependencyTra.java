package com.atguigu.principle.inversion.tra;

/**
 * @author caoyi
 * @create 2021-01-03-16:19
 */
public class DependencyTra {
  public static void main(String[] args) {
    Person person = new Person();
    person.receive(new Email());
  }
}

//完成Person接收消息的功能
//传统方式 1
/*存在问题：
*  1.简单，比较容易实现
*  2.如果获取的对象是微信了，不再是电子邮件了， Person的代码就需要调整，增加接收方法，同时要新增微信类。
*  3.解决思路：引入一个抽象的接口 IReceiver， 表示接收者，这样Person类与接口发生依赖
* */
class Person{
  public void receive(Email email){
    email.getInfo();
  }
}
//电子邮件类
class Email{
  public String getInfo(){
    return "电子邮件信息：Hello";
  }
}