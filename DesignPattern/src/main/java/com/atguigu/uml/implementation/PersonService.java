package com.atguigu.uml.implementation;

/**
 * @author caoyi
 * @create 2021-01-04-10:56
 */
public interface PersonService {
  public void delete(Integer id);
}
