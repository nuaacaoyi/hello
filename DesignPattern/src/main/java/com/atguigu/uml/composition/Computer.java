package com.atguigu.uml.composition;

/**
 * @author caoyi
 * @create 2021-01-04-11:14
 */
public class Computer {
  private Mouse mouse; //鼠标和计算机可以分离
  private Monitor monitor = new Monitor();//鼠标和显示器不能分离了，创建computer的时候，必须创建monitor

  public void setMainBoard(MainBoard mainBoard) {
    this.mainBoard = mainBoard;
  }

  private MainBoard mainBoard = new MainBoard();//主板不能分离，没有主板，电脑没法用，所以创建computer的时候必须创建主板

  public void setMouse(Mouse mouse) {
    this.mouse = mouse;
  }

  public void setMonitor(Monitor monitor) {
    this.monitor = monitor;
  }
}
