package com.woniuxy.principle.c_interfaceisolation.positive;

/**
 * @Author caoyi
 * @Create 2022-05-05-17:02
 * @Description: TODO
 */
public class Dog implements Eatable,Swimable {
  @Override
  public void eat() {
    System.out.println("狗啃骨头");
  }

  @Override
  public void swim() {
    System.out.println("狗刨");
  }
}
