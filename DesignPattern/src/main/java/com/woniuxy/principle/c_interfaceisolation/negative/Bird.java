package com.woniuxy.principle.c_interfaceisolation.negative;

/**
 * @Author caoyi
 * @Create 2022-05-05-17:00
 * @Description: TODO
 */
public class Bird implements Animal {
  @Override
  public void eat() {
    System.out.println("鸟吃虫子");
  }

  @Override
  public void swim() {
    throw new RuntimeException("You can You up...\n");
  }

  @Override
  public void fly() {
    System.out.println("我飞！");
  }
}
