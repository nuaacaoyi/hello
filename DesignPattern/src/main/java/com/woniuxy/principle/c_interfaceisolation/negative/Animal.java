package com.woniuxy.principle.c_interfaceisolation.negative;

/**
 * @author caoyi
 * @create 2021-01-05-13:41
 * 反例，违反了接口隔离原则
 */
public interface Animal {
  void eat();

  // 所有的动物都会游泳么？
  void swim();

  // 所有的动物都会飞么？
  void fly();
}
