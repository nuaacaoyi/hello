package com.woniuxy.principle.c_interfaceisolation.negative;

/**
 * @Author caoyi
 * @Create 2022-05-05-16:58
 * @Description: TODO
 */
public class Dog implements Animal {
  @Override
  public void eat() {
    System.out.println("狗啃骨头");
  }

  @Override
  public void swim() {
    System.out.println("狗刨");
  }

  @Override
  public void fly() {
    throw new RuntimeException("你行你来...\n");
  }
}
