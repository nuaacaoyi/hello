package com.woniuxy.principle.e_demeter.positive;


/**
 * @author caoyi
 * @create 2021-01-05-16:20
 * 以关机为例
 */


class Computer{
  private void saveData(){
    System.out.println("保存数据");
  }
  private void killProcess(){
    System.out.println("杀死进程");
  }
  private void closeScreen(){
    System.out.println("关闭屏幕");
  }
  private void powerOff(){
    System.out.println("断电");
  }

  public void shutdownComputer(){
    saveData();
    killProcess();
    closeScreen();
    powerOff();
  }
}

// =====================分界线，上面是服务端；下面是客户端================================
class Person{
  private Computer c = new Computer();
  // 正例
  public void shutdownComputer(){
    c.shutdownComputer();
  }
}

public class AppTest {

}
