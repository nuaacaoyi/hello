package com.woniuxy.principle.e_demeter.negative;


/**
 * @author caoyi
 * @create 2021-01-05-16:20
 * 以关机为例
 */


class Computer{
  public void saveData(){
    System.out.println("保存数据");
  }
  public void killProcess(){
    System.out.println("杀死进程");
  }
  public void closeScreen(){
    System.out.println("关闭屏幕");
  }
  public void powerOff(){
    System.out.println("断电");
  }
}

// =====================分界线，上面是服务端；下面是客户端================================
class Person{
  private Computer c = new Computer();
  // 反例，此时person对于Computer的细节就知道的太多了，对于person而言，只需要知道关机按钮就行
  // 此时代码的复杂度非常高，而且顺序全都用Person决定，可能用户出错
  public void shutdownComputer(){
    c.saveData();
    c.killProcess();
    c.closeScreen();
    c.powerOff();
  }
}
public class AppTest {

}
