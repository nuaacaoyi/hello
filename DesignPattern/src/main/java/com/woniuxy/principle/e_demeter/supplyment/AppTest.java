package com.woniuxy.principle.e_demeter.supplyment;

import java.util.List;

/**
 * @author caoyi
 * @create 2021-01-05-16:31
 * supplyment 意思是补充
 * 什么是朋友：
 *    a 类中的字段
 *    b 方法的参数
 *    c 方法的返回值
 *    d 方法中实例化出的对象
 */
class Foo{
  public Bar get(){
    return new Bar();
  }
}
class Bar{

}
public class AppTest {
  // 只要是朋友，调用朋友的所有方法都是可以的，不违反迪米特法则
  private List list;// List是朋友
  private List<String> ss; // String也是朋友
  public String f1(Integer i){// Integer String都是朋友
    String s = String.valueOf(i);  // String
    return s;
  }
  public void f2(){
    Foo f = new Foo();//Foo 是 AppTest2的朋友
    Bar bar = f.get();//Bar 不是 AppTest2的朋友，它不是f2中实例化出来的，它是通过Foo实例化的
    // 此处如果调用了bar的方法，就违反了迪米特法则
  }
}
