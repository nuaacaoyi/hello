package com.woniuxy.principle.b_openclose.negative;

/**
 * @author caoyi
 * @create 2021-01-05-13:24
 *
 * 需求：打印输出car的价格
 * 需求变化：价格需要打折
 */
public class AppTest {
  public static void main(String[] args) {
    Car car = new Car();
    car.setBrand("奔驰");
    car.setColor("黑色");
    car.setLouyou(true);
    car.setPrice(666666);
    //变化来了，现在所有的汽车都需要打8折
    //违反开闭原则的做法是，直接打开Car的源码，在 getPrice方法中修改

    System.out.println(car.toString());
  }
}
