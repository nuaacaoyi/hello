package com.woniuxy.principle.b_openclose.positive;

/**
 * @author caoyi
 * @create 2021-01-05-13:23
 */
public class Car {
  private String brand;
  private String color;
  private boolean louyou;
  private double price;

  @Override
  public String toString() {
    return "Car{" +
        "brand='" + brand + '\'' +
        ", color='" + color + '\'' +
        ", louyou=" + louyou +
        ", price=" + getPrice() +
        '}';
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public boolean isLouyou() {
    return louyou;
  }

  public void setLouyou(boolean louyou) {
    this.louyou = louyou;
  }

  //反例，违反了开闭原则
  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}
