package com.woniuxy.principle.b_openclose.positive;

/**
 * @author caoyi
 * @create 2021-01-05-13:24
 *
 * 需求：打印输出car的价格
 * 需求变化：价格需要打折
 */
public class AppTest {
  public static void main(String[] args) {
    Car car = new DiscountCar();
    car.setBrand("奔驰");
    car.setColor("黑色");
    car.setLouyou(true);
    car.setPrice(666666);
    //变化来了，现在所有的汽车都需要打8折
    //符合开闭原则的做法是，始终保证Car的源代码不会被修改。 创建一个Car的子类，重写Car的getPrice方法

    System.out.println(car.toString());
  }
}
