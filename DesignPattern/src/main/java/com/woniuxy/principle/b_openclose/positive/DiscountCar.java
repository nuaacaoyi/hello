package com.woniuxy.principle.b_openclose.positive;

/**
 * @author caoyi
 * @create 2021-01-05-13:29
 */
public class DiscountCar extends Car {
  @Override
  public double getPrice() {
    return super.getPrice() * 0.8;
  }
}
