package com.woniuxy.principle.a_singleresponsibility.positive;

import java.io.*;

/**
 * @author caoyi
 * @create 2021-01-05-11:28
 */
public class AppTest2 {
  public static String loadFile(String path) throws IOException {
    Reader in = new FileReader(path);
    BufferedReader br = new BufferedReader(in);
    String line = null;
    StringBuilder sb = new StringBuilder("");
    while ((line = br.readLine()) != null){
      sb.append(line);
      sb.append(" ");
    }
    br.close();
    in.close();
    return  sb.toString();
  }
  public static String[] getWords(String str,String regstr){
    String[] s = str.split(regstr);
    // 问题又又来了，统一文本中有多少个句子？ 以。 ！ ？为分隔符
    return s;
  }

  public static void main(String[] args) throws IOException {
    String str = loadFile("D:\\tmp\\1.txt");
    System.out.println(getWords(str,"[^a-zA-Z]+"));
  }
}
