package com.woniuxy.principle.a_singleresponsibility.negative;

import java.io.*;

/**
 * @author caoyi
 * @create 2021-01-05-10:24
 * 统计一个文本文件中，有多少个字符？
 * --> 需求变更为   统计一个文本文件中，有多少个单词？
 */
public class AppTest {
  public static void main(String[] args) throws IOException {
    // 统计一个文本文件中，有多少个字符    字符流查码表，字节流不查码表
    // Reader默认查询的码表是与操作系统一致的码表，我们操作系统是中文的，所以Reader就会使用GBK码表
    // 而GBK种一个汉字占两个字节（UTF-8一个汉字占仨字节，ASCII码没有汉字），且汉字的两个字节都是以1开头的（字节码最高位）
    // GBK解码时会根据字节码最高位判断，如果为1就取两个字节作为字符，如果为0则取一个字节作为字符
    Reader in = new FileReader("D:\\tmp\\1.txt");

    // Reader解读时按照系统默认GBK得到 GBK数字，  然后去Unicode查该GBK编码对应的Unicode编码，返回的就是 Unicode编码
    //int n = in.read();
    //System.out.println(n + " " +(char)n);
    /*int n;
    int count =0;
    while ((n = in.read()) != -1)
    {
      count++;
    }
    System.out.println("文件中字符数量：" + count);
    */
    BufferedReader br = new BufferedReader(in);
    String line = null;
    StringBuilder sb = new StringBuilder("");
    while ((line = br.readLine()) != null){
      sb.append(line);
      sb.append(" ");
    }
    // 判断单词数量
    //String[] s = sb.toString().split(" ");
    // 问题来了，当文本中两个单词之间有很多很多的空格怎么办？
    //String[] s = sb.toString().split(" +");
    // 问题又来了，当文本中两个单词之间有很多很多的其他字符怎么办？如#￥%换行等？
    String[] s = sb.toString().split("[^a-zA-Z]+");
    // 问题又又来了，统一文本中有多少个句子？ 以。 ！ ？为分隔符
    System.out.println("文件中单词数量：" + s.length);
    br.close();
    in.close();

   /* // 验证，字节流读取不判断码表，所以也不会判断字节最高位是1还是0（也就是正数还是负数）
    OutputStream out = new FileOutputStream(":\\tmp\\2.txt");
    out.write(97);// 写入97，记事本打开时解码为 a
    out.write(-97);// 写入-97，记事本打开时解码将  -97 98 合为一个字符展示
                      // 但是写入 -129，也就是超出128时，发生溢出，这个值又强转为 正数了
    out.write(98);
    out.close();*/

    /*// 编码+解码过程
    String s ="北";
    // 编码：字符 --> 码表 --> 数字
    // 解码：数字 --> 码表 --> 字符
    byte[] bb = s.getBytes("gbk");
    System.out.println(Arrays.toString(bb));
    // 解码：数字 --> 码表 --> 字符
    byte[] bb2 = s.getBytes("unicode");
    System.out.println(Arrays.toString(bb2));
    */

  }
}
