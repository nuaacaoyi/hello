package com.woniuxy.principle.f_liskov.positive;

/**
 * @author caoyi
 * @create 2021-01-05-16:53
 * 继承的作用：
 *    1.提高代码重用性，子类可以继承父类的所有成员
 *    2.多态的前提
 * 两个类能不能发生继承关系的依据是什么？
 *    1.主要看有没有“is a”关系
 *    2.在两个类有了 is a 关系之后，还要考虑子类对象在替换了父类对象之后，业务逻辑是否变化！如果变化，则不能发生继承关系
 *
 * 正方形 和 长方形 有“is a”关系。那么 我们能不能让正方形直接去继承长方形类？ 那要考虑业务场景
 *    业务场景，变换长方形，只要其 长小于宽，就一直增加其长度
 */
class Utils{
  public static void transform(Rectangle r){
    while (r.getWidth() <= r.getLength()){
      r.setWidth(r.getWidth() + 1);
      System.out.println(r.getWidth() + "--" + r.getLength());
    }
  }
}

class Rectangle{
  private double length;
  private double width;
  @Override
  public String toString() {
    return "Rectangle{" +
        "length=" + length +
        ", width=" + width +
        '}';
  }

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }
}

// 正例：取消继承关系
class Square{
  private double sidelength;
  public double getLength() {
    return sidelength;
  }

  public void setLength(double length) {
    sidelength = length;
  }

  public double getWidth() {
    return sidelength;
  }

  public void setWidth(double width) {
    sidelength = width;
  }
}

public class AppTest {
  public static void main(String[] args) {
    // 如果这里使用正方形的实例替换，就会陷入死循环，再也出不来了
    Rectangle r = new Rectangle();
    r.setWidth(12);
    r.setLength(20);

    Utils.transform(r);
  }
}
