package com.woniuxy.principle.f_liskov.supplement;

/**
 * @author caoyi
 * @create 2021-01-05-16:43
 * 方法重写：在子类和父类中，出现了返回类型相同、方法名相同、方法参数相同的方法时，构成方法重写
 * 方法重写的两个限制：
 *    1）子类重写父类的方法时，子类方法的访问修饰符不能比父类的更严格；
 *    2）子类重写父类的方法时，子类方法不能比父类抛出更多的异常
 * 为什么存在以上两个限制： 为了保证，在子类对象替换父类对象后，语法不会报错。 事实上就是为了保证代码符合里氏替换原则
 */
class Fu{
  public void f1(){

  }
}
class Zi extends  Fu{
  public void f1(){

  }
}
public class AppTest {
  public static void main(String[] args) {
    Fu fu = new Fu(); // 可以将第二个Fu换为Zi
    fu.f1();
  }
}
