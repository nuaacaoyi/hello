package com.woniuxy.principle.d_dependencyreverse.negative;

/**
 * @author caoyi
 * @create 2021-01-05-14:14
 * main和Person之间，Person是下层；  Person和Dog之间，Dog是下层
 * 违反了依赖倒置原则，每当下层变动时（如要增加喂养的动物），上层就需要变动一次，扩展性十分的差
 * 我们希望的是，当下层增加一个动物时，上层完全不需要变动
 */
class Person{
  public void feed(Dog dog){
    System.out.println("开始喂养");
    dog.eat();
  }
}
class Dog{
  public void eat(){
    System.out.println("狗啃骨头");
  }
}

// ===============================划分作者与用户，上面是作者（服务端开发），下面是用户（客户端开发）=================
public class AppTest {
  public static void main(String[] args) {
    Person p = new Person();
    Dog d = new Dog();

    p.feed(d);

    // 变化来了，客户端不仅仅需要喂狗，还需要喂猫
    // 这个时候就需要改Person里面的eat方法（或者重载），这违反了开闭原则

  }
}
class Cat{
  public void eat(){
    System.out.println("猫吃鱼");
  }
}
