package com.woniuxy.principle.g_composite.c;


import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author caoyi
 * @create 2021-01-05-18:01
 *  第二次优化，针对于b包的问题，MySet必须依赖于这样一个事实：addAll方法必须回调add，但是jdk未来的版本中，不会有这个保证
 *  修改代码如下：我们自己重写addAll，这次重写addAll，不再让count累加c.size(),但是保证 addAll回调add
 *
 *  产生新问题1：如果在新的jdk版本中，HashSet突然多了一个 添加元素的方法(如 addSome)，这个addSome是我们始料未及的
 *            MySet也未重写，因此无法计数
 *  产生新问题2：我们重写了 addAll方法 和 add方法，要知道，在HashSet的所有方法中，难免有其他方法会依赖于 这两个方法，
 *            私自改动很可能导致依赖于这两个方法的 依赖者 无法使用。。这也违背了开闭原则，客户端的程序员修改了服务端的程序员
 */
class MySet extends HashSet{
  private int count = 0;

  public int getCount() {
    return count;
  }

  @Override
  public boolean add(Object o) {
    count++;
    return super.add(o);
  }

  // 重写addAll方法，保证调用add方法，以便于计数
  @Override
  public boolean addAll(Collection c) {
    boolean modified = false;
    for (Object obj :
        c) {
      if(add(obj)) modified=true;
    }
    return modified;
  }
}

public class AppTest {
  public static void main(String[] args) {
    MySet set = new MySet();
    Set set2 = new HashSet();
    set2.add("葵花宝典");
    set2.add("北冥神功");
    set.addAll(set2);

    set.add("a");
    set.add("b");
    set.add("c");
    set.remove("b");
    set.remove("c");

    System.out.println(set.getCount());
  }
}
