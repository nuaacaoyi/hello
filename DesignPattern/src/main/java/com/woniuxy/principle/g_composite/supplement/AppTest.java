package com.woniuxy.principle.g_composite.supplement;

import java.util.Stack;

/**
 * @author caoyi
 * @create 2021-01-05-19:36
 * Stack是违反了 组合优于继承原则的反例  为了复用一些代码，继承了Vector，导致它一些不需要的方法也继承了（如get(i)），导致客户使用这个类的时候可能是 Stack用的都不像个栈
 * 这个类非常垃圾（(lll￢ω￢)）
 */
public class AppTest {
  public static void main(String[] args) {
    Stack<String> s = new Stack<>();

    s.push("啤酒");
    s.push("白酒");
    s.push("红酒");
    s.push("醪糟");

    System.out.println(s.pop());
    System.out.println(s.pop());
    System.out.println(s.pop());
    System.out.println(s.pop());
  }
}
