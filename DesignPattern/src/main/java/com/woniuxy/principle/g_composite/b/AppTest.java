package com.woniuxy.principle.g_composite.b;


import java.util.HashSet;
import java.util.Set;

/**
 * @author caoyi
 * @create 2021-01-05-18:01
 * 第一次优化，addAll会回调add方法，所以做如下修改，把 addAll的重写删掉
 *  此时代码看上去很完美，解决了计数问题
 *  但是，目前的代码必须依赖于一个事实，就是HashSet的addAll方法必须回调add方法。
 *  万一，哪一天addAll不再回调，或者我们重写了这个方法，不再回调add方法了，怎么办？
 *  比如：HashMap，在jdk1.6 1.7 1.8中底层结构换了3次，只是应用层未变
 */
class MySet extends HashSet{
  private int count = 0;

  public int getCount() {
    return count;
  }

  @Override
  public boolean add(Object o) {
    count++;
    return super.add(o);
  }

}

public class AppTest {
  public static void main(String[] args) {
    MySet set = new MySet();
    Set set2 = new HashSet();
    set2.add("葵花宝典");
    set2.add("北冥神功");
    set.addAll(set2);

    set.add("a");
    set.add("b");
    set.add("c");
    set.remove("b");
    set.remove("c");

    System.out.println(set.getCount());
  }
}
