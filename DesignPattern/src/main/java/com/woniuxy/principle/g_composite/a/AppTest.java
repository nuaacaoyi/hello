package com.woniuxy.principle.g_composite.a;


import java.util.Collection;
import java.util.HashSet;

/**
 * @author caoyi
 * @create 2021-01-05-18:01
 * 需求：制作一个集合，要求该集合能记录曾经加过多少个元素。（不是统计目前集合中的元素数量）
 */
class MySet extends HashSet{
  private int count = 0;

  public int getCount() {
    return count;
  }

  @Override
  public boolean add(Object o) {
    count++;
    return super.add(o);
  }

  // 需要把所有的增加元素的入口都重写，才能保证 count计数准确
  @Override
  public boolean addAll(Collection c) {
    count += c.size();
    // addAll方法内部调用了add方法，所以两个地方都写会导致 count翻倍
    // 所以这里实际上又不需要对addAll重写
    return super.addAll(c);
  }
}

public class AppTest {
  public static void main(String[] args) {
    MySet set = new MySet();
    set.add("a");
    set.add("b");
    set.add("c");
    set.remove("b");
    set.remove("c");

    System.out.println(set.getCount());
  }
}
