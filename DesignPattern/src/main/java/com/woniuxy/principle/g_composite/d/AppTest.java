package com.woniuxy.principle.g_composite.d;


import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author caoyi
 * @create 2021-01-05-18:01
 *  第三次优化，针对c包的问题，修改代码：
 *    1.不再重写add 和 addAll方法
 *    2.我们额外制作两个代替add和addAll的方法： add2  addAll2。
 *      然后还要在API文档中说明，每当使用add和addAll方法时候，都要去调用add2和addAll2
 *
 *  产生新问题：要求客户干这干那，太勉强了，同样违反了开闭原则。
 *    1.目前这种情况对用户要求有点过分；
 *    2.更致命的问题是：就是那么寸，在jdk新版本中，HashSet中新增了两个方法，恰好叫 add2 和addAll2.。完犊子
 */
class MySet extends HashSet{
  private int count = 0;

  public int getCount() {
    return count;
  }

  public boolean add2(Object o) {
    count++;
    return super.add(o);
  }

  public boolean addAll2(Collection c) {
    boolean modified = false;
    for (Object obj :
        c) {
      if(add(obj)) modified=true;
    }
    return modified;
  }
}

public class AppTest {
  public static void main(String[] args) {
    MySet set = new MySet();
    Set set2 = new HashSet();
    set2.add("葵花宝典");
    set2.add("北冥神功");
    set.addAll(set2);

    set.add("a");
    set.add("b");
    set.add("c");
    set.remove("b");
    set.remove("c");

    System.out.println(set.getCount());
  }
}
