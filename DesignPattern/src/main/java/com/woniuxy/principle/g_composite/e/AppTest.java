package com.woniuxy.principle.g_composite.e;


import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author caoyi
 * @create 2021-01-05-18:01
 * 针对于d包中的问题，修改代码：
 *    1.我们MySet不要继承 HashSet了
 *    2.取而代之，我们让MySet和HashSet发生关联关系（组合）
 */
class MySet{
  private Set set = new HashSet();
  private int count = 0;

  public int getCount() {
    return count;
  }

  public boolean add(Object o) {
    count++;
    return set.add(o);
  }

  public boolean addAll(Collection c) {
    count += c.size();
    return set.addAll(c);
  }
}

public class AppTest {
  public static void main(String[] args) {
    MySet set = new MySet();
    Set set2 = new HashSet();
    set2.add("葵花宝典");
    set2.add("北冥神功");
    set.addAll(set2);

    set.add("a");
    set.add("b");
    set.add("c");

    System.out.println(set.getCount());
  }
}
