package com.woniuxy.pattern.j_proxy.b;

/**
 * @author caoyi
 * @create 2021-01-11-14:42
 * 有以下功能：能做加减乘除
 *  变化来了：要求，为每个方法，添加日志功能，在方法开始时和结束时，分别打印出日志信息
 *   为每个方法开始和结束位置添加代码。。。
 *
 *   问题：1.代码在重复；2.如果需求变化要求打印英文，每个地方都要改；
 *        3.非核心业务和核心业务耦合在一起，导致代码膨胀 ；4.新增新业务时，如开放、平方、求余等，完犊子了
 */
interface ICalc{
  int add(int a, int b);
  int sub(int a, int b);
  int mul(int a, int b);
  int div(int a, int b);
}

class CalcImpl implements ICalc{
  @Override
  public int add(int a, int b) {
    System.out.println("add方法开始，参数为 " + a + " 和 " + b);
    int r = a + b;
    System.out.println("add方法结束，结果为 " + r);
    return r;
  }

  @Override
  public int sub(int a, int b) {
    System.out.println("sub方法开始，参数为 " + a + " 和 " + b);
    int r = a - b;
    System.out.println("sub方法结束，结果为 " + r);
    return r;
  }

  @Override
  public int mul(int a, int b) {
    System.out.println("mul方法开始，参数为 " + a + " 和 " + b);
    int r = a * b;
    System.out.println("mul方法结束，结果为 " + r);
    return r;
  }

  @Override
  public int div(int a, int b) {
    System.out.println("div方法开始，参数为 " + a + " 和 " + b);
    int r = a/b;
    System.out.println("div方法结束，结果为 " + r);
    return r;
  }
}

public class AppTest {
  public static void main(String[] args) {
    ICalc c = new CalcImpl();
    System.out.println(c.add(1,2));
  }
}
