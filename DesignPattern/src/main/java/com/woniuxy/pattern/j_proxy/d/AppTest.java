package com.woniuxy.pattern.j_proxy.d;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * @author caoyi
 * @create 2021-01-11-14:42
 * 针对于c包的问题，解决方案：动态代理（最适合方法，虽然有其他方法勉强可以解决c包问题，但是就动态代理最合适）
 * 解决这个问题，需要先学习下jdk的api 动态代理
 */
interface ICalc{
  int add(int a, int b);
  int sub(int a, int b);
  int mul(int a, int b);
  int div(int a, int b);
}

class CalcImpl implements ICalc {
  @Override
  public int add(int a, int b) {
    int r = a + b;
    return r;
  }

  @Override
  public int sub(int a, int b) {
    int r = a - b;
    return r;
  }

  @Override
  public int mul(int a, int b) {
    int r = a * b;
    return r;
  }

  @Override
  public int div(int a, int b) {
    int r = a/b;
    return r;
  }
}
// ==================================================================================
class MyHandler implements InvocationHandler{
  private Object target;

  public MyHandler(Object target) {
    this.target = target;
  }

  public MyHandler() {
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    // 参数1：代理对象本身
    // 参数2：方法
    // 参数3：实参列表
    System.out.println(method.getName()+"方法开始，参数为 " + Arrays.toString(args));

    Object invoke = method.invoke(target, args);

    System.out.println(method.getName()+"方法结束，结果为 " + invoke);
    return invoke;
  }
}

public class AppTest {
  public static void main(String[] args) {

    // 目标对象（真实对象）
    ICalc c = new CalcImpl();
    // System.out.println(c.add(2,3));

    // 创建代理对象
    // 参数1：ClassLoader 类加载器
    // 我们都知道，要实例化一个对象，是需要调用类的构造器的，在程序运行期间第一次调用构造器时，就会引起类的加载
    // 加载类的时候，就是JVM拿着ClassLoader去加载类的字节码，只有字节码被加载到内存当中，才能进一步去实例化出来的对象
    // 简单来说，只要涉及实例化类的对象，就一定要 加载 类的字节码，而加载字节码，就必须使用类加载器
    // 下面我们使用的是动态代理的api来创建一个类的对象，这是一种不常用的实例化类对象的方式，尽管不常用，但毕竟涉及实例化类的对象
    // 因此也需要加载类的字节码，因此是一定需要类加载器  ClassLoader，所以我们手动传入类加载器
    ClassLoader c1 = CalcImpl.class.getClassLoader();

    // 参数2：Class[]
    // 我们已经知道，下面的代码，是用来实例化一个对象的，实例化对象，就一定是实例化某一个类的对象，问题是，到底是哪个类呢？
    // 类名在哪里？字节码又在哪里？
    // 答：这个类并不在硬盘上，而是在内存中！是由动态代理在内存中“动态”生成的！（jvm_mechanism.png）
    // 要知道，这个在内存中直接生成的字节码，会去实现下面方法中的第2个参数中，所指定的接口！
    // 所以，利用动态代理生成的对象，就能够向上转型 称为 ICalc 接口类型。 那么这个代理对象就拥有 add sub mul div方法

    // 参数3：InvocationHandler 这是一个接口，需要实现
    // 我们已经知道，下面的代理对象proxy所属的类实现了 ICalc接口。 所以，这个代理对象拥有add sub mul div方法
    // 那么，我们就可以使用代理对象调用这四个方法；
    // 注意，每次对代理对象任何方法的调用，都不会进入真正的实现方法中。
    // 而是 统统进入第三个参数的 invoke方法中
    ICalc proxy = (ICalc) Proxy.newProxyInstance(c1, new Class[]{ICalc.class}, new MyHandler(c));

    System.out.println(proxy.add(1,2));
    System.out.println(proxy.sub(1,2));
    System.out.println(proxy.mul(1,2));
    System.out.println(proxy.div(1,2));
  }
}

