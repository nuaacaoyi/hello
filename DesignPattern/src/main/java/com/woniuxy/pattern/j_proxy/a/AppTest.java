package com.woniuxy.pattern.j_proxy.a;

/**
 * @author caoyi
 * @create 2021-01-11-14:42
 * 有以下功能：能做加减乘除
 *  变化来了：要求，为每个方法，添加日志功能，在方法开始时和结束时，分别打印出日志信息
 *   为每个方法开始和结束位置添加代码。。。
 *
 */
interface ICalc{
  int add(int a, int b);
  int sub(int a, int b);
  int mul(int a, int b);
  int div(int a, int b);
}

class CalcImpl implements ICalc{
  @Override
  public int add(int a, int b) {
    int r = a + b;
    return r;
  }

  @Override
  public int sub(int a, int b) {
    int r = a - b;
    return r;
  }

  @Override
  public int mul(int a, int b) {
    int r = a * b;
    return r;
  }

  @Override
  public int div(int a, int b) {
    int r = a/b;
    return r;
  }
}

public class AppTest {
  public static void main(String[] args) {
    ICalc c = new CalcImpl();
    System.out.println(c.add(1,2));
  }
}
