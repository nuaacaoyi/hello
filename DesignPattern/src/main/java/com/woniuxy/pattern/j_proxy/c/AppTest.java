package com.woniuxy.pattern.j_proxy.c;

import java.util.Arrays;

/**
 * @author caoyi
 * @create 2021-01-11-14:42
 * 针对于b包的问题，我们提高代码的重用性，解决方案：增加两个方法
 *  优点：1）提高了代码重用性；2）如果需求再次变化，只需要修改一个地方
 *  仍存在 b包中的34问题： 3.非核心业务和核心业务耦合在一起，导致代码膨胀 ；4.新增新业务时，如开放、平方、求余等，完犊子了
 *  更奇葩的需求：早上6-12之间，不要日志；下午14-18点之间要日志
 */
interface ICalc{
  int add(int a, int b);
  int sub(int a, int b);
  int mul(int a, int b);
  int div(int a, int b);
}

class CalcImpl implements ICalc{
  private void begin(String methodName, Object... params){
    System.out.println(methodName+"方法开始，参数为 " + Arrays.toString(params));
  }
  private void end(String methodName, Object r){
    System.out.println(methodName+"方法结束，结果为 " + r);
  }

  @Override
  public int add(int a, int b) {
    begin("add", a, b);
    int r = a + b;
    end("add", r);
    return r;
  }

  @Override
  public int sub(int a, int b) {
    begin("sub", a, b);
    int r = a - b;
    end("sub", r);
    return r;
  }

  @Override
  public int mul(int a, int b) {
    begin("mul", a, b);
    int r = a * b;
    end("mul", r);
    return r;
  }

  @Override
  public int div(int a, int b) {
    begin("div", a, b);
    int r = a/b;
    end("div", r);
    return r;
  }
}

public class AppTest {
  public static void main(String[] args) {
    ICalc c = new CalcImpl();
    System.out.println(c.add(1,2));
    System.out.println(c.sub(1,2));
    System.out.println(c.mul(1,2));
    System.out.println(c.div(1,2));
  }
}
