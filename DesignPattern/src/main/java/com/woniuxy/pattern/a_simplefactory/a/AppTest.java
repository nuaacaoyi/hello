package com.woniuxy.pattern.a_simplefactory.a;

/**
 * @author caoyi
 * @create 2021-01-05-19:56
 * 问题：服务端程序和客户端程序耦合程度太深，当服务端程序改动（如类名调整），客户端的程序必须跟随调整
 *  违反了迪米特法则
 */

interface Food{
  void eat();
}

class Hambuger implements Food{
  @Override
  public void eat() {
    System.out.println("汉堡包");
  }
}

//=============================服务端程序员 和 客户端程序员 分割线================================

public class AppTest {
  public static void main(String[] args) {
    Food f = new Hambuger();
    f.eat();
  }
}
