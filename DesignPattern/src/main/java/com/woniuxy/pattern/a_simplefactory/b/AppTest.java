package com.woniuxy.pattern.a_simplefactory.b;

/**
 * @author caoyi
 * @create 2021-01-05-19:56
 * 针对于a包的问题：服务端程序和客户端程序耦合程度太深，当服务端程序改动（如类名调整），客户端的程序必须跟随调整
 * 修改方案：直接使用简单工厂设计模式  (注意，这个工厂也是服务端程序员提供的)
 *    此时，客户端程序员只需要记住 getFood(n) 就行了，不需要关心 服务端的程序变化
 * 简单工厂设计模式优点：
 *    1.把具体产品的类型，从客户端代码中，解耦出来
 *    2.服务器端如果修改了具体产品的类名，或者实现等程序，客户端也不需要了解
 * 缺点：
 *    1.客户端不得不 死记硬背 数字与具体产品的对应关系（接口规范）
 *    2.如果具体产品特别多，则简单工厂会变得十分臃肿，100个产品就要有100个case
 *    3.最重要的是，变化来了：客户端需要扩展具体产品的时候，势必要修改简单工厂中的代码，这样又违反了开闭原则
 *
 * 由此，引出了 工厂方法 的模式
 */

interface Food{
  void eat();
}

class Hambuger implements Food{
  @Override
  public void eat() {
    System.out.println("汉堡包");
  }
}
class RiceNoodle implements Food{
  @Override
  public void eat() {
    System.out.println("过桥米线");
  }
}

class FoodFactory{
  public static Food getFood(int n){
    Food food = null;
    switch (n){
      case 1:
        food = new Hambuger();
        break;
      case 2:
        food = new RiceNoodle();
        break;
    }
    return food;
  }
}

//=============================服务端程序员 和 客户端程序员 分割线================================

public class AppTest {
  public static void main(String[] args) {
    Food f = FoodFactory.getFood(1);
    f.eat();
  }
}
