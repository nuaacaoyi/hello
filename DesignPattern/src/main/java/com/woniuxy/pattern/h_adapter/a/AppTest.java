package com.woniuxy.pattern.h_adapter.a;


/**
 * @author caoyi
 * @create 2021-01-11-9:19
 * 适配器模式之前的问题场景：
 *  一个类的接口转换成客户希望的另一个接口。适配器模式可以让那些接口不兼容的类一起工作。
 *
 * 通俗一点的解释：根据已有接口，生成想要的接口
 */

class Calc{
  public int add(int a, int b){
    return a + b;
  }
}


// ======================================服务端程序员 和  客户端程序员 时空线======================================
// 变化来了，客户端希望计算3个数的和，而Calc的add方法只能接受2个参数
// 这就是适配器，本来只能接收俩参数，现在可以接收仨参数
// 如果继承只是为了重用，最好不要继承，用组合。。 组合优于继承
/*class CalcAdapter extends Calc{
  public int add(int a, int b, int c){
    return add(a, add(b, c));
  }
}*/
class CalcAdapter{
  private Calc calc;

  public CalcAdapter(Calc calc) {
    this.calc = calc;
  }

  public int add(int a, int b, int c){
    return calc.add(a, calc.add(b,c));
  }
}

public class AppTest {
  public static void main(String[] args) {
    Calc c = new Calc();
    CalcAdapter ca = new CalcAdapter(c);
    int r = ca.add(1, 2, 4);
    System.out.println(r);
  }
}
