package com.woniuxy.pattern.h_adapter.b;

import java.util.Arrays;

/**
 * @author caoyi
 * @create 2021-01-11-9:34
 */
// 此处服务端的代码都是场景4代码
class Waveform{
  // 这两行是一个标准的id自增长逻辑，非常666666666
  private static long counter;
  private final long id = counter++;

  @Override
  public String toString() {
    return "Waveform{" +
        "id=" + id +
        '}';
  }
}

class Filter{
  public String name(){
    return getClass().getSimpleName();
  }
  public Waveform process(Waveform input){
    return input;
  }
}

class LowPass extends Filter{
  double cutoff;// 截至

  public LowPass(double cutoff) {
    this.cutoff = cutoff;
  }

  @Override
  public Waveform process(Waveform input) {
    return input;  // Dummy processing 假装处理了一下
  }
}

class HighPass extends Filter{
  double cutoff;// 截至

  public HighPass(double cutoff) {
    this.cutoff = cutoff;
  }

  @Override
  public Waveform process(Waveform input) {
    return input;  // Dummy processing 假装处理了一下
  }
}
class BandPass extends Filter{
  double lowCutoff, highCutoff;// 截至

  public BandPass(double lowCutoff, double highCutoff) {
    this.highCutoff = highCutoff;
    this.lowCutoff = lowCutoff;
  }

  @Override
  public Waveform process(Waveform input) {
    return input;  // Dummy processing 假装处理了一下
  }
}
// ============================ 服务端程序员 和 客户端程序员 时空线============================
// 变化来了：场景4：我们发现一个业务场景（电子滤波器），同样也适用于 Apply.process()方法


class Processor{
  public String name(){
    return getClass().getSimpleName();
  }

  Object process(Object input){
    return input;
  }
}

class Upcase extends Processor{
  // 参数一样，但是返回值是父类中同名方法的返回值子类型，这叫 返回值的协变类型
  String process(Object input){
    return ((String)input).toUpperCase();
  }
}
class DownCase extends Processor{
  // 参数一样，但是返回值是父类中同名方法的返回值子类型，这叫 返回值的协变类型
  String process(Object input){
    return ((String)input).toLowerCase();
  }
}
class Splitter extends Processor{
  String process(Object input){
    return Arrays.toString(((String)input).split(" "));
  }
}

// 为了避免代码重复，封装一个方法
class Apply{
  public static void process(Processor p, Object s){
    System.out.println("Using Processor: " + p.name() + "; Result: " + p.process(s));
  }
}

public class AppTest {
  public static void main(String[] args) {
    // 场景1
    // f1(new C());
    // 如果方法的形参传入是一个类，那么就只能 f1就只支持ABC三个类传入
    // 但是如果形参传入是个接口，那么就很好处理了
    // f1(new Z());

    // 场景2
    // 使用各种方法，结果发现了各种重复代码
    /*String s = "How Are You!";
    Processor p = new Upcase();
    System.out.println("Using Processor: " + p.name() + "; Result: " + p.process(s));

    Processor p2 = new DownCase();
    System.out.println("Using Processor: " + p2.name() + "; Result: " + p2.process(s));

    Processor p3 = new Splitter();
    System.out.println("Using Processor: " + p3.name() + "; Result: " + p3.process(s));*/

    // 场景3 我们提炼出了Apply方法，让客户端可以方便的使用process方法
    String s = "How Are You!";
    Apply.process(new Upcase(), s);
    Apply.process(new DownCase(), s);
    Apply.process(new Splitter(), s);

    // 场景4 ，我们只能自己写，没法使用 Apply.process方法，这样就会导致代码重复
    // so 适配器来了
    Waveform wf = new Waveform();
    Filter f = new LowPass(1);
    System.out.println("Using Processor: " + f.name() + "; Result: " + f.process(wf));
  }
}
