package com.woniuxy.pattern.c_abstractfactory;


/**
 * @author caoyi
 * @create 2021-01-05-19:56
 * 针对于工厂方法模式的缺点：
 *    如果有多个产品等级（如食物分类为 饮料 和 吃的，各类又包含各自的实现），那么工厂类的数量就会爆炸式增长！！！
 * 修改代码：使用抽象工厂模式
 *    优点：
 *      1.具备简单工厂和工厂方法的优点；
 *      2.更重要的是 抽象工厂方法 把工厂类的数量减少了。 无论有多少个产品等级，工厂就一个
 *
 *    抬杠：
 *      为什么 三秦工厂中 ，就必须是米线搭配冰峰呢？为什么不能是米线搭配可乐呢？
 *      解释：抽象工厂中，可以生产多个产品。但是同一个工厂的多个产品必须有内在联系，形成一个产品簇
 *    缺点：
 *      1.客户端增加产品簇没有问题，完全是可扩展的； 但是增加（或修改或删除）产品等级（产品分类）不行，此时必须修改源码，违反了开闭原则
 *
 */

/* 此时有两类产品（食物和饮料），每类包含两个产品，工厂方法需要搞 12个类。。。爆炸*/
interface Food{
  void eat();
}

class Hambuger implements Food{
  @Override
  public void eat() {
    System.out.println("汉堡包");
  }
}

class RiceNoodle implements Food{
  @Override
  public void eat() {
    System.out.println("过桥米线");
  }
}

interface Drink{
  public void drink();
}

class Cola implements Drink{
  @Override
  public void drink() {
    System.out.println("可口可乐！！");
  }
}

/*抽象工厂模式，工厂就只有三个类*/
class Icepeak implements Drink{
  @Override
  public void drink() {
    System.out.println("冰峰，从小就喝");
  }
}
interface Factory{
  public Food getFood();
  public Drink getDrink();
}
class KFCFactory implements Factory{
  @Override
  public Food getFood() {
    return new Hambuger();
  }

  @Override
  public Drink getDrink() {
    return new Cola();
  }
}
class SanQinFactory implements Factory{
  @Override
  public Food getFood() {
    return new RiceNoodle();
  }

  @Override
  public Drink getDrink() {
    return new Icepeak();
  }
}

/* 工厂方法模式

interface FoodFactory{
  public Food getFood();
}

class HamburgerFactory implements FoodFactory{
  @Override
  public Food getFood() {
    return new Hambuger();
  }
}

class RiceNoodleFactory implements FoodFactory{
  @Override
  public Food getFood() {
    return new RiceNoodle();
  }
}

interface DrinkFactory{
  public Drink getDrink();
}
class ColaFactory implements DrinkFactory{
  @Override
  public Drink getDrink() {
    return new Cola();
  }
}
class IcepeakFactory implements DrinkFactory{
  @Override
  public Drink getDrink() {
    return new Icepeak();
  }
}*/
class Buisiness{
  public static void taste(Factory ff){
    Food f = ff.getFood();
    Drink d = ff.getDrink();
    System.out.println("评委1，品尝");
    f.eat();
    d.drink();

    Food f2 = ff.getFood();
    Drink d2 = ff.getDrink();
    System.out.println("评委2，品尝");
    f2.eat();
    d2.drink();

    Food f3 = ff.getFood();
    Drink d3 = ff.getDrink();
    System.out.println("评委3，品尝");
    f3.eat();
    d3.drink();
  }
}

//=============================服务端程序员 和 客户端程序员 分割线================================

// 需求变化：客户端增加食物
class ItalyNoodle implements Food{
  @Override
  public void eat() {
    System.out.println("意大利面配牛排");
  }
}
class Redwine implements Drink{
  @Override
  public void drink() {
    System.out.println("红酒只喝82年laffie");
  }
}
class HaoxianglaiFactory implements Factory{
  @Override
  public Food getFood() {
    return new ItalyNoodle();
  }

  @Override
  public Drink getDrink() {
    return new Redwine();
  }
}

public class AppTest {
  public static void main(String[] args) {
    Buisiness.taste(new HaoxianglaiFactory());
  }
}
