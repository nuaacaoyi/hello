package com.woniuxy.pattern.d_prototype.a;

import java.util.Date;

/**
 * @author caoyi
 * @create 2021-01-06-15:51
 * OA项目中有个周报提报，每周很多内容不需要变动，增设功能，要求能够让用户加载之前设定的模板，避免重复性的维护
 * 此时就有了对象的克隆的需求
 *
 */
class WeekReport{
  private int id;
  private String emp;
  private String summary;
  private String plain;
  private String suggestion;
  private Date time;

  @Override
  public String toString() {
    return "WeekReport{" +
        "id=" + id +
        ", emp='" + emp + '\'' +
        ", summary='" + summary + '\'' +
        ", plain='" + plain + '\'' +
        ", suggestion='" + suggestion + '\'' +
        ", time=" + time +
        '}';
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmp() {
    return emp;
  }

  public void setEmp(String emp) {
    this.emp = emp;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getPlain() {
    return plain;
  }

  public void setPlain(String plain) {
    this.plain = plain;
  }

  public String getSuggestion() {
    return suggestion;
  }

  public void setSuggestion(String suggestion) {
    this.suggestion = suggestion;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
}
public class AppTest {
  public static void main(String[] args) {
    WeekReport wr = new WeekReport();
    wr.setEmp("张珊珊");
    wr.setSummary("讲解完了7大原则");
    wr.setPlain("讲解完设计模式");
    wr.setSuggestion("无");
    wr.setTime(new Date());

    // 第二周又写周报了  每个值又都填了一遍，我们想要的是克隆
    WeekReport wr2 = new WeekReport();
    wr2.setEmp("张珊珊");
    wr2.setSummary("讲解完了7大原则");
    wr2.setPlain("讲解完设计模式");
    wr2.setSuggestion("无");
    wr2.setTime(new Date());
    System.out.println(wr2);
  }
}
