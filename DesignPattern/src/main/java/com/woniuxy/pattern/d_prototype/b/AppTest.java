package com.woniuxy.pattern.d_prototype.b;

import java.util.Date;

/**
 * @author caoyi
 * @create 2021-01-06-15:51
 * 针对于a包的问题，修改代码如下
 *    使用原型模式来解决问题，步骤如下：
 *      1）必须让目标类实现Cloneable接口，该接口中没有任何抽象方法。这样的接口仅仅是一个“标记接口”，作用是告诉JVM，
 *          任何实现了该Cloneable接口的类的对象，可以被克隆！
 *      2）必须重写java.lang.Object的clone方法，一定要把该方法的访问修饰符，重写为public！！不然无法调用clone方法，存疑？
 *
 *    存在问题：已经达到了深拷贝的目的，也就是修改副本对象的任何属性，都对原来对象没有任何影响
 *    问题是：
 *      1.引用类型成员变量中的引用类型，仍然是指向同一个
 *      2.如果对象深度比较深，则深拷贝实现起来很麻烦
 */

class WeekReport implements Cloneable{
  private int id;
  private String emp;
  private String summary;
  private String plain;
  private String suggestion;
  private Date time;

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public String toString() {
    return "WeekReport{" +
        "id=" + id +
        ", emp='" + emp + '\'' +
        ", summary='" + summary + '\'' +
        ", plain='" + plain + '\'' +
        ", suggestion='" + suggestion + '\'' +
        ", time=" + time +
        '}';
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmp() {
    return emp;
  }

  public void setEmp(String emp) {
    this.emp = emp;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getPlain() {
    return plain;
  }

  public void setPlain(String plain) {
    this.plain = plain;
  }

  public String getSuggestion() {
    return suggestion;
  }

  public void setSuggestion(String suggestion) {
    this.suggestion = suggestion;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
}

public class AppTest {
  public static void main(String[] args) {
    WeekReport wr = new WeekReport();
    wr.setEmp("张珊珊");
    wr.setSummary("讲解完了7大原则");
    wr.setPlain("讲解完设计模式");
    wr.setSuggestion("无");
    wr.setTime(new Date());
    System.out.println(wr);

    // 这里有一个潜在问题，克隆是在不调用构造器的前提下创建一个新的对象，它可能会与单例模式冲突
    // 思考1：clone方法，会不会引起构造器的调用？答：不会！
    // 思考2：那么clone方法是如何实现克隆对象的效果呢？clone方法是直接复制内存中的二进制，效率比构造器更高！
    // 思考3：既然clone方法没有引起构造器的调用，那么克隆出的对象和原先的对象地址是否一致？不一致。
    WeekReport wr2 = null;
    try {
      wr2 = (WeekReport) wr.clone();
      wr2.setSummary("lalalal");
      wr2.setPlain("heiheihei");

      wr2.getTime().setTime(0); // 这句代码会同时修改 wr 和 wr2，因为浅拷贝不会克隆引用类型的成员
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    System.out.println(wr2);
  }
}
