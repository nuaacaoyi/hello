package com.woniuxy.pattern.d_prototype.d;

import java.io.*;
import java.util.Date;

/**
 * @author caoyi
 * @create 2021-01-06-15:51
 * 针对于c包的问题，修改方案如下：
 *  不再用普通的io流，直接使用内存，因此也不再需要 文件路径
 */

class WeekReport implements Cloneable, Serializable {
  private int id;
  private String emp;
  private String summary;
  private String plain;
  private String suggestion;
  private Date time;

  @Override
  protected Object clone() throws CloneNotSupportedException {
    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(out);
      oos.writeObject(this);// 序列化时，对象的所有属性层级关系会被序列化自动处理！！
      oos.close();

      // 从内存中取出数据
      byte[] bb = out.toByteArray();
      InputStream in = new ByteArrayInputStream(bb);
      ObjectInputStream ois = new ObjectInputStream(in);
      Object clone = ois.readObject();
      ois.close();// 关流时只关上层流

      return clone;


    }
    catch (Exception e){
      throw new RuntimeException(e);
    }

  }

  @Override
  public String toString() {
    return "WeekReport{" +
        "id=" + id +
        ", emp='" + emp + '\'' +
        ", summary='" + summary + '\'' +
        ", plain='" + plain + '\'' +
        ", suggestion='" + suggestion + '\'' +
        ", time=" + time +
        '}';
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmp() {
    return emp;
  }

  public void setEmp(String emp) {
    this.emp = emp;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getPlain() {
    return plain;
  }

  public void setPlain(String plain) {
    this.plain = plain;
  }

  public String getSuggestion() {
    return suggestion;
  }

  public void setSuggestion(String suggestion) {
    this.suggestion = suggestion;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
}

public class AppTest {
  public static void main(String[] args) {
    WeekReport wr = new WeekReport();
    wr.setEmp("张珊珊");
    wr.setSummary("讲解完了7大原则");
    wr.setPlain("讲解完设计模式");
    wr.setSuggestion("无");
    wr.setTime(new Date());
    System.out.println(wr);

    // 这里有一个潜在问题，克隆是在不调用构造器的前提下创建一个新的对象，它可能会与单例模式冲突
    // 思考1：clone方法，会不会引起构造器的调用？答：不会！
    // 思考2：那么clone方法是如何实现克隆对象的效果呢？clone方法是直接复制内存中的二进制，效率比构造器更高！
    // 思考3：既然clone方法没有引起构造器的调用，那么克隆出的对象和原先的对象地址是否一致？不一致。
    WeekReport wr2 = null;
    try {
      wr2 = (WeekReport) wr.clone();
      wr2.setSummary("lalalal");
      wr2.setPlain("heiheihei");

      wr2.getTime().setTime(0); // 这句代码会同时修改 wr 和 wr2，因为浅拷贝不会克隆引用类型的成员
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    System.out.println(wr2);
  }
}
