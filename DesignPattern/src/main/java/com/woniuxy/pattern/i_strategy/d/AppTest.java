package com.woniuxy.pattern.i_strategy.d;


/**
 * @author caoyi
 * @create 2021-01-11-11:27
 * 针对于c包的问题：
 * 修改方案： 策略模式
 *   把飞行和叫 方法，从父类中分离出来（注意，这里的分离与c包中的分离不一样）
 * 该方案解决了
 *   1.代码重用性的问题，
 *   2.可以 运行时改变方法行为   ， 如在main客户端中 给橡皮鸭 setFb(new FlyWithKick()); 此时橡皮鸭就可以被一脚踢飞
 *      典型案例： 坦克大战中吃道具改变子弹发射方式
 */
interface FlyBehaviour{
  void fly();
}
class FlyWithWings implements FlyBehaviour{
  @Override
  public void fly() {
    System.out.println("用翅膀飞~~~~~");
  }
}
class FlyWithRocket implements FlyBehaviour{
  @Override
  public void fly() {
    System.out.println("背上绑个窜天猴~~~~~");
  }
}
class FlyWithKick implements FlyBehaviour{
  @Override
  public void fly() {
    System.out.println("用脚踢，飞了~~~~");
  }
}
class FlyWithNoWay implements FlyBehaviour{
  @Override
  public void fly() {
    System.out.println("飞不起来！！");
  }
}

interface QuackBehaviour{
  void quack();
}
class Quack implements QuackBehaviour{
  @Override
  public void quack() {
    System.out.println("嘎嘎叫");
  }
}
class SQuack implements QuackBehaviour{
  @Override
  public void quack() {
    System.out.println("吱吱叫");
  }
}
class MuteQuack implements QuackBehaviour{
  @Override
  public void quack() {
    System.out.println("<<Slience>>");
  }
}

abstract class Duck{
  protected FlyBehaviour fb;
  protected QuackBehaviour qb;

  public FlyBehaviour getFb() {
    return fb;
  }

  public void setFb(FlyBehaviour fb) {
    this.fb = fb;
  }

  public QuackBehaviour getQb() {
    return qb;
  }

  public void setQb(QuackBehaviour qb) {
    this.qb = qb;
  }

  public void swim(){
    System.out.println("鸭子会游泳");
  }

  public void performFly(){
    fb.fly();
  }
  public void performQuack(){
    qb.quack();
  }

  public abstract void display(); // 鸭子的叫声相同，会游泳相同，但外观不同
}

class MallardDuck extends Duck{
  public MallardDuck() {
    this.fb = new FlyWithWings();
    this.qb = new Quack();
  }

  @Override
  public void display() {
    System.out.println("野鸭子，很漂亮");
  }
}

class ReadHeadDuck extends Duck{
  public ReadHeadDuck() {
    this.fb = new FlyWithWings();
    this.qb = new Quack();
  }
  @Override
  public void display() {
    System.out.println("红头鸭，挺逗的");
  }
}

// 橡皮鸭不应该会飞，但是继承后也会飞了。。程序员被策划骂的狗血淋头。
// 所以程序员重写了fly方法，当该方法被调用的时候直接抛出异常
class RubberDuck extends Duck{
  public RubberDuck() {
    this.fb = new FlyWithNoWay();
    this.qb = new SQuack();
  }

  @Override
  public void display() {
    System.out.println("橡皮鸭子");
  }

}

class DecoyDuck extends Duck{
  public DecoyDuck() {
    this.fb = new FlyWithNoWay();
    this.qb = new MuteQuack();
  }

  @Override
  public void display() {
    System.out.println("诱饵鸭");
  }
}
// ================================服务端程序员 和 客户端程序员时空线==============================================
// 变化来了，新增飞行方式：竹蜻蜓
class FlyWithDragonfly implements FlyBehaviour{
  @Override
  public void fly() {
    System.out.println("使用竹蜻蜓飞");
  }
}

public class AppTest {
  public static void main(String[] args) {
    RubberDuck d = new RubberDuck();
    d.performQuack();
    d.swim();
    d.performFly();
    d.display();

    System.out.println("吃道具了，现在橡皮鸭会飞了");
    d.setFb(new FlyWithKick());
    d.performFly();
  }
}
