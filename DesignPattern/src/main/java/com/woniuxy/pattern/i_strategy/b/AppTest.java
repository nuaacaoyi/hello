package com.woniuxy.pattern.i_strategy.b;

/**
 * @author caoyi
 * @create 2021-01-11-11:27
 * 有一家游戏公司，制作了一款鸭子游戏：在这个鸭子游戏中，角色都是鸭子，不同的鸭子之间有共性，
 *    所以，为了提高代码的重用性，开发人员就制作了一个 鸭子父类： Duck，
 * 变化来了：
 *    - 场景1
 *      游戏公司的策划改方案了，他想要让 游戏中的鸭子能飞起来！
 *      程序员想：我在抽象类父类中增加一个飞的方法就ok了
 *    - 场景2：问题看似解决了，但是实际上出现了更麻烦的问题，所有Duck子类的鸭子，统统都会飞了
 *            要知道，父类中的方法，并不是所有的子类都能通用的！！修改父类改变的是一个体系！！！
 *      程序员给 橡皮鸭重写了fly方法，让其不能飞
 *      但是，问题没有被解决，因为Duck可能有很多的子类鸭子都不能飞！！！
 *      此时，程序员就要给每一个新的鸭子 都判断下，它会不会叫，会不会飞，针对于不同的鸭子，要有不同的处理方法，非常麻烦！！！
 *
 */
abstract class Duck{
  public void quack(){
    System.out.println("嘎嘎嘎");
  }
  public void swim(){
    System.out.println("鸭子会游泳");
  }
  public void fly(){
    System.out.println("鸭子竟然会飞");
  }
  public abstract void display(); // 鸭子的叫声相同，会游泳相同，但外观不同
}

class MallardDuck extends Duck{
  @Override
  public void display() {
    System.out.println("野鸭子，很漂亮");
  }
}

class ReadHeadDuck extends Duck{
  @Override
  public void display() {
    System.out.println("红头鸭，挺逗的");
  }
}

// 橡皮鸭不应该会飞，但是继承后也会飞了。。程序员被策划骂的狗血淋头。
// 所以程序员重写了fly方法，当该方法被调用的时候直接抛出异常
class RubberDuck extends Duck{
  @Override
  public void quack() {
    System.out.println("吱吱叫");
  }

  @Override
  public void display() {
    System.out.println("橡皮鸭子");
  }

  @Override
  public void fly() {
    System.out.println("橡皮鸭不会飞，你个2货");
  }
}


public class AppTest {
  public static void main(String[] args) {
    Duck d = new MallardDuck();
    d.quack();
    d.swim();
    d.fly();
    d.display();
  }
}
