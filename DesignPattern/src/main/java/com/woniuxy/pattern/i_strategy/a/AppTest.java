package com.woniuxy.pattern.i_strategy.a;

/**
 * @author caoyi
 * @create 2021-01-11-11:27
 * 有一家游戏公司，制作了一款鸭子游戏：在这个鸭子游戏中，角色都是鸭子，不同的鸭子之间有共性，
 *    所以，为了提高代码的重用性，开发人员就制作了一个 鸭子父类： Duck，
 */
abstract class Duck{
  public void quack(){
    System.out.println("嘎嘎嘎");
  }
  public void swim(){
    System.out.println("鸭子会游泳");
  }
  public abstract void display(); // 鸭子的叫声相同，会游泳相同，但外观不同
}

class MallardDuck extends Duck{
  @Override
  public void display() {
    System.out.println("野鸭子，很漂亮");
  }
}
class ReadHeadDuck extends Duck{
  @Override
  public void display() {
    System.out.println("红头鸭，挺逗的");
  }
}


public class AppTest {
  public static void main(String[] args) {
    Duck d = new MallardDuck();
    d.quack();
    d.swim();
    d.display();
  }
}
