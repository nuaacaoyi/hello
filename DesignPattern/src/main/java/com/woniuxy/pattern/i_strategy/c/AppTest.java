package com.woniuxy.pattern.i_strategy.c;

/**
 * @author caoyi
 * @create 2021-01-11-11:27
 * 针对于b包的问题：
 *      但是，问题没有被解决，因为Duck可能有很多的子类鸭子都不能飞！！！
 *      此时，程序员就要给每一个新的鸭子 都判断下，它会不会叫，会不会飞，针对于不同的鸭子，要有不同的处理方法，非常麻烦！！！
 * 解决方案：
 *    我们希望那些不会飞的鸭子，压根就没有fly方法；不会叫的鸭子，压根就没有quack方法
 *    把这两个经常在子类中变化的方法，从父类中分出来，分成两个接口： Quackable Flyable
 *
 * 当前仍然没有解决问题：以前是每加入一个新的鸭子角色，程序员就要判断鸭子是否会飞会叫，以为其重写相关方法；
 * 现在是每加入一个新的鸭子，程序员仍然要判断，然后编写类时判断是否要实现接口
 *
 * 如此，程序员仍然没有减少工作量，仍然要不断的判断新的鸭子角色。。
 * 另外一个缺点：fly方法没有重用性，每次实现都要写重复代码（这个问题可以使用接口默认方法体解决）；
 * 另外，如果对于48种鸭子，有12种飞行方法，又该如何？
 */
abstract class Duck{
  public void swim(){
    System.out.println("鸭子会游泳");
  }
  public abstract void display(); // 鸭子的叫声相同，会游泳相同，但外观不同
}
interface Quackable{
  public void quack();
}
interface Flyable{
  void fly();
}

class MallardDuck extends Duck implements Quackable,Flyable{
  @Override
  public void display() {
    System.out.println("野鸭子，很漂亮");
  }

  @Override
  public void quack() {
    System.out.println("嘎嘎叫");
  }

  @Override
  public void fly() {
    System.out.println("飞起来了");
  }
}

class ReadHeadDuck extends Duck implements Quackable,Flyable{
  @Override
  public void display() {
    System.out.println("红头鸭，挺逗的");
  }
  @Override
  public void quack() {
    System.out.println("嘎嘎叫");
  }

  @Override
  public void fly() {
    System.out.println("飞起来了");
  }
}

// 橡皮鸭不应该会飞，但是继承后也会飞了。。程序员被策划骂的狗血淋头。
// 所以程序员重写了fly方法，当该方法被调用的时候直接抛出异常
class RubberDuck extends Duck implements Quackable{
  @Override
  public void quack() {
    System.out.println("吱吱叫");
  }

  @Override
  public void display() {
    System.out.println("橡皮鸭子");
  }

}

class DecoyDuck extends Duck{
  @Override
  public void display() {
    System.out.println("诱饵鸭");
  }
}

public class AppTest {
  public static void main(String[] args) {
    Duck d = new MallardDuck();
    ((MallardDuck) d).fly();
  }
}
