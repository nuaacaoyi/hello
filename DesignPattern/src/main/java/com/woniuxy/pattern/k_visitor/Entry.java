package com.woniuxy.pattern.k_visitor;

import java.util.Iterator;

/**
 * @Author caoyi
 * @Create 2021-01-20-18:23
 * @Description: 抽象元素类，必须实现接口：可接受访问者
 */

public abstract class Entry implements Element{
  public abstract String getName();
  public abstract int getSize();
  public abstract void printList(String prefix);
  public  void printList(){
    printList("");
  }
  public  Entry add(Entry entry) throws RuntimeException{
    throw new RuntimeException();
  }
  public Iterator iterator() throws RuntimeException{
    throw new RuntimeException();
  }
  public  String toString(){
    return getName()+"<"+getSize()+">";
  }
}
