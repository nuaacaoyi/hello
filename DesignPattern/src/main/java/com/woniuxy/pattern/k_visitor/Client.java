package com.woniuxy.pattern.k_visitor;

import java.util.Iterator;

/**
 * @Author caoyi
 * @Create 2021-01-20-18:20
 * @Description: 学习自百度 https://www.cnblogs.com/zyrblog/p/9244754.html
 * 什么叫做访问，如果大家学过数据结构，对于这点就很清晰了，遍历就是访问的一般形式，单独读取一个元素进行相应的处理也叫作访问，
 * 读取到想要查看的内容+对其进行处理就叫做访问，那么我们平常是怎么访问的，基本上就是直接拿着需要访问的地址（引用）来读写内存就可以了。
 *
 *  为什么还要有一个访问者模式呢，这就要放到OOP之中了，在面向对象编程的思想中，我们使用类来组织属性，以及对属性的操作，
 *  那么我们理所当然的将访问操作放到了类的内部，这样看起来没问题，但是当我们想要使用另一种遍历方式要怎么办呢，我们必须将这个类进行修改，
 *  这在设计模式中是大忌，在设计模式中就要保证，对扩展开放，对修改关闭的开闭原则。
 *
 *  因此，我们思考，可不可以将访问操作独立出来变成一个新的类，当我们需要增加访问操作的时候，直接增加新的类，原来的代码不需要任何的改变，
 *  如果可以这样做，那么我们的程序就是好的程序，因为可以扩展，符合开闭原则。而访问者模式就是实现这个的，
 *  使得使用不同的访问方式都可以对某些元素进行访问。
 */
public class Client {
  public static void main(String[] args) {
    Directory root=new Directory("根目录");

    Directory life=new Directory("我的生活");
    File eat=new File("吃火锅.txt",100);
    File sleep=new File("睡觉.html",100);
    File study=new File("学习.txt",100);
    life.add(eat);
    life.add(sleep);
    life.add(study);

    Directory work=new Directory("我的工作");
    File write=new File("写博客.doc",200);
    File paper=new File("写论文.html",200);
    File homework=new File("写家庭作业.docx",200);
    work.add(write);
    work.add(paper);
    work.add(homework);

    Directory relax=new Directory("我的休闲");
    File music=new File("听听音乐.js",200);
    File walk=new File("出去转转.psd",200);
    relax.add(music);
    relax.add(walk);

    Directory read=new Directory("我的阅读");
    File book=new File("学习书籍.psd",200);
    File novel=new File("娱乐小说.txt",200);
    read.add(book);
    read.add(novel);

    root.add(life);
    root.add(work);
    root.add(relax);
    root.add(read);

    root.accept(new ListVisitor());
    System.out.println("========================");
    FileVisitor visitor=new FileVisitor(".psd");
    root.accept(visitor);
    Iterator it = visitor.getFiles();
    while(it.hasNext()){
      System.out.println(it.next());
    }
  }
}
