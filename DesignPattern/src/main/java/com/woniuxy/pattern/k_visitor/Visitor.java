package com.woniuxy.pattern.k_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-18:22
 * @Description: TODO
 */

public abstract class Visitor {

  public abstract void visit(File file);
  public abstract void visit(Directory directory);

}
