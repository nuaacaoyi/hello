package com.woniuxy.pattern.k_visitor;

/**
 * @Author caoyi
 * @Create 2021-01-20-18:21
 * @Description: 抽象元素能力：可以接收访问者
 */
public interface Element {
  public abstract void accept(Visitor visitor);
}
