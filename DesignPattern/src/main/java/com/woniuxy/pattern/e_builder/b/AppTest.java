package com.woniuxy.pattern.e_builder.b;

/**
 * @author caoyi
 * @create 2021-01-10-15:03
 * 需求： 定义一个电脑类，并且实例化出电脑类的对象，以及给该对象的属性赋值
 *
 * 针对于a包中的问题，修改代码如下：
 *  服务端程序员，专门创建一个 Computer Builder的类，这个类专门负责封装组装电脑的过程
 *
 * 该写法只是过渡写法，还不是建造者模式
 *  优点：客户端程序员不再需要一个成员一个成员的set了
 *  缺点：能build的产品对象属性都写死了，可能结果产品不是客户端程序员需要的
 */
class Computer{
  private String cpu;
  private String gpu;
  private String memory;
  private String harddisk;

  @Override
  public String toString() {
    return "Computer{" +
        "cpu='" + cpu + '\'' +
        ", gpu='" + gpu + '\'' +
        ", memory='" + memory + '\'' +
        ", harddisk='" + harddisk + '\'' +
        '}';
  }

  public String getCpu() {
    return cpu;
  }

  public void setCpu(String cpu) {
    this.cpu = cpu;
  }

  public String getGpu() {
    return gpu;
  }

  public void setGpu(String gpu) {
    this.gpu = gpu;
  }

  public String getMemory() {
    return memory;
  }

  public void setMemory(String memory) {
    this.memory = memory;
  }

  public String getHarddisk() {
    return harddisk;
  }

  public void setHarddisk(String harddisk) {
    this.harddisk = harddisk;
  }
}

// 电脑建造者类，建造这类必须关联产品
class ComputerBuilder{
  private Computer computer = new Computer();

  public Computer build(){
    computer.setCpu("i7 8750hk"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
    computer.setGpu("rtx2080ti");
    computer.setMemory("32g");
    computer.setHarddisk("500G固态+2T机械");
    return computer;
  }
}


// ======================================服务端程序员 和 客户端程序员 时空线=============================================

public class AppTest {
  public static void main(String[] args) {
    ComputerBuilder cb  = new ComputerBuilder();
    Computer c = cb.build();

    System.out.println(c);
  }
}
