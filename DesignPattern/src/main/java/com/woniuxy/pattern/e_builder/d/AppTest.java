package com.woniuxy.pattern.e_builder.d;


/**
 * @author caoyi
 * @create 2021-01-10-15:03
 * 需求： 定义一个电脑类，并且实例化出电脑类的对象，以及给该对象的属性赋值
 *
 * 针对于c包的问题，修改代码如下：
 *    创建一个建造者接口，把制作产品的具体步骤稳定下来！然后让建造者类去实现接口，接口中的方法步骤都必须实现，少实现一个方法就会报错！
 *    当前仍然是过渡写法
 *    优点: 建造者类中的建造过程是稳定的。不会漏掉某一步！这样当客户端想要扩展建造者时，也不会漏掉某一步
 *    缺点：1）代码仍然有重复；
 *         2）现在又变成了客户端程序员自己配置电脑，违反了迪米特法则。。
 *         比如说：客户端程序员理论上不应该知道，组装电脑的过程是 第一步选cpu 第二步选gpu等等，而想要用d包方案，客户端必须
 *         知道建造过程
 */
class Computer{
  private String cpu;
  private String gpu;
  private String memory;
  private String harddisk;

  @Override
  public String toString() {
    return "Computer{" +
        "cpu='" + cpu + '\'' +
        ", gpu='" + gpu + '\'' +
        ", memory='" + memory + '\'' +
        ", harddisk='" + harddisk + '\'' +
        '}';
  }

  public String getCpu() {
    return cpu;
  }

  public void setCpu(String cpu) {
    this.cpu = cpu;
  }

  public String getGpu() {
    return gpu;
  }

  public void setGpu(String gpu) {
    this.gpu = gpu;
  }

  public String getMemory() {
    return memory;
  }

  public void setMemory(String memory) {
    this.memory = memory;
  }

  public String getHarddisk() {
    return harddisk;
  }

  public void setHarddisk(String harddisk) {
    this.harddisk = harddisk;
  }
}
interface ComputerBuilder {
  void setCpu();
  void setGpu();
  void setMemory();
  void setHarddisk();
  Computer build();
}

// 电脑建造者类，建造这类必须关联产品
class AdvacedComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();

  @Override
  public void setCpu() {
    computer.setCpu("i7 8750hk"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
  }

  @Override
  public void setGpu() {
    computer.setGpu("rtx2080ti");
  }

  @Override
  public void setMemory() {
    computer.setMemory("32g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("500G固态+2T机械");
  }

  @Override
  public Computer build(){
    return computer;
  }
}

class MiddleComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();

  @Override
  public void setCpu() {
    computer.setCpu("i7 7700hq"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
  }

  @Override
  public void setGpu() {
    computer.setGpu("gtx1060ti");
  }

  @Override
  public void setMemory() {
    computer.setMemory("16g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("250G固态+1T机械");
  }

  public Computer build(){
    return computer;
  }
}

class LowComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();

  @Override
  public void setCpu() {
    computer.setCpu("i7 7500u"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
  }

  @Override
  public void setGpu() {
    computer.setGpu("gtx940mx");
  }

  @Override
  public void setMemory() {
    computer.setMemory("8g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("1T机械");
  }

  public Computer build(){
    return computer;
  }
}


// ======================================服务端程序员 和 客户端程序员 时空线=============================================

public class AppTest {
  public static void main(String[] args) {
    AdvacedComputerBuilder acb  = new AdvacedComputerBuilder();
    MiddleComputerBuilder mcb = new MiddleComputerBuilder();
    LowComputerBuilder lcb = new LowComputerBuilder();

    // 玩游戏
    acb.setCpu();
    acb.setGpu();
    acb.setMemory();
    acb.setHarddisk();
    Computer c = acb.build();
    System.out.println(c);

    // 程序开发
    mcb.setCpu();
    mcb.setGpu();
    mcb.setMemory();
    mcb.setHarddisk();
    Computer c2 = acb.build();
    System.out.println(c2);

    // 办公娱乐
    lcb.setCpu();
    lcb.setGpu();
    lcb.setMemory();
    lcb.setHarddisk();
    Computer c3 = acb.build();
    System.out.println(c3);
  }
}
