package com.woniuxy.pattern.e_builder.e;


/**
 * @author caoyi
 * @create 2021-01-10-15:03
 * 需求： 定义一个电脑类，并且实例化出电脑类的对象，以及给该对象的属性赋值
 *
 * 针对于d包的问题，修改代码如下: 建造者模式
 *    优点：1.创建对象的过程是稳定不变的（因为有ComputerBuilder接口）
 *         2.创建对象的过程只写了一次，没有重复代码（这是由指挥者完成的）
 *         3.当需要扩展指挥者时，不用修改之前的代码，这符合开闭原则
 */
class Computer{
  private String cpu;
  private String gpu;
  private String memory;
  private String harddisk;

  @Override
  public String toString() {
    return "Computer{" +
        "cpu='" + cpu + '\'' +
        ", gpu='" + gpu + '\'' +
        ", memory='" + memory + '\'' +
        ", harddisk='" + harddisk + '\'' +
        '}';
  }

  public String getCpu() {
    return cpu;
  }

  public void setCpu(String cpu) {
    this.cpu = cpu;
  }

  public String getGpu() {
    return gpu;
  }

  public void setGpu(String gpu) {
    this.gpu = gpu;
  }

  public String getMemory() {
    return memory;
  }

  public void setMemory(String memory) {
    this.memory = memory;
  }

  public String getHarddisk() {
    return harddisk;
  }

  public void setHarddisk(String harddisk) {
    this.harddisk = harddisk;
  }
}

interface ComputerBuilder {
  void setCpu();
  void setGpu();
  void setMemory();
  void setHarddisk();
  Computer build();
}

// 电脑建造者类，建造这类必须关联产品
class AdvacedComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();

  @Override
  public void setCpu() {
    computer.setCpu("i7 8750hk"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
  }

  @Override
  public void setGpu() {
    computer.setGpu("rtx2080ti");
  }

  @Override
  public void setMemory() {
    computer.setMemory("32g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("500G固态+2T机械");
  }

  @Override
  public Computer build(){
    return computer;
  }
}

class MiddleComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();

  @Override
  public void setCpu() {
    computer.setCpu("i7 7700hq"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
  }

  @Override
  public void setGpu() {
    computer.setGpu("gtx1060ti");
  }

  @Override
  public void setMemory() {
    computer.setMemory("16g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("250G固态+1T机械");
  }

  public Computer build(){
    return computer;
  }
}

class LowComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();

  @Override
  public void setCpu() {
    computer.setCpu("i7 7500u"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
  }

  @Override
  public void setGpu() {
    computer.setGpu("gtx940mx");
  }

  @Override
  public void setMemory() {
    computer.setMemory("8g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("1T机械");
  }

  public Computer build(){
    return computer;
  }
}

class Director{

  public Computer build(ComputerBuilder computerBuilder){
    computerBuilder.setCpu();
    computerBuilder.setGpu();
    computerBuilder.setMemory();
    computerBuilder.setHarddisk();
    return computerBuilder.build();
  }
}

// ======================================服务端程序员 和 客户端程序员 时空线=============================================
class MiddleHighComputerBuilder implements ComputerBuilder{
  private Computer computer = new Computer();
  @Override
  public void setCpu() {
    computer.setCpu("i5 8500hq");
  }

  @Override
  public void setGpu() {
    computer.setGpu("gtx1070");
  }

  @Override
  public void setMemory() {
    computer.setMemory("16g");
  }

  @Override
  public void setHarddisk() {
    computer.setHarddisk("1T");
  }

  @Override
  public Computer build() {
    return computer;
  }
}


public class AppTest {
  public static void main(String[] args) {
    AdvacedComputerBuilder acb  = new AdvacedComputerBuilder();
    MiddleComputerBuilder mcb = new MiddleComputerBuilder();
    LowComputerBuilder lcb = new LowComputerBuilder();

    Director director = new Director();

    // 玩游戏
    Computer c = director.build(acb);
    System.out.println(c);

    // 程序开发
    Computer c2 = director.build(mcb);
    System.out.println(c2);

    // 办公娱乐
    Computer c3 = director.build(lcb);
    System.out.println(c3);

    MiddleHighComputerBuilder mhcb = new MiddleHighComputerBuilder();
    Computer c4 = director.build(mhcb);
    System.out.println(c4);
  }
}
