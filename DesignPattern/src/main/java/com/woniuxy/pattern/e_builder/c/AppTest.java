package com.woniuxy.pattern.e_builder.c;


/**
 * @author caoyi
 * @create 2021-01-10-15:03
 * 需求： 定义一个电脑类，并且实例化出电脑类的对象，以及给该对象的属性赋值
 *
 * 针对于b包中的问题，修改代码如下：
 *  针对于不同的需求，我们需要创建不同额建造者，来分别生产不同配置的产品
 *
 *  此时仍然不是建造者模式：
 *    优点：可以根据客户端的不同需求，使用不同的建造者生产产品
 *    缺点：1）多个不同的建造者代码在重复；
 *         2）建造的过程不稳定，如果在某个建造者创建产品的过程中，漏掉了某一步（比如没装显卡），编译器也不会有提示
 *         为何会有第二个问题，是因为客户端程序员可能扩展建造者，更改步骤，这些步骤对于扩展是开放的
 */
class Computer{
  private String cpu;
  private String gpu;
  private String memory;
  private String harddisk;

  @Override
  public String toString() {
    return "Computer{" +
        "cpu='" + cpu + '\'' +
        ", gpu='" + gpu + '\'' +
        ", memory='" + memory + '\'' +
        ", harddisk='" + harddisk + '\'' +
        '}';
  }

  public String getCpu() {
    return cpu;
  }

  public void setCpu(String cpu) {
    this.cpu = cpu;
  }

  public String getGpu() {
    return gpu;
  }

  public void setGpu(String gpu) {
    this.gpu = gpu;
  }

  public String getMemory() {
    return memory;
  }

  public void setMemory(String memory) {
    this.memory = memory;
  }

  public String getHarddisk() {
    return harddisk;
  }

  public void setHarddisk(String harddisk) {
    this.harddisk = harddisk;
  }
}

// 电脑建造者类，建造这类必须关联产品
class AdvacedComputerBuilder{
  private Computer computer = new Computer();

  public Computer build(){
    computer.setCpu("i7 8750hk"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
    computer.setGpu("rtx2080ti");
    computer.setMemory("32g");
    computer.setHarddisk("500G固态+2T机械");
    return computer;
  }
}
class MiddleComputerBuilder{
  private Computer computer = new Computer();

  public Computer build(){
    computer.setCpu("i7 7700hq"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
    computer.setGpu("gtx1060ti");
    computer.setMemory("16g");
    computer.setHarddisk("250G固态+1T机械");
    return computer;
  }
}
class LowComputerBuilder{
  private Computer computer = new Computer();

  public Computer build(){
    computer.setCpu("i7 7500u"); // 后缀是u代表的是低功耗，带不起游戏； hk代表超频
    computer.setGpu("gtx940mx");
    computer.setMemory("8g");
    computer.setHarddisk("1T机械");
    return computer;
  }
}


// ======================================服务端程序员 和 客户端程序员 时空线=============================================

public class AppTest {
  public static void main(String[] args) {
    AdvacedComputerBuilder acb  = new AdvacedComputerBuilder();
    MiddleComputerBuilder mcb = new MiddleComputerBuilder();
    LowComputerBuilder lcb = new LowComputerBuilder();

    // 玩游戏
    Computer c = acb.build();
    System.out.println(c);

    // 程序开发
    Computer c2 = acb.build();
    System.out.println(c2);

    // 办公娱乐
    Computer c3 = acb.build();
    System.out.println(c3);
  }
}
