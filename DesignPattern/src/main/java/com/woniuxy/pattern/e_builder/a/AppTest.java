package com.woniuxy.pattern.e_builder.a;

/**
 * @author caoyi
 * @create 2021-01-10-15:03
 * 需求： 定义一个电脑类，并且实例化出电脑类的对象，以及给该对象的属性赋值
 *
 * 存在问题：
 *    1.客户端程序员，在实例化好产品的对象之后，必须为该对象的每一个属性赋值，这样对于客户端程序员来说，太麻烦了
 *    2.违反了迪米特法则
 */
class Computer{
  private String cpu;
  private String gpu;
  private String memory;
  private String harddisk;

  @Override
  public String toString() {
    return "Computer{" +
        "cpu='" + cpu + '\'' +
        ", gpu='" + gpu + '\'' +
        ", memory='" + memory + '\'' +
        ", harddisk='" + harddisk + '\'' +
        '}';
  }

  public String getCpu() {
    return cpu;
  }

  public void setCpu(String cpu) {
    this.cpu = cpu;
  }

  public String getGpu() {
    return gpu;
  }

  public void setGpu(String gpu) {
    this.gpu = gpu;
  }

  public String getMemory() {
    return memory;
  }

  public void setMemory(String memory) {
    this.memory = memory;
  }

  public String getHarddisk() {
    return harddisk;
  }

  public void setHarddisk(String harddisk) {
    this.harddisk = harddisk;
  }
}

// ======================================服务端程序员 和 客户端程序员 时空线=============================================

public class AppTest {
  public static void main(String[] args) {
    Computer c = new Computer();
    c.setCpu("i7 7500u");
    c.setGpu("gt940mx");
    c.setMemory("16g");
    c.setHarddisk("1T机械");

    System.out.println(c);
  }
}
