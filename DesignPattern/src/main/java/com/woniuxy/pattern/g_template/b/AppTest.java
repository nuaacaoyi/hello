package com.woniuxy.pattern.g_template.b;

import com.sun.scenario.effect.impl.state.LinearConvolveKernel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author caoyi
 * @create 2021-01-10-20:01
 * 测试ArrayList和LinkedList的增加效率，查询效率
 *  应对a包的问题，解决方案：模板方法模式
 */
// 基本其他设计模式都可以用接口，但是模板必须用抽象类，因为他有方法体
abstract class Template{
  public void template(){
    System.out.println("开始");
    long start = System.currentTimeMillis();

    code();

    long end = System.currentTimeMillis();
    System.out.println("总耗时：" + (end - start));
    System.out.println("结束");
  }
  public abstract void code();
}

// ===========================服务端程序员 和 客户端程序员分割线================================
class A extends Template{
  @Override
  public void code() {
    List<Integer> list = new LinkedList<>();
    for (int i = 0; i < 100000; i++) {
      list.add(0,1);
    }
  }
}
class B extends Template{
  @Override
  public void code() {
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < 100000; i++) {
      list.add(0,1);
    }
  }
}


public class AppTest {
  public static void main(String[] args) {
    Template t = new A();
    t.template();
    Template b = new B();
    b.template();
  }
}
