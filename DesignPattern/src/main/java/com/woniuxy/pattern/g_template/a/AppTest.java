package com.woniuxy.pattern.g_template.a;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author caoyi
 * @create 2021-01-10-20:01
 * 测试ArrayList和LinkedList的增加效率，查询效率
 *  问题：
 *    每次要测试的代码发生变化时，都势必要修改原有的代码，但是呢，程序两端的代码又不需要改变
 */
public class AppTest {
  public static void main(String[] args) {
    System.out.println("开始");
    long start = System.currentTimeMillis();
    /*try {
      Thread.sleep(1000);
    }catch (InterruptedException e){
      e.printStackTrace();
    }*/
    // 497ms
    /*List<Integer> list = new ArrayList<>();
    for (int i = 0; i < 100000; i++) {
      list.add(0,1);
    }*/

    // 8ms
    List<Integer> list = new LinkedList<>();
    for (int i = 0; i < 100000; i++) {
      list.add(0,1);
    }

    long end = System.currentTimeMillis();
    System.out.println(end - start);
  }
}
