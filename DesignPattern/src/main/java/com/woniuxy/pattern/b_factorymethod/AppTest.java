package com.woniuxy.pattern.b_factorymethod;


/**
 * @author caoyi
 * @create 2021-01-05-19:56
 * 针对于简单工厂模式的问题，修改方案 采用工厂方法模式，具体如下
 * 优点：
 *    1.把具体产品的类型，从客户端代码中，解耦出来  -- 秉承于简单工厂模式优点
 *    2.服务器端如果修改了具体产品的类名，或者实现等程序，客户端也不需要了解 -- 秉承于简单工厂模式优点
 *    3.客户端新增产品时，不需要修改作者原来的代码，只需要自己增加工厂和food而已
 * 吐槽（纯抬杠）：
 *    1.我们已经知道简单工厂也好，工厂方法也好，都有一个优点，就是服务器端的具体产品类名变化了以后，而客户端不需要知道
 *      但是反观我们现在的代码，客户端仍然依赖于具体的工厂的类名啊。如果工厂类名调整，客户端也是要调整的。
 *      解释：工厂的名字，是被视为接口的（是对外暴露的），作者有义务保持 接口名字稳定，这是一种行业规范
 *    2.既然 意大利面这个产品是 客户端做出来的，那为什么还要搞工厂呢，自己在客户端实例化不好么
 *      解释：
 *        1）作者开发功能时，不仅仅只是提供一个工厂类，还会有配套的业务逻辑接口，客户端开发如果就自己实例化，不实现服务端接口，可能会有很多功能不可用
 *        2）你编写的编码，将来也可能放到服务端被其他人使用，要 开放扩展
 * 缺点：
 *    如果有多个产品等级（如食物分类为 饮料 和 吃的，各类又包含各自的实现），那么工厂类的数量就会爆炸式增长！！！
 *
 */

interface Food{
  void eat();
}

class Hambuger implements Food{
  @Override
  public void eat() {
    System.out.println("汉堡包");
  }
}

class RiceNoodle implements Food{
  @Override
  public void eat() {
    System.out.println("过桥米线");
  }
}

interface FoodFactory{
  public Food getFood();
}

class HamburgerFactory implements FoodFactory{
  @Override
  public Food getFood() {
    return new Hambuger();
  }
}
class RiceNoodleFactory implements FoodFactory{
  @Override
  public Food getFood() {
    return new RiceNoodle();
  }
}

//=============================服务端程序员 和 客户端程序员 分割线================================

// 需求变化：客户端增加食物
class ItalyNoodle implements Food{
  @Override
  public void eat() {
    System.out.println("意大利面配牛排");
  }
}
class ItalyNoodleFactory implements FoodFactory{
  @Override
  public Food getFood() {
    return new ItalyNoodle();
  }
}

public class AppTest {
  public static void main(String[] args) {
    FoodFactory ff = new ItalyNoodleFactory();
    Food food = ff.getFood();
    food.eat();
  }
}
