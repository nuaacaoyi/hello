package com.woniuxy.pattern.f_decorator.b;

/**
 * @author caoyi
 * @create 2021-01-10-16:36
 *  为了解决a包种的问题，满足a包的业务场景变化，可以尝试如下解决方案（过渡）：
 *    为加牛奶的Decaf咖啡创建一个类，为了加糖的Decaf咖啡创建一个类。。。。。。会导致 类爆炸！！！
 *    具体不做实现了，往下搞。。。
 */
abstract class Beverage{
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Beverage() {
  }

  public Beverage(String description) {
    this.description = description;
  }

  public abstract double cost();
}

class Decaf extends Beverage{
  public Decaf() {
    super("无咖啡因咖啡");
  }

  @Override
  public double cost() {
    return 1;
  }
}

class Espresso extends Beverage{
  public Espresso() {
    super("浓缩咖啡");
  }

  @Override
  public double cost() {
    return 2;
  }
}

class DarkRoast extends Beverage{
  public DarkRoast() {
    super("焦炒咖啡");
  }

  @Override
  public double cost() {
    return 1.5;
  }
}

class HouseBlend extends Beverage{
  public HouseBlend() {
    super("混合咖啡");
  }

  @Override
  public double cost() {
    return 3;
  }
}


// =======================================服务端程序员 和 客户端程序员 时空线===============================================
public class AppTest {
  public static void main(String[] args) {
    Beverage b = new Decaf();
    System.out.println(b.getDescription() + ": " + b.cost());

    Beverage b2 = new Espresso();
    System.out.println(b2.getDescription() + ": " + b2.cost());

    Beverage b3 = new DarkRoast();
    System.out.println(b3.getDescription() + ": " + b3.cost());

    Beverage b4 = new HouseBlend();
    System.out.println(b4.getDescription() + ": " + b4.cost());
  }
}
