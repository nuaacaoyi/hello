package com.woniuxy.pattern.f_decorator.c;

/**
 * @author caoyi
 * @create 2021-01-10-16:36
 *  为了解决b包的问题，解决方案；
 *    直接在父类Beverage种，添加4个boolean属性，分别代表是否添加了四种调料
 *
 *  优点： 1）可以解决类爆炸的问题 2）增加一个新的产品 茶，客户端扩展完全ok，符合开闭原则
 *  缺点： 当增加一个新的产品 枸杞 时，还需要去修改源码，违反了 开闭原则
 */
abstract class Beverage{

  private String description;
  private boolean milk,soy,mocha,bubble;

  public boolean isMilk() {
    return milk;
  }

  public void setMilk(boolean milk) {
    this.milk = milk;
  }

  public boolean isSoy() {
    return soy;
  }

  public void setSoy(boolean soy) {
    this.soy = soy;
  }

  public boolean isMocha() {
    return mocha;
  }

  public void setMocha(boolean mocha) {
    this.mocha = mocha;
  }

  public boolean isBubble() {
    return bubble;
  }

  public void setBubble(boolean bubble) {
    this.bubble = bubble;
  }

  public String getDescription() {
    String str = description;
    if(isMilk()){
      str += " 牛奶";
    }
    if(isSoy()){
      str += " 豆浆";
    }
    if(isMocha()){
      str += " 摩卡";
    }
    if(isBubble()){
      str += " 泡沫";
    }
    return str;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Beverage() {
  }

  public Beverage(String description) {
    this.description = description;
  }

  public double cost(){
    double total = 0;
    if(isMilk()){
      total += 0.1;
    }
    if(isSoy()){
      total += 0.2;
    }
    if(isMocha()){
      total += 0.5;
    }
    if(isBubble()){
      total += 0.9;
    }
    return total;
  };
}

class Decaf extends Beverage{
  public Decaf() {
    super("无咖啡因咖啡");
  }

  @Override
  public double cost() {
    return 1+super.cost();
  }
}

class Espresso extends Beverage{
  public Espresso() {
    super("浓缩咖啡");
  }

  @Override
  public double cost() {
    return 2+super.cost();
  }
}

class DarkRoast extends Beverage{
  public DarkRoast() {
    super("焦炒咖啡");
  }

  @Override
  public double cost() {
    return 1.5+super.cost();
  }
}

class HouseBlend extends Beverage{
  public HouseBlend() {
    super("混合咖啡");
  }

  @Override
  public double cost() {
    return 3+super.cost();
  }
}


// =======================================服务端程序员 和 客户端程序员 时空线===============================================
public class AppTest {
  public static void main(String[] args) {
    Beverage b = new Decaf();
    b.setBubble(true);
    b.setMilk(true);
    System.out.println(b.getDescription() + ": " + b.cost());

    Beverage b2 = new Espresso();
    System.out.println(b2.getDescription() + ": " + b2.cost());

    Beverage b3 = new DarkRoast();
    System.out.println(b3.getDescription() + ": " + b3.cost());

    Beverage b4 = new HouseBlend();
    System.out.println(b4.getDescription() + ": " + b4.cost());
  }
}
