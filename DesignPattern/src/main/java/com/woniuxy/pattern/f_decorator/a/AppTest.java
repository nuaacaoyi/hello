package com.woniuxy.pattern.f_decorator.a;

/**
 * @author caoyi
 * @create 2021-01-10-16:36
 *  业务场景：星巴克卖咖啡，一开始只有4种咖啡 Decaf、Espresso、 DarkRoast、HouseBlend
 *  因为所有的咖啡都有共性，所以开发人员把它们的共性上提到一个父类种： Beverage
 *
 *  变化来了：星巴克增加了一个新业务：调料。也就是可以给咖啡种放调料：牛奶、都将、摩卡、泡沫
 */
abstract class Beverage{
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Beverage() {
  }

  public Beverage(String description) {
    this.description = description;
  }

  public abstract double cost();
}

class Decaf extends Beverage{
  public Decaf() {
    super("无咖啡因咖啡");
  }

  @Override
  public double cost() {
    return 1;
  }
}
class Espresso extends Beverage{
  public Espresso() {
    super("浓缩咖啡");
  }

  @Override
  public double cost() {
    return 2;
  }
}
class DarkRoast extends Beverage{
  public DarkRoast() {
    super("焦炒咖啡");
  }

  @Override
  public double cost() {
    return 1.5;
  }
}
class HouseBlend extends Beverage{
  public HouseBlend() {
    super("混合咖啡");
  }

  @Override
  public double cost() {
    return 3;
  }
}


// =======================================服务端程序员 和 客户端程序员 时空线===============================================
public class AppTest {
  public static void main(String[] args) {
    Beverage b = new Decaf();
    System.out.println(b.getDescription() + ": " + b.cost());

    Beverage b2 = new Espresso();
    System.out.println(b2.getDescription() + ": " + b2.cost());

    Beverage b3 = new DarkRoast();
    System.out.println(b3.getDescription() + ": " + b3.cost());

    Beverage b4 = new HouseBlend();
    System.out.println(b4.getDescription() + ": " + b4.cost());
  }
}
