package com.woniuxy.pattern.f_decorator.d;

/**
 * @author caoyi
 * @create 2021-01-10-16:36
 *  为了解决c包种的问题，满足a包的业务场景变化，装饰器设计模式。
 */
abstract class Beverage{
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Beverage() {
  }

  public Beverage(String description) {
    this.description = description;
  }

  public abstract double cost();
}

class Decaf extends Beverage{
  public Decaf() {
    super("无咖啡因咖啡");
  }

  @Override
  public double cost() {
    return 1;
  }
}

class Espresso extends Beverage{
  public Espresso() {
    super("浓缩咖啡");
  }

  @Override
  public double cost() {
    return 2;
  }
}

class DarkRoast extends Beverage{
  public DarkRoast() {
    super("焦炒咖啡");
  }

  @Override
  public double cost() {
    return 1.5;
  }
}

class HouseBlend extends Beverage{
  public HouseBlend() {
    super("混合咖啡");
  }

  @Override
  public double cost() {
    return 3;
  }
}
/*判断两个类之间能否有继承关系，要看：1）两个类之间有无 is a关系；2）是否符合里氏替换原则
*   上面只是规范，在这个设计模式中，调料并不是一种饮料，但是为了制作出装饰器模式，这里也需要继承
* */
// 增加一个调料类
abstract class Condiment extends Beverage{
  // 让调料类关联饮料类
  protected Beverage beverage;

  public Condiment(Beverage beverage) {
    //super("调料");
    this.beverage = beverage;
  }
}

class Milk extends Condiment{
  public Milk(Beverage beverage) {
    super(beverage);
  }

  @Override
  public double cost() {
    return beverage.cost() + 0.2;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " 牛奶";
  }
}
class Soy extends Condiment{
  public Soy(Beverage beverage) {
    super(beverage);
  }

  @Override
  public double cost() {
    return beverage.cost() + 0.3;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " 豆浆";
  }
}
class Mocha extends Condiment{
  public Mocha(Beverage beverage) {
    super(beverage);
  }

  @Override
  public double cost() {
    return beverage.cost() + 0.4;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " 摩卡";
  }
}
class Bubble extends Condiment{
  public Bubble(Beverage beverage) {
    super(beverage);
  }

  @Override
  public double cost() {
    return beverage.cost() + 0.1;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " 泡沫";
  }
}

// =======================================服务端程序员 和 客户端程序员 时空线===============================================
// 扩展 茶 和 枸杞
class Tea extends Beverage{
  public Tea() {
    super("红茶养身");
  }

  @Override
  public double cost() {
    return 15;
  }
}

class Gouqi extends Condiment{
  public Gouqi(Beverage beverage) {
    super(beverage);
  }

  @Override
  public double cost() {
    return beverage.cost() + 1.1;
  }

  @Override
  public String getDescription() {
    return beverage.getDescription() + " 枸杞";
  }
}

public class AppTest {
  public static void main(String[] args) {
   /* Beverage b = new Decaf();
   *//* Milk milk = new Milk(b);
    Mocha mocha  = new Mocha(milk);*//*
    Beverage milk = new Milk(b);
    Beverage mocha  = new Mocha(milk);
    Beverage bz = new Milk(mocha);*/
    Beverage b = new Tea();
    Beverage b2 = new Gouqi(b);
    Beverage bz = new Gouqi(b2);

    System.out.println(bz.getDescription() + ": " + bz.cost());
  }
}
