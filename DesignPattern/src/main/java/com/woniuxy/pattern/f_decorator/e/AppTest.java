package com.woniuxy.pattern.f_decorator.e;

import java.io.*;

/**
 * @author caoyi
 * @create 2021-01-10-18:32
 * 在 d包中，我们已经学习完了“装饰器模式”
 * 起始，我们以前学习的jdk中的流，就是装饰器模式的一种体现
 *
 * 使用装饰器模式，自己实现一个  MyBufferedReader
 */
class MyBufferedReader extends Reader{
  private Reader in;

  public MyBufferedReader(Reader in) {
    this.in = in;
  }

  public String readLine() throws IOException {
    StringBuilder sb = new StringBuilder();
    int n;
    while (true){
      n = in.read();
      if(n == '\n' || n == -1){
        break;
      }
      sb.append((char)n);
    }
    if(sb.toString().length() ==0 ){
      if(n == '\n'){   // 这里表明是读到了中间的空行
        return "";
      }
      else if (n == -1){ // 这里表明是读到了末尾
        return null;
      }
    }
    return sb.toString();
  }

  @Override
  public int read(char[] cbuf, int off, int len) throws IOException {
    // 空方法体，不做实现
    return 0;
  }

  @Override
  public void close() throws IOException {
    in.close();
  }
}
// ====================================================================
// 制作一个可自动生成行号的 reader ，jdk已有 LineNumberReader
class MyLineNumberReader extends MyBufferedReader{
  private int count = 0;

  public MyLineNumberReader(Reader in) {
    super(in);
  }

  @Override
  public String readLine() throws IOException {
    String s = super.readLine();
    if(s != null){
      count++;
    }
    return s;
  }

  public int getCount() {
    return count;
  }
}



public class AppTest {
  public static void main(String[] args) throws Exception {
    Reader in = new FileReader("D:\\tmp\\1.txt");
    // MyBufferedReader mbr = new MyBufferedReader(in);
    MyLineNumberReader mlnr = new MyLineNumberReader(in);
    // LineNumberReader mlnr = new LineNumberReader(in);

        String line;
    while ((line = mlnr.readLine()) != null){
      // 为什么不把取数写在 readline中，这里体现了单一职责原则
      System.out.println(mlnr.getCount() + ": " + line);
    }

    mlnr.close();
    /*
    // 输入流
    InputStream in = new FileInputStream("D:\\tmp\\1.txt");
    // 缓冲区
    BufferedInputStream ris = new BufferedInputStream(in);
    // 字节流转字符流
    InputStreamReader isr = new InputStreamReader(ris,"gbk");

    // 读
    isr.read();

    isr.close();*/
  }
}
