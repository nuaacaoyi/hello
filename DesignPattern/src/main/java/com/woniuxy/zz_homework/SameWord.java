package com.woniuxy.zz_homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;

/**
 * @author caoyi
 * @create 2021-01-05-14:55
 * 从两个文件中找到相同的词
 */
public class SameWord {
  public static String loadFile(String path) throws IOException {
    Reader in = new FileReader(path);
    BufferedReader br = new BufferedReader(in);
    String line = null;
    StringBuilder sb = new StringBuilder("");
    while ((line = br.readLine()) != null){
      sb.append(line);
      sb.append("\n");
    }
    br.close();
    return  sb.toString();
  }

  public static String[] getWords(String str,String regstr){
    String[] s = str.split(regstr);
    // 问题又又来了，统一文本中有多少个句子？ 以。 ！ ？为分隔符
    return s;
  }
  public static void main(String[] args) throws IOException {
    String regStr = "[^a-zA-z]+";
    String path1 = "D:\\tmp\\1.txt";
    String path2 = "D:\\tmp\\2.txt";

    String str1 = loadFile(path1);
    String str2 = loadFile(path2);
    String[] word1 = getWords(str1, regStr);
    String[] word2 = getWords(str2, regStr);
    String[] sameWord = getSameWord(word1, word2);

    for (String sw :
        sameWord) {
      System.out.println(sw);
    }
  }

  // 把双重循环或者其他算法 抽取成方法， 这样才符合 单一职责 原则
  private static String[] getSameWord(String[] word1, String[] word2) {
    // 同一个文本文件中可能自身就有单词重复，所以要去重，使用set保存单词
   /* Set<String> set = new HashSet();
    for (String s1 :
        word1) {
      for (String s2 :
          word2) {
        if(s1!=null && s1.equals(s2)){
          set.add(s1);
        }
      }
    }
    return  set.toArray(new String[]{});//这里括号中的参数是 指明 方法返回的类型*/

    // 方法2
    // 1.Arrays.asList(word1)获取的是一个只读列表；2. new一下，可以将只读变为可写
    List<String> list1= new ArrayList<>(Arrays.asList(word1));
    List<String> list2= new ArrayList<>(Arrays.asList(word2));

    // 计算两个list的交集，并存入调用者，即list1中。 list2不变
    list1.retainAll(list2);

    // 去重
    Set<String> set = new HashSet(list1);

    return set.toArray(new String[]{});
  }
}
