package com.woniuxy.zz_homework;

/**
 * @author caoyi
 * @create 2021-01-10-12:18
 * 编写一个死锁程序
 *
 * 不太恰当的例子： 交易毒品， 卖家说先给钱；买家说先交货。。。死锁     浅和白粉都是锁；买卖双方是线程。
 *  著名案例：  哲学家吃饭问题，解决方案，让其中一个人反着拿筷子
 *
 *  如何避免死锁：避免 synchronized嵌套
 */

class T3 implements Runnable{
  private Object obj, obj2;

  public T3() {
  }

  public T3(Object obj, Object obj2) {
    this.obj = obj;
    this.obj2 = obj2;
  }

  @Override
  public void run() {
    while (true){
      synchronized (obj){
        synchronized (obj2){
          System.out.println("AAAA");
          System.out.println("BBBB");
          System.out.println("CCCC");
          System.out.println("DDDD");
        }
      }
    }
  }
}
class T4 implements Runnable{
  private Object obj, obj2;

  public T4() {
  }

  public T4(Object obj, Object obj2) {
    this.obj = obj;
    this.obj2 = obj2;
  }

  @Override
  public void run() {
    while (true){
      synchronized (obj2){
        synchronized (obj){
          System.out.println("1111");
          System.out.println("2222");
          System.out.println("3333");
          System.out.println("4444");
        }
      }
    }
  }
}

public class ThreadDeadLock {
  public static void main(String[] args) {
    // 死锁的前提：必须有两个锁以上，一个锁是不可能死锁的
    Object obj = new Object();
    Object obj2 = new Object();


    Thread th3 = new Thread(new T3(obj,obj2));
    Thread th4 = new Thread(new T4(obj,obj2));

    th3.start();
    th4.start();

  }
}
