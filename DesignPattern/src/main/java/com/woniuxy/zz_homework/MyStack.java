package com.woniuxy.zz_homework;

import java.util.LinkedList;

/**
 * @author caoyi
 * @create 2021-01-06-14:00
 * 借用 LinkedList  实现  栈 这一数据结构
 */
// T 叫做类型参数；  Stack<T> 叫做参数化类型
class Stack<T> {
  private LinkedList<T> list = new LinkedList<>();

  public void push(T t){
    list.add(t);
  }

  public T pop(){
    return list.removeLast();
  }
}

public class MyStack {
  public static void main(String[] args) {
    Stack<String> s = new Stack<>();
    s.push("a");
    s.push("b");
    s.push("c");

    System.out.println(s.pop());
  }
}
