package com.woniuxy.zz_homework;

/**
 * @author caoyi
 * @create 2021-01-05-15:59
 * 需求：两个线程打印： 12A34B56C..........5152Z
 */
class Foo{
  public int x = 0;
}

class T implements Runnable{
  public T() {
  }

  public T(Foo foo) {
    this.foo = foo;
  }

  private Foo foo;
  @Override
  public void run() {
    synchronized (foo) {
      for (int i = 0; i < 53; i++) {
        System.out.println(i);
        foo.x = i;
        try {// 打印完成之后唤醒别人，自己睡
          if(i%2==0) {
            foo.notifyAll();
            foo.wait();
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
class S implements Runnable{
  private Foo foo;

  public S() {
  }

  public S(Foo foo) {
    this.foo = foo;
  }

  @Override
  public void run() {
    synchronized (foo) {
      // 防止这个先抢到资源
      while (foo.x == 0){ // 这里到底是用while还是用if， 用if存在 虚假唤醒？
        try {
          foo.notifyAll();
          foo.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      for (int i = 'A'; i < 'Z'; i++) {
        System.out.println((char) i);
        try {
          foo.notifyAll();
          foo.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}

public class ThreadTest {
  public static void main(String[] args) {
    //Object obj = new Object();
    Foo foo = new Foo();

    T t = new T(foo);
    S s = new S(foo);
    Thread th = new Thread(t);
    Thread th2 = new Thread(s);
    // 没有obj时 th 和 th2之间没有通信，护枪CPU资源
    // 要想实现两个线程的协同工作，必须让其 拥有 同一把 串行锁，实现协同
    // 串行锁对象可以是任意对象
    th.start();
    th2.start();
  }

}
