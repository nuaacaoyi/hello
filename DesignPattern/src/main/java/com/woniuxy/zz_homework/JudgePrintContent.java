package com.woniuxy.zz_homework;

/**
 * @author caoyi
 * @create 2021-01-05-15:28
 * * 引起类的装载（加载）只有四种方式：1）调用构造器；2）class.forName；3）调用静态字段；4）调用静态方法
 * 所以该程序的执行流程是：
 *  1）调用类的静态方法，引发 类加载，此时先执行 静态代码块
 *  2）类中静态代码块 包含  三个 static，从上到下执行
 *  3）执行第一个静态代码块 static JudgePrintContent t = new JudgePrintContent();
 *    3.1）此时，进入类的实例化
 *    3.2）在构造器之前执行 构造代码块  打印2
 *    3.3）执行第二个构造代码块  int a = 110;
 *    3.4）执行构造器 打印1，此时 a有值，b默认0
 *  4）往下执行第二个静态代码块 打印 1
 *  5）往下执行第三个静态代码块 static int b =112;
 *  6）最后调用 f1() 打印4
 */
public class JudgePrintContent {
  public static void main(String[] args) {
    f1();
    new JudgePrintContent();//第二次实例化，类的静态代码块不再执行
  }

  static JudgePrintContent t = new JudgePrintContent();// 静态成员 相当于静态代码块
  static{ // 1--静态代码块，在 new 实例化时，先执行静态代码块，后执行构造代码块
    System.out.println("1");
  }
  { // 2--写在类中的代码块叫做构造代码块， 这个代码块的执行时机是 new实例化时
    System.out.println("2");
  }
  JudgePrintContent(){// 3--构造器也在实例化时执行，在构造代码块之后
    System.out.println("3");
    System.out.println("a="+a+", b="+b);
  }
  public static void f1(){
    System.out.println("4");
  }
  int a =110; // 4--等价于构造代码块
  static int b =112; // 5--等价于静态代码块
}
