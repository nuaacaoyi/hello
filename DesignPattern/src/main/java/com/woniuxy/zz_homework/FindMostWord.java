package com.woniuxy.zz_homework;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;

/**
 * @author caoyi
 * @create 2021-01-11-8:53
 */
public class FindMostWord {
  public static String loadFile(String path){
    try {
      Reader in = new FileReader(path);
      BufferedReader br = new BufferedReader(in);
      String line = null;
      StringBuilder sb = new StringBuilder("");
      while ((line = br.readLine()) != null){
        sb.append(line);
        sb.append("\n");
      }

      br.close();
      return  sb.toString();
    } catch (IOException e) {
      throw new RuntimeException();
    }
  }

  public static String[] parseWord(String str){
    String[] words = str.split("[^a-zA-z]+");
    return words;
  }

  public static Map<String, Integer> countWords(String[] words){
    Map<String, Integer> map = new HashMap<>();
    for (int i = 0; i < words.length; i++) {
      String word = words[i];
      if(map.containsKey(word)){
        Integer c = map.get(word);
        c++;
        map.put(word,c);
      }else {
        map.put(word,1);
      }
    }
    return map;
  }

  public static Integer getMaxValue(Map<String,Integer> map){
    Collection<Integer> values = map.values();
    return Collections.max(values);
  }
  public static List<String> getKeyByValue(Map<String,Integer> map, Integer value){
    List<String> list = new ArrayList<>();
    for (Map.Entry<String, Integer> e :
        map.entrySet()) {
      if (value == e.getValue()){
        list.add(e.getKey());
      }
    }
    return list;
  }

  public static void main(String[] args) {
    String path = "D:\\tmp\\1.txt";
    String str = loadFile(path);
    String[] words = parseWord(str);
    Map<String, Integer> map = countWords(words);
    Integer maxcounts = getMaxValue(map);
    List<String> maxWords = getKeyByValue(map, maxcounts);
    System.out.println(maxWords);
  }
}
