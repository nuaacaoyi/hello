package com.woniuxy.zz_homework;


/**
 * @author caoyi
 * @create 2021-01-06-14:28
 * 子线程循环10次，主线程循环20次；子线程循环10次，主线程循环20次；。。。总共循环50次
 *
 * 1.先分析，有几个独立任务，有几个任务，就做几个类，实现 Runnable
 * 2.如果只有一个独立任务，可以把这个对象作为共享对象
 */
class TR implements Runnable{
  private int count = 0;
  @Override
  public void run() {
    while (true){
      synchronized (this){
        for (int i = 1; i <= 10; i++) {
          System.out.println(Thread.currentThread().getName() + ": " + i);
        }
        try {
          this.notifyAll(); // notifyAll可以唤醒 wait阻塞
          this.wait();  // 1.释放cpu； 2.释放锁； 3.唤醒等待锁的线程（不会唤醒因为wait而阻塞的线程）
          count++;
          if(getCount() >= 50){
            this.notifyAll();
            break;
          }
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public int getCount() {
    return count;
  }
}

public class ThreadTest2 {
  public static void main(String[] args) {

    TR tr = new TR();
    Thread th = new Thread(tr);
    th.start();

    while (tr.getCount() < 50){
      synchronized (th){
        for (int i = 1; i <= 20; i++) {
          System.out.println(Thread.currentThread().getName() + ": " + i);
        }
        try {
          th.notifyAll(); // notifyAll可以唤醒 wait阻塞
          th.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
