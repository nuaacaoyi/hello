package com.heima.pattern.g_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:05
 * @Description: 租房者类，同事类的一个具体实现
 */
public class Tenant extends Person{

  public Tenant(String name, Mediator mediator) {
    super(name, mediator);
  }

  // 和中介沟通的方法
  public void constact(String message){
    mediator.constact(message, this);
  }

  // 获取信息的方法
  public void getMessage(String message){
    System.out.println("租房者："+name + "；获取到的信息是："+ message);
  }
}
