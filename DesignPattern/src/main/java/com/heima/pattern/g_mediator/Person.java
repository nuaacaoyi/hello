package com.heima.pattern.g_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:04
 * @Description: 抽象 同事类 colleague
 */
public abstract class Person {
  // 姓名
  protected String name;
  // 使用的中介是哪个
  protected Mediator mediator;

  public Person(String name, Mediator mediator) {
    this.name = name;
    this.mediator = mediator;
  }

}
