package com.heima.pattern.g_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:03
 * @Description: 抽象中介者类
 */
public abstract class Mediator {
  public abstract void constact(String message, Person person);
}
