package com.heima.pattern.g_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:09
 * @Description: 具体的中介者类
 */
public class MediatorStructure extends Mediator {
  // 聚合 房主 和 承租人
  private HouseOwner houseOwner;
  private Tenant tenant;

  public HouseOwner getHouseOwner() {
    return houseOwner;
  }

  public void setHouseOwner(HouseOwner houseOwner) {
    this.houseOwner = houseOwner;
  }

  public Tenant getTenant() {
    return tenant;
  }

  public void setTenant(Tenant tenant) {
    this.tenant = tenant;
  }

  @Override
  public void constact(String message, Person person) {
    if(person == houseOwner){
      tenant.getMessage(message);
    }
    else {
      houseOwner.getMessage(message);
    }
  }
}
