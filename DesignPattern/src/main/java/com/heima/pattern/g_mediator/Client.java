package com.heima.pattern.g_mediator;

/**
 * @Author caoyi
 * @Create 2021-01-21-13:12
 * @Description: TODO
 */
public class Client {
  public static void main(String[] args) {
    // 创建中介者对象
    MediatorStructure ms = new MediatorStructure();

    // 创建两个同事类对象
    HouseOwner p1 = new HouseOwner("张三", ms);
    Tenant p2 = new Tenant("李四", ms);

    // 中介者要知道具体的房主和租房者
    ms.setHouseOwner(p1);
    ms.setTenant(p2);

    p2.constact("我要租三室的房子！！");

    p1.constact("我这里有三室的房子。。");


  }
}
