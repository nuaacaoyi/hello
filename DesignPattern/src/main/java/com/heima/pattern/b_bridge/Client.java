package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:34
 * @Description: TODO
 */
public class Client {
  public static void main(String[] args) {
    // 创建linux系统对象
    OperatingSystem system = new Linux(new AviFile());
    // 使用操作系统播放视频文件
    system.play("战狼3");


    OperatingSystem system2 = new Mac(new Mp4File());
    system2.play("苍老师的动作片");
  }
}

// ===================时空线，变化来了，需要新增一个操作系统mac 和 新增一个文件格式=======================
class Mac extends OperatingSystem{
  public Mac(VideoFile videoFile) {
    super(videoFile);
  }

  @Override
  public void play(String fileName) {
    System.out.println("在Mac下播放");
    videoFile.decode(fileName);
  }
}
class Mp4File implements VideoFile {
  @Override
  public void decode(String fileName) {
    System.out.println("解码Mp4格式的视频：" + fileName);
  }
}