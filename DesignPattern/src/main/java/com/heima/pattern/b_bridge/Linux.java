package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:31
 * @Description: TODO
 */
public class Linux extends OperatingSystem{
  public Linux(VideoFile videoFile) {
    super(videoFile);
  }

  @Override
  public void play(String fileName) {
    System.out.println("在Linux下播放");
    videoFile.decode(fileName);
  }
}
