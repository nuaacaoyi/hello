package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:27
 * @Description: rmvb视频文件的解码（具体实现化角色）
 */
public class RmvbFile implements VideoFile {
  @Override
  public void decode(String fileName) {
    System.out.println("解码rmvb格式的视频：" + fileName);
  }
}
