package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:29
 * @Description: 抽象的操作系统类
 */
public abstract class OperatingSystem {

  // 声明videoFile变量， 聚合关系
  protected VideoFile videoFile;

  public OperatingSystem() {
  }

  public OperatingSystem(VideoFile videoFile) {
    this.videoFile = videoFile;
  }

  public abstract void play(String fileName);
}
