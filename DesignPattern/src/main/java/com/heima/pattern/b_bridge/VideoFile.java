package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:25
 * @Description: 视频文件格式接口  （抽象实现化角色）
 */
public interface VideoFile {
  // 解码功能
  void decode(String fileName);

}
