package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:26
 * @Description: avi视频文件解码方式 （具体的实现化角色）
 */
public class AviFile implements VideoFile {
  @Override
  public void decode(String fileName) {
    System.out.println("解码avi格式的视频：" + fileName);
  }
}
