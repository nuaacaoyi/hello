package com.heima.pattern.b_bridge;

/**
 * @Author caoyi
 * @Create 2021-01-12-16:31
 * @Description: 扩展抽象化角色（windows操作系统）
 */
public class Windows extends OperatingSystem {
  public Windows(VideoFile videoFile) {
    super(videoFile);
  }

  @Override
  public void play(String fileName) {
    System.out.println("在windows下播放");
    videoFile.decode(fileName);
  }
}
