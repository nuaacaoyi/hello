package com.heima.pattern.d_combination;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-12-20:11
 * @Description: 菜单类，属于树枝节点角色
 */
public class Menu extends MenuComponent{
  // 菜单下可以有多个子菜单或者子菜单项
  private List<MenuComponent> childList = new ArrayList<>();

  public Menu(String name, int level) {
    this.name = name;
    this.level = level;
  }

  @Override
  public void add(MenuComponent menuComponent) {
    childList.add(menuComponent);
  }

  @Override
  public void remove(MenuComponent menuComponent) {
    childList.remove(menuComponent);
  }

  @Override
  public MenuComponent getChild(int index) {
    return childList.get(index);
  }

  @Override
  public void print() {
    // 根据层级打印缩进空格
    for (int i = 0; i < level; i++) {
      System.out.print("--");
    }
    // 打印菜单名称
    System.out.println(name);

    // 打印子菜单名称或者子菜单项名称
    for (MenuComponent m :
        childList) {
      m.print();
    }
  }
}
