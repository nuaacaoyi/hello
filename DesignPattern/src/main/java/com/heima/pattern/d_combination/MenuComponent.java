package com.heima.pattern.d_combination;


/**
 * @Author caoyi
 * @Create 2021-01-12-20:06
 * @Description: 菜单组件：抽象根节点
 */
public abstract class MenuComponent {
  // 所有菜单共有属性和方法

  // 菜单名称
  protected String name;

  // 菜单层级
  protected int level;

  // 添加子菜单
  public void add(MenuComponent menuComponent){
    // 由于某些节点（比如某个文件）不能向下添加子节点，所以这里面默认实现是一个异常
    throw new UnsupportedOperationException();
  }

  // 移除子菜单
  public void remove(MenuComponent menuComponent){
    throw new UnsupportedOperationException();
  }

  // 获取指定的子菜单
  public MenuComponent getChild(int index){
    throw new UnsupportedOperationException();
  }

  // 获取菜单或者菜单项的名称
  public String getName() {
    return name;
  }

  // 打印菜单名称的方法（包含子菜单和子菜单项）
  public abstract void print();
}
