package com.heima.pattern.d_combination;

/**
 * @Author caoyi
 * @Create 2021-01-12-20:19
 * @Description: 菜单项类，属于叶子节点角色
 */
public class MenuItem extends MenuComponent{
  public MenuItem(String name, int level) {
    this.name = name;
    this.level = level;
  }

  @Override
  public void print() {
    // 根据层级打印缩进空格
    for (int i = 0; i < level; i++) {
      System.out.print("--");
    }
    System.out.println(name);
  }
}
