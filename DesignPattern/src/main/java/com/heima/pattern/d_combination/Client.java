package com.heima.pattern.d_combination;

/**
 * @Author caoyi
 * @Create 2021-01-12-20:20
 * @Description: 针对于文件资源管理器（有目录有文件），给客户一个通用的打印方法
 */
public class Client {
  public static void main(String[] args) {
    // 创建菜单树
    MenuComponent menu1 = new Menu("菜单管理",2);
    menu1.add(new MenuItem("页面访问", 3));
    menu1.add(new MenuItem("展开菜单", 3));
    menu1.add(new MenuItem("编辑菜单", 3));
    menu1.add(new MenuItem("删除菜单", 3));
    menu1.add(new MenuItem("新增菜单", 3));
    MenuComponent menu2 = new Menu("权限配置",2);
    menu2.add(new MenuItem("页面访问", 3));
    menu2.add(new MenuItem("提交保存", 3));
    MenuComponent menu3 = new Menu("角色管理",2);
    menu3.add(new MenuItem("页面访问", 3));
    menu3.add(new MenuItem("新增角色", 3));
    menu3.add(new MenuItem("修改角色", 3));

    // 创建一级菜单：系统管理
    MenuComponent component = new Menu("系统管理", 1);
    component.add(menu1);
    component.add(menu2);
    component.add(menu3);

    // 打印菜单名称（如果有子菜单一块打印）
    component.print();
  }
}
