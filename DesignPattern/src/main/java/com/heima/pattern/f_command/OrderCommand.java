package com.heima.pattern.f_command;

import java.util.Map;

/**
 * @Author caoyi
 * @Create 2021-01-19-15:28
 * @Description: 具体的命令类
 */
public class OrderCommand implements Command {
  public OrderCommand() {
  }

  // 持有接收者对象
  private SeniorChef reciver;

  // 订单对象
  private Order order;

  public OrderCommand(SeniorChef reciver, Order order) {
    this.reciver = reciver;
    this.order = order;
  }

  @Override
  public void execute() {
    System.out.println(order.getDiningTable() + "桌的订单：");
    Map<String, Integer> foodDir = order.getFoodDir();
    for (String foodName :
        foodDir.keySet()) {
      reciver.makeFood(foodName, foodDir.get(foodName));
    }
    System.out.println(order.getDiningTable() + "桌的菜准备完毕！");
  }
}
