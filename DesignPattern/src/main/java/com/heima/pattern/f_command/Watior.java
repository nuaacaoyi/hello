package com.heima.pattern.f_command;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author caoyi
 * @Create 2021-01-19-15:33
 * @Description: 服务员类（属于请求者角色）
 */
public class Watior {
  // 持有多个命令对象
  private List<Command> commands = new ArrayList<>();

  public void setCommand(Command command){
    commands.add(command);
  }

  // 发起命令
  public void sendCommand(){
    System.out.println("服务员发起命令：大厨，新订单来了");

    for (Command cmd :
        commands) {
      if(cmd != null){
        cmd.execute();
      }
    }
  }
}
