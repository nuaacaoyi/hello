package com.heima.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-19-15:21
 * @Description: 抽象命令接口
 */
public interface Command {
  void execute(); // 只需要定义一个统一的执行方法
}
