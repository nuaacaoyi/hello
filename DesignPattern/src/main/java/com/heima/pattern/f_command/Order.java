package com.heima.pattern.f_command;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author caoyi
 * @Create 2021-01-19-15:22
 * @Description: 订单类
 */
public class Order {
  // 下订单的餐桌号
  private int diningTable;

  // 所下的餐品及份数
  private Map<String, Integer> foodDir = new HashMap<>();

  public int getDiningTable() {
    return diningTable;
  }

  public void setDiningTable(int diningTable) {
    this.diningTable = diningTable;
  }

  public Map<String, Integer> getFoodDir() {
    return foodDir;
  }

  public void setFood(String name, int num) {
    this.foodDir.put(name,num);
  }


}
