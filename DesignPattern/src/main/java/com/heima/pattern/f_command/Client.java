package com.heima.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-19-15:18
 * @Description: 点餐--》做菜
 * 服务员 发起命令，交由厨师做菜
 * 厨师 接收命令
 * 命令  包含菜单内容
 */
public class Client {
  public static void main(String[] args) {
    // 创建订单对象 ，两个订单
    Order order1 = new Order();
    order1.setDiningTable(1);
    order1.setFood("西红柿鸡蛋面", 1);
    order1.setFood("黄瓜变蛋", 1);
    order1.setFood("小杯可乐", 2);

    // 第二个订单对象
    Order order2 = new Order();
    order2.setDiningTable(2);
    order2.setFood("尖椒肉丝盖饭", 1);
    order2.setFood("红豆奶茶", 2);

    // 创建厨师对象  接收者
    SeniorChef reciver = new SeniorChef();
    // 创建命令对象
    OrderCommand cmd1 = new OrderCommand(reciver, order1);
    OrderCommand cmd2 = new OrderCommand(reciver, order2);

    // 创建服务员对象  调用者
    Watior invoker = new Watior();
    invoker.setCommand(cmd1);
    invoker.setCommand(cmd2);

    // 服务员发起命令
    invoker.sendCommand();
  }
}
