package com.heima.pattern.f_command;

/**
 * @Author caoyi
 * @Create 2021-01-19-15:26
 * @Description: 厨师类，用于接收命令
 */
public class SeniorChef {
  public void makeFood(String name, int num){
    System.out.println("大厨做了" + num + "份 " + name);
  }
}
