package com.heima.pattern.a_proxy.static_proxy;

/**
 * @author caoyi
 * @create 2021-01-11-21:38
 * 火车站可以卖票  目标类
 */
class TrainStation implements SellTickets{
  @Override
  public void sell() {
    System.out.println("火车站直接售票");
  }
}