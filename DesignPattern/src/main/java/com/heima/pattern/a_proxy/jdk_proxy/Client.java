package com.heima.pattern.a_proxy.jdk_proxy;

/**
 * @Author caoyi
 * @Create 2021-01-11-21:56
 * @Description: TO DO
 */
public class Client {
  public static void main(String[] args) {
    // 获取代理对象
    SellTickets proxyObject = ProxyFactory.getProxyObject();
    proxyObject.sell();
  }
}
