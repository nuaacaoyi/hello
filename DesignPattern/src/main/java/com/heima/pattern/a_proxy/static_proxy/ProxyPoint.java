package com.heima.pattern.a_proxy.static_proxy;

/**
 * @author caoyi
 * @create 2021-01-11-21:39
 * 代售点  代理类
 */
class ProxyPoint implements SellTickets{
  // 代售点和火车站 是 聚合关系
  TrainStation trainStation = new TrainStation();

  @Override
  public void sell() {
    System.out.println("代售点收取一部分服务费");
    trainStation.sell();
  }
}