package com.heima.pattern.a_proxy.jdk_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author caoyi
 * @create 2021-01-11-21:47
 * 获取代理对象的工厂类
 *     代理类（或者说代理对象）也要实现对应的业务接口
 */
public class ProxyFactory {
  // 声明一个目标对象(被代理对象：火车站)
  private static TrainStation station = new TrainStation();

  // 获取代理对象的方法
  public static SellTickets getProxyObject(){
    // 使用 Proxy.newProxyInstance方法获取代理对象
    /*ClassLoader loader, 参数1：类加载器，用于JVM加载代理类， 通过目标类获取
      Class<?>[] interfaces, 参数2：代理类实现的接口的字节码对象数组
      InvocationHandler h  参数3：代理对象的调用处理器（回调？）   * */
    SellTickets proxyInstance = (SellTickets)Proxy.newProxyInstance(
        station.getClass().getClassLoader(),
        station.getClass().getInterfaces(),
        new InvocationHandler() {
          @Override
          public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            // 增强目标类的业务逻辑
            System.out.println("代售点收取了一定的费用（jdk动态代理）");
            // 执行目标类的方法
            Object invoke = method.invoke(station, args);
            // 返回目标类方法的返回值，保证程序的 可持续性
            return invoke;
          }
        }

    );


    return proxyInstance;
  }
}
