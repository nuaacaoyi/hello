package com.heima.pattern.a_proxy.cglib_proxy;

/**
 * @Author caoyi
 * @Create 2021-01-12-14:28
 * @Description: TO DO
 */
public class Client {
  public static void main(String[] args) {
    // 创建代理工厂对象
    ProxyFactory factory = new ProxyFactory();

    // 获取代理对象，TrainStation类的子类对象
    TrainStation proxyObject = factory.getProxyObject();

    // 调用sell方法买票
    proxyObject.sell();
  }
}
