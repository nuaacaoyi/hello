package com.heima.pattern.a_proxy.static_proxy;

/**
 * @author caoyi
 * @create 2021-01-11-21:38
 * 售票业务
 */
public interface SellTickets {
  void sell();
}
