package com.heima.pattern.a_proxy.static_proxy;

/**
 * @author caoyi
 * @create 2021-01-11-21:39
 *
 * 从代码中可以看到，客户端直接访问的就是代理类，而代理类也确实对目标类进行了增强，这就是静态代理
 */
public class Client {
  public static void main(String[] args) {
    // 创建代售点类对象
    ProxyPoint proxyPoint = new ProxyPoint();

    // 调用方法进行售票
    proxyPoint.sell();
  }
}
