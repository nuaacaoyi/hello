package com.heima.pattern.a_proxy.cglib_proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Author caoyi
 * @Create 2021-01-12-14:11
 * @Description: 代理对象工厂，用来获取代理对象
 * cglib的代理类是目标类的子类
 */
public class ProxyFactory implements MethodInterceptor {
  // 声明目标类对象
  private TrainStation station = new TrainStation();

  public TrainStation getProxyObject(){
    // 1.创建Enhancer对象，类似于JDK代理中的Proxy类
    Enhancer enhancer = new Enhancer();

    // 2.设置父类的字节码对象
    enhancer.setSuperclass(TrainStation.class);

    // 3.设置回调函数    这里的this代表的是  实现了MethodInterceptor接口的对象，也就是 包含intercept方法的对象
    // 为了简化写法，所以ProxyFactory本身就实现了这个接口，因此也包含这个回调方法
    enhancer.setCallback(this);

    // 4.创建代理对象
    TrainStation proxyObject = (TrainStation) enhancer.create();
    return proxyObject;
  }

  @Override
  public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
    System.out.println("增强的内容cglib");
    Object invoke = method.invoke(station, objects);
    return invoke;
  }
}
