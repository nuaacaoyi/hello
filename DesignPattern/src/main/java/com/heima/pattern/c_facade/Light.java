package com.heima.pattern.c_facade;

/**
 * @Author caoyi
 * @Create 2021-01-12-19:25
 * @Description: 电灯类
 */
public class Light {
  public void on(){
    System.out.println("打开电灯。。。");
  }

  public void off(){
    System.out.println("关灯。。。");
  }
}
