package com.heima.pattern.c_facade;

/**
 * @Author caoyi
 * @Create 2021-01-12-19:26
 * @Description: 小明爷爷每天需要操作电灯、电视、空调很烦。。小明买了个智能音箱，控制着三个，爷爷就不烦了
 */
public class Client {
  public static void main(String[] args) {
    // 创建一个智能音箱对象
    SmartAppliancesFacade saf = new SmartAppliancesFacade();
    // 控制家电
    saf.say("打开家电");
  }
}
