package com.heima.pattern.c_facade;

/**
 * @Author caoyi
 * @Create 2021-01-12-19:27
 * @Description: 外观类，智能音箱，用户主要和该类的对象进行交互。。该类对象控制具体子模块
 */
public class SmartAppliancesFacade {
  // 聚合电灯、电视机、空调对象
  private Light light;
  private TV tv;
  private AirCondition airCondition;

  public SmartAppliancesFacade() {
    light = new Light();
    tv = new TV();
    airCondition = new AirCondition();
  }

  public SmartAppliancesFacade(Light light, TV tv, AirCondition airCondition) {
    this.light = light;
    this.tv = tv;
    this.airCondition = airCondition;
  }

  // 接收语音并对 电灯、电视、空调对象进行操作
  public void say(String message){
    if(message.contains("打开")){
      on();
    }else if(message.contains("关闭")){
      off();
    }else {
      System.out.println("您说的话我听不懂。。");
    }
  }

  // 一键打开功能
  private void on(){
    light.on();
    tv.on();
    airCondition.on();
  }

  private void off(){
    light.off();;
    tv.off();
    airCondition.off();
  }
}
