package com.heima.pattern.c_facade;

/**
 * @Author caoyi
 * @Create 2021-01-12-19:27
 * @Description: TODO
 */
public class AirCondition {
  public void on(){
    System.out.println("打开空调。。。");
  }
  public void off(){
    System.out.println("关闭空调。。。");
  }
}
