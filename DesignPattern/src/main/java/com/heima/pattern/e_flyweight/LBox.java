package com.heima.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:18
 * @Description: 俄罗斯方块的L图形
 */
public class LBox extends AbstractBox {
  @Override
  public String getShape() {
    return "L";
  }
}
