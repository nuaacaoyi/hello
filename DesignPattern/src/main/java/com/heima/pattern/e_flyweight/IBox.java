package com.heima.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:18
 * @Description: 俄罗斯方块的I图形，具体享元角色
 */
public class IBox extends AbstractBox {
  @Override
  public String getShape() {
    return "I";
  }
}
