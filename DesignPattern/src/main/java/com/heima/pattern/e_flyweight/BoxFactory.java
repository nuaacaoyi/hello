package com.heima.pattern.e_flyweight;

import java.util.HashMap;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:19
 * @Description: 工厂类，该类设计为单例
 */
public class BoxFactory {
  private HashMap<String, AbstractBox> map;

  // 构造方法中初始化map
  private BoxFactory() {
    map = new HashMap<>();
    map.put("I", new IBox());
    map.put("L", new LBox());
    map.put("O", new OBox());
  }

  private static BoxFactory factory = new BoxFactory();

  // 提供一个方法获取该工厂的类对象
  public static BoxFactory getInstance(){
    return factory;
  }

  // 根据名称获取 图形 对象
  public AbstractBox getShape(String name){
    return map.get(name);
  }
}
