package com.heima.pattern.e_flyweight.jdk;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:34
 * @Description: TODO
 */
public class Demo {
  public static void main(String[] args) {
    Integer i1 = 127;
    Integer i2 = 127;

    System.out.println("i1和i2对象是否是同一个对象？" + (i1 == i2));

    Integer i3 = 128;
    Integer i4 = 128;

    System.out.println("i3和i4对象是否是同一个对象？" + (i3 == i4));

    // 反编译后发现  Integer i = 127; 在JVM中的代码是： Integer i = Integer.valueOf((int)127);
    /*
    public static Integer valueOf(int i) {
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }
    */
    // 这里面是一种自动装箱技术
    // 同时这里用到的是享元模式的思想，对于 -128 ~ 127之间的int数，不做重复创建对象
  }
}
