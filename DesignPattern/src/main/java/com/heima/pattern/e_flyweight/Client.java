package com.heima.pattern.e_flyweight;

import javax.swing.*;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:24
 * @Description: 俄罗斯方块游戏 获取各个小图形
 */
public class Client {
  public static void main(String[] args) {
    // 获取I图形
    AbstractBox box1 = BoxFactory.getInstance().getShape("I");
    box1.display("灰色");

    // 获取L图形
    AbstractBox box2 = BoxFactory.getInstance().getShape("L");
    box2.display("绿色");

    // 获取O图形
    AbstractBox box3 = BoxFactory.getInstance().getShape("O");
    box3.display("蓝色");
    // 获取O图形
    AbstractBox box4 = BoxFactory.getInstance().getShape("O");
    box4.display("红色");

    System.out.println("两次获取的O图形是否为同一个对象：" + (box3==box4));
  }
}
