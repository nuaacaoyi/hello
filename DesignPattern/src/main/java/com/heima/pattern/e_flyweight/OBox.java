package com.heima.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:18
 * @Description: 俄罗斯方块的 O 图形
 */
public class OBox extends AbstractBox {
  @Override
  public String getShape() {
    return "O";
  }
}
