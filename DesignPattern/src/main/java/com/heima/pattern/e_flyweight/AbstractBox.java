package com.heima.pattern.e_flyweight;

/**
 * @Author caoyi
 * @Create 2021-01-19-14:15
 * @Description: 抽象享元角色
 */
public abstract class AbstractBox {

  // 获取图形的方法
  public abstract String getShape();

  // 显示图形及颜色的方法
  public void display(String color){
    System.out.println("方块形状："+getShape() + "；方块颜色："+ color);
  }
}
