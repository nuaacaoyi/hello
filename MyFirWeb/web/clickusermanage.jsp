<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: chy
  Date: 2020/3/28
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <table border="1">
        <tr>
            <th>用户名</th>
            <th>Session</th>
            <th>踢出</th>
        </tr>
        <c:forEach items="${map }" var="entry">
            <tr>
                <td>${entry.key }</td>
                <td>${entry.value }</td>
                <td><a href="${pageContext.request.contextPath }/clickServletManage?name=${entry.key }">踢出</a> </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
