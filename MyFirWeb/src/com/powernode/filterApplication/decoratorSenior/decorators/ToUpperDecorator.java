package com.powernode.filterApplication.decoratorSenior.decorators;

import com.powernode.filterApplication.decoratorSenior.service.ISomeService;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:43
 */
public class ToUpperDecorator extends SomeServiceWrapper {
    public ToUpperDecorator() {
        super();
    }

    public ToUpperDecorator(ISomeService s) {
        super(s);
    }

    @Override
    public String doSome() {
        return super.doSome().toUpperCase();
    }
}
