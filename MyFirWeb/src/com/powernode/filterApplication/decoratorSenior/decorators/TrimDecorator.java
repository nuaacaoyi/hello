package com.powernode.filterApplication.decoratorSenior.decorators;

import com.powernode.filterApplication.decoratorSenior.service.ISomeService;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:42
 * 具体装饰者，要求：1.继承自装饰者基类；2.要有带参构造器;3.具体装饰者只对装饰者基类业务方法进行增强
 */
public class TrimDecorator extends SomeServiceWrapper {
    public TrimDecorator() {
        super();
    }

    public TrimDecorator(ISomeService s) {
        super(s);
    }

    @Override
    public String doSome() {
        return super.doSome().trim();
    }
}
