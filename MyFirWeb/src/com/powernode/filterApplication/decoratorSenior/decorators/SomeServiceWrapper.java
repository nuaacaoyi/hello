package com.powernode.filterApplication.decoratorSenior.decorators;

import com.powernode.filterApplication.decoratorSenior.service.ISomeService;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:40
 * 创建装饰者基类  不做任何增强
 * 作为基类，必须要有无参构造器
 */
public class SomeServiceWrapper implements ISomeService {
    private  ISomeService s;

    public SomeServiceWrapper() {
    }

    public SomeServiceWrapper(ISomeService s) {
        this.s = s;
    }

    @Override
    public String doSome() {
        return s.doSome();
    }
}
