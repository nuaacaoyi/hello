package com.powernode.filterApplication.decoratorSenior.test;

import com.powernode.filterApplication.decoratorSenior.decorators.ToUpperDecorator;
import com.powernode.filterApplication.decoratorSenior.decorators.TrimDecorator;
import com.powernode.filterApplication.decoratorSenior.service.ISomeService;
import com.powernode.filterApplication.decoratorSenior.service.SomeServiceImpl;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:43
 */
public class MyTestMain {
    public static void main(String[] args) {
        //创建目标对象
        ISomeService target=new SomeServiceImpl();
        //使用目标对象作为参数，创建创始者
        ISomeService service1=new TrimDecorator(target);
        //将第一次增强过后的结果作为第二次增强的参数传入， 形成 “装饰者链”
        ISomeService service2=new ToUpperDecorator(service1);

        //使用装饰者的业务方法
        String result=service2.doSome();
        System.out.println("result = "+result);
    }
}
