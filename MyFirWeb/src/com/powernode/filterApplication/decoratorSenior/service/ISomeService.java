package com.powernode.filterApplication.decoratorSenior.service;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:38
 */
public interface ISomeService {
    String doSome();
}
