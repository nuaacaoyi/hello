package com.powernode.filterApplication.decoratorBase;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:12
 */
public class DecoratorMainTest {
    public static void main(String[] args) {
        IDecoratorTest service=new DecoratorTestOriginImpl();
        String result=service.doSome();
        System.out.println("result = "+ result);


        IDecoratorTest service2=new DecoratorTest(service );
        System.out.println("result = "+service2.doSome());
    }
}
