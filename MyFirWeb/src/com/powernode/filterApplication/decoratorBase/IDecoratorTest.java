package com.powernode.filterApplication.decoratorBase;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:08
 * 业务逻辑接口
 */
public interface IDecoratorTest {
    String doSome();
}
