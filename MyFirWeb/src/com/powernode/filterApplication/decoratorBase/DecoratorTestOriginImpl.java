package com.powernode.filterApplication.decoratorBase;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:07
 * 目标类（需要用装饰者增强的类）
 */
public class DecoratorTestOriginImpl implements IDecoratorTest {
    @Override
    //待增强的目标方法
    public String doSome() {
        return "caoyi";
    }
}
