package com.powernode.filterApplication.decoratorBase;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:15
 * 装饰者类的要求：1.装饰者要与目标类实现相同的接口；2. 装饰者类要有 目标类的实例化对象 成员变量，且该成员变量是由装饰者类的带参构造器传入的
 */
public class DecoratorTest implements IDecoratorTest {
    //通过带参构造器传入目标对象
    public DecoratorTest(IDecoratorTest service) {
        this.service = service;
    }

    private IDecoratorTest service;
    @Override
    public String doSome() {
        return service.doSome().toUpperCase();
    }
}
