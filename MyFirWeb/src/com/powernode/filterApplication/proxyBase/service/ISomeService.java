package com.powernode.filterApplication.proxyBase.service;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:38
 */
public interface ISomeService {
    String doSome();
    String doOther();
}
