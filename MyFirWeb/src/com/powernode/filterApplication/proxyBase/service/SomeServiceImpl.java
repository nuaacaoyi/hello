package com.powernode.filterApplication.proxyBase.service;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:39
 */
public class SomeServiceImpl implements ISomeService {
    @Override
    public String doSome() {
        return "caoyi";
    }

    public String doOther(){ return "hello";}
}
