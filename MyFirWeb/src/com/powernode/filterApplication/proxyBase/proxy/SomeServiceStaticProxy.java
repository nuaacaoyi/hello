package com.powernode.filterApplication.proxyBase.proxy;

import com.powernode.filterApplication.proxyBase.service.ISomeService;
import com.powernode.filterApplication.proxyBase.service.SomeServiceImpl;

/**
 * @author CY_JFXX
 * @create 2020-03-29 15:29
 * 静态代理类
 *
 */
public class SomeServiceStaticProxy implements ISomeService {
    @Override
    public String doOther() {
        return null;
    }

    private  ISomeService target;

    //在无参构造器中实例化成员变量
    public SomeServiceStaticProxy() {
        target = new SomeServiceImpl();
    }

    @Override
    public String doSome() {
        // ...  判断 逻辑，增加权限控制
        return "对不起，您无权使用";
    }
}
