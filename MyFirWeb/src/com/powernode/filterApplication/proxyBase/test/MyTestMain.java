package com.powernode.filterApplication.proxyBase.test;

import com.powernode.filterApplication.proxyBase.proxy.SomeServiceStaticProxy;
import com.powernode.filterApplication.proxyBase.service.ISomeService;

/**
 * @author CY_JFXX
 * @create 2020-03-29 14:43
 */
public class MyTestMain {
    public static void main(String[] args) {
        ISomeService service=new SomeServiceStaticProxy();

        //使用装饰者的业务方法
        String result=service.doSome();
        System.out.println("result = "+result);

        System.out.println(service.doOther());
    }
}
