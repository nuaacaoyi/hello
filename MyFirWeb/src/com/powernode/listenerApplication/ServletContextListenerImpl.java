package com.powernode.listenerApplication;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author CY_JFXX
 * @create 2020-03-28 11:38
 */
public class ServletContextListenerImpl implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //创建一个map，key为用户名，value为 与当前用户绑定的session对象
        Map<String, HttpSession> map=new HashMap<>();
        ServletContext sc=sce.getServletContext();
        sc.setAttribute("map",map);

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
