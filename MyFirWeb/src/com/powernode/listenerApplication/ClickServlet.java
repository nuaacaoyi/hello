package com.powernode.listenerApplication;

import com.powernode.beans.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-28 11:56
 */
public class ClickServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         req.setCharacterEncoding("UTF-8");
         String name=req.getParameter("name");
         String ageS=req.getParameter("age");
         Integer age=Integer.valueOf(ageS);

         //创建USER对象
        User user=new User(name,age);
        //获取当前请求对应的session
        HttpSession session=req.getSession();
        //将user与session绑定
        session.setAttribute("user",user);
        resp.sendRedirect(req.getContextPath()+"/index.jsp");

    }
}
