package com.powernode.listenerApplication;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

/**
 * @author CY_JFXX
 * @create 2020-03-28 12:07
 */
public class ClickManage extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext sc=req.getServletContext();
        Map<String, HttpSession> map= (Map<String, HttpSession>) sc.getAttribute("map");
        //获取管理员要踢出的名字
        String clickName=req.getParameter("name");
        //从map中获取当前用户所对应的session
        HttpSession session=map.get(clickName);
        //使session失效
        session.invalidate();
        map.remove(clickName);
        req.getRequestDispatcher(req.getContextPath()+"/clickusermanage.jsp").forward(req,resp);
    }
}
