package com.powernode.filter;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author CY_JFXX
 * @create 2020-03-29 8:56
 */
public class SomeFilter implements Filter {

    public SomeFilter() {
        System.out.println("创建someFilter");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("初始化someFilter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("执行了someFilter---before");
        //修改请求
        request.setAttribute("company","HNJFXXKJ");
        request.setAttribute("employee","CY");


        //此时如果不做处理，过滤器就是拦截器，请求不会往下走了
        //所以要执行以下代码， 将请求放行到下一个资源
        chain.doFilter(request,response);
        System.out.println("执行了someFilter---after");

        //修改响应
        PrintWriter out=response.getWriter();
        out.println("-------SomeFilter-----<br>");


    }

    @Override
    public void destroy() {
        System.out.println("销毁somefilter");
    }
}
