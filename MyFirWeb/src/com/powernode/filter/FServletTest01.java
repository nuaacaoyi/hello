package com.powernode.filter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * @author CY_JFXX
 * @create 2020-03-29 10:41
 */
public class FServletTest01 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         //获取请求域中携带的属性
        Enumeration<String> names=req.getAttributeNames();
        while (names.hasMoreElements()){
            String name=names.nextElement();
            String value=(String)req.getAttribute(name);
            System.out.println(name+" --> "+value);
        }

        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out=resp.getWriter();
        out.print("FServletTest01<br>");
    }
}
