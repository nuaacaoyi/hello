package com.powernode.beans;

import com.powernode.jsp.School;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;
import java.io.Serializable;

/**
 * @author cy_hnmx
 * @create 2020-03-13-13:15
 */
public class Student implements HttpSessionActivationListener, Serializable {
    private String name;
    private Integer age;
    private School school;

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public Student(String name, Integer age, School school) {
        this.name = name;
        this.age = age;
        this.school = school;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    //钝化 当 当前类的对象 将要被钝化（内存数据写入到硬盘），触发该方法
    //场景类似Java基础IO里的serialable 序列化
    public void sessionWillPassivate(HttpSessionEvent httpSessionEvent) {
        System.out.println("Student将要被钝化");
    }

    @Override
    //当 当前类 的对象 被活化（硬盘数据恢复到内存） 时，触发该方法
    public void sessionDidActivate(HttpSessionEvent httpSessionEvent) {
        System.out.println("Student 已经活化");
    }
}
