package com.powernode.beans;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import java.io.Serializable;
import java.util.Map;

/**
 * @author CY_JFXX
 * @create 2020-03-28 11:44
 * 用户类 注册 绑定session的监听
 */
public class User implements HttpSessionBindingListener, Serializable {
    private String name;

    public User() {
    }

    private int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        HttpSession session=event.getSession();
        ServletContext sc=session.getServletContext();
        Map<String,HttpSession> map= (Map<String, HttpSession>) sc.getAttribute("map");
        //将当前的用户名和session放入到全局域
        map.put(name,session);
        //将map写回全局域
        sc.setAttribute("map",map);
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
