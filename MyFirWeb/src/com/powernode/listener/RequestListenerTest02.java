package com.powernode.listener;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;

/**
 * @author cy_hnmx
 * @create 2020-03-27-12:53
 */
public class RequestListenerTest02 implements ServletRequestAttributeListener {
    //当向Request域中添加属性时，会触发该方法的执行
    @Override
    public void attributeAdded(ServletRequestAttributeEvent servletRequestAttributeEvent) {
        String attName = servletRequestAttributeEvent.getName();
        String attValue=servletRequestAttributeEvent.getValue().toString();
        System.out.println("向Request域中添加了一个属性: "+attName+" --> "+attValue);
    }
    //当向Request域中删除属性时，会触发该方法的执行
    @Override
    public void attributeRemoved(ServletRequestAttributeEvent servletRequestAttributeEvent) {

    }

    @Override
    public void attributeReplaced(ServletRequestAttributeEvent servletRequestAttributeEvent) {
        String attName = servletRequestAttributeEvent.getName();
        String attValue=servletRequestAttributeEvent.getValue().toString();
        //这里展示的是修改前的值
        System.out.println("向Request域中修改了一个属性: "+attName+" --> "+attValue);


    }
}
