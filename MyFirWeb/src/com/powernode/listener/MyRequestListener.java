package com.powernode.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author cy_hnmx
 * @create 2020-03-27-12:46
 * request监听器  监听 Request的创建与销毁
 */
public class MyRequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        System.out.println("请求对象被销毁");

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        //获取当前的request
        HttpServletRequest request=(HttpServletRequest)servletRequestEvent.getServletRequest();
        String clientIp=request.getRemoteAddr();//获取请求携带的ip
        HttpSession session=request.getSession();//获取session
        ServletContext sc=request.getServletContext();

        //将当前的session对象存放到 全局域的map中的list value里
        Map<String, List<HttpSession>> map= (Map<String, List<HttpSession>>) sc.getAttribute("map");
        //从map中获取由当前ip所发出的所有 sessions
         List<HttpSession> sessions = map.get(clientIp);
        if(sessions ==null){
            sessions=new ArrayList<>();

        }
        sessions.add(session);
        //变化后的list写回map
        map.put(clientIp,sessions);
        sc.setAttribute("map",map);

    }
}
