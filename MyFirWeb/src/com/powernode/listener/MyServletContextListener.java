package com.powernode.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cy_hnmx
 * @create 2020-03-27-17:33
 */
public class MyServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        //创建一个map，key为ip，value为该ip上发出的所有会话对象
        Map<String,List<HttpSession>> map= new HashMap<>();

        //获取全局域对象
        ServletContext sc=servletContextEvent.getServletContext();

        sc.setAttribute("map",map);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
