package com.powernode.java;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-24 19:01
 */
public class cookieServletTest02 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         //获取请求中的cookie
        Cookie[] cookies=req.getCookies();

        for (Cookie cookie:cookies
             ) {
            System.out.println(cookie.getName()+"-->"+cookie.getValue());
        }
    }
}
