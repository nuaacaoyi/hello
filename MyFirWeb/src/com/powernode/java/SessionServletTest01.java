package com.powernode.java;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-24 20:00
 */
public class SessionServletTest01 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         req.setCharacterEncoding("utf-8");
         //获取用户提交参数
         String username=req.getParameter("username");
         //将参数放入request域  其他请求读取不到
        req.setAttribute("user",username);

        //获取session对象
        HttpSession session=req.getSession();
        //将参数写入session域，同一会话的其他请求能够get到
        session.setAttribute("user1",username);
        resp.getWriter().print("testRequest: "+username);
        resp.getWriter().print("testSession: "+username);
    }
}
