package com.powernode.java;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author cy_hnmx
 * @create 2020-03-24-15:33
 */
public class cookieServletTest01 extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //创建cookie对象
        Cookie cookie=new Cookie("company","bjpowernode");
        Cookie cookie2=new Cookie("teacher","reyco");
        //给cookie设置绑定路径
        cookie.setPath(req.getContextPath()+"xxx/ggg/ooo");


        //设置cookie的有效期。 这个值为一个整型值，单位为秒。该值大于0，表示将Cookie存放到客户端的硬盘空间；该值<0 与不设置相同，会将Cookie存放到浏览器的缓存
        //该值为0时，表示Cookie一生成，马上失效
        cookie.setMaxAge(60*60);//设置cookie的有效期为一小时



        //将cookie响应到客户端
        resp.addCookie(cookie);
        resp.addCookie(cookie2);
    }
}
