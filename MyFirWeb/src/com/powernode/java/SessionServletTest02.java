package com.powernode.java;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-24 20:05
 */
public class SessionServletTest02 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //从request域中读取user
         String user=req.getParameter("user");

         //在获取session数据的时候，一般函数参数传入false，因为新session不可能有数据
        HttpSession session=req.getSession(false);
        String user1=null;
         //从session域中读取user1 因此这里需要加判断
        if(session!=null) {
            user1 = session.getAttribute("user1").toString();
        }
         resp.getWriter().print("testRequest: "+user);
        resp.getWriter().print("testSession: "+user1);


    }
}
