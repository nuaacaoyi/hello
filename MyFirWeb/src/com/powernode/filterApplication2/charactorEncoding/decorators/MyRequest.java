package com.powernode.filterApplication2.charactorEncoding.decorators;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * @author CY_JFXX
 * @create 2020-03-29 16:43
 * 以tomcat提供的HttpServletRequestWrapper 为基类，  实现具体的解决get乱码的 装饰者类
 */
public class MyRequest extends HttpServletRequestWrapper {

    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @throws IllegalArgumentException if the request is null
     */
    public MyRequest(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String name) {
        return this.getParameterValues(name)[0];
    }

    //将原来的 乱码Map  替换为 新的 非乱码的Map
    @Override
    public Map<String, String[]> getParameterMap() {
        //获取原来的Map，其中的数据是包含乱码的
        Map<String, String[]> originMap =   super.getParameterMap();
        //创建一个 新Map，将来其中的数据是 解决过乱码的数据。将来用户获取的Map也是这个新Map
        Map<String, String[]> newMap=new HashMap<>();

        try {
            //将原始Map中的数据解决乱码问题后，写入新Map
            //遍历原始Map
            for (String key:originMap.keySet()) {
                //获取当前key对应的 值数组
                String[] values=originMap.get(key);
                //遍历这个值数组  ，对每一个值进行中文乱码问题解决
                for (int i = 0; i <values.length; i++) {
                    //按照字符当前的编码格式进行打散，即进行编码
                    byte[] bytes= values[i].getBytes("ISO8859-1");
                    //按照新的目标编码格式进行组装，即及逆行解码
                    values[i]=new String(bytes,"UTF-8");

                }
                //将解决了乱码问题的value放回新的 Map
                newMap.put(key,values);
            }
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        return newMap;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        //获取新的 Map
        Map<String,String[]> newMap=this.getParameterMap();
        //将keySet转换为 Vector
        Vector keyVector=(Vector)newMap.keySet();
        //将Vector转换为 Enumeration 并返回
        return keyVector.elements();
    }

    @Override
    public String[] getParameterValues(String name) {
        //获取新的 Map
        Map<String,String[]> newMap=this.getParameterMap();
        String[] values=newMap.get(name);


        return values;
    }
}
