package com.powernode.filterApplication2.charactorEncoding;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-29 15:59
 */
public class CharactorEncodingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //解决POST请求中中文乱码问题  tomcate8已经自行解决get提交的中文乱码问题
        request.setCharacterEncoding("UTF-8");
        //解决响应中中文乱码问题，这个代码必须放到 chain.doFilter之前
        response.setContentType("text/html;charset=utf-8");


        chain.doFilter(request,response);

        //解决get提交中文乱码问题的第二个狸猫换太子（将Request换为装饰后的Request）
        //ServletRequest newRequest= new MyRequest((HttpServletRequest) request);
        //chain.doFilter(newRequest,response);


    }

    @Override
    public void destroy() {

    }
}
