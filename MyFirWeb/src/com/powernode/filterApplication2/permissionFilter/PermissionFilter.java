package com.powernode.filterApplication2.permissionFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author CY_JFXX
 * @create 2020-03-29 17:25
 */
public class PermissionFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req=(HttpServletRequest)request;
        String servletPath=req.getServletPath();
        //若ServletPath 以 /permission开头，那么，这个请求的资源必须登录后才可访问
        if(servletPath.startsWith("/permission")){
            HttpSession session=req.getSession(false);
            if(session!=null){
                String user=(String)session.getAttribute("user");
                if(user != null){
                    chain.doFilter(request,response);
                }else{
                    req.getRequestDispatcher("/filterpermissionin.jsp").forward(request,response);
                    return;
                }
            }
        }else {
            chain.doFilter(request,response);
        }


    }

    @Override
    public void destroy() {

    }
}
